﻿Public Class FaxSent
    Public Property ID As Integer
    Public Property ToNumber As String
    Public Property FaxPage As String
    Public Property DateSent As Date
    Public Property Status As Integer
    Public Property Ticketid As Integer
    Public Property RestID As Integer
    Public Property DateConfirmed As Date
End Class
