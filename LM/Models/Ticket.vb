﻿Public Class Ticket
    Public Property ID As Integer
    Public Property ShoppingCartID As String
    Public Property ModifiedDate As DateTime
    Public Property CreatedDate As DateTime
    Public Property ClosingDate As DateTime
    Public Property ActiveDate As DateTime
    Public Property Paid As Boolean
    Public Property Voided As Boolean
    Public Property VoidReason As String
    Public Property SubTotal As Double
    Public Property DiscountTotal As Double
    Public Property TaxTotal As Double
    Public Property TotalPrice As Double
    Public Property PaidAmount As Double
    Public Property DeliveryPrice As Double
    Public Property FeePrice As Double

    Public Property PaymentID As Integer

    '1 = Cart
    '2 = Paid
    '3 = Confirmed
    Public Property TicketStatus As Integer

    Public Property BilltoAddress As Integer
    Public Property DeliveryAddress As Integer

    Public Property TicketTypeID As Integer
    'Public Overridable Property TicketType As TicketType

    Public Property RestaurantID As Integer

    Public Property ReceiptID As String

    Public Property Username As String

    Public Property LMDID As Integer
    Public Property LMDStatus As Integer


    Public Overridable Property ticketitems As ICollection(Of TicketItem)


End Class
