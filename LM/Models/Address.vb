﻿Imports System.ComponentModel.DataAnnotations
Public Class Address
    Public Property ID As Integer

    Public Property UserID As Integer
    Public Property SessionID As String
    Public Property Type As Integer
    Public Property AddressDefault As Boolean

    <Required(ErrorMessage:="Please Enter an Email")> _
<StringLength(100, MinimumLength:=2, ErrorMessage:="Please Enter an Email")> _
    Public Property Email As String

    <Required(ErrorMessage:="Please Enter Your Name")> _
<StringLength(100, MinimumLength:=2, ErrorMessage:="Please Enter Correct Your Name")> _
    Public Property Name As String
    <Required(ErrorMessage:="Please Enter Your Address")> _
<StringLength(100, MinimumLength:=2, ErrorMessage:="Please Enter A Correct Address")> _
    Public Property Address1 As String
    Public Property Address2 As String
    <Required(ErrorMessage:="Please Enter Your City")> _
<StringLength(100, MinimumLength:=2, ErrorMessage:="Please Enter A Correct  City")> _
    Public Property City As String
    <Required(ErrorMessage:="Please Select a State")> _
    <StringLength(2, MinimumLength:=2, ErrorMessage:="Please Select a State")> _
    Public Property State As String
    <Required(ErrorMessage:="Please Enter Your Zipcode")> _
<StringLength(10, MinimumLength:=5, ErrorMessage:="Please Enter A Correct Zipcode")> _
    Public Property Zip As String
    <Required(ErrorMessage:="Please Enter A Correct Phone Number")> _
<StringLength(20, MinimumLength:=10, ErrorMessage:="Please Enter A Correct Phone Number")> _
    Public Property phone As String



End Class
