﻿Public Class LMMenuL2ModifierGroup
    Public Property ID As Integer
    Public Property moddate As DateTime
    Public Property createdate As DateTime
    Public Property Name As String
    Public Property Active As Boolean
    Public Property Exclusive As Boolean
    Public Property Required As Boolean

    Public Property MinQty As Integer
    Public Property MaxQty As Integer
    Public Property FreeQty As Integer

    Public Property ModifierID As Integer
    Public Overridable Property Modifier As LMMenuModifier

    Public Property ControlTypeID As Integer
    Public Property ControlType As ControlTypeModel

    Public Property SubQtyMax As Integer

    Public Overridable Property L2Modifiers As ICollection(Of LMMenuL2Modifier)
End Class
