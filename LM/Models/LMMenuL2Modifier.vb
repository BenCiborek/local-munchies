﻿Public Class LMMenuL2Modifier
    Public Property ID As Integer
    Public Property ModDate As DateTime
    Public Property CreateDate As DateTime
    Public Property Name As String
    Public Property Price As Double
    Public Property Extra_Price As Double
    Public Property Active As Boolean
    Public Property Selected As Boolean

    Public Property L2MenuModifierGroupID As Integer
    Public Overridable Property L2MenuModifierGroup As LMMenuL2ModifierGroup

    Public Overridable Property L3Modifiers As ICollection(Of LMMenuL3Modifier)

End Class
