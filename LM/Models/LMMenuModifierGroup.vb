﻿Public Class LMMenuModifierGroup
    Public Property ID As Integer
    Public Property moddate As DateTime
    Public Property createdate As DateTime
    Public Property Name As String
    Public Property Active As Boolean
    Public Property Exclusive As Boolean
    Public Property Required As Boolean

    Public Property MenuID As Integer
    Public Overridable Property Menu As LMMenu



    Public Overridable Property Modifiers As ICollection(Of LMMenuModifier)

End Class
