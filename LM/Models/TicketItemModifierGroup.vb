﻿Public Class TicketItemModifierGroup
    Public Property ID As Integer
    Public Property ModifiedDate As DateTime

    Public Property Name As String

    Public Property MinQty As Integer
    Public Property MaxQty As Integer
    Public Property FreeQty As Integer

    Public Property TicketItemID As Integer
    Public Overridable Property TicketItem As TicketItem

    Public Property MenuItemModifierGroupID As Integer

    Public Overridable Property Modifiers As ICollection(Of TicketItemModifier)

    Public Property TotalPrice As Double
    Public Property ExtraPrice As Double

End Class
