﻿Public Class TicketItem
    Public Property Id As Integer
    Public Property ModifiedDate As DateTime

    Public Property Name As String
    Public Property Quantity As Double
    Public Property ItemBasePrice As Double
    Public Property ItemTaxRate As Double
    Public Property ItemTax As Double
    Public Property ItemDiscount As Double
    Public Property ItemModifierPrice As Double
    Public Property TotalItemPrice As Double

    Public Property DisplayOrder As Integer


    Public Property ItemID As Integer


    Public Property TicketID As Integer
    Public Overridable Property Ticket As Ticket

    Public Property RestaurantID As Integer
    Public Overridable Property Restaurant As LMRestaurant

    Public Overridable Property ModifierGroups As ICollection(Of TicketItemModifierGroup)

    Public Property SpecialInstructions As String

End Class
