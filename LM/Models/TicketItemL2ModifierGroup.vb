﻿Public Class TicketItemL2ModifierGroup
    Public Property ID As Integer
    Public Property ModifiedDate As DateTime

    Public Property Name As String

    Public Property MinQty As Integer
    Public Property MaxQty As Integer
    Public Property FreeQty As Integer

    Public Property L1ModifierID As Integer
    Public Overridable Property L1Modifier As TicketItemModifier

    Public Property MenuItemL2ModifierGroupID As Integer

    Public Overridable Property Modifiers As ICollection(Of TicketItemL2Modifier)

    Public Property TotalPrice As Double
    Public Property ExtraPrice As Double
End Class
