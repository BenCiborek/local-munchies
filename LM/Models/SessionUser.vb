﻿Public Class SessionUser
    Public Property id As Integer
    Public Property userID As Integer
    Public Overridable Property user As User

    Public Property Session As String
    Public Property Active As Boolean

    Public Property Start As DateTime
    Public Property LastAction As DateTime

End Class
