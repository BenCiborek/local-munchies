﻿Public Class TicketItemL2Modifier
    Public Property ID As Integer
    Public Property TicketItemID As Integer
    Public Property MenuItemModifierID As Integer
    'Public Property ItemCount As Integer
    Public Property Name As String
    Public Property ModifierPrice As Double
    Public Property ExtraPrice As Double
    Public Property ModifierType As Integer

    Public Property Qty As Integer

    Public Property DisplayOrder As Integer

    Public Property TicketItemL2ModifierGroupID As Integer
    Public Overridable Property TicketItemL2ModifierGroup As TicketItemL2ModifierGroup

    'Notes to accomodate Side A/Side B/Whole - Ramellas
    Public Property Notes As String

End Class
