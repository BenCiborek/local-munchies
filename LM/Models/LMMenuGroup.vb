﻿Public Class LMMenuGroup
    Public Property ID As Integer
    Public Property ModDate As DateTime
    Public Property CreateDate As DateTime
    Public Property Name As String
    Public Property Description As String
    Public Property Active As Boolean
    Public Property DisplayOrder As Integer

    Public Property MenuCategoryID As Integer
    Public Overridable Property MenuCategory As LMMenuCategory

    Public Overridable Property MenuItems As ICollection(Of LMMenuItem)

End Class
