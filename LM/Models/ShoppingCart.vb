﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports System.Data.Entity

Partial Public Class ShoppingCart
    Dim Db As FBEntities = New FBEntities
    Dim HttpContext As HttpContextBase

    Dim DeliveryFee As Double = 3.99
    Dim Fee As Double = 0.99

    Dim SalesTaxRate As Double = 0.065

    Property ShoppingCartID As String
    Public Const CartSessionKey As String = "CartID"
    Public Shared Function GetCart(ByVal context As HttpContextBase) As ShoppingCart
        Dim cart = New ShoppingCart
        cart.ShoppingCartID = cart.GetCartId(context)
        Return cart
    End Function

    ' Helper method to simplify shopping cart calls
    Public Shared Function GetCart(ByVal controller As Controller) As ShoppingCart
        Return GetCart(controller.HttpContext)
    End Function

    Public Sub AddItemToTicket(ByVal MenuItem As LMMenuItem, ByVal quantity As Integer, ByVal selectedModifiers As List(Of SelectedModifier), ByVal specialinstructions As String, ByVal username As String, ByVal l2selectedmodifiers As List(Of SelectedL2Modifier))
        ' Get the matching cart and product instances
        'Dim cartItem = Db.Carts.SingleOrDefault(Function(x) x.CartId = ShoppingCartID AndAlso x.ProductId = Product.ProductId)

        Dim Ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = ShoppingCartID And x.TicketStatus = 1)
        'Create a ticket if none exist
        If Ticket Is Nothing Then
            Ticket = New Ticket With
                     {.ShoppingCartID = ShoppingCartID,
                      .ModifiedDate = Now(),
                      .CreatedDate = Now(),
                      .ClosingDate = "1/1/1900",
                      .ActiveDate = Now(),
                      .Paid = False,
                      .Voided = False,
                      .VoidReason = 0,
                      .SubTotal = 0,
                      .DiscountTotal = 0,
                      .TaxTotal = 0,
                      .TotalPrice = 0,
                      .PaidAmount = 0,
                      .DeliveryPrice = 0,
                      .FeePrice = Fee,
                      .TicketTypeID = 1,
                      .TicketStatus = 1,
                      .PaymentID = 0,
                      .RestaurantID = 0}
            Db.Tickets.Add(Ticket)
            Db.SaveChanges()
        End If


        Dim ItemTaxRate As Double = 0
        Dim ItemTax As Double = 0
        If MenuItem.Taxable Then
            ItemTaxRate = SalesTaxRate
            ItemTax = MenuItem.Price * ItemTaxRate
        End If
        ' Create a new ticket item 
        Dim ticketItem = New TicketItem With
                       {.ModifiedDate = Now(),
                        .Name = MenuItem.Name,
                        .Quantity = quantity,
                        .ItemBasePrice = MenuItem.Price,
                        .ItemTaxRate = ItemTaxRate,
                        .ItemTax = ItemTax,
                        .ItemDiscount = 0,
                        .ItemModifierPrice = 0,
                        .TotalItemPrice = MenuItem.Price,
                        .DisplayOrder = 0,
                        .ItemID = MenuItem.ID,
                        .RestaurantID = MenuItem.MenuGroup.MenuCategory.Menu.RestaurantID,
                        .SpecialInstructions = specialinstructions,
                        .TicketID = Ticket.ID}


        Db.TicketItems.Add(ticketItem)
        Db.SaveChanges()



        'Add Modifiers and Groups
        For Each selectedModifier In selectedModifiers

            Dim ModifierGroup = Db.TicketModifierGroups.SingleOrDefault(Function(x) x.MenuItemModifierGroupID = selectedModifier.MenuModifier.MenuModifierGroupID And x.TicketItemID = ticketItem.Id)

            'Add Modifier Groups
            If ModifierGroup Is Nothing Then
                ModifierGroup = New TicketItemModifierGroup With {
                    .ModifiedDate = Now(),
                    .Name = selectedModifier.MenuModifier.MenuModifierGroup.Name,
                    .MinQty = selectedModifier.MenuModifier.MinQty,
                    .MaxQty = selectedModifier.MenuModifier.MaxQty,
                    .FreeQty = selectedModifier.MenuModifier.FreeQty,
                    .TicketItemID = ticketItem.Id,
                    .MenuItemModifierGroupID = selectedModifier.MenuModifier.MenuModifierGroupID
                    }
                Db.TicketModifierGroups.Add(ModifierGroup)
                Db.SaveChanges()
            End If

            'Add Modifier
            Dim ticketModifier As New TicketItemModifier With
                {.TicketItemID = ticketItem.Id,
                 .MenuItemModifierID = selectedModifier.SelectedModifier.ID,
                 .Name = selectedModifier.SelectedModifier.Name,
                 .ModifierPrice = selectedModifier.SelectedModifier.Price,
                 .ExtraPrice = selectedModifier.SelectedModifier.Extra_Price,
                 .ModifierType = 1,
                 .DisplayOrder = 0,
                 .TicketItemModifierGroupID = ModifierGroup.ID
               }
            Db.TicketModifiers.Add(ticketModifier)
            Db.SaveChanges()
        Next

        For Each l2modifier In l2selectedmodifiers
            'Get L1 Modifier Group
            Dim L1ModifierGroup = ticketItem.ModifierGroups.Where(Function(x) x.MenuItemModifierGroupID = l2modifier.SelectedModifier.L2MenuModifierGroup.Modifier.MenuModifierGroupID).FirstOrDefault
            'Get L1 Modifier
            Dim L1Modifier = L1ModifierGroup.Modifiers.Where(Function(x) x.MenuItemModifierID = l2modifier.SelectedModifier.L2MenuModifierGroup.ModifierID).FirstOrDefault
            'Add L2 Group if needed
            Dim ModifierGroup = Db.TicketL2ModifierGroups.SingleOrDefault(Function(x) x.L1Modifier.MenuItemModifierID = l2modifier.SelectedModifier.L2MenuModifierGroup.ModifierID And x.L1Modifier.TicketItemID = ticketItem.Id And x.MenuItemL2ModifierGroupID = l2modifier.SelectedModifier.L2MenuModifierGroupID)
            If ModifierGroup Is Nothing Then
                ModifierGroup = New TicketItemL2ModifierGroup With {
                    .ModifiedDate = Now(),
                    .Name = l2modifier.SelectedModifier.L2MenuModifierGroup.Name,
                    .MinQty = l2modifier.SelectedModifier.L2MenuModifierGroup.MinQty,
                    .MaxQty = l2modifier.SelectedModifier.L2MenuModifierGroup.MaxQty,
                    .FreeQty = l2modifier.SelectedModifier.L2MenuModifierGroup.FreeQty,
                    .L1ModifierID = L1Modifier.ID,
                .MenuItemL2ModifierGroupID = l2modifier.SelectedModifier.L2MenuModifierGroup.ID
                    }
                Db.TicketL2ModifierGroups.Add(ModifierGroup)
                Db.SaveChanges()
            End If

            'Add L2 Modifier
            Dim ticketModifier As New TicketItemL2Modifier With
                {.TicketItemID = ticketItem.Id,
                 .MenuItemModifierID = l2modifier.SelectedModifier.ID,
                 .Name = l2modifier.SelectedModifier.Name,
                 .ModifierPrice = l2modifier.SelectedModifier.Price,
                 .ExtraPrice = l2modifier.SelectedModifier.Extra_Price,
                 .Qty = l2modifier.Qty,
                 .ModifierType = 1,
                 .DisplayOrder = 0,
                .TicketItemL2ModifierGroupID = ModifierGroup.ID
               }
            Db.TicketL2Modifiers.Add(ticketModifier)
            Db.SaveChanges()
        Next


        updateTicketPrice(Ticket)

        ' Save Changes

    End Sub

    Public Sub updateTicketPrice(ByRef ticket As Ticket)
        'Update Item Prices
        ticket.SubTotal = 0
        ticket.TaxTotal = 0
        If Not ticket.ticketitems Is Nothing Then
            For Each item In ticket.ticketitems
                updateItemPrice(item)
                ticket.SubTotal += item.TotalItemPrice
                ticket.TaxTotal += item.ItemTax
            Next
        End If


        'Update Ticket Prices
        ticket.TotalPrice = ticket.SubTotal + ticket.FeePrice + ticket.DeliveryPrice + ticket.TaxTotal - ticket.DiscountTotal
        Db.Entry(ticket).State = EntityState.Modified
        Db.SaveChanges()
    End Sub

    Public Sub updateItemPrice(ByVal Item As TicketItem)
        Item.ItemModifierPrice = 0
        If Not Item.ModifierGroups Is Nothing Then

            For Each modifierGroup In Item.ModifierGroups
                updateModifierPrice(modifierGroup)
                Item.ItemModifierPrice += modifierGroup.ExtraPrice
                If modifierGroup.TotalPrice <> 0 Then
                    Item.ItemBasePrice = modifierGroup.TotalPrice
                End If
            Next

        End If
        Item.TotalItemPrice = Item.ItemModifierPrice + Item.ItemBasePrice
        Item.ItemTax = Item.TotalItemPrice * Item.ItemTaxRate
        Db.Entry(Item).State = EntityState.Modified
        Db.SaveChanges()

    End Sub

    Public Sub updateModifierPrice(ByRef ModifierGroup As TicketItemModifierGroup)
        ModifierGroup.TotalPrice = 0
        ModifierGroup.ExtraPrice = 0

        If Not ModifierGroup.Modifiers Is Nothing Then
            Dim x = 1 'Used for Free Qty
            For Each modifier In ModifierGroup.Modifiers
                If x > ModifierGroup.FreeQty Then
                    ModifierGroup.TotalPrice += modifier.ModifierPrice
                    ModifierGroup.ExtraPrice += modifier.ExtraPrice
                    If Not modifier.TicketItemL2Groups Is Nothing Then
                        For Each L2ModGroup In modifier.TicketItemL2Groups
                            updateL2ModifierPrice(L2ModGroup)
                            ModifierGroup.TotalPrice += L2ModGroup.TotalPrice
                            ModifierGroup.ExtraPrice += L2ModGroup.ExtraPrice
                        Next
                    End If
                End If
                x += 1
            Next

        End If
        If ModifierGroup.Modifiers.Count < ModifierGroup.FreeQty Then
            ModifierGroup.TotalPrice = 0
            ModifierGroup.ExtraPrice = 0
        End If
        Db.Entry(ModifierGroup).State = EntityState.Modified
        Db.SaveChanges()
    End Sub

    Public Sub updateL2ModifierPrice(ByRef ModifierGroup As TicketItemL2ModifierGroup)
        ModifierGroup.TotalPrice = 0
        ModifierGroup.ExtraPrice = 0
        If Not ModifierGroup.Modifiers Is Nothing Then
            Dim x = 1
            For Each modifier In ModifierGroup.Modifiers
                If x > ModifierGroup.FreeQty Then
                    ModifierGroup.TotalPrice += modifier.ModifierPrice
                    ModifierGroup.ExtraPrice += modifier.ExtraPrice
                End If
                x += 1
            Next
        End If
        Db.Entry(ModifierGroup).State = EntityState.Modified
        Db.SaveChanges()
    End Sub

    Public Function GetItemPrice(ByVal MenuItem As LMMenuItem, ByVal quantity As Integer, ByVal selectedModifiers As List(Of SelectedModifier), ByVal L2SelectedModifiers As List(Of LMMenuL2Modifier))
        Dim price = MenuItem.Price

        Dim x = 1
        Dim currentMod = 0
        For Each modifier In selectedModifiers.OrderBy(Function(c) c.MenuModifier.ID)
            If currentMod <> modifier.MenuModifier.ID Then
                currentMod = modifier.MenuModifier.ID
                x = 1
            End If

            If x > modifier.MenuModifier.FreeQty Then

                If modifier.SelectedModifier.Price <> 0 Then
                    price = modifier.SelectedModifier.Price
                End If
                price += modifier.SelectedModifier.Extra_Price

            End If
            x += 1
        Next
        x = 1
        currentMod = 0

        For Each modifier In L2SelectedModifiers
            If currentMod <> modifier.L2MenuModifierGroup.ID Then
                currentMod = modifier.L2MenuModifierGroup.ID
                x = 1
            End If

            If x > modifier.L2MenuModifierGroup.FreeQty Then

                If modifier.Price <> 0 Then
                    price = modifier.Price
                End If
                price += modifier.Extra_Price

            End If
            x += 1
        Next
        Return price
    End Function


    Public Sub RemoveFromCart(ByVal id As Integer)

        'This function has been updated to remove all quantities of the item, not just one

        ' Get the cart
        Dim cartItem = Db.TicketItems.Single(
            Function(cart) cart.Ticket.ShoppingCartID = ShoppingCartID AndAlso cart.Id = id)
        Dim Ticketid = cartItem.TicketID



        If cartItem IsNot Nothing Then
            'If cartItem.Count > 1 Then
            'cartItem.Count -= 1
            'itemCount = cartItem.Count
            'Else
            Db.TicketItems.Remove(cartItem)
            'End If

            ' Save Changes
            Db.SaveChanges()
        End If
        updateTicketPrice(Db.Tickets.Find(Ticketid))
    End Sub

    Public Sub EmptyCart()
        Dim Ticket = Db.Tickets.Where(Function(cart) cart.ShoppingCartID = ShoppingCartID).FirstOrDefault

        For Each cartItem In Ticket.ticketitems
            Db.TicketItems.Remove(cartItem)
        Next

        ' Save changes
        Db.SaveChanges()
    End Sub

    Public Sub CloseCart(ByVal context As HttpContextBase)
        context.Session(CartSessionKey) = Nothing
    End Sub

    Public Sub AddCarttoUser(ByVal user As String)
        Dim Ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = ShoppingCartID And x.TicketStatus = 1)

        If Not Ticket Is Nothing Then
            Ticket.Username = user
            Db.Entry(Ticket).State = EntityState.Modified
            Db.SaveChanges()
        End If
    End Sub

    Function AddTip(ByVal tip As Double, ByVal cartid As String)
        Dim Ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid And x.TicketStatus = 1)

        If Not Ticket Is Nothing Then
            Ticket.DeliveryPrice = tip
            Db.Entry(Ticket).State = EntityState.Modified
            Db.SaveChanges()

            updateTicketPrice(Ticket)
        End If
        Return True
    End Function

    'Public Function GetCartItems() As List(Of Cart)
    '    Return storeDb.Carts.Where(Function(cart) cart.CartId = ShoppingCartID).ToList()
    'End Function

    'Public Function GetCount() As Integer
    '    ' Get the count of each item in the cart and sum them up
    '    Dim count? = (
    '        From cartItems In storeDb.Carts
    '        Where cartItems.CartId = ShoppingCartID
    '        Select CType(cartItems.Count, Integer?)).Sum()

    '    ' Return 0 if all entries are null
    '    Return If(count, 0)
    'End Function

    'Public Function GetTotal() As Decimal
    '    ' Multiply Product price by count of that Product to get
    '    ' the current price for each of those Products in the cart
    '    ' sum all Product price totals to get the cart total

    '    Dim total? = (
    '        From cartItems In storeDb.Carts
    '        Where cartItems.CartId = ShoppingCartID
    '        Select CType(cartItems.Count, Integer?) * cartItems.Price).Sum()
    '    Return If(total, Decimal.Zero)
    'End Function

    'Public Function GetInsuranceTotal() As Decimal

    '    Dim total? = (
    '        From cartItems In storeDb.Carts
    '        Where cartItems.CartId = ShoppingCartID And cartItems.Product_Type.ToLower = "insurance"
    '        Select CType(cartItems.Count, Integer?) * cartItems.Price).Sum()
    '    Return If(total, Decimal.Zero)

    'End Function

    'Public Function GetCashTotal() As Decimal

    '    Dim total? = (
    '        From cartItems In storeDb.Carts
    '        Where cartItems.CartId = ShoppingCartID And cartItems.Product_Type.ToLower = "cash"
    '        Select CType(cartItems.Count, Integer?) * cartItems.Price).Sum()
    '    Return If(total, Decimal.Zero)

    'End Function

    'Public Function CreateOrder(ByVal order As Order, ByVal carter As ShoppingCart) As String
    '    Dim orderTotal = 0D

    '    Dim cartItems = GetCartItems()

    '    ' Iterate over the items in the cart, adding the order details for each
    '    For Each item In cartItems
    '        Dim orderDetails = New OrderDetail With
    '                           {.ProductId = item.ProductId,
    '                            .OrderId = order.OrderId,
    '                            .UnitPrice = item.Price,
    '                            .Quantity = item.Count}

    '        ' Set the order total of the shopping cart
    '        orderTotal += (item.Count * item.Price)

    '        storeDb.OrderDetails.Add(orderDetails)

    '        order.AddandPayment = item.AddandPayment
    '    Next item

    '    ' Set the order's total to the orderTotal count
    '    order.Total = orderTotal



    '    ' Save the order
    '    storeDb.SaveChanges()

    '    ' Empty the shopping cart
    '    EmptyCart()

    '    ' Return the OrderId as the confirmation number
    '    Return order.OrderId
    'End Function

    ' We're using HttpContextBase to allow access to cookies.
    Public Function GetCartId(ByVal context As HttpContextBase) As String
        If context.Session(CartSessionKey) Is Nothing Then
            If Not String.IsNullOrWhiteSpace(context.User.Identity.Name) Then
                context.Session(CartSessionKey) = context.User.Identity.Name
            Else
                ' Generate a new random GUID using System.Guid class
                Dim tempCartId As Guid = Guid.NewGuid()

                ' Send tempCartId back to client as a cookie
                context.Session(CartSessionKey) = tempCartId.ToString()
            End If
        End If

        Return context.Session(CartSessionKey).ToString()
    End Function

    Public Function UpdateTicketType(ByVal ticketID As Integer, ByVal ticketTypeID As Integer) As List(Of String)
        Dim arr As New List(Of String)
        Try
            Dim ticket = Db.Tickets.Find(ticketID)
            ticket.TicketTypeID = ticketTypeID
            If ticketTypeID = 1 Then
                ticket.FeePrice = Fee
            Else
                ticket.FeePrice = DeliveryFee
            End If
            Db.Entry(ticket).State = EntityState.Modified
            Db.SaveChanges()

            updateTicketPrice(ticket)

            arr.Add("1")
            arr.Add(ticket.FeePrice)
        Catch ex As Exception
            arr.Add("0")
        End Try
        Return (arr)
    End Function

    Public Function AddCoupon(ByVal ticketID As Integer, ByVal Code As String) As List(Of String)
        Dim arr As New List(Of String)
        Try
            Dim ticket = Db.Tickets.Find(ticketID)
            Dim Coupon = Db.CouponCodes.Where(Function(x) x.Code = Code And x.Active = True).FirstOrDefault()
            If Not Coupon Is Nothing Then
                ticket.DiscountTotal = Coupon.Discount
                arr.Add("1")
                arr.Add(ticket.DiscountTotal)
            Else
                ticket.DiscountTotal = 0
                arr.Add("0")
            End If
            Db.Entry(ticket).State = EntityState.Modified
            Db.SaveChanges()

            updateTicketPrice(ticket)

        Catch ex As Exception
            arr.Add("0")
        End Try
        Return (arr)
    End Function

    '' When a user has logged in, migrate their shopping cart to
    '' be associated with their username
    'Public Sub MigrateCart(ByVal userName As String)
    '    Dim shoppingCart = storeDb.Carts.Where(Function(c) c.CartId = ShoppingCartID)
    '    For Each item In shoppingCart
    '        item.CartId = userName
    '    Next item
    '    storeDb.SaveChanges()
    'End Sub


    'Function UpdateCartCount(id As Integer, cartCount As Integer) As Integer

    '    Dim cartItem = storeDb.Carts.Single(Function(x) x.CartId = ShoppingCartID And x.RecordId = id)

    '    Dim itemCount = 0

    '    If cartItem IsNot Nothing Then

    '        If cartCount > 0 Then

    '            cartItem.Count = cartCount
    '            itemCount = cartItem.Count

    '        Else

    '            storeDb.Carts.Remove(cartItem)

    '        End If

    '        storeDb.SaveChanges()

    '    End If

    '    Return itemCount

    'End Function


End Class
