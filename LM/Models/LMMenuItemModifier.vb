﻿Public Class LMMenuItemModifier
    Public Property ID As Integer
    Public Property MinQty As Integer
    Public Property MaxQty As Integer
    Public Property FreeQty As Integer

    Public Property MenuModifierGroupID As Integer
    Public Overridable Property MenuModifierGroup As LMMenuModifierGroup

    Public Property MenuItemID As Integer
    Public Overridable Property MenuItem As LMMenuItem

End Class
