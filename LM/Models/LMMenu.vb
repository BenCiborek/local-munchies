﻿Public Class LMMenu
    Public Property ID As Integer
    Public Property Name As String
    Public Property Active As Boolean
    Public Property OnlineOrderingActive As Integer
    Public Property RestaurantID As Integer
    Public Overridable Property Restaurant As LMRestaurant

    Public Overridable Property MenuCategories As ICollection(Of LMMenuCategory)

    Public Overridable Property MenuModifierGroups As ICollection(Of LMMenuModifierGroup)

End Class
