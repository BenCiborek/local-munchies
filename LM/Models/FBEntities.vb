﻿
Imports System.Data.Entity
Public Class FBEntities
    Inherits DbContext

    Public Property Users As DbSet(Of User)
    Public Property OOVotes As DbSet(Of OOVote)

    Public Property Locations As DbSet(Of Location)

    Public Property Questions As DbSet(Of FB_Question)
    Public Property Picks As DbSet(Of FB_Pick)

    Public Property Restaurants As DbSet(Of LMRestaurant)
    Public Property Categories As DbSet(Of LMCategories)
    Public Property OrderOptions As DbSet(Of LMOrderOption)

    Public Property Menus As DbSet(Of LMMenu)
    Public Property MenuItems As DbSet(Of LMMenuItem)
    Public Property MenuCategories As DbSet(Of LMMenuCategory)
    Public Property MenuGroups As DbSet(Of LMMenuGroup)
    Public Property MenuItemModifiers As DbSet(Of LMMenuItemModifier)
    Public Property MenuModifiers As DbSet(Of LMMenuModifier)
    Public Property MenuModifierGroups As DbSet(Of LMMenuModifierGroup)
    Public Property MenuL2Modifiers As DbSet(Of LMMenuL2Modifier)
    Public Property MenuL2ModifierGroups As DbSet(Of LMMenuL2ModifierGroup)
    Public Property ControlTypes As DbSet(Of ControlTypeModel)
    Public Property MenuL3Modifiers As DbSet(Of LMMenuL3Modifier)

    Public Property ContacUs As DbSet(Of ContactUs)

    Public Property Tickets As DbSet(Of Ticket)
    Public Property TicketItems As DbSet(Of TicketItem)
    Public Property TicketModifiers As DbSet(Of TicketItemModifier)
    Public Property TicketModifierGroups As DbSet(Of TicketItemModifierGroup)
    Public Property TicketL2Modifiers As DbSet(Of TicketItemL2Modifier)
    Public Property TicketL2ModifierGroups As DbSet(Of TicketItemL2ModifierGroup)

    Public Property Payments As DbSet(Of Payment)

    Public Property Addresses As DbSet(Of Address)

    Public Property Emails As DbSet(Of Email)

    Public Property Faxsent As DbSet(Of FaxSent)
    Public Property RestaurantInfo As DbSet(Of RestaurantInfo)

    Public Property CouponCodes As DbSet(Of CouponCodes)

    'QuickBooks Info
    Public Property QBTokenInfos As DbSet(Of QBTokenInfo)
    Public Property QBClasses As DbSet(Of QBClass)

    'Email SignUps
    Public Property Signups As DbSet(Of SignUp)



    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of LMRestaurant)().HasMany(Function(c) c.Categories).WithMany(Function(p) p.Restaurants).Map(Function(m)
                                                                                                                             m.MapLeftKey("CategoryId")
                                                                                                                             m.MapRightKey("RestaurantId")
                                                                                                                             m.ToTable("LMRestsTOCats")

                                                                                                                         End Function)
        modelBuilder.Entity(Of LMRestaurant)().HasOptional(Function(s) s.Restaurantinfo).WithRequired(Function(e) e.Restaurant)

    End Sub
End Class
