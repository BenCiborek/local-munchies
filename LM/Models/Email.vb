﻿Imports System.ComponentModel.DataAnnotations
Public Class Email
    Public Property ID As Integer
    <StringLength(1)> _
    Public Property InteractionType As String
    Public Property InteractionNumber As Integer
    Public Property Email_To As String
    Public Property Email_From As String
    Public Property Email_Subject As String
    Public Property Email_Body As String
    Public Property Email_Attachment_bln As Boolean
    Public Property Email_Attachment_Path As String
    Public Property Email_Create_Date As DateTime
    Public Property Email_Status As Integer
    Public Property Email_Sent_Date As DateTime
    Public Property Email_Type As Integer

End Class
