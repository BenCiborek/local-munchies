﻿Public Class LMMenuL3Modifier
    Public Property ID As Integer
    Public Property Name As String
    Public Property Extra_Price As Double
    Public Property Active As Boolean
    Public Property Selected As Boolean
    Public Property DisplayOrder As Integer

    Public Property L2MenuModifierID As Integer
    Public Overridable Property L2MenuModifier As LMMenuL2Modifier
End Class
