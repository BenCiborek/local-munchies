﻿Imports System.Net
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Text
Imports System.Security.Cryptography
Imports System.Xml

Public Class CreditCard
    Function ChargeCard(ByVal total As Double, ByVal nameOnCard As String, ByVal cnum As String, ByVal zip As String, ByVal expdate As String, ByVal Cvv As String, ByVal TagNumber As String, ByRef ResponseCode As String, ByRef Message As String, ByRef Authcode As String) As Boolean

        Dim GatewayID = ConfigurationManager.AppSettings("CCGatewayID")
        Dim password = ConfigurationManager.AppSettings("CCPassword")
        Try







#If Not Debug Then
            Dim ws As FirstDataE4.ServiceSoapClient = New FirstDataE4.ServiceSoapClient
            Dim txn As FirstDataE4.Transaction = New FirstDataE4.Transaction
            Dim result As FirstDataE4.TransactionResult
#Else
            'Dim ws As FirstDataE4Test.ServiceSoapClient = New FirstDataE4Test.ServiceSoapClient
            'Dim txn As FirstDataE4Test.Transaction = New FirstDataE4Test.Transaction
            'Dim result As FirstDataE4Test.TransactionResult
            Dim ws As FirstDataE4.ServiceSoapClient = New FirstDataE4.ServiceSoapClient
            Dim txn As FirstDataE4.Transaction = New FirstDataE4.Transaction
            Dim result As New FirstDataE4.TransactionResult
#End If

            txn.ExactID = GatewayID
            txn.Password = password

            txn.Card_Number = cnum
            txn.CardHoldersName = nameOnCard
            txn.Transaction_Type = "00"
            txn.Expiry_Date = expdate
            txn.DollarAmount = total
            txn.Reference_No = TagNumber
            txn.VerificationStr2 = Cvv
            txn.ZipCode = zip

#If Not Debug Then
            result = ws.SendAndCommit(txn)
#Else
            If txn.Card_Number = "4111111111111111" Then
                result.EXact_Resp_Code = ""
                result.EXact_Message = "Approved"
                result.Authorization_Num = "TEST"
                result.Transaction_Approved = True
            Else
                result.EXact_Resp_Code = ""
                result.EXact_Message = "Denied Invalid Testing Card. Please use 4111-1111-1111-1111"
                result.Authorization_Num = ""
                result.Transaction_Approved = False
            End If

#End If


            ResponseCode = result.EXact_Resp_Code
            Message = result.EXact_Message
            Authcode = result.Authorization_Num

            If result.Transaction_Approved Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Message = ex.ToString
            Return False
        End Try


    End Function
    Function ChargeCard2(ByVal total As Double, ByVal nameOnCard As String, ByVal cnum As String, ByVal zip As String, ByVal expdate As String, ByVal Cvv As String, ByVal TagNumber As String, ByRef ResponseCode As String, ByRef Message As String, ByRef Authcode As String) As Boolean
        Dim GatewayID = ConfigurationManager.AppSettings("CCGatewayID")
        Dim password = ConfigurationManager.AppSettings("CCPassword")

        Dim string_builder As New StringBuilder()
        Using string_writer As New StringWriter(string_builder)
            'Build Transaction
            Using xml_writer As New XmlTextWriter(string_writer)
                'build XML string 
                xml_writer.Formatting = Formatting.Indented
                xml_writer.WriteStartElement("Transaction")
                xml_writer.WriteElementString("ExactID", GatewayID)
                'Gateway ID
                xml_writer.WriteElementString("Password", password)
                'Password
                xml_writer.WriteElementString("Card_Number", cnum)
                xml_writer.WriteElementString("CardHoldersName", nameOnCard)
                xml_writer.WriteElementString("Transaction_Type", "00")
                xml_writer.WriteElementString("Expiry_Date", expdate)
                xml_writer.WriteElementString("DollarAmount", total)
                xml_writer.WriteElementString("Reference_No", TagNumber)
                xml_writer.WriteElementString("VerificationStr2", Cvv)
                xml_writer.WriteElementString("ZipCode", zip)
                xml_writer.WriteEndElement()
            End Using
        End Using
        Dim xml_string As String = string_builder.ToString()

        'SHA1 hash on XML string
        Dim encoder As New ASCIIEncoding()
        Dim xml_byte As Byte() = encoder.GetBytes(xml_string)
        Dim sha1_crypto As New SHA1CryptoServiceProvider()
        Dim hash As String = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "")
        Dim hashed_content As String = hash.ToLower()

        'assign values to hashing and header variables
        Dim keyID As String = ConfigurationManager.AppSettings("CCHashKeyID")
        'key ID
        Dim key As String = ConfigurationManager.AppSettings("CCHashKey")
        'Hmac key
        Dim method As String = "POST" & vbLf
        Dim type As String = "application/xml"
        'REST XML
        Dim time As String = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")
        Dim uri As String = "/transaction/v13"
        Dim hash_data As String = method & type & vbLf & hashed_content & vbLf & time & vbLf & uri
        'hmac sha1 hash with key + hash_data
        Dim hmac_sha1 As HMAC = New HMACSHA1(Encoding.UTF8.GetBytes(key))
        'key
        Dim hmac_data As Byte() = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data))
        'data
        'base64 encode on hmac_data
        Dim base64_hash As String = Convert.ToBase64String(hmac_data)
        'Test

        ' Dim url As String = "https://api.demo.globalgatewaye4.firstdata.com" & uri
        Dim url As String = "https://api.globalgatewaye4.firstdata.com" & uri
        'DEMO Endpoint
        'begin HttpWebRequest 
        Dim web_request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
        web_request.Method = "POST"
        web_request.ContentType = type
        web_request.Accept = "*/*"
        web_request.Headers.Add("x-gge4-date", time)
        web_request.Headers.Add("x-gge4-content-sha1", hashed_content)
        web_request.Headers.Add("Authorization", "GGE4_API " & keyID & ":" & base64_hash)
        web_request.ContentLength = xml_string.Length

        ' write and send request data 
        Using stream_writer As New StreamWriter(web_request.GetRequestStream())
            stream_writer.Write(xml_string)
        End Using

        'get response and read into string
        Dim response_string As String
        Try
            Using web_response As HttpWebResponse = DirectCast(web_request.GetResponse(), HttpWebResponse)
                Using response_stream As New StreamReader(web_response.GetResponseStream())
                    response_string = response_stream.ReadToEnd()
                End Using

                'load xml
                Dim xmldoc As New XmlDocument()
                xmldoc.LoadXml(response_string)
                Dim nodelist As XmlNodeList = xmldoc.SelectNodes("TransactionResult")
                'bind XML source DataList control
                Dim Datalist1 As New DataList
                DataList1.DataSource = nodelist
                DataList1.DataBind()
                'output raw XML for debugging

                'Build Test
                If True Then
                    Return True
                Else
                    Return False
                End If

                'request_label.Text = "<b>Request</b><br />" & web_request.Headers.ToString() & System.Web.HttpUtility.HtmlEncode(xml_string)
                'response_label.Text = "<b>Response</b><br />" & web_response.Headers.ToString() & System.Web.HttpUtility.HtmlEncode(response_string)
            End Using

            'read stream for remote error response
        Catch ex As WebException
           Message = ex.ToString
            Return False
        End Try
    End Function
End Class
