﻿Public Class LMMenuModifier
    Public Property ID As Integer
    Public Property ModDate As DateTime
    Public Property CreateDate As DateTime
    Public Property Name As String
    Public Property Price As Double
    Public Property Extra_Price As Double
    Public Property Active As Boolean
    Public Property Selected As Boolean

    Public Property MenuModifierGroupID As Integer
    Public Overridable Property MenuModifierGroup As LMMenuModifierGroup

    Public Overridable Property L2ModifierGroups As ICollection(Of LMMenuL2ModifierGroup)

End Class
