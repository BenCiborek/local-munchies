﻿Public Class LMMenuCategory
    Public Property ID As Integer
    Public Property ModDate As DateTime
    Public Property CreateDate As DateTime
    Public Property Name As String
    Public Property Description As String
    Public Property Active As Boolean
    Public Property DisplayOrder As Integer

    Public Property MenuID As Integer
    Public Overridable Property Menu As LMMenu

    Public Overridable Property MenuGroups As ICollection(Of LMMenuGroup)

End Class
