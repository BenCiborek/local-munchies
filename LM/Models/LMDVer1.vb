﻿Public Class LMDVer1
    Public Property TicketID As Integer
    Public Property LMID As Integer
    Public Property ScheduledPickupTime As DateTime
    Public Property OrderNumber As String
    Public Property OrderTotal As Double
    Public Property Tip As Double
    Public Property Notes As String
    Public Property DeliveryName As String
    Public Property DeliveryAddress1 As String
    Public Property DeliveryAddress2 As String
    Public Property DeliveryCity As String
    Public Property DeliveryState As String
    Public Property DeliveryZip As String
    Public Property DeliveryPhone As String

End Class
