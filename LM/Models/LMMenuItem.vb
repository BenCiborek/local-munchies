﻿Public Class LMMenuItem
    Public Property ID As Integer


    Public Property Name As String
    Public Property Description As String
    Public Property Price As Double
    Public Property Taxable As Boolean
    Public Property isUpsell As Boolean

    Public Property MenuGroupID As Integer
    Public Overridable Property MenuGroup As LMMenuGroup

    Public Overridable Property Modifiers As ICollection(Of LMMenuItemModifier)


End Class
