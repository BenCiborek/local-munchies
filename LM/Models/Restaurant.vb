﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports Geocoding.Google

Public Class Restaurant
    Dim db As New FBEntities
    Public Function isOpen(ByVal Restaurant As LMRestaurant)
        Dim retval = False
        Dim timeutc = Now.ToUniversalTime
        Dim cstZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
        Dim currenttime As Date = TimeZoneInfo.ConvertTimeFromUtc(timeutc, cstZone)
        Dim dt As DateTime = DateAdd(DateInterval.Minute, 20, currenttime) 'Add 20 min to take orders early and stop orders from coming in too late.
        Select Case dt.DayOfWeek
            Case DayOfWeek.Sunday
                If dt.TimeOfDay > Restaurant.SunOpen.TimeOfDay And dt.TimeOfDay < Restaurant.SunClose.TimeOfDay Or (Restaurant.SunClose.TimeOfDay < Restaurant.SunOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.SunClose.TimeOfDay Or dt.TimeOfDay > Restaurant.SunOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
            Case DayOfWeek.Monday
                If dt.TimeOfDay > Restaurant.MonOpen.TimeOfDay And dt.TimeOfDay < Restaurant.MonClose.TimeOfDay Or (Restaurant.MonClose.TimeOfDay < Restaurant.MonOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.MonClose.TimeOfDay Or dt.TimeOfDay > Restaurant.MonOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
            Case DayOfWeek.Tuesday
                If dt.TimeOfDay > Restaurant.TueOpen.TimeOfDay And dt.TimeOfDay < Restaurant.TueClose.TimeOfDay Or (Restaurant.TueClose.TimeOfDay < Restaurant.TueOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.TueClose.TimeOfDay Or dt.TimeOfDay > Restaurant.TueOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
            Case DayOfWeek.Wednesday
                If dt.TimeOfDay > Restaurant.WedOpen.TimeOfDay And dt.TimeOfDay < Restaurant.WedClose.TimeOfDay Or (Restaurant.WedClose.TimeOfDay < Restaurant.WedOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.WedClose.TimeOfDay Or dt.TimeOfDay > Restaurant.WedOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
            Case DayOfWeek.Thursday
                If dt.TimeOfDay > Restaurant.ThuOpen.TimeOfDay And dt.TimeOfDay < Restaurant.ThuClose.TimeOfDay Or (Restaurant.ThuClose.TimeOfDay < Restaurant.ThuOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.ThuClose.TimeOfDay Or dt.TimeOfDay > Restaurant.ThuOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
            Case DayOfWeek.Friday
                If dt.TimeOfDay > Restaurant.FriOpen.TimeOfDay And dt.TimeOfDay < Restaurant.FriClose.TimeOfDay Or (Restaurant.FriClose.TimeOfDay < Restaurant.FriOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.FriClose.TimeOfDay Or dt.TimeOfDay > Restaurant.FriOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
            Case DayOfWeek.Saturday
                If dt.TimeOfDay > Restaurant.SatOpen.TimeOfDay And dt.TimeOfDay < Restaurant.SatClose.TimeOfDay Or (Restaurant.SatClose.TimeOfDay < Restaurant.SatOpen.TimeOfDay And (dt.TimeOfDay < Restaurant.SatClose.TimeOfDay Or dt.TimeOfDay > Restaurant.SatOpen.TimeOfDay)) Then
                    retval = True
                Else
                    retval = False
                End If
        End Select
        Return retval
    End Function
    Public Function isAddressinDeliveryZone(ByVal AddressGeocode As GeoCode, ByVal Restaurant As LMRestaurant)
        '0 = AddressNotFound
        '1 = Non-Deliverable
        '2 = Deliverable

        Dim retval As Integer = 0
        Dim Restaurantinfo = db.RestaurantInfo.Where(Function(x) x.RestaurantID = Restaurant.ID).FirstOrDefault()
        If AddressGeocode.Lat = 0 And AddressGeocode.Lon = 0 Or Restaurantinfo Is Nothing Then
            'Address Not Found
            retval = 0
        Else
            Dim RestaurantGeocode As New GeoCode
            RestaurantGeocode.Lat = Restaurant.Lat
            RestaurantGeocode.Lon = Restaurant.Lon

            If distance(RestaurantGeocode, AddressGeocode, "M") <= Restaurantinfo.DeliveryRadius Then
                'Address Within Radius
                retval = 2
            Else
                'Address Not Within Radius
                retval = 1
            End If
        End If

        Return retval
    End Function


    Public Function AddressToGeocode(ByVal address As String)
        Dim Geocode As New GeoCode
        Geocode.Lat = 0
        Geocode.Lon = 0
        Try


            Dim geocoder As GoogleGeocoder = New GoogleGeocoder() With {.ApiKey = ConfigurationManager.AppSettings("GoogleAPIKey")}
            Dim addresses As GoogleAddress() = geocoder.Geocode(address)

            If address.Count > 0 Then
                Dim firstResult = addresses.First()
                Geocode.Lat = firstResult.Coordinates.Latitude
                Geocode.Lon = firstResult.Coordinates.Longitude
                Geocode.Address = firstResult.FormattedAddress
            End If
        Catch ex As Exception

        End Try
        Return Geocode
    End Function

    Public Function distance(ByVal location1 As GeoCode, ByVal location2 As GeoCode, ByVal unit As Char) As Double
        Dim theta As Double = location1.Lon - location2.Lon
        Dim dist As Double = Math.Sin(deg2rad(location1.Lat)) * Math.Sin(deg2rad(location2.Lat)) + Math.Cos(deg2rad(location1.Lat)) * Math.Cos(deg2rad(location2.Lat)) * Math.Cos(deg2rad(theta))
        dist = Math.Acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60 * 1.1515
        If unit = "K" Then
            dist = dist * 1.609344
        ElseIf unit = "N" Then
            dist = dist * 0.8684
        End If
        Return dist
    End Function

    Private Function deg2rad(ByVal deg As Double) As Double
        Return (deg * Math.PI / 180.0)
    End Function
    Private Function rad2deg(ByVal rad As Double) As Double
        Return rad / Math.PI * 180.0
    End Function

End Class
