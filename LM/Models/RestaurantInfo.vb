﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class RestaurantInfo
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)>
    Public Property ID As Integer
    <Required> _
    Public Property RestaurantID As Integer
    Public Overridable Property Restaurant As LMRestaurant

    Public Property OrderFaxNumber As String
    Public Property OrderPhoneNumber As String

    '1 - Fax
    '2 - Email
    Public Property SubmissionType As Integer
    Public Property EmailTo As String


    Public Property DeliveryRadius As Double

    Public Property hasLMDAccount As Boolean

    Public Property Notes As String


End Class
