﻿Imports System.ComponentModel.DataAnnotations
Public Class User

    Public Property UserID As Integer
    Public Property UserName As String
    <Required()>
    <DataType(DataType.EmailAddress)>
    Public Property Email As String

    Public Property password As String
    Public Property Roles As String
End Class
