﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Public Class LMRestaurant

    Public Property ID As Integer
    Public Property Name As String
    Public Property Address As String
    Public Property Address2 As String
    Public Property City As String
    Public Property State As String
    Public Property zip As String
    Public Property Phone As String
    Public Property Active As Boolean

    Public Overridable Property Categories As ICollection(Of LMCategories)
    Public Overridable Property OrderOptions As ICollection(Of LMOrderOption)

    Public Overridable Property Menus As ICollection(Of LMMenu)

    'LatLon
    Public Property Lat As Double
    Public Property Lon As Double

    'Open times
    Public Property SunOpen As DateTime
    Public Property SunClose As DateTime
    Public Property MonOpen As DateTime
    Public Property MonClose As DateTime
    Public Property TueOpen As DateTime
    Public Property TueClose As DateTime
    Public Property WedOpen As DateTime
    Public Property WedClose As DateTime
    Public Property ThuOpen As DateTime
    Public Property ThuClose As DateTime
    Public Property FriOpen As DateTime
    Public Property FriClose As DateTime
    Public Property SatOpen As DateTime
    Public Property SatClose As DateTime

    Public Property URLName As String

    Public Property LocationID As String

    Public Property WebAddress As String

    Public Property OnlineOrdering As Boolean

    Public Overridable Property Restaurantinfo As RestaurantInfo

    Public Property LocalRank As Integer

End Class
