﻿Public Class LMOrderOption
    Public Property id As Integer
    Public Property name As String
    Public Property active As Boolean

    Public Overridable Property Restaurants As ICollection(Of LMRestaurant)
End Class
