﻿Imports System.IO

Public Class EmailModel
    Dim db As FBEntities = New FBEntities
    Public Function RenderViewToString(controller As Controller, viewname As String, model As Object, mastername As String) As String
        Controller.ViewData.Model = model

        Using sw As StringWriter = New StringWriter
            Dim viewResult As ViewEngineResult = ViewEngines.Engines.FindView(controller.ControllerContext, viewname, Nothing)
            Dim viewContext As ViewContext = New ViewContext(Controller.ControllerContext, viewResult.View, Controller.ViewData, Controller.TempData, sw)
            viewResult.View.Render(viewContext, sw)
            Return sw.ToString()
        End Using
    End Function

    Function AddEmail(ByVal interactiontype As Char, ByVal interactionnumber As Integer, ByVal emailto As String, emailfrom As String, emailsubject As String, emailbody As String, ByVal emailattachment_bln As String, ByVal emailattachmentPath As String, ByVal emailtype As Integer)
        Dim email As New Email
        email.InteractionType = interactiontype
        email.InteractionNumber = interactionnumber
        email.Email_To = emailto
        email.Email_From = emailfrom
        email.Email_Subject = emailsubject
        email.Email_Body = emailbody
        email.Email_Attachment_bln = emailattachment_bln
        email.Email_Attachment_Path = emailattachmentPath
        email.Email_Create_Date = Now()
        email.Email_Status = 1
        email.Email_Sent_Date = "1/1/1900"
        email.Email_Type = emailtype

        Db.emails.Add(email)
        Db.SaveChanges()

        Return True
    End Function



End Class
