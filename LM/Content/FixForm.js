﻿
    $(function () {
        //Fix CSS Formats
        $(".text-box").addClass("form-control");
        
        $(".editor-label").addClass("control-label");
        $(".field-validation-error").addClass("text-danger");
        //Hide ID
        $("#ID").addClass("hidden");
        $("label[for = 'ID']").addClass("hidden");
        $("#id").addClass("hidden");
        $("label[for = 'id']").addClass("hidden");

        $("form").addClass("form-inline");

    })
