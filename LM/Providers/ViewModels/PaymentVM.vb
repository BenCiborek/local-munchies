﻿Public Class PaymentVM
    Public Property BilltoAddress As Address
    Public Property ExpMonths As List(Of SelectListItem)
    Public Property ExpYears As List(Of SelectListItem)
    Public Property BillableAmount As Double

    Public Property OrderTotal As Double

    Public Property OrderType As Integer
    Public Property ShipToAdds As List(Of Address)
    Public Property BillToAdds As List(Of Address)

    Public Property EmailAddress As String

    Public Property SubTotal As Double
    Public Property Fee As Double
    Public Property Tip As Double
    Public Property Promo As Double


    Public Property showTip As Boolean
End Class
