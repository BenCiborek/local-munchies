﻿Public Class ReceiptVM
    Public Property Ticket As Ticket
    Public Property Rests As List(Of LMRestaurant)
    Public Property Billto As Address
    Public Property Deliverto As Address
    Public Property Notes As String

End Class
