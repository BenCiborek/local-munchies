﻿Public Class FaxTicketVM
    Public Property Items As List(Of TicketItem)
    Public Property RestaurantName As String
    Public Property TotalAmount As Double
    Public Property OrderNumber As Integer
    Public Property Confirmation As Integer
    Public Property OrderType As String
    Public Property IntOrderType As Integer
    Public Property DeliveryAddress As Address
    Public Property BilltoAddress As Address
    Public Property DeliveryPrice As Double
    Public Property Notes As String
End Class
