﻿Public Class CustomersServedVM
    Public Property TotalCustomers As Double
    Public Property TotalCustomersDay As Double
    Public Property TotalCustomersWeek As Double
    Public Property AvgCustomersPerDayLastWeek As Double
    Public Property TotalCustomersMonth As Double
    Public Property AvgCustomersPerDayLastMonth As Double
    Public Property TotalCustomersYear As Double
    Public Property AvgCustomersPerDayLastYear As Double
End Class
