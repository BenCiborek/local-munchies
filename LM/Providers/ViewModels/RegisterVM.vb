﻿Imports System.ComponentModel.DataAnnotations
Public Class RegisterVM
    <Required()> _
    <EmailAddress(ErrorMessage:="Please Enter a Valid Email Address")> _
    Public Property Email As String

    <Required()> _
    <StringLength(50, ErrorMessage:="The {0} must be at least {2} characters long.", MinimumLength:=6)> _
    <RegularExpression("([a-zA-Z]+[\d]+|[\d]+[a-zA-Z]+)[^\s]*", ErrorMessage:="Password must contain at least 1 letter and 1 number")> _
    <DataType(DataType.Password)> _
    Public Property Password As String

    <Required()> _
    <DataType(DataType.Password)> _
    <Compare("Password", ErrorMessage:="The Password and Confirmation Password do not match.")> _
    Public Property ConfirmPassword As String

    Public Property Message As String
End Class
