﻿Public Class CompletedOrderResultVM
    Public Property Ticket As Ticket
    Public Property Billto As Address
    Public Property Delivery As Address
    Public Property Restaurants As List(Of LMRestaurant)
End Class
