Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ControlType3
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuModifierGroups", "SubQtyMax", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMMenuModifierGroups", "SubQtyMax")
        End Sub
    End Class
End Namespace
