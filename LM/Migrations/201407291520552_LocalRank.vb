Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class LocalRank
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMRestaurants", "LocalRank", Function(c) c.Int(nullable:=False, defaultValue:=99))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMRestaurants", "LocalRank")
        End Sub
    End Class
End Namespace
