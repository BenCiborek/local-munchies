Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class RestaurantNotes2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropIndex("dbo.RestaurantInfoes", New String() { "ID" })
            DropPrimaryKey("dbo.RestaurantInfoes")
            AlterColumn("dbo.RestaurantInfoes", "ID", Function(c) c.Int(nullable := False, identity := True))
            AddPrimaryKey("dbo.RestaurantInfoes", "ID")
            CreateIndex("dbo.RestaurantInfoes", "ID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.RestaurantInfoes", New String() { "ID" })
            DropPrimaryKey("dbo.RestaurantInfoes")
            AlterColumn("dbo.RestaurantInfoes", "ID", Function(c) c.Int(nullable := False))
            AddPrimaryKey("dbo.RestaurantInfoes", "ID")
            CreateIndex("dbo.RestaurantInfoes", "ID")
        End Sub
    End Class
End Namespace
