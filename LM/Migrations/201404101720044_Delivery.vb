Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Delivery
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.RestaurantInfoes", "RestaurantID", "dbo.LMRestaurants")
            DropIndex("dbo.RestaurantInfoes", New String() { "RestaurantID" })
            AddColumn("dbo.RestaurantInfoes", "DeliveryRadius", Function(c) c.Double(nullable := False))
            AlterColumn("dbo.RestaurantInfoes", "id", Function(c) c.Int(nullable := False))
            AddForeignKey("dbo.RestaurantInfoes", "id", "dbo.LMRestaurants", "ID")
            CreateIndex("dbo.RestaurantInfoes", "id")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.RestaurantInfoes", New String() { "id" })
            DropForeignKey("dbo.RestaurantInfoes", "id", "dbo.LMRestaurants")
            AlterColumn("dbo.RestaurantInfoes", "id", Function(c) c.Int(nullable := False, identity := True))
            DropColumn("dbo.RestaurantInfoes", "DeliveryRadius")
            CreateIndex("dbo.RestaurantInfoes", "RestaurantID")
            AddForeignKey("dbo.RestaurantInfoes", "RestaurantID", "dbo.LMRestaurants", "ID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
