Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ControlType1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.ControlTypeModels",
                Function(c) New With
                    {
                        .ID = c.Int(nullable:=False, identity:=True),
                        .Name = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)


        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.ControlTypeModels")
        End Sub
    End Class
End Namespace
