Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket22420141
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Addresses", "Type", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Addresses", "AddressDefault", Function(c) c.Boolean(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Addresses", "AddressDefault")
            DropColumn("dbo.Addresses", "Type")
        End Sub
    End Class
End Namespace
