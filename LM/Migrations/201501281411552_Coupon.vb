Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Coupon
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CouponCodes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Code = c.String(),
                        .Active = c.Boolean(nullable := False),
                        .Discount = c.Double(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.CouponCodes")
        End Sub
    End Class
End Namespace
