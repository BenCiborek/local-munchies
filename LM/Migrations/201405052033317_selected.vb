Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class selected
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuModifiers", "Selected", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMMenuModifiers", "Selected")
        End Sub
    End Class
End Namespace
