Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Ticket4
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Tickets", "UserID", "dbo.Users")
            DropForeignKey("dbo.Tickets", "RestaurantID", "dbo.LMRestaurants")
            DropIndex("dbo.Tickets", New String() { "UserID" })
            DropIndex("dbo.Tickets", New String() { "RestaurantID" })
            AddColumn("dbo.TicketItemModifiers", "MenuItemModifierID", Function(c) c.Int(nullable := False))
            AddColumn("dbo.TicketItemModifierGroups", "MenuItemModifierGroupID", Function(c) c.Int(nullable := False))
            DropColumn("dbo.Tickets", "UserID")
            DropColumn("dbo.TicketItemModifiers", "MenuItemID")
            DropColumn("dbo.TicketItemModifiers", "ItemCount")
            DropColumn("dbo.TicketItemModifierGroups", "MenuItemModifierID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.TicketItemModifierGroups", "MenuItemModifierID", Function(c) c.Int(nullable := False))
            AddColumn("dbo.TicketItemModifiers", "ItemCount", Function(c) c.Int(nullable := False))
            AddColumn("dbo.TicketItemModifiers", "MenuItemID", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Tickets", "UserID", Function(c) c.Int(nullable := False))
            DropColumn("dbo.TicketItemModifierGroups", "MenuItemModifierGroupID")
            DropColumn("dbo.TicketItemModifiers", "MenuItemModifierID")
            CreateIndex("dbo.Tickets", "RestaurantID")
            CreateIndex("dbo.Tickets", "UserID")
            AddForeignKey("dbo.Tickets", "RestaurantID", "dbo.LMRestaurants", "ID", cascadeDelete := True)
            AddForeignKey("dbo.Tickets", "UserID", "dbo.Users", "UserID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
