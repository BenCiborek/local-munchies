Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class L21
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.LMMenuL2Modifier",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModDate = c.DateTime(nullable := False),
                        .CreateDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .Price = c.Double(nullable := False),
                        .Extra_Price = c.Double(nullable := False),
                        .Active = c.Boolean(nullable := False),
                        .MenuModifierGroupID = c.Int(nullable := False),
                        .L2MenuModifierGroup_ID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.LMMenuL2ModifierGroup", Function(t) t.L2MenuModifierGroup_ID) _
                .Index(Function(t) t.L2MenuModifierGroup_ID)
            
            CreateTable(
                "dbo.LMMenuL2ModifierGroup",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .moddate = c.DateTime(nullable := False),
                        .createdate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .Active = c.Boolean(nullable := False),
                        .Exclusive = c.Boolean(nullable := False),
                        .Required = c.Boolean(nullable := False),
                        .ModifierID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.LMMenuModifiers", Function(t) t.ModifierID, cascadeDelete := True) _
                .Index(Function(t) t.ModifierID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.LMMenuL2ModifierGroup", New String() { "ModifierID" })
            DropIndex("dbo.LMMenuL2Modifier", New String() { "L2MenuModifierGroup_ID" })
            DropForeignKey("dbo.LMMenuL2ModifierGroup", "ModifierID", "dbo.LMMenuModifiers")
            DropForeignKey("dbo.LMMenuL2Modifier", "L2MenuModifierGroup_ID", "dbo.LMMenuL2ModifierGroup")
            DropTable("dbo.LMMenuL2ModifierGroup")
            DropTable("dbo.LMMenuL2Modifier")
        End Sub
    End Class
End Namespace
