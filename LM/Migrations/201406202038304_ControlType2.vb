Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ControlType2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuModifierGroups", "ControlTypeID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddForeignKey("dbo.LMMenuModifierGroups", "ControlTypeID", "dbo.ControlTypeModels", "ID", cascadeDelete := True)
            CreateIndex("dbo.LMMenuModifierGroups", "ControlTypeID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.LMMenuModifierGroups", New String() { "ControlTypeID" })
            DropForeignKey("dbo.LMMenuModifierGroups", "ControlTypeID", "dbo.ControlTypeModels")
            DropColumn("dbo.LMMenuModifierGroups", "ControlTypeID")
        End Sub
    End Class
End Namespace
