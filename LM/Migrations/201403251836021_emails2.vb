Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class emails2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Emails", "InteractionType", Function(c) c.String(maxLength := 1))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Emails", "InteractionType")
        End Sub
    End Class
End Namespace
