Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class lmdenabled1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Tickets", "LMDID", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Tickets", "LMDStatus", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Tickets", "LMDStatus")
            DropColumn("dbo.Tickets", "LMDID")
        End Sub
    End Class
End Namespace
