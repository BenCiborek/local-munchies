Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ControlType5
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.TicketItemL2Modifier", "Qty", Function(c) c.Int(nullable:=False, defaultValue:=1))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.TicketItemL2Modifier", "Qty")
        End Sub
    End Class
End Namespace
