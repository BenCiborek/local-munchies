Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class WebAddress
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMRestaurants", "WebAddress", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMRestaurants", "WebAddress")
        End Sub
    End Class
End Namespace
