Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class cart11
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMRestaurants", "URLName", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMRestaurants", "URLName")
        End Sub
    End Class
End Namespace
