Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class emailsubmission
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.RestaurantInfoes", "OrderType", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddColumn("dbo.RestaurantInfoes", "EmailTo", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.RestaurantInfoes", "EmailTo")
            DropColumn("dbo.RestaurantInfoes", "OrderType")
        End Sub
    End Class
End Namespace
