Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Ticket3
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Tickets", "ShoppingCartID", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Tickets", "ShoppingCartID")
        End Sub
    End Class
End Namespace
