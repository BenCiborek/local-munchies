Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class cart13
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Locations",
                Function(c) New With
                    {
                        .id = c.String(nullable := False, maxLength := 128),
                        .City = c.String(),
                        .State = c.String()
                    }) _
                .PrimaryKey(Function(t) t.id)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Locations")
        End Sub
    End Class
End Namespace
