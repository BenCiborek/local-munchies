Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class wholehalf
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.TicketItemModifiers", "Notes", Function(c) c.String())
            AddColumn("dbo.TicketItemL2Modifier", "Notes", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.TicketItemL2Modifier", "Notes")
            DropColumn("dbo.TicketItemModifiers", "Notes")
        End Sub
    End Class
End Namespace
