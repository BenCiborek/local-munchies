Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class cart
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.TicketItems", "RestaurantID", Function(c) c.Int(nullable := False))
            AddForeignKey("dbo.TicketItems", "RestaurantID", "dbo.LMRestaurants", "ID", cascadeDelete := True)
            CreateIndex("dbo.TicketItems", "RestaurantID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.TicketItems", New String() { "RestaurantID" })
            DropForeignKey("dbo.TicketItems", "RestaurantID", "dbo.LMRestaurants")
            DropColumn("dbo.TicketItems", "RestaurantID")
        End Sub
    End Class
End Namespace
