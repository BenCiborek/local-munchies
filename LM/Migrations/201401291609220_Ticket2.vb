Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Ticket2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenus", "OnlineOrderingActive", Function(c) c.Int(nullable:=True))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMMenus", "OnlineOrderingActive")
        End Sub
    End Class
End Namespace
