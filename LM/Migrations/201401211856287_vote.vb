Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class vote
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.OOVotes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .VDate = c.DateTime(nullable := False),
                        .VRestID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.OOVotes")
        End Sub
    End Class
End Namespace
