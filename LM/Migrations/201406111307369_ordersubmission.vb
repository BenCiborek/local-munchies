Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ordersubmission
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.RestaurantInfoes", "SubmissionType", Function(c) c.Int(nullable := False))
            DropColumn("dbo.RestaurantInfoes", "OrderType")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.RestaurantInfoes", "OrderType", Function(c) c.Int(nullable := False))
            DropColumn("dbo.RestaurantInfoes", "SubmissionType")
        End Sub
    End Class
End Namespace
