Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class L22
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.LMMenuL2Modifier", "L2MenuModifierGroup_ID", "dbo.LMMenuL2ModifierGroup")
            DropIndex("dbo.LMMenuL2Modifier", New String() { "L2MenuModifierGroup_ID" })
            RenameColumn(table := "dbo.LMMenuL2Modifier", name := "L2MenuModifierGroup_ID", newName := "L2MenuModifierGroupID")
            AddForeignKey("dbo.LMMenuL2Modifier", "L2MenuModifierGroupID", "dbo.LMMenuL2ModifierGroup", "ID", cascadeDelete := True)
            CreateIndex("dbo.LMMenuL2Modifier", "L2MenuModifierGroupID")
            DropColumn("dbo.LMMenuL2Modifier", "MenuModifierGroupID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.LMMenuL2Modifier", "MenuModifierGroupID", Function(c) c.Int(nullable := False))
            DropIndex("dbo.LMMenuL2Modifier", New String() { "L2MenuModifierGroupID" })
            DropForeignKey("dbo.LMMenuL2Modifier", "L2MenuModifierGroupID", "dbo.LMMenuL2ModifierGroup")
            RenameColumn(table := "dbo.LMMenuL2Modifier", name := "L2MenuModifierGroupID", newName := "L2MenuModifierGroup_ID")
            CreateIndex("dbo.LMMenuL2Modifier", "L2MenuModifierGroup_ID")
            AddForeignKey("dbo.LMMenuL2Modifier", "L2MenuModifierGroup_ID", "dbo.LMMenuL2ModifierGroup", "ID")
        End Sub
    End Class
End Namespace
