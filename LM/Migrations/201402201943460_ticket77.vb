Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket77
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Addresses", "Address1", Function(c) c.String(nullable := False, maxLength := 100))
            AddColumn("dbo.Addresses", "Address2", Function(c) c.String())
            AlterColumn("dbo.Addresses", "Email", Function(c) c.String(nullable := False, maxLength := 100))
            AlterColumn("dbo.Addresses", "Name", Function(c) c.String(nullable := False, maxLength := 100))
            AlterColumn("dbo.Addresses", "City", Function(c) c.String(nullable := False, maxLength := 100))
            AlterColumn("dbo.Addresses", "Zip", Function(c) c.String(nullable := False, maxLength := 10))
            AlterColumn("dbo.Addresses", "phone", Function(c) c.String(nullable := False, maxLength := 20))
            DropColumn("dbo.Addresses", "Street")
            DropColumn("dbo.Addresses", "Street2")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Addresses", "Street2", Function(c) c.String())
            AddColumn("dbo.Addresses", "Street", Function(c) c.String())
            AlterColumn("dbo.Addresses", "phone", Function(c) c.String())
            AlterColumn("dbo.Addresses", "Zip", Function(c) c.String())
            AlterColumn("dbo.Addresses", "City", Function(c) c.String())
            AlterColumn("dbo.Addresses", "Name", Function(c) c.String())
            AlterColumn("dbo.Addresses", "Email", Function(c) c.String())
            DropColumn("dbo.Addresses", "Address2")
            DropColumn("dbo.Addresses", "Address1")
        End Sub
    End Class
End Namespace
