Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class wholehalf1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.LMMenuL3Modifier",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Extra_Price = c.Double(nullable := False),
                        .Active = c.Boolean(nullable := False),
                        .Selected = c.Boolean(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .L2MenuModifierID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.LMMenuL2Modifier", Function(t) t.L2MenuModifierID, cascadeDelete := True) _
                .Index(Function(t) t.L2MenuModifierID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.LMMenuL3Modifier", New String() { "L2MenuModifierID" })
            DropForeignKey("dbo.LMMenuL3Modifier", "L2MenuModifierID", "dbo.LMMenuL2Modifier")
            DropTable("dbo.LMMenuL3Modifier")
        End Sub
    End Class
End Namespace
