Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class QBInfo
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.QBTokenInfoes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .AccessToken = c.String(),
                        .accessTokenSecret = c.String(),
                        .consumerKey = c.String(),
                        .consumerSecret = c.String(),
                        .AppToken = c.String(),
                        .CompanyID = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.QBClasses",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .LMRestaurantID = c.String(),
                        .QBClassID = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.QBClasses")
            DropTable("dbo.QBTokenInfoes")
        End Sub
    End Class
End Namespace
