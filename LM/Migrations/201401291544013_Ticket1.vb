Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Ticket1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Tickets",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .CreatedDate = c.DateTime(nullable := False),
                        .ClosingDate = c.DateTime(nullable := False),
                        .ActiveDate = c.DateTime(nullable := False),
                        .Paid = c.Boolean(nullable := False),
                        .Voided = c.Boolean(nullable := False),
                        .VoidReason = c.String(),
                        .SubTotal = c.Double(nullable := False),
                        .DiscountTotal = c.Double(nullable := False),
                        .TaxTotal = c.Double(nullable := False),
                        .TotalPrice = c.Double(nullable := False),
                        .PaidAmount = c.Double(nullable := False),
                        .DeliveryPrice = c.Double(nullable := False),
                        .FeePrice = c.Double(nullable := False),
                        .TicketTypeID = c.Int(nullable := False),
                        .UserID = c.Int(nullable := False),
                        .RestaurantID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketTypes", Function(t) t.TicketTypeID, cascadeDelete := True) _
                .ForeignKey("dbo.Users", Function(t) t.UserID, cascadeDelete := True) _
                .ForeignKey("dbo.LMRestaurants", Function(t) t.RestaurantID, cascadeDelete := True) _
                .Index(Function(t) t.TicketTypeID) _
                .Index(Function(t) t.UserID) _
                .Index(Function(t) t.RestaurantID)
            
            CreateTable(
                "dbo.TicketTypes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.TicketItems",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .Quantity = c.Double(nullable := False),
                        .ItemBasePrice = c.Double(nullable := False),
                        .ItemTaxRate = c.Double(nullable := False),
                        .ItemDiscount = c.Double(nullable := False),
                        .ItemModifierPrice = c.Double(nullable := False),
                        .TotalItemPrice = c.Double(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .ItemID = c.Int(nullable := False),
                        .TicketID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Tickets", Function(t) t.TicketID, cascadeDelete := True) _
                .Index(Function(t) t.TicketID)
            
            CreateTable(
                "dbo.TicketItemModifiers",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .TicketItemID = c.Int(nullable := False),
                        .MenuItemID = c.Int(nullable := False),
                        .ItemCount = c.Int(nullable := False),
                        .Name = c.String(),
                        .ModifierPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False),
                        .ModifierType = c.Int(nullable := False),
                        .TotalPrice = c.Double(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .TicketItemModifierGroupID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItemModifierGroups", Function(t) t.TicketItemModifierGroupID, cascadeDelete := True) _
                .Index(Function(t) t.TicketItemModifierGroupID)
            
            CreateTable(
                "dbo.TicketItemModifierGroups",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .MinQty = c.Int(nullable := False),
                        .MaxQty = c.Int(nullable := False),
                        .FreeQty = c.Int(nullable := False),
                        .TicketItemID = c.Int(nullable := False),
                        .MenuItemModifierID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItems", Function(t) t.TicketItemID, cascadeDelete := True) _
                .Index(Function(t) t.TicketItemID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.TicketItemModifierGroups", New String() { "TicketItemID" })
            DropIndex("dbo.TicketItemModifiers", New String() { "TicketItemModifierGroupID" })
            DropIndex("dbo.TicketItems", New String() { "TicketID" })
            DropIndex("dbo.Tickets", New String() { "RestaurantID" })
            DropIndex("dbo.Tickets", New String() { "UserID" })
            DropIndex("dbo.Tickets", New String() { "TicketTypeID" })
            DropForeignKey("dbo.TicketItemModifierGroups", "TicketItemID", "dbo.TicketItems")
            DropForeignKey("dbo.TicketItemModifiers", "TicketItemModifierGroupID", "dbo.TicketItemModifierGroups")
            DropForeignKey("dbo.TicketItems", "TicketID", "dbo.Tickets")
            DropForeignKey("dbo.Tickets", "RestaurantID", "dbo.LMRestaurants")
            DropForeignKey("dbo.Tickets", "UserID", "dbo.Users")
            DropForeignKey("dbo.Tickets", "TicketTypeID", "dbo.TicketTypes")
            DropTable("dbo.TicketItemModifierGroups")
            DropTable("dbo.TicketItemModifiers")
            DropTable("dbo.TicketItems")
            DropTable("dbo.TicketTypes")
            DropTable("dbo.Tickets")
        End Sub
    End Class
End Namespace
