Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class emails1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Emails",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .InteractionNumber = c.Int(nullable := False),
                        .Email_To = c.String(),
                        .Email_From = c.String(),
                        .Email_Subject = c.String(),
                        .Email_Body = c.String(),
                        .Email_Attachment_bln = c.Boolean(nullable := False),
                        .Email_Attachment_Path = c.String(),
                        .Email_Create_Date = c.DateTime(nullable := False),
                        .Email_Status = c.Int(nullable := False),
                        .Email_Sent_Date = c.DateTime(nullable := False),
                        .Email_Type = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Emails")
        End Sub
    End Class
End Namespace
