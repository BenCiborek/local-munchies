Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket75
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Addresses", "phone", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Addresses", "phone")
        End Sub
    End Class
End Namespace
