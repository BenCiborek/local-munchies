Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket74
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Addresses", "Email", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Addresses", "Email")
        End Sub
    End Class
End Namespace
