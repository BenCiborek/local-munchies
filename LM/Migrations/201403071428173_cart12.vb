Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class cart12
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMRestaurants", "LocationID", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMRestaurants", "LocationID")
        End Sub
    End Class
End Namespace
