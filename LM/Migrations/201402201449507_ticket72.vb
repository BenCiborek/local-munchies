Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket72
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Addresses",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .UserID = c.Int(nullable := False),
                        .SessionID = c.Guid(nullable := False),
                        .Name = c.String(),
                        .Street = c.String(),
                        .Street2 = c.String(),
                        .City = c.String(),
                        .State = c.String(),
                        .Zip = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Addresses")
        End Sub
    End Class
End Namespace
