Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class SpecialInstructions
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.TicketItems", "SpecialInstructions", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.TicketItems", "SpecialInstructions")
        End Sub
    End Class
End Namespace
