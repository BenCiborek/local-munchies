Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class OO
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMRestaurants", "OnlineOrdering", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMRestaurants", "OnlineOrdering")
        End Sub
    End Class
End Namespace
