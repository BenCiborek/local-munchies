Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class RestaurantNotes1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropIndex("dbo.RestaurantInfoes", New String() { "id" })
            CreateIndex("dbo.RestaurantInfoes", "ID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.RestaurantInfoes", New String() { "ID" })
            CreateIndex("dbo.RestaurantInfoes", "id")
        End Sub
    End Class
End Namespace
