Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket22420143
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Tickets", "PaymentID", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Tickets", "PaymentID")
        End Sub
    End Class
End Namespace
