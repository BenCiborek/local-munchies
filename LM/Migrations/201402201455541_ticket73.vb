Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket73
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.Addresses", "SessionID", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Addresses", "SessionID", Function(c) c.Guid(nullable := False))
        End Sub
    End Class
End Namespace
