Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class emails3
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuItems", "Taxable", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.TicketItems", "ItemTax", Function(c) c.Double(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.TicketItems", "ItemTax")
            DropColumn("dbo.LMMenuItems", "Taxable")
        End Sub
    End Class
End Namespace
