Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket22420142
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Payments",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Type = c.Int(nullable := False),
                        .Authcode = c.String(),
                        .Authorized = c.Boolean(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            AddColumn("dbo.Tickets", "TicketStatus", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Tickets", "ReceiptID", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Tickets", "ReceiptID")
            DropColumn("dbo.Tickets", "TicketStatus")
            DropTable("dbo.Payments")
        End Sub
    End Class
End Namespace
