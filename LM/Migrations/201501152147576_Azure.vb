Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Azure
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Locations",
                Function(c) New With
                    {
                        .id = c.String(nullable := False, maxLength := 128),
                        .City = c.String(),
                        .State = c.String()
                    }) _
                .PrimaryKey(Function(t) t.id)
            
            CreateTable(
                "dbo.LMMenuL2ModifierGroup",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .moddate = c.DateTime(nullable := False),
                        .createdate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .Active = c.Boolean(nullable := False),
                        .Exclusive = c.Boolean(nullable := False),
                        .Required = c.Boolean(nullable := False),
                        .MinQty = c.Int(nullable := False),
                        .MaxQty = c.Int(nullable := False),
                        .FreeQty = c.Int(nullable := False),
                        .ModifierID = c.Int(nullable := False),
                        .ControlTypeID = c.Int(nullable := False),
                        .SubQtyMax = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.LMMenuModifiers", Function(t) t.ModifierID, cascadeDelete := True) _
                .ForeignKey("dbo.ControlTypeModels", Function(t) t.ControlTypeID, cascadeDelete := True) _
                .Index(Function(t) t.ModifierID) _
                .Index(Function(t) t.ControlTypeID)
            
            CreateTable(
                "dbo.ControlTypeModels",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.LMMenuL2Modifier",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModDate = c.DateTime(nullable := False),
                        .CreateDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .Price = c.Double(nullable := False),
                        .Extra_Price = c.Double(nullable := False),
                        .Active = c.Boolean(nullable := False),
                        .Selected = c.Boolean(nullable := False),
                        .L2MenuModifierGroupID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.LMMenuL2ModifierGroup", Function(t) t.L2MenuModifierGroupID, cascadeDelete := True) _
                .Index(Function(t) t.L2MenuModifierGroupID)
            
            CreateTable(
                "dbo.LMMenuL3Modifier",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Extra_Price = c.Double(nullable := False),
                        .Active = c.Boolean(nullable := False),
                        .Selected = c.Boolean(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .L2MenuModifierID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.LMMenuL2Modifier", Function(t) t.L2MenuModifierID, cascadeDelete := True) _
                .Index(Function(t) t.L2MenuModifierID)
            
            CreateTable(
                "dbo.RestaurantInfoes",
                Function(c) New With
                    {
                        .id = c.Int(nullable := False),
                        .RestaurantID = c.Int(nullable := False),
                        .OrderFaxNumber = c.String(),
                        .OrderPhoneNumber = c.String(),
                        .SubmissionType = c.Int(nullable := False),
                        .EmailTo = c.String(),
                        .DeliveryRadius = c.Double(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.id) _
                .ForeignKey("dbo.LMRestaurants", Function(t) t.id) _
                .Index(Function(t) t.id)
            
            CreateTable(
                "dbo.Tickets",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ShoppingCartID = c.String(),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .CreatedDate = c.DateTime(nullable := False),
                        .ClosingDate = c.DateTime(nullable := False),
                        .ActiveDate = c.DateTime(nullable := False),
                        .Paid = c.Boolean(nullable := False),
                        .Voided = c.Boolean(nullable := False),
                        .VoidReason = c.String(),
                        .SubTotal = c.Double(nullable := False),
                        .DiscountTotal = c.Double(nullable := False),
                        .TaxTotal = c.Double(nullable := False),
                        .TotalPrice = c.Double(nullable := False),
                        .PaidAmount = c.Double(nullable := False),
                        .DeliveryPrice = c.Double(nullable := False),
                        .FeePrice = c.Double(nullable := False),
                        .PaymentID = c.Int(nullable := False),
                        .TicketStatus = c.Int(nullable := False),
                        .BilltoAddress = c.Int(nullable := False),
                        .DeliveryAddress = c.Int(nullable := False),
                        .TicketTypeID = c.Int(nullable := False),
                        .RestaurantID = c.Int(nullable := False),
                        .ReceiptID = c.String(),
                        .Username = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.TicketItems",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .Quantity = c.Double(nullable := False),
                        .ItemBasePrice = c.Double(nullable := False),
                        .ItemTaxRate = c.Double(nullable := False),
                        .ItemTax = c.Double(nullable := False),
                        .ItemDiscount = c.Double(nullable := False),
                        .ItemModifierPrice = c.Double(nullable := False),
                        .TotalItemPrice = c.Double(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .ItemID = c.Int(nullable := False),
                        .TicketID = c.Int(nullable := False),
                        .RestaurantID = c.Int(nullable := False),
                        .SpecialInstructions = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Tickets", Function(t) t.TicketID, cascadeDelete := True) _
                .ForeignKey("dbo.LMRestaurants", Function(t) t.RestaurantID, cascadeDelete := True) _
                .Index(Function(t) t.TicketID) _
                .Index(Function(t) t.RestaurantID)
            
            CreateTable(
                "dbo.TicketItemModifierGroups",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .MinQty = c.Int(nullable := False),
                        .MaxQty = c.Int(nullable := False),
                        .FreeQty = c.Int(nullable := False),
                        .TicketItemID = c.Int(nullable := False),
                        .MenuItemModifierGroupID = c.Int(nullable := False),
                        .TotalPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItems", Function(t) t.TicketItemID, cascadeDelete := True) _
                .Index(Function(t) t.TicketItemID)
            
            CreateTable(
                "dbo.TicketItemModifiers",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .TicketItemID = c.Int(nullable := False),
                        .MenuItemModifierID = c.Int(nullable := False),
                        .Name = c.String(),
                        .ModifierPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False),
                        .ModifierType = c.Int(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .TicketItemModifierGroupID = c.Int(nullable := False),
                        .Notes = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItemModifierGroups", Function(t) t.TicketItemModifierGroupID, cascadeDelete := True) _
                .Index(Function(t) t.TicketItemModifierGroupID)
            
            CreateTable(
                "dbo.TicketItemL2ModifierGroup",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .MinQty = c.Int(nullable := False),
                        .MaxQty = c.Int(nullable := False),
                        .FreeQty = c.Int(nullable := False),
                        .L1ModifierID = c.Int(nullable := False),
                        .MenuItemL2ModifierGroupID = c.Int(nullable := False),
                        .TotalPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItemModifiers", Function(t) t.L1ModifierID, cascadeDelete := True) _
                .Index(Function(t) t.L1ModifierID)
            
            CreateTable(
                "dbo.TicketItemL2Modifier",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .TicketItemID = c.Int(nullable := False),
                        .MenuItemModifierID = c.Int(nullable := False),
                        .Name = c.String(),
                        .ModifierPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False),
                        .ModifierType = c.Int(nullable := False),
                        .Qty = c.Int(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .TicketItemL2ModifierGroupID = c.Int(nullable := False),
                        .Notes = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItemL2ModifierGroup", Function(t) t.TicketItemL2ModifierGroupID, cascadeDelete := True) _
                .Index(Function(t) t.TicketItemL2ModifierGroupID)
            
            CreateTable(
                "dbo.Payments",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Type = c.Int(nullable := False),
                        .Authcode = c.String(),
                        .Authorized = c.Boolean(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.Addresses",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .UserID = c.Int(nullable := False),
                        .SessionID = c.String(),
                        .Type = c.Int(nullable := False),
                        .AddressDefault = c.Boolean(nullable := False),
                        .Email = c.String(nullable := False, maxLength := 100),
                        .Name = c.String(nullable := False, maxLength := 100),
                        .Address1 = c.String(nullable := False, maxLength := 100),
                        .Address2 = c.String(),
                        .City = c.String(nullable := False, maxLength := 100),
                        .State = c.String(nullable := False, maxLength := 2),
                        .Zip = c.String(nullable := False, maxLength := 10),
                        .phone = c.String(nullable := False, maxLength := 20)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.Emails",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .InteractionType = c.String(maxLength := 1),
                        .InteractionNumber = c.Int(nullable := False),
                        .Email_To = c.String(),
                        .Email_From = c.String(),
                        .Email_Subject = c.String(),
                        .Email_Body = c.String(),
                        .Email_Attachment_bln = c.Boolean(nullable := False),
                        .Email_Attachment_Path = c.String(),
                        .Email_Create_Date = c.DateTime(nullable := False),
                        .Email_Status = c.Int(nullable := False),
                        .Email_Sent_Date = c.DateTime(nullable := False),
                        .Email_Type = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.FaxSents",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ToNumber = c.String(),
                        .FaxPage = c.String(),
                        .DateSent = c.DateTime(nullable := False),
                        .Status = c.Int(nullable := False),
                        .Ticketid = c.Int(nullable := False),
                        .RestID = c.Int(nullable := False),
                        .DateConfirmed = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            AddColumn("dbo.Users", "Email", Function(c) c.String(nullable := False))
            AddColumn("dbo.LMRestaurants", "URLName", Function(c) c.String())
            AddColumn("dbo.LMRestaurants", "LocationID", Function(c) c.String())
            AddColumn("dbo.LMRestaurants", "WebAddress", Function(c) c.String())
            AddColumn("dbo.LMRestaurants", "OnlineOrdering", Function(c) c.Boolean(nullable := False))
            AddColumn("dbo.LMRestaurants", "LocalRank", Function(c) c.Int(nullable := False))
            AddColumn("dbo.LMMenus", "OnlineOrderingActive", Function(c) c.Int(nullable := False))
            AddColumn("dbo.LMMenuItems", "Taxable", Function(c) c.Boolean(nullable := False))
            AddColumn("dbo.LMMenuModifiers", "Selected", Function(c) c.Boolean(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.TicketItemL2Modifier", New String() { "TicketItemL2ModifierGroupID" })
            DropIndex("dbo.TicketItemL2ModifierGroup", New String() { "L1ModifierID" })
            DropIndex("dbo.TicketItemModifiers", New String() { "TicketItemModifierGroupID" })
            DropIndex("dbo.TicketItemModifierGroups", New String() { "TicketItemID" })
            DropIndex("dbo.TicketItems", New String() { "RestaurantID" })
            DropIndex("dbo.TicketItems", New String() { "TicketID" })
            DropIndex("dbo.RestaurantInfoes", New String() { "id" })
            DropIndex("dbo.LMMenuL3Modifier", New String() { "L2MenuModifierID" })
            DropIndex("dbo.LMMenuL2Modifier", New String() { "L2MenuModifierGroupID" })
            DropIndex("dbo.LMMenuL2ModifierGroup", New String() { "ControlTypeID" })
            DropIndex("dbo.LMMenuL2ModifierGroup", New String() { "ModifierID" })
            DropForeignKey("dbo.TicketItemL2Modifier", "TicketItemL2ModifierGroupID", "dbo.TicketItemL2ModifierGroup")
            DropForeignKey("dbo.TicketItemL2ModifierGroup", "L1ModifierID", "dbo.TicketItemModifiers")
            DropForeignKey("dbo.TicketItemModifiers", "TicketItemModifierGroupID", "dbo.TicketItemModifierGroups")
            DropForeignKey("dbo.TicketItemModifierGroups", "TicketItemID", "dbo.TicketItems")
            DropForeignKey("dbo.TicketItems", "RestaurantID", "dbo.LMRestaurants")
            DropForeignKey("dbo.TicketItems", "TicketID", "dbo.Tickets")
            DropForeignKey("dbo.RestaurantInfoes", "id", "dbo.LMRestaurants")
            DropForeignKey("dbo.LMMenuL3Modifier", "L2MenuModifierID", "dbo.LMMenuL2Modifier")
            DropForeignKey("dbo.LMMenuL2Modifier", "L2MenuModifierGroupID", "dbo.LMMenuL2ModifierGroup")
            DropForeignKey("dbo.LMMenuL2ModifierGroup", "ControlTypeID", "dbo.ControlTypeModels")
            DropForeignKey("dbo.LMMenuL2ModifierGroup", "ModifierID", "dbo.LMMenuModifiers")
            DropColumn("dbo.LMMenuModifiers", "Selected")
            DropColumn("dbo.LMMenuItems", "Taxable")
            DropColumn("dbo.LMMenus", "OnlineOrderingActive")
            DropColumn("dbo.LMRestaurants", "LocalRank")
            DropColumn("dbo.LMRestaurants", "OnlineOrdering")
            DropColumn("dbo.LMRestaurants", "WebAddress")
            DropColumn("dbo.LMRestaurants", "LocationID")
            DropColumn("dbo.LMRestaurants", "URLName")
            DropColumn("dbo.Users", "Email")
            DropTable("dbo.FaxSents")
            DropTable("dbo.Emails")
            DropTable("dbo.Addresses")
            DropTable("dbo.Payments")
            DropTable("dbo.TicketItemL2Modifier")
            DropTable("dbo.TicketItemL2ModifierGroup")
            DropTable("dbo.TicketItemModifiers")
            DropTable("dbo.TicketItemModifierGroups")
            DropTable("dbo.TicketItems")
            DropTable("dbo.Tickets")
            DropTable("dbo.RestaurantInfoes")
            DropTable("dbo.LMMenuL3Modifier")
            DropTable("dbo.LMMenuL2Modifier")
            DropTable("dbo.ControlTypeModels")
            DropTable("dbo.LMMenuL2ModifierGroup")
            DropTable("dbo.Locations")
        End Sub
    End Class
End Namespace
