Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class RestaurantNotes
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.RestaurantInfoes", "Notes", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.RestaurantInfoes", "Notes")
        End Sub
    End Class
End Namespace
