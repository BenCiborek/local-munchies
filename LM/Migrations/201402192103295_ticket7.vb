Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket7
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Users", "Email", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Users", "Email")
        End Sub
    End Class
End Namespace
