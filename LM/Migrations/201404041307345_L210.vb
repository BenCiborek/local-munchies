Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class L210
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuL2ModifierGroup", "MinQty", Function(c) c.Int(nullable := False))
            AddColumn("dbo.LMMenuL2ModifierGroup", "MaxQty", Function(c) c.Int(nullable := False))
            AddColumn("dbo.LMMenuL2ModifierGroup", "FreeQty", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMMenuL2ModifierGroup", "FreeQty")
            DropColumn("dbo.LMMenuL2ModifierGroup", "MaxQty")
            DropColumn("dbo.LMMenuL2ModifierGroup", "MinQty")
        End Sub
    End Class
End Namespace
