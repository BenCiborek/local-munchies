Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class SignUp
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.SignUps",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Email = c.String(),
                        .SignUpDate = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.SignUps")
        End Sub
    End Class
End Namespace
