Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ControlType4
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.LMMenuModifierGroups", "ControlTypeID", "dbo.ControlTypeModels")
            DropIndex("dbo.LMMenuModifierGroups", New String() {"ControlTypeID"})
            AddColumn("dbo.LMMenuL2ModifierGroup", "ControlTypeID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddColumn("dbo.LMMenuL2ModifierGroup", "SubQtyMax", Function(c) c.Int(nullable:=False))
            AddForeignKey("dbo.LMMenuL2ModifierGroup", "ControlTypeID", "dbo.ControlTypeModels", "ID", cascadeDelete:=True)
            CreateIndex("dbo.LMMenuL2ModifierGroup", "ControlTypeID")
            DropColumn("dbo.LMMenuModifierGroups", "ControlTypeID")
            DropColumn("dbo.LMMenuModifierGroups", "SubQtyMax")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.LMMenuModifierGroups", "SubQtyMax", Function(c) c.Int(nullable := False))
            AddColumn("dbo.LMMenuModifierGroups", "ControlTypeID", Function(c) c.Int(nullable := False))
            DropIndex("dbo.LMMenuL2ModifierGroup", New String() { "ControlTypeID" })
            DropForeignKey("dbo.LMMenuL2ModifierGroup", "ControlTypeID", "dbo.ControlTypeModels")
            DropColumn("dbo.LMMenuL2ModifierGroup", "SubQtyMax")
            DropColumn("dbo.LMMenuL2ModifierGroup", "ControlTypeID")
            CreateIndex("dbo.LMMenuModifierGroups", "ControlTypeID")
            AddForeignKey("dbo.LMMenuModifierGroups", "ControlTypeID", "dbo.ControlTypeModels", "ID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
