Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket2242014
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Tickets", "BilltoAddress", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Tickets", "DeliveryAddress", Function(c) c.Int(nullable := False))
            AlterColumn("dbo.Users", "Email", Function(c) c.String(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Users", "Email", Function(c) c.String())
            DropColumn("dbo.Tickets", "DeliveryAddress")
            DropColumn("dbo.Tickets", "BilltoAddress")
        End Sub
    End Class
End Namespace
