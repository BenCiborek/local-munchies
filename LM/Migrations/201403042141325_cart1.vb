Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class cart1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.TicketItemModifierGroups", "TotalPrice", Function(c) c.Double(nullable := False))
            AddColumn("dbo.TicketItemModifierGroups", "ExtraPrice", Function(c) c.Double(nullable := False))
            DropColumn("dbo.TicketItemModifiers", "TotalPrice")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.TicketItemModifiers", "TotalPrice", Function(c) c.Double(nullable := False))
            DropColumn("dbo.TicketItemModifierGroups", "ExtraPrice")
            DropColumn("dbo.TicketItemModifierGroups", "TotalPrice")
        End Sub
    End Class
End Namespace
