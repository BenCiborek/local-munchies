Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ticket76
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.Addresses", "State", Function(c) c.String(nullable := False, maxLength := 2))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Addresses", "State", Function(c) c.String())
        End Sub
    End Class
End Namespace
