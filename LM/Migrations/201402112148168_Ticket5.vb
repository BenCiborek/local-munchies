Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Ticket5
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Tickets", "TicketTypeID", "dbo.TicketTypes")
            DropIndex("dbo.Tickets", New String() { "TicketTypeID" })
            DropTable("dbo.TicketTypes")
        End Sub
        
        Public Overrides Sub Down()
            CreateTable(
                "dbo.TicketTypes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateIndex("dbo.Tickets", "TicketTypeID")
            AddForeignKey("dbo.Tickets", "TicketTypeID", "dbo.TicketTypes", "ID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
