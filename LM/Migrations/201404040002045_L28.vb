Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class L28
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.LMMenuModifierGroups", "LMMenuModifier_ID", "dbo.LMMenuModifiers")
            DropForeignKey("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID", "dbo.LMMenuModifierGroups")
            DropIndex("dbo.LMMenuModifierGroups", New String() { "LMMenuModifier_ID" })
            DropIndex("dbo.LMMenuModifiers", New String() { "LMMenuModifierGroup_ID" })
            DropColumn("dbo.LMMenuModifierGroups", "LMMenuModifier_ID")
            DropColumn("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID", Function(c) c.Int())
            AddColumn("dbo.LMMenuModifierGroups", "LMMenuModifier_ID", Function(c) c.Int())
            CreateIndex("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID")
            CreateIndex("dbo.LMMenuModifierGroups", "LMMenuModifier_ID")
            AddForeignKey("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID", "dbo.LMMenuModifierGroups", "ID")
            AddForeignKey("dbo.LMMenuModifierGroups", "LMMenuModifier_ID", "dbo.LMMenuModifiers", "ID")
        End Sub
    End Class
End Namespace
