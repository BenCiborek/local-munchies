Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class lmdenabled
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.RestaurantInfoes", "hasLMDAccount", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.RestaurantInfoes", "hasLMDAccount")
        End Sub
    End Class
End Namespace
