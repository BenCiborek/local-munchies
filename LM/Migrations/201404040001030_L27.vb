Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class L27
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuModifierGroups", "LMMenuModifier_ID", Function(c) c.Int())
            AddColumn("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID", Function(c) c.Int())
            AddForeignKey("dbo.LMMenuModifierGroups", "LMMenuModifier_ID", "dbo.LMMenuModifiers", "ID")
            AddForeignKey("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID", "dbo.LMMenuModifierGroups", "ID")
            CreateIndex("dbo.LMMenuModifierGroups", "LMMenuModifier_ID")
            CreateIndex("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.LMMenuModifiers", New String() { "LMMenuModifierGroup_ID" })
            DropIndex("dbo.LMMenuModifierGroups", New String() { "LMMenuModifier_ID" })
            DropForeignKey("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID", "dbo.LMMenuModifierGroups")
            DropForeignKey("dbo.LMMenuModifierGroups", "LMMenuModifier_ID", "dbo.LMMenuModifiers")
            DropColumn("dbo.LMMenuModifiers", "LMMenuModifierGroup_ID")
            DropColumn("dbo.LMMenuModifierGroups", "LMMenuModifier_ID")
        End Sub
    End Class
End Namespace
