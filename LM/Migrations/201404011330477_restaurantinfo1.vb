Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class restaurantinfo1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.FaxSents",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ToNumber = c.String(),
                        .FaxPage = c.String(),
                        .DateSent = c.DateTime(nullable := False),
                        .Status = c.Int(nullable := False),
                        .Ticketid = c.Int(nullable := False),
                        .RestID = c.Int(nullable := False),
                        .DateConfirmed = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.RestaurantInfoes",
                Function(c) New With
                    {
                        .id = c.Int(nullable := False, identity := True),
                        .RestaurantID = c.Int(nullable := False),
                        .OrderFaxNumber = c.String(),
                        .OrderPhoneNumber = c.String()
                    }) _
                .PrimaryKey(Function(t) t.id) _
                .ForeignKey("dbo.LMRestaurants", Function(t) t.RestaurantID, cascadeDelete := True) _
                .Index(Function(t) t.RestaurantID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.RestaurantInfoes", New String() { "RestaurantID" })
            DropForeignKey("dbo.RestaurantInfoes", "RestaurantID", "dbo.LMRestaurants")
            DropTable("dbo.RestaurantInfoes")
            DropTable("dbo.FaxSents")
        End Sub
    End Class
End Namespace
