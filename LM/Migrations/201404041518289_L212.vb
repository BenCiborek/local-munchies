Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class L212
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.TicketItemL2Modifier",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .TicketItemID = c.Int(nullable := False),
                        .MenuItemModifierID = c.Int(nullable := False),
                        .Name = c.String(),
                        .ModifierPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False),
                        .ModifierType = c.Int(nullable := False),
                        .DisplayOrder = c.Int(nullable := False),
                        .TicketItemL2ModifierGroupID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItemL2ModifierGroup", Function(t) t.TicketItemL2ModifierGroupID, cascadeDelete := True) _
                .Index(Function(t) t.TicketItemL2ModifierGroupID)
            
            CreateTable(
                "dbo.TicketItemL2ModifierGroup",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .ModifiedDate = c.DateTime(nullable := False),
                        .Name = c.String(),
                        .MinQty = c.Int(nullable := False),
                        .MaxQty = c.Int(nullable := False),
                        .FreeQty = c.Int(nullable := False),
                        .L1ModifierID = c.Int(nullable := False),
                        .MenuItemL2ModifierGroupID = c.Int(nullable := False),
                        .TotalPrice = c.Double(nullable := False),
                        .ExtraPrice = c.Double(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.TicketItemModifiers", Function(t) t.L1ModifierID, cascadeDelete := True) _
                .Index(Function(t) t.L1ModifierID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.TicketItemL2ModifierGroup", New String() { "L1ModifierID" })
            DropIndex("dbo.TicketItemL2Modifier", New String() { "TicketItemL2ModifierGroupID" })
            DropForeignKey("dbo.TicketItemL2ModifierGroup", "L1ModifierID", "dbo.TicketItemModifiers")
            DropForeignKey("dbo.TicketItemL2Modifier", "TicketItemL2ModifierGroupID", "dbo.TicketItemL2ModifierGroup")
            DropTable("dbo.TicketItemL2ModifierGroup")
            DropTable("dbo.TicketItemL2Modifier")
        End Sub
    End Class
End Namespace
