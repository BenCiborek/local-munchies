Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class selected1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LMMenuL2Modifier", "Selected", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LMMenuL2Modifier", "Selected")
        End Sub
    End Class
End Namespace
