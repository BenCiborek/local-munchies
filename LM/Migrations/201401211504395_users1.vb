Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class users1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Users",
                Function(c) New With
                    {
                        .UserID = c.Int(nullable := False, identity := True),
                        .UserName = c.String(),
                        .password = c.String(),
                        .Roles = c.String()
                    }) _
                .PrimaryKey(Function(t) t.UserID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Users")
        End Sub
    End Class
End Namespace
