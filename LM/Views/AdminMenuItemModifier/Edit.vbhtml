﻿@ModelType LM.LMMenuItemModifier

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuItemModifier</legend>

        @Html.HiddenFor(Function(model) model.ID)

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MinQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MinQty)
            @Html.ValidationMessageFor(Function(model) model.MinQty)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MaxQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MaxQty)
            @Html.ValidationMessageFor(Function(model) model.MaxQty)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.FreeQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.FreeQty)
            @Html.ValidationMessageFor(Function(model) model.FreeQty)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MenuModifierGroupID, "MenuModifierGroup")
        </div>
        <div class="editor-field">
            @Html.DropDownList("MenuModifierGroupID", String.Empty)
            @Html.ValidationMessageFor(Function(model) model.MenuModifierGroupID)
        </div>
        @Html.HiddenFor(Function(model) model.MenuItemID)
        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.itemid = Model.MenuItemID})
</div>
