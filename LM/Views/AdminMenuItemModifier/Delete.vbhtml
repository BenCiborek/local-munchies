﻿@ModelType LM.LMMenuItemModifier

@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>LMMenuItemModifier</legend>

    <div class="display-label">MinQty</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MinQty)
    </div>

    <div class="display-label">MaxQty</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MaxQty)
    </div>

    <div class="display-label">FreeQty</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.FreeQty)
    </div>

    <div class="display-label">MenuModifierGroup</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuModifierGroup.Name)
    </div>

    <div class="display-label">MenuItem</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuItem.Name)
    </div>
</fieldset>
@Using Html.BeginForm()
    @<p>
        <input type="submit" value="Delete" /> |
        @Html.ActionLink("Back to List", "Index", New With {.itemid = Model.MenuItemID})
    </p>
End Using
