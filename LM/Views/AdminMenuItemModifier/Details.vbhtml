﻿@ModelType LM.LMMenuItemModifier

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>LMMenuItemModifier</legend>

    <div class="display-label">MinQty</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MinQty)
    </div>

    <div class="display-label">MaxQty</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MaxQty)
    </div>

    <div class="display-label">FreeQty</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.FreeQty)
    </div>

    <div class="display-label">MenuModifierGroup</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuModifierGroup.Name)
    </div>

    <div class="display-label">MenuItem</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuItem.Name)
    </div>
</fieldset>
<p>

    @Html.ActionLink("Edit", "Edit", New With {.id = Model.ID}) |
    @Html.ActionLink("Back to List", "Index", New With {.itemid = Model.MenuItemID})
</p>
