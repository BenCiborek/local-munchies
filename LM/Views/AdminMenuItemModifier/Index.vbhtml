﻿@ModelType IEnumerable(Of LM.LMMenuItemModifier)

@Code
    ViewData("Title") = "Menu Item Modifiers"
    Dim aryurl As Array = Split(ViewBag.BreadCrumbURLS, ",")
    Dim aryName As Array = Split(ViewBag.BreadCrumbNames, ",")
    Dim x = 0
End Code

<h2>Menu Item Modifier</h2>

<ol class="breadcrumb">
    @While x < aryurl.Length
        @<li><a href="@aryurl(x)">@aryName(x)</a></li>
        x += 1
    End While
</ol>

<p>
    @Html.ActionLink("Create New", "Create", New With {.itemid = ViewBag.MenuItemID})
</p>
<table class="table">
    <tr>
        <th>
            MinQty
        </th>
        <th>
            MaxQty
        </th>
        <th>
            FreeQty
        </th>
        <th>
            MenuModifierGroup
        </th>
        <th>
            MenuItem
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MinQty)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MaxQty)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.FreeQty)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MenuModifierGroup.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MenuItem.Name)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
