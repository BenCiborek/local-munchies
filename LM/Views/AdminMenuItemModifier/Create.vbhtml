﻿@ModelType LM.LMMenuItemModifier

@Code
    ViewData("Title") = "Create"
End Code

<h2>Create</h2>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuItemModifier</legend>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MinQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MinQty)
            @Html.ValidationMessageFor(Function(model) model.MinQty)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MaxQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MaxQty)
            @Html.ValidationMessageFor(Function(model) model.MaxQty)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.FreeQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.FreeQty)
            @Html.ValidationMessageFor(Function(model) model.FreeQty)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MenuModifierGroupID, "MenuModifierGroup")
        </div>
        <div class="editor-field">
            @Html.DropDownList("MenuModifierGroupID", String.Empty)
            @Html.ValidationMessageFor(Function(model) model.MenuModifierGroupID)
        </div>
                <input id="MenuItemID" name="MenuItemID" type="hidden" value="@ViewBag.MenuItemID" />
        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.itemid = ViewBag.MenuItemID})
</div>
