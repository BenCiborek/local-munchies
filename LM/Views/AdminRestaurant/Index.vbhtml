﻿@ModelType IEnumerable(Of LM.LMRestaurant)

@Code
    ViewData("Title") = "Index"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th></th>
        <th>
            Name
        </th>
        <th>
            Address
        </th>
        <th>
            Address2
        </th>
        <th>
            City
        </th>
        <th>
            State
        </th>
        <th>
            zip
        </th>
        <th>
            Phone
        </th>
        <th>
            Active
        </th>
        <th>
            Lat
        </th>
        <th>
            Lon
        </th>
        <th>
            SunOpen
        </th>
        <th>
            SunClose
        </th>
        <th>
            MonOpen
        </th>
        <th>
            MonClose
        </th>
        <th>
            TueOpen
        </th>
        <th>
            TueClose
        </th>
        <th>
            WedOpen
        </th>
        <th>
            WedClose
        </th>
        <th>
            ThuOpen
        </th>
        <th>
            ThuClose
        </th>
        <th>
            FriOpen
        </th>
        <th>
            FriClose
        </th>
        <th>
            SatOpen
        </th>
        <th>
            SatClose
        </th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) <br />
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) <br />
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Address)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Address2)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.City)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.State)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.zip)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Phone)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Active)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Lat)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Lon)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.SunOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.SunClose)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MonOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MonClose)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.TueOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.TueClose)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.WedOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.WedClose)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.ThuOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.ThuClose)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.FriOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.FriClose)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.SatOpen)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.SatClose)
        </td>
        
    </tr>
Next

</table>
