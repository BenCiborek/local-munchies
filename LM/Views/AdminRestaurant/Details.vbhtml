﻿@ModelType LM.LMRestaurant

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>LMRestaurant</legend>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Address</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Address)
    </div>

    <div class="display-label">Address2</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Address2)
    </div>

    <div class="display-label">City</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.City)
    </div>

    <div class="display-label">State</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.State)
    </div>

    <div class="display-label">zip</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.zip)
    </div>

    <div class="display-label">Phone</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Phone)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">Lat</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Lat)
    </div>

    <div class="display-label">Lon</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Lon)
    </div>

    <div class="display-label">SunOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.SunOpen)
    </div>

    <div class="display-label">SunClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.SunClose)
    </div>

    <div class="display-label">MonOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MonOpen)
    </div>

    <div class="display-label">MonClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MonClose)
    </div>

    <div class="display-label">TueOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.TueOpen)
    </div>

    <div class="display-label">TueClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.TueClose)
    </div>

    <div class="display-label">WedOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.WedOpen)
    </div>

    <div class="display-label">WedClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.WedClose)
    </div>

    <div class="display-label">ThuOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.ThuOpen)
    </div>

    <div class="display-label">ThuClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.ThuClose)
    </div>

    <div class="display-label">FriOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.FriOpen)
    </div>

    <div class="display-label">FriClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.FriClose)
    </div>

    <div class="display-label">SatOpen</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.SatOpen)
    </div>

    <div class="display-label">SatClose</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.SatClose)
    </div>
</fieldset>
<p>

    @Html.ActionLink("Edit", "Edit", New With {.id = Model.ID}) |
    @Html.ActionLink("Back to List", "Index")
</p>
