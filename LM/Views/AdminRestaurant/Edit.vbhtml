﻿@ModelType LM.LMRestaurant

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMRestaurant</legend>

        @Html.HiddenFor(Function(model) model.ID)

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Address)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Address)
            @Html.ValidationMessageFor(Function(model) model.Address)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Address2)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Address2)
            @Html.ValidationMessageFor(Function(model) model.Address2)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.City)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.City)
            @Html.ValidationMessageFor(Function(model) model.City)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.State)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.State)
            @Html.ValidationMessageFor(Function(model) model.State)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.zip)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.zip)
            @Html.ValidationMessageFor(Function(model) model.zip)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Phone)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Phone)
            @Html.ValidationMessageFor(Function(model) model.Phone)
        </div>

                <div class="editor-label">
            @Html.LabelFor(Function(model) model.WebAddress)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.WebAddress)
            @Html.ValidationMessageFor(Function(model) model.WebAddress)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Lat)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Lat)
            @Html.ValidationMessageFor(Function(model) model.Lat)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Lon)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Lon)
            @Html.ValidationMessageFor(Function(model) model.Lon)
        </div>

                <div class="editor-label">
            @Html.LabelFor(Function(model) model.URLName)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.URLName)
            @Html.ValidationMessageFor(Function(model) model.URLName)
        </div>
        <div class="editor-label">
            <label>Location</label>
        </div>
        <div class="editor-field">
            @Html.Action("locationdrop", "LMRestaurant", New With {.selected = Model.LocationID})
        </div>
                <div class="editor-label">
            @Html.LabelFor(Function(model) model.OnlineOrdering)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.OnlineOrdering)
            @Html.ValidationMessageFor(Function(model) model.OnlineOrdering)
        </div>

                        <div class="editor-label">
            @Html.LabelFor(Function(model) model.LocalRank)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.LocalRank)
            @Html.ValidationMessageFor(Function(model) model.LocalRank)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.SunOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.SunOpen)
            @Html.ValidationMessageFor(Function(model) model.SunOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.SunClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.SunClose)
            @Html.ValidationMessageFor(Function(model) model.SunClose)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MonOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MonOpen)
            @Html.ValidationMessageFor(Function(model) model.MonOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MonClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MonClose)
            @Html.ValidationMessageFor(Function(model) model.MonClose)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.TueOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.TueOpen)
            @Html.ValidationMessageFor(Function(model) model.TueOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.TueClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.TueClose)
            @Html.ValidationMessageFor(Function(model) model.TueClose)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.WedOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.WedOpen)
            @Html.ValidationMessageFor(Function(model) model.WedOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.WedClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.WedClose)
            @Html.ValidationMessageFor(Function(model) model.WedClose)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.ThuOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.ThuOpen)
            @Html.ValidationMessageFor(Function(model) model.ThuOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.ThuClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.ThuClose)
            @Html.ValidationMessageFor(Function(model) model.ThuClose)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.FriOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.FriOpen)
            @Html.ValidationMessageFor(Function(model) model.FriOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.FriClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.FriClose)
            @Html.ValidationMessageFor(Function(model) model.FriClose)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.SatOpen)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.SatOpen)
            @Html.ValidationMessageFor(Function(model) model.SatOpen)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.SatClose)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.SatClose)
            @Html.ValidationMessageFor(Function(model) model.SatClose)
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index")
</div>
