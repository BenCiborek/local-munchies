﻿@Code
    ViewData("Title") = " | FAQ"
End Code

<h1>FAQ<small> Frequently Asked Questions</small></h1>

<h3>Who is Local Munchies?</h3>
<p>Local Munchies uses a hyper-local approach to deliver a unique restaurant experience. The foundation of Local Munchies is a complete and comprehensive restaurant guide containing every restaurant in your city and their menus. In addition to this strong foundation, we have partnered up with these restaurants to offer you the option to place orders directly to these restaurants.</p>
<h3>Where can I use Local Munchies?</h3>
<p>Local Munchies is currently serving Kent, Ohio. We have large plans that include other college campuses and cities. Check out the homepage drop down box to see if we are in your city,</p>
<h3>How Does Local Munchies Work?</h3>
        <p><strong>STEP 1</strong>:Select your city on the homepage<br />
        Select your city on the drop down to view all the restaurants in your city.<br />
        <strong>STEP 2</strong>: Select a Restaurant<br />
        Click on any restaurant name to view their menu.<br />
        <strong>STEP 3</strong>: Order from the Menu<br />
        To add an item to your order, click on that item. You can customize that item by choosing quantity, additional options and special instructions. When you are done customizing the item click the "Add Item" button. Once all items are added to your basket, you will be brought back to the restaurant’s menu. You can either select more items and add them to your order or click your cart to proceed to checkout at the top of the page.<br />
        <strong>STEP 4</strong>: Checkout with Credit Card<br />
        <strong>New Users</strong><br />
        If this is your first time ordering, the Checkout page is where you can set up your Local Munchies account. You will be asked to enter your delivery information, select a password and provide billing information. Then simply click the "Submit Order" button to send your order to the restaurant.<br />
        <strong>Returning Members</strong><br />
        Simply confirm your delivery and billing information and click on the "Submit Order" button.</p>

<h3>What happens after I submit my order?</h3>
<p>Your order is sent electronically to the restaurant the instant you click "Submit Order", via email or fax. The restaurant will then receive an automated call to inform them that they have received a new order, and will be prompted to enter a given two-digit number to confirm that they received the order. Finally, you will then receive an email confirming that the restaurant has your order.</p>
<h3>How do I know that the restaurant received my order?</h3>
<p>Once you click on "Submit Order", you will see the Local Munchies Thank You page. From here we submit the order electronically to the restaurant and call them to confirm that the order has been received. Once confirmed you will receive a confirmation email letting you know that the order has been successfully received.</p>
<h3>What do I do if I did not receive an email order confirmation?</h3>
<p>1.     Confirm that you are receiving outside email.<br />
2.     Check any spam folders on your email.<br />
3.     Contact us to confirm your email address is correct in our system by emailing support@localmunchies.com</p>
<h3>How do I change or cancel my order after I have placed it?</h3>
<p>Prior to “submitting order” you are asked to review your order and confirm its accuracy. By clicking the checkbox you indicate that you are satisfied with your order, making all orders final.</p>
<h3>When will my food arrive?</h3>
<p>On average, delivery orders take 45 to 75 minutes to arrive.<br />
Please be aware that delivery time may be longer during poor weather conditions or special events in your area.<br />
You can always contact the restaurant directly to inquire about the status of your order. Restaurant contact information can be found in the order confirmation email.</p>
<h3>When can I pick up my food?</h3>
<p>Please allow 15 to 20 minutes for your order to be prepared after receiving a confirmation email.<br />
You can always contact the restaurant directly to inquire about the status of your order. Restaurant contact information can be found in the order confirmation email.</p>
<h3>Who should I call to check the status of my order?</h3>
<p>Please contact the restaurant to find out the status of your order.  The restaurant's phone number is provided with your email confirmation and on the restaurant's profile page at www.localmunchies.com. You can also contact one of Local Munchies sales representatives by responding to your confirmation email.</p>
<h3>What if I have a confirmation email from the restaurant but they have no record of my order?</h3>
<p>If you have an email confirmation, then the restaurant did indeed receive your order. The restaurant typed a two-digit confirmation number over our automated phone call generates your email confirmation.</p>
<h3>What do I do if my food arrived and there is something wrong with my order?</h3>
<p>If you received a delivery and there appears to be a problem with your order, please contact the restaurant directly for an immediate resolution. The restaurant's phone number is provided with your email confirmation and on the restaurant's profile at page on www.localmunchies.com. You can also contact one of Local Munchies sales representatives by responding to your confirmation email.<br />
If you do not wish to have the restaurant fix the problem, then please reply to your order email confirmation and a Local Munchies representative will make any necessary adjustments to your order.</p>
<h3>Is there a minimum to order?</h3>
<p>For many restaurants, there is a minimum to order for delivery. That way, the restaurants can make sure that the delivery they are bringing to you are worth their effort. The restaurant sets these delivery minimums.<br />
For a take out order there is no order minimums. Order away.</p>
<h3>Can I save my credit card information?</h3>
<p>When you reach Step 4 on your first order, you will be asked to enter your credit card information. To save your credit card information that you entered, you must check the box that is labeled "Save Credit Card." Then, when you place the order, that credit card information will be saved as the default credit card for future orders placed under your account.<br />
You can also go to "My Account" located on the top of the screen and enter your credit card information there. If you already have information but would rather use a different card, click "Delete Card" then enter the new information and click "Save Changes."</p>
<h3>How do I change my credit card information?</h3>
<p>To change your credit card info, you need to go into “My Account” and click the delete credit card link, then enter the new information.</p>
<h3>When is my credit card processed?</h3>
<p>Local Munchies processes your credit card when you place your order.</p>
<h3>Is my credit card information secure?</h3>
<p>You can safely enter your credit card number via our secure server, which encrypts all submitted information and transmits and stores such information in an encrypted state. Your credit card information is processed by Local Munchies and never shared with any restaurants.</p>
<h3>I forgot my username/password, what do I do?</h3>
<p>Your username is generally the email address that you first registered with.<br />
To reset your password, click on the "Forgot Password" link. You will need to enter your registered email address and then you will receive an email from Local Munchies that allows you to reset your password.<br />
To receive a username reminder, click on the "Forgot Username" link. You will need to enter your registered email address and then you will receive an email from Local Munchies with any username(s) associated with your email address.</p>
<h3>How does a pickup order work?</h3>
<p>The ordering process is the same as a delivery order - Choose from the restaurants available for pickup, add items to your order from the menu (no need to worry about delivery minimums!), checkout and pay with your credit card, then go get your food.<br />
<strong>Important</strong>: You will need to bring your ID when picking up your order.</p>
<h3>How do I get my food?</h3>
<p>Pickup orders will not be delivered - you have to pick up your food in person at the restaurant. Just arrive at the time listed in your confirmation email with your ID, sign the slip, and the food is yours. If you'd like to confirm that your food is ready before heading out, please feel free to call the restaurant.</p>
<h3>What browsers does Local Munchies support?</h3>
<p>Internet Explorer 7, 8 and 9 for Windows<br />
Firefox<br />
Safari<br />
Chrome<br />
If you are having trouble navigating the website or have any other questions, please email customer care at support@localmunchies.com</p>

