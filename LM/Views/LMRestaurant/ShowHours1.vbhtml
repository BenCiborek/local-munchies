﻿@modeltype LM.LMRestaurant
@Code
    Layout = Nothing
    Dim dt As DateTime = ViewBag.now
    Dim classSun = ""
    Dim classmon = ""
    Dim classtue = ""
    Dim classwed = ""
    Dim classthu = ""
    Dim classfri = ""
    Dim classSat = ""
    
    Select Case dt.DayOfWeek
        Case DayOfWeek.Sunday
            If (dt.TimeOfDay > Model.SunOpen.TimeOfDay And dt.TimeOfDay < Model.SunClose.TimeOfDay) Or (Model.SunClose.TimeOfDay < Model.SunOpen.TimeOfDay And (dt.TimeOfDay < Model.SunClose.TimeOfDay Or dt.TimeOfDay > Model.SunOpen.TimeOfDay)) Then
                classSun = "success"
            Else
                classSun = "danger"
            End If
        Case DayOfWeek.Monday
            If dt.TimeOfDay > Model.MonOpen.TimeOfDay And dt.TimeOfDay < Model.MonClose.TimeOfDay Or (Model.MonClose.TimeOfDay < Model.MonOpen.TimeOfDay And (dt.TimeOfDay < Model.MonClose.TimeOfDay Or dt.TimeOfDay > Model.MonOpen.TimeOfDay)) Then
                classmon = "success"
            Else
                classmon = "danger"
            End If
        Case DayOfWeek.Tuesday
            If dt.TimeOfDay > Model.TueOpen.TimeOfDay And dt.TimeOfDay < Model.TueClose.TimeOfDay Or (Model.TueClose.TimeOfDay < Model.TueOpen.TimeOfDay And (dt.TimeOfDay < Model.TueClose.TimeOfDay Or dt.TimeOfDay > Model.TueOpen.TimeOfDay)) Then
                classtue = "success"
            Else
                classtue = "danger"
            End If
        Case DayOfWeek.Wednesday
            If dt.TimeOfDay > Model.WedOpen.TimeOfDay And dt.TimeOfDay < Model.WedClose.TimeOfDay Or (Model.WedClose.TimeOfDay < Model.WedOpen.TimeOfDay And (dt.TimeOfDay < Model.WedClose.TimeOfDay Or dt.TimeOfDay > Model.WedOpen.TimeOfDay)) Then
                classwed = "success"
            Else
                classwed = "danger"
            End If
        Case DayOfWeek.Thursday
            If dt.TimeOfDay > Model.ThuOpen.TimeOfDay And dt.TimeOfDay < Model.ThuClose.TimeOfDay Or (Model.ThuClose.TimeOfDay < Model.ThuOpen.TimeOfDay And (dt.TimeOfDay < Model.ThuClose.TimeOfDay Or dt.TimeOfDay > Model.ThuOpen.TimeOfDay)) Then
                classthu = "success"
            Else
                classthu = "danger"
            End If
        Case DayOfWeek.Friday
            If dt.TimeOfDay > Model.FriOpen.TimeOfDay And dt.TimeOfDay < Model.FriClose.TimeOfDay Or (Model.FriClose.TimeOfDay < Model.FriOpen.TimeOfDay And (dt.TimeOfDay < Model.FriClose.TimeOfDay Or dt.TimeOfDay > Model.FriOpen.TimeOfDay)) Then
                classfri = "success"
            Else
                classfri = "danger"
            End If
        Case DayOfWeek.Saturday
            If dt.TimeOfDay > Model.SatOpen.TimeOfDay And dt.TimeOfDay < Model.SatClose.TimeOfDay Or (Model.SatClose.TimeOfDay < Model.SatOpen.TimeOfDay And (dt.TimeOfDay < Model.SatClose.TimeOfDay Or dt.TimeOfDay > Model.SatOpen.TimeOfDay)) Then
                classSat = "success"
            Else
                classSat = "danger"
            End If
    End Select
End Code

<div class="showhoursall">
    <span class="text-@classSun">S: @IIf(Model.SunOpen > CDate("1/1/1900") And Model.SunClose > CDate("1/1/1900"), Model.SunOpen.ToString("h:mm tt") & " - " & Model.SunClose.ToString("h:mm tt"), "Closed")</span><br />
    <span class="text-@classmon">M: @IIf(Model.MonOpen > CDate("1/1/1900") And Model.MonClose > CDate("1/1/1900"), Model.MonOpen.ToString("h:mm tt") & " - " & Model.MonClose.ToString("h:mm tt"), "Closed")</span><br />
    <span class="text-@classtue">T: @IIf(Model.TueOpen > CDate("1/1/1900") And Model.TueClose > CDate("1/1/1900"), Model.TueOpen.ToString("h:mm tt") & " - " & Model.TueClose.ToString("h:mm tt"), "Closed")</span><br />
    <span class="text-@classwed">W: @IIf(Model.WedOpen > CDate("1/1/1900") And Model.WedClose > CDate("1/1/1900"), Model.WedOpen.ToString("h:mm tt") & " - " & Model.WedClose.ToString("h:mm tt"), "Closed")</span><br />
    <span class="text-@classthu">T: @IIf(Model.ThuOpen > CDate("1/1/1900") And Model.ThuClose > CDate("1/1/1900"), Model.ThuOpen.ToString("h:mm tt") & " - " & Model.ThuClose.ToString("h:mm tt"), "Closed")</span><br />
    <span class="text-@classfri">F: @IIf(Model.FriOpen > CDate("1/1/1900") And Model.FriClose > CDate("1/1/1900"), Model.FriOpen.ToString("h:mm tt") & " - " & Model.FriClose.ToString("h:mm tt"), "Closed")</span><br />
    <span class="text-@classSat">S: @IIf(Model.SatOpen > CDate("1/1/1900") And Model.SatClose > CDate("1/1/1900"), Model.SatOpen.ToString("h:mm tt") & " - " & Model.SatClose.ToString("h:mm tt"), "Closed")</span>
</div>

