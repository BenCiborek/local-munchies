﻿@modeltype LM.LMRestaurant
@Code
    Layout = Nothing
    Dim cssClass = ""
End Code
@For Each item In Model.OrderOptions.Where(Function(x) x.id = 5 Or x.id = 1 Or x.id = 2)
    Select Case item.id
        Case 1
            cssClass = "label-info"
        Case 2
            cssClass = "label-primary"
        Case 5
            cssClass = "label-warning"
        Case Else
            cssClass = ""
    End Select
    @<span class="label @cssClass">@item.Name</span> 
Next