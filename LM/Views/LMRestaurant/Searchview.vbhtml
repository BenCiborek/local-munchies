﻿@modeltype LM.DetailSearchResultVM
@Code
    Layout = Nothing
    Dim ImagePath As String
    ImagePath = "/images/RestaurantLogos/" & Model.Restaurant.ID & ".jpg"
End Code

<div class="@IIf(Model.Restaurant.OnlineOrdering, "onlineordering", "") @IIf(Model.DeliversToAddress =2, "deliverstome","")">
    <div class="row">
        <div class="col-md-2 col-sm-0 col-xs-0" style="height:100%;vertical-align:middle; text-align:center;">
            @If System.IO.File.Exists(Server.MapPath(ImagePath)) Then
                
               @<img src="@ImagePath" style="padding:5px;" data-src="holder.js/100x100"  class="img-responsive img-rounded" alt="" />
            End If
                   
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <h3 class="restSearchName"> <a href="@Url.RouteUrl("Restaurant", New With {.id = Model.Restaurant.ID, .name = Model.Restaurant.URLName})">@Model.Restaurant.Name</a></h3>
            <div> @Html.Action("ShowHours2", "LMRestaurant", New With {.rest = Model.Restaurant})</div>
             <a href="@Url.RouteUrl("Restaurant", New With {.id = Model.Restaurant.ID, .name = Model.Restaurant.URLName})"><address class="LMSearchAddress">
                @Model.Restaurant.Address<br />
                @If Model.Restaurant.Address2 <> "" Then
                    Model.Restaurant.Address2.ToString()@<br />
                End If
                @Model.Restaurant.City, @Model.Restaurant.State @Model.Restaurant.zip
            </address></a>
            </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            @If Model.Restaurant.LocalRank <> 99 Then
                @<p class="restSearchName">

                    @If Model.Restaurant.OnlineOrdering Then
                        @<a class="btn-primary btn-sm btn" href="@Url.RouteUrl("Restaurant", New With {.id = Model.Restaurant.ID, .name = Model.Restaurant.URLName})">Order Today Online! <span class="glyphicon glyphicon-cutlery"></span> > </a>
                    End If
                </p>
            End If

            <p class="restSearchName">
                <small>
                    @Html.Action("ShowCats", "LMCategories", New With {.Rest = Model.Restaurant})
                </small>
            </p>
            <p class="restSearchName">
                <small>
                    @Html.Action("ShowOrderingOptions", "LMRestaurant", New With {.Rest = Model.Restaurant})
                </small>
            </p>
             @If Model.Restaurant.LocalRank <> 99 Then
                @<h5 class="text-warning">
                    #@Model.Restaurant.LocalRank MunchRank!
                 </h5>
            End If
 
           
        </div>
        </div>
            @*<div id="oovote-@Model.ID" style="text-align:center;">
                <a class="oovote" data-restid="@Model.ID" href="#!"><small>Click if you would like Online Ordering for @Model.Name.
                </small>
                </a>
            </div>*@



       

</div>
