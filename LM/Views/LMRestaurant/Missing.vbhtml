﻿@Code
    ViewData("Title") = "Missing"
End Code

<h2>The Restaurant you are looking for has been lost. Please try one of the locations below.</h2>
<div>
    <div id="filters">
            @Html.Action("Filtersbar","LMSearch")
        </div>
     <div id="searchMain" class="searchmain">

           @* @Html.Action("SearchMain", "LMSearch", New With {.find = Model.SearchCriteria.Find, .cat = Model.SearchCriteria.Categorie, .OrderOption = Model.SearchCriteria.OrderOption, .LocationID = Model.LocationID})*@
        </div>
</div>
<script>
    $(function () {
        updateSearch(0, 0, '', 'False','')
    })

    function updateSearch(selCat, selOpt, find, skipsession,address) {
        //$(".searchbar-cats").removeClass("in").addClass("collapse");
        //alert("cat:" + selCat + " Opt:" + selOpt + " Find:" + find)
        if (selCat == undefined) {
            selCat = 0
        }
        if (selOpt == undefined) {
            selOpt = 0
        }


        $.ajax({
            type: 'POST',
            url: '/LMSearch/SearchMain?find=' + find + "&cat=" + selCat + "&OrderOption=" + selOpt + "&LocationID=Kent-OH&skipSession=" + skipsession + "&address=" + address,
            //data: {
            //    strstatus: values
            //},
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                $('#searchMain').addClass('ajaxRefreshing');
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                $('#searchMain').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#searchMain').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#searchMain').removeClass('ajaxRefreshing');
                $('#searchMain').html('<div>Error: ' + thrownError + '</div>')
            },
            complete: function () {
                $('#searchMain').removeClass('ajaxRefreshing');
            }
        });

    }

</script>
