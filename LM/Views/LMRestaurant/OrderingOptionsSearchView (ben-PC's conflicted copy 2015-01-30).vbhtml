﻿@modeltype IEnumerable(Of LM.LMOrderOption)
@Code
    Layout = Nothing
     Dim restids As List(Of Integer) = ViewBag.restids
End Code

        <div class="row">
            @For Each item In Model.ToList.OrderByDescending(Function(x) x.Restaurants.Where(Function(y) restids.Contains(y.ID)).Count)
                @<div class="col-md-2 col-sm-4 col-xs-6"  style="margin-bottom:5px;">
                    <a class="btn  btn-default btn-block @IIf(ViewBag.selectedtype = item.id, "active", "") optitem" id="@item.id" >
                        @item.name
                        @*<small style="color:@IIf(ViewBag.selectedtype = item.id, "#eeeeee", "#000")">(@item.Restaurants.Where(Function(f) restids.Contains(f.ID)).Count())</small>*@
                    </a>
                 </div>
            Next
        </div>

@*        <ul class="rightnav nav nav-pills nav-stacked">
            @For Each item In Model.ToList.OrderByDescending(Function(x) x.Restaurants.Where(Function(y) restids.Contains(y.ID)).Count)
                @<li class="@IIf(ViewBag.selectedtype = item.id, "active", "")"><a><span class="optitem filteritem" id="@item.id">@item.name </span><small @IIf(ViewBag.selectedtype = item.id, "", "hidden") class="pull-right remfilter remopt white">&times;</small></a></li>
            Next
        </ul>*@



