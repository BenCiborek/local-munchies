﻿@modeltype LM.LocationsVM
@Code
    Layout = Nothing
End Code

<select id="locationid" name="locationid">
    <option value="">Please Select a Location</option>
    @For Each location In Model.Locations
        @<option value="@location.id" @IIf(model.Selected = location.id,"selected","") >@location.id</option>
    Next
</select>

