﻿@modeltype IEnumerable(of LM.LMRestaurant)
@Code
    Layout = Nothing
End Code

<div>
    <table class="table">
        @For Each item In Model
            @<tr>
                <td>
                    @item.Name
                </td>
             </tr>
        Next
    </table>
</div>
