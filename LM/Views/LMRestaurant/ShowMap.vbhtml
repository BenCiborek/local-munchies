﻿@Code
    Layout = Nothing
End Code
<script src="https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var lat = $("#hdnlat").val();
            var lon = $("#hdnlon").val();
            var latlng = new google.maps.LatLng(lat, lon);
            var myOptions = { zoom: 15, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP };
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            var marker = new google.maps.Marker({position: latlng,map: map});
        }); 
    </script>
<input type="hidden" id="hdnlat" value="@ViewBag.Lat" />
<input type="hidden" id="hdnlon" value="@ViewBag.Lon" />
<div id="map_canvas" style="width: 100%; height: 100%; position: relative; background-color: rgb(229, 227, 223);"></div>