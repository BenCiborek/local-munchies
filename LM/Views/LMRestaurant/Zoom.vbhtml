﻿@Modeltype LM.LMRestaurant
@Code
    ViewData("Title") = "| " & Model.Name & " | " & Model.City & ", " & Model.State
End Code
<a href="@Url.RouteUrl("Search", New With {.id=Model.LocationID})" class="btn btn-small btn-primary" style="margin-bottom:5px;">< Back to Search Results</a>    
<div class="">
        <div class="row">
            @*Column 1*@
            <div class="col-xs-12 col-sm-9">
                <div class="well well-sm"itemscope itemtype="http://schema.org/Organization">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2  itemprop="name" class="restSearchName">@Model.Name</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">

                            <div>@Html.Action("ShowHours2", "LMRestaurant", New With {.rest = Model})</div>

                         <div  itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" >   <address class="LMSearchAddress">
                                <span itemprop="streetAddress">@Model.Address</span><br />
                                @If Model.Address2 <> "" Then
                                   @<span itemprop="streetAddress2"> @Model.Address2.ToString()</span>@<br />
                                End If
                                <span itemprop="addressLocality">@Model.City</span>, <span itemprop="addressRegion">@Model.State</span> <span itemprop="postalCode">@Model.zip</span>
                            </address>
                         </div>
                            <span itemprop="telephone"><a href="tel:+1 @Model.Phone">@Model.Phone</a></span>
                            @If Model.WebAddress <> "" Then
                                @<br />
                                @<a href="http://@Model.WebAddress" target="_blank">Visit @Model.Name's Website</a>
                            End If
                            <div>
                                <small>
                                    @Html.Action("ShowCats", "LMCategories", New With {.Rest = Model})
                                </small>
                                <br />
                                <small>
                                    @Html.Action("ShowOrderingOptions", "LMRestaurant", New With {.rest = Model})
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SignUp">
                    @Html.Action("SignUp", "Account")
                </div>
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ordering Options</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#Menu">Menu</a></li>
                                @* <li><a href="#Specials">Specials</a></li>*@
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="Menu">
                                    @If Model.Menus.Count < 1 Then
                                        @<h2>Menu Coming Soon.</h2>
                                    Else
                                        
                                        For Each item In Model.Menus
                                            If item.OnlineOrderingActive = "1" Then
                                                'Online Ordering Active
                                        @Html.Action("Order", "Menu", New With {.id = item.ID}) 
                                            Else
                                                'Online ordering not active - Show regular Menu
                                        @Html.Action("ShowMore", "Menu", New With {.id = item.ID})    
                                            End If
                                        Next
                                        
                                    End If
                                </div>
                                @* <div class="tab-pane" id="Specials">Specials</div>*@
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @*Column 2*@
            <div class="col-xs-12 col-sm-3 sidebar-offcanvas">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Map</h3>
                    </div>
                    <div class="panel-body" style="height:300px;">
                       @Html.Action("ShowMap", "LMRestaurant", New With {.Lat = Model.Lat, .Lon = Model.Lon})
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hours</h3>
                    </div>
                    <div class="panel-body">
                        @Html.Action("ShowHours1", "LMRestaurant", New With {.rest = Model})
                    </div>
                </div>
               @* <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">You May Also Enjoy</h3>
                    </div>
                    <div class="panel-body">
                        @Html.Action("FindLikeBusinesses", "LMRestaurant", New With {.restid = Model.ID})
                    </div>
                </div>*@
            </div>
        </div>
    </div>

<script>
    $(function () {
      $('#myTab a').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
      })
      $('.navbar-search').hide()
  })
</script> 