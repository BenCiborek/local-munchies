﻿@Code
    ViewData("Title") = "| Shopping Cart | Account"
End Code

@Html.Action("_checkoutheader","Checkout",New With {.checkoutStep = 2})

<div>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Sign in
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <form>
                        <div class="alert alert-danger hidden" id="signinalert"><span id="signinalertmessage"></span></div>
                        <div class="form-group">
                            <label for="SIEmail">Email address</label>
                            <input type="email" class="form-control" id="SIEmail" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="SIPassword">Password</label>
                            <input type="password" class="form-control" id="SIPassword" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-default" id="btnSignIn">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Create an Account
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <form>
                        <div class="alert alert-danger hidden" id="registeralert"><span id="registeralertmessage"></span></div>
                        <div class="form-group">
                            <label for="RegEmail">Email address</label>
                            <input type="email" class="form-control" id="RegEmail" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="RegPassword">Password</label>
                            <input type="password" class="form-control" id="RegPassword" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="RegPassConfirm">Confirm Password</label>
                            <input type="password" class="form-control" id="RegPassConfirm" placeholder="Confirm Password">
                        </div>
                        <button type="submit" class="btn btn-default" id="btnRegister">Register</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="@Url.Action("SelectAddress","Checkout")">Checkout as Guest
                    </a>
                </h4>
            </div>
        </div>
    </div>
    <br />
    <div>
        <a class="btn btn-default" href="@Url.Action("Index","Cart")"><span class="glyphicon glyphicon-chevron-left"></span> Back to Cart</a>
    </div>

</div>


<script>
    $(function () {
        $('.navbar-search').hide()

        $("#btnSignIn").on('click', function (e) {
            e.preventDefault()
            if (signin($('#SIEmail').val(), $('#SIPassword').val())) {
                window.location.href = '/Checkout/SelectAddress';
            }
        });

        $("#btnRegister").on('click', function (e) {
            e.preventDefault()
            if (register($('#RegEmail').val(), $('#RegPassConfirm').val(), $('#RegPassword').val())) {
                window.location.href = '/Checkout/SelectAddress';
            }
        });

    })

    function signin(user, pass) {
        var worked = false
        $.ajax({
            url: '/Account/JsonLogin',
            data: {
                user: user,
                pass: pass
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
                $('#signinalert').addClass('ajaxRefreshing');
                $('#signinalert').removeClass('alert-danger');

                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
               // $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = true
                }
                else {
                    worked = false
                    $('#signinalert').addClass('alert-danger');
                    $("#signinalert").removeClass("hidden");
                    $("#signinalertmessage").html(result[1]);
                }
            },
            error: function (result) {
                alert("Ajax Error: signing in");
                worked = false
            }
        });
        return worked
    }

    function register(user,passconfirm,pass) {
        var worked = false
        $.ajax({
            url: '/Account/JsonRegister',
            data: {
                Email: user,
                ConfirmPassword: passconfirm,
                Password: pass
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
                $('#registeralert').addClass('ajaxRefreshing');
                $('#registeralert').removeClass('alert-danger');

                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                // $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = true
                }
                else {
                    worked = false
                    $('#registeralert').addClass('alert-danger');
                    $("#registeralert").removeClass("hidden");
                    $("#registeralertmessage").html(result[1]);


                }
            },
            error: function (result) {
                alert("Ajax Error: registering");
                worked = false
            }
        });
        return worked
    }

  
</script>
