﻿@modeltype LM.CheckoutHeaderVM
@Code
    ViewData("Title") = "_checkoutheader"
    Layout = Nothing
    Dim PassedStepclass As String = " badge-success"
End Code
<div class="row">
    <div class="col-xs-3" style="text-align: center;">
        <span class="badge @IIf(Model.checkoutStep >= 1, PassedStepclass, "")"><span class="glyphicon glyphicon-ok"></span></span>
        <h6 class="text-muted">Review</h6>
    </div>
    <div class="col-xs-3" style="text-align: center;">
        <span class="badge @IIf(Model.checkoutStep >= 2, PassedStepclass, "")"><span class="glyphicon glyphicon-log-in"></span></span>
        <h6 class="text-muted">Account</h6>
    </div>
  @*  <div class="col-xs-2" style="text-align: center;">
        <span class="badge @IIf(Model.checkoutStep >= 3, PassedStepclass, "")"><span class="glyphicon glyphicon-send"></span></span>
        <h6 class="text-muted">Address</h6>
    </div>*@
    <div class="col-xs-3" style="text-align: center;">
        <span class="badge @IIf(Model.checkoutStep >= 4, PassedStepclass, "")"><span class="glyphicon glyphicon-usd"></span></span>
        <h6 class="text-muted">Pay</h6>
    </div>
    <div class="col-xs-3" style="text-align: center;">
        <span class="badge @IIf(Model.checkoutStep >= 5, PassedStepclass, "")"><span class="glyphicon glyphicon-cutlery"></span></span>
        <h6 class="text-muted">Munch</h6>
    </div>
</div>


