﻿@modeltype LM.PaymentVM
@Code
    Layout = Nothing
End Code

<div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h3>Order Type: @IIf(Model.OrderType = 1, "Take-Out", "Delivery")</h3>
        </div>
    </div>

    <div>
        <div>
            <div class="form-group">
                <div class="row">
                    <h3 id="TotalAmount" class="pull-right">Total: @FormatCurrency(Model.BillableAmount)</h3>
                    <input type="hidden" id="hdnAmount" value="@Model.BillableAmount" />

                </div>
            </div>
            <div class="panel panel-default" id="deliverypanel">
                <div class="panel-heading">
                    <h4 class="panel-title">Delivery Address
                    </h4>
                </div>
                <div class="panel-body">
                    <div>

                        @If Model.OrderType = 2 Then
                            @<form role="form">
                                <div class="alert alert-danger hidden" id="deliveryalert"><span id="deliveryalertmessage"></span></div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" id="deliveryEmail" value="Delivery@D.com">
                                </div>
                                <div class="form-group">
                                    <label for="deliveryName">Name</label>
                                    <input type="text" class="form-control" id="deliveryName" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="deliveryAddress1">Address 1</label>
                                    <input type="text" class="form-control" id="deliveryAddress1" placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <label for="deliveryAddress2">Address 2</label>
                                    <input type="text" class="form-control" id="deliveryAddress2" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="deliveryCity">City</label>
                                    <input type="text" class="form-control" id="deliveryCity" placeholder="City">
                                </div>
                                <div class="form-group">
                                    <label for="deliveryState">State</label>
                                    <select id="deliveryState" class="form-control">
                                        <option value="">Select State</option>
                                        <option value='AL'>AL</option>
                                        <option value='AK'>AK</option>
                                        <option value='AS'>AS</option>
                                        <option value='AZ'>AZ</option>
                                        <option value='AR'>AR</option>
                                        <option value='CA'>CA</option>
                                        <option value='CO'>CO</option>
                                        <option value='CT'>CT</option>
                                        <option value='DE'>DE</option>
                                        <option value='DC'>DC</option>
                                        <option value='FM'>FM</option>
                                        <option value='FL'>FL</option>
                                        <option value='GA'>GA</option>
                                        <option value='GU'>GU</option>
                                        <option value='HI'>HI</option>
                                        <option value='ID'>ID</option>
                                        <option value='IL'>IL</option>
                                        <option value='IN'>IN</option>
                                        <option value='IA'>IA</option>
                                        <option value='KS'>KS</option>
                                        <option value='KY'>KY</option>
                                        <option value='LA'>LA</option>
                                        <option value='ME'>ME</option>
                                        <option value='MH'>MH</option>
                                        <option value='MD'>MD</option>
                                        <option value='MA'>MA</option>
                                        <option value='MI'>MI</option>
                                        <option value='MN'>MN</option>
                                        <option value='MS'>MS</option>
                                        <option value='MO'>MO</option>
                                        <option value='MT'>MT</option>
                                        <option value='NE'>NE</option>
                                        <option value='NV'>NV</option>
                                        <option value='NH'>NH</option>
                                        <option value='NJ'>NJ</option>
                                        <option value='NM'>NM</option>
                                        <option value='NY'>NY</option>
                                        <option value='NC'>NC</option>
                                        <option value='ND'>ND</option>
                                        <option value='MP'>MP</option>
                                        <option value='OH'>OH</option>
                                        <option value='OK'>OK</option>
                                        <option value='OR'>OR</option>
                                        <option value='PW'>PW</option>
                                        <option value='PA'>PA</option>
                                        <option value='PR'>PR</option>
                                        <option value='RI'>RI</option>
                                        <option value='SC'>SC</option>
                                        <option value='SD'>SD</option>
                                        <option value='TN'>TN</option>
                                        <option value='TX'>TX</option>
                                        <option value='UT'>UT</option>
                                        <option value='VT'>VT</option>
                                        <option value='VI'>VI</option>
                                        <option value='VA'>VA</option>
                                        <option value='WA'>WA</option>
                                        <option value='WV'>WV</option>
                                        <option value='WI'>WI</option>
                                        <option value='WY'>WY</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="deliveryZip">Zip</label>
                                    <input type="number" class="form-control" id="deliveryZip" placeholder="Zipcode">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" id="deliveryPhone" placeholder="Phone Number" value="1111111111">
                                </div>
                            </form>
                        Else
                            @<span>No Delivery Address Needed</span>
                        End If

                    </div>
                </div>

            </div>
            <div class="panel panel-default" id="Billtopanel">
                <div class="panel-heading">
                    <h4 class="panel-title">Billing Address
                    </h4>
                </div>
                <div class="panel-body">
                    <div>
                        <form role="form">
                            <div class="alert alert-danger hidden" id="Billtoalert"><span id="Billtoalertmessage"></span></div>
                            <div class="form-group @IIf(Model.EmailAddress <> "", "hidden", "")">
                                <label for="BilltoEmail">Email address</label>
                                <input type="email" class="form-control" id="BilltoEmail" value="@Model.EmailAddress">
                            </div>
                            <div class="form-group">
                                <label for="BilltoName">Name</label>
                                <input type="text" class="form-control" id="BilltoName" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="BilltoAddress1">Address 1</label>
                                <input type="text" class="form-control" id="BilltoAddress1" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <label for="BilltoAddress2">Address 2</label>
                                <input type="text" class="form-control" id="BilltoAddress2" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="BilltoCity">City</label>
                                <input type="text" class="form-control" id="BilltoCity" placeholder="City">
                            </div>
                            <div class="form-group">
                                <label for="BilltoState">State</label>
                                <select id="BilltoState" class="form-control">
                                    <option value="">Select State</option>
                                    <option value='AL'>AL</option>
                                    <option value='AK'>AK</option>
                                    <option value='AS'>AS</option>
                                    <option value='AZ'>AZ</option>
                                    <option value='AR'>AR</option>
                                    <option value='CA'>CA</option>
                                    <option value='CO'>CO</option>
                                    <option value='CT'>CT</option>
                                    <option value='DE'>DE</option>
                                    <option value='DC'>DC</option>
                                    <option value='FM'>FM</option>
                                    <option value='FL'>FL</option>
                                    <option value='GA'>GA</option>
                                    <option value='GU'>GU</option>
                                    <option value='HI'>HI</option>
                                    <option value='ID'>ID</option>
                                    <option value='IL'>IL</option>
                                    <option value='IN'>IN</option>
                                    <option value='IA'>IA</option>
                                    <option value='KS'>KS</option>
                                    <option value='KY'>KY</option>
                                    <option value='LA'>LA</option>
                                    <option value='ME'>ME</option>
                                    <option value='MH'>MH</option>
                                    <option value='MD'>MD</option>
                                    <option value='MA'>MA</option>
                                    <option value='MI'>MI</option>
                                    <option value='MN'>MN</option>
                                    <option value='MS'>MS</option>
                                    <option value='MO'>MO</option>
                                    <option value='MT'>MT</option>
                                    <option value='NE'>NE</option>
                                    <option value='NV'>NV</option>
                                    <option value='NH'>NH</option>
                                    <option value='NJ'>NJ</option>
                                    <option value='NM'>NM</option>
                                    <option value='NY'>NY</option>
                                    <option value='NC'>NC</option>
                                    <option value='ND'>ND</option>
                                    <option value='MP'>MP</option>
                                    <option value='OH'>OH</option>
                                    <option value='OK'>OK</option>
                                    <option value='OR'>OR</option>
                                    <option value='PW'>PW</option>
                                    <option value='PA'>PA</option>
                                    <option value='PR'>PR</option>
                                    <option value='RI'>RI</option>
                                    <option value='SC'>SC</option>
                                    <option value='SD'>SD</option>
                                    <option value='TN'>TN</option>
                                    <option value='TX'>TX</option>
                                    <option value='UT'>UT</option>
                                    <option value='VT'>VT</option>
                                    <option value='VI'>VI</option>
                                    <option value='VA'>VA</option>
                                    <option value='WA'>WA</option>
                                    <option value='WV'>WV</option>
                                    <option value='WI'>WI</option>
                                    <option value='WY'>WY</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="BilltoZip">Zip</label>
                                <input type="number" class="form-control" id="BilltoZip" placeholder="Zipcode">
                            </div>
                            <div class="form-group" @IIf(Model.EmailAddress <> "", "hidden", "")">
                                <label for="BilltoPhone">Phone Number</label>
                                <input type="email" class="form-control" id="BilltoPhone" placeholder="Phone Number">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div>
                <form>
                    <input type="hidden" id="BilltoID" value="0" />
                    <input type="hidden" id="deliveryID" value="0" />
                    <input type="hidden" id="ordertype" value="@Model.OrderType" />
                </form>
            </div>
        </div>
        <div>
            <form role="form">


                <div class="row">
                </div>
                <div class="form-group">
                    <label for="CCNum">Credit Card Number</label>
                    <input type="number" autocomplete="off" class="form-control" id="CCNum" placeholder="Credit Card Number" />
                </div>
                <div class="form-group">
                    <label for="ExpDate">Expiration Date</label>
                    <select id="ExpMonth">
                        @For Each item In Model.ExpMonths
                            @<option value="@item.Value">@item.Text</option>
                        Next
                    </select>
                    -
                <select id="ExpYear">
                    @For Each item In Model.ExpYears
                        @<option value="@item.Value">@item.Text</option>
                    Next
                </select>
                </div>
                <div class="form-group">
                    <label for="SecCode">Security Code</label>
                    <input type="number" class="form-control" id="SecCode" autocomplete="off" placeholder="" />
                </div>
            </form>
            <div class="form-group">
                <div class="row">
                    <h4 id="Subtotal" class="pull-right">Subtotal: @FormatCurrency(Model.SubTotal)</h4>
                </div>
                <div class="row">
                    <h4 id="Promo" class="pull-right">Coupon:  @FormatCurrency(Model.Promo * -1, , , TriState.False, TriState.True)</h4>
                </div>
                <div class="row">
                    <h4 id="Fees" class="pull-right">Fees: @FormatCurrency(Model.Fee)</h4>
                </div>
                <div class="row">
                    <h4 id="Tip" class="pull-right">Tip <small><a href="@Url.Action("Payment","Checkout")"> (Change)</a> </small>: @FormatCurrency(Model.Tip)</h4>
                </div>
                <div class="row">
                    <h3 id="Amount" class="pull-right">Total: @FormatCurrency(Model.BillableAmount)</h3>
                </div>
            </div>
            <div class="alert alert-danger hidden" id="alert"><span id="alertmessage"></span></div>
            <div>
                <a class="btn btn-default" href="@Url.Action("Index", "Cart")"><span class="glyphicon glyphicon-chevron-left"></span>Back to Cart</a>
                <button type="submit" class="btn btn-success pull-right" id="btnOrder">Place Order <span class="glyphicon glyphicon-chevron-right"></span></button>
            </div>
        </div>
    </div>

</div>
<div class="modal" id="orderTIP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" style="text-align: center;">
                            <h3>How much would you like to Tip?</h3>
                        </div>
                    </div>
                    <div class="row">
                        @*NoTip*@
                        <div class="col-xs-6">
                            <a class="btn btn-default btn-block btn-large btnTip" data-amount="0">
                                <br />
                                No Tip<br />@FormatCurrency(0)<br />
                                <br />
                            </a>
                        </div>
                        @*10%*@
                        <div class="col-xs-6">
                            <a class="btn btn-default btn-block btn-large btnTip" data-amount="@Math.Round(0.1 * Model.OrderTotal, 2)">
                                <br />
                                10%<br />@FormatCurrency(Math.Round(0.1 * Model.OrderTotal, 2))<br />
                                <br />
                            </a>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        @*15%*@
                        <div class="col-xs-6">
                            <a class="btn btn-default btn-block btn-large btnTip" data-amount="@Math.Round(0.15 * Model.OrderTotal, 2)">
                                <br />
                                15%<br />@FormatCurrency(Math.Round(0.15 * Model.OrderTotal, 2))<br />
                                <br />
                            </a>
                        </div>
                        @*20%*@
                        <div class="col-xs-6">
                            <a class="btn btn-default btn-block btn-large btnTip" data-amount="@Math.Round(0.2 * Model.OrderTotal, 2)">
                                <br />
                                20%<br />@FormatCurrency(Math.Round(0.2 * Model.OrderTotal, 2))<br />
                                <br />
                            </a>
                        </div>
                    </div>
                   <div class="row" style="margin-top:10px;">
                        @*15%*@
                        <div class="col-xs-12">
                            <a class="btn btn-success btn-block btn-large btnTip" data-amount="@Math.Round(Math.Ceiling((0.2 * Model.OrderTotal) / 5.0) * 5.0, 2)">
                                <br />
                                Super Tip!<br />@FormatCurrency(Math.Round(Math.Ceiling((0.2 * Model.OrderTotal) / 5.0) * 5.0, 2))<br />
                                <br />
                            </a>
                        </div>
                    </div>
                    <div style="margin-top:30px"></div>
                </div>
            </div>
        </div>
    </div>

<script>

    var ccerror = ""
    var receipt = ""
    $(function () {
        $('.navbar-search').hide()
       @If Model.showTip Then
            @:$('#orderTIP').modal('show')
            Else
       End If

        //$(".btnTip").on('click', function (e) {
        //    $('#orderTIP').modal('hide');
        //    var tipamount = $(this).attr("data-amount");
        //    addTip(tipamount)
           
        //});

        $("#btnOrder").on('click', function (e) {
            e.preventDefault()
            var billamount = $("#hdnAmount").val()
            var ordertype = $("#ordertype").val()
            var deliverable = "0"
            if (true) {
                var Authcode = ''

                if (addAddress(2, 'Billto')) {

                    if (ordertype == 2) {
                        //Delivery Order. Add Delivery Address
                        if (!addAddress(1, 'delivery')) {
                            $('html, body').animate({
                                scrollTop: $("#deliverypanel").offset().top
                            }, 1000);
                        } else {
                            deliverable = isDeliverable()
                        }
                    }

                    if ($("#BilltoID").val() == "0" || (ordertype == "2" && ($("#deliveryID").val() == "0" || deliverable == "0"))) {
                        if (deliverable == "0" && $("#deliveryID").val() != "0") {
                            try {
                                ga('send', 'event', 'payment', 'delivery', 'AddressOutsideOfZone', 1);
                            }
                            catch (err) {
                                //Skip Error
                            }
                            //Display undeliverable Error Message
                            $('#deliveryalert').addClass('alert-danger');
                            $('#deliveryalert').removeClass("hidden");
                            $('#deliveryalertmessage').html("This Address is outside of the delivery zone. Please return to Cart or select a different delivery address.");
                            $('html, body').animate({
                                scrollTop: $("#deliverypanel").offset().top
                            }, 1000);
                        }

                    }
                    else {
                        //Add Addresses to ticket
                        if (addaddresstoticket($("#deliveryID").val(), $("#BilltoID").val())) {
                            //Move to Payment 
                            //Close Order
                            if (AddPaymentToOrder(billamount, $('#CCNum').val(), $('#ExpMonth').val() + $('#ExpYear').val(), $('#SecCode').val())) {

                                //
                                window.location.href = '/Checkout/Receipt/' + receipt;
                            }
                            else {
                                $('#alert').addClass('alert-danger');
                                $("#alert").removeClass("hidden");
                                $("#alertmessage").html(ccerror);
                            }
                        }
                        else {
                            //Show Alert Box with Error
                            $('#alert').addClass('alert-danger');
                            $("#alert").removeClass("hidden");
                            $("#alertmessage").html(ccerror);
                        }
                    }
                }
                else {
                    $('html, body').animate({
                        scrollTop: $("#Billtopanel").offset().top
                    }, 1000);
                }

            }


        });

    })

    function addTip(tipAmount) {
        $('#orderTIP').modal('hide');
        $.ajax({
            url: '/Cart/AddTip',
            data: {
                tip: tipAmount
            },
            dataType: "html",
            type: 'POST',
            beforeSend: function (xhr) {

               // $('#checkout').addClass('ajaxRefreshing');
               // $('#checkout').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                //xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#orderTIP').modal('hide');
                //alert("Here")
                $('#checkout').html(evt);
                $('#orderTIP').modal('hide');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //$('#checkout').removeClass('ajaxRefreshing');
            },
            complete: function () {
               // $('#checkout').removeClass('ajaxRefreshing');
            }
        });
    }


    function addAddress(addtypeint, addtype) {


        var email = $("#" + addtype + "Email").val()
        var name = $("#" + addtype + "Name").val()
        var address1 = $("#" + addtype + "Address1").val()
        var address2 = $("#" + addtype + "Address2").val()
        var city = $("#" + addtype + "City").val()
        var state = $("#" + addtype + "State").val()
        var zip = $("#" + addtype + "Zip").val()
        var phone = $("#" + addtype + "Phone").val()


        return addaddress(addtypeint, email, name, address1, address2, city, state, zip, phone, addtype)
    }

    function verifyCC(number, expmonth, expyear, cvv, amount) {
        var verified = true
        //alert(number + "-" + expmonth + "-" + expyear + "-" + cvv);
        error = "We had Problems Charging your card. Please Verify all of the info."

        return verified
    }

    function isDeliverable() {
        var worked = "0"

        $.ajax({
            url: '/Cart/isDeliverable',
            data: {
                Address: $("#deliveryAddress1").val(),
                city: $("#deliveryCity").val(),
                State: $("#deliveryState").val(),
                zip: $("#deliveryZip").val()
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = "1"
                }
                else {
                    worked = "0"
                }
            },
            error: function (result) {
                alert("Ajax Error: Finding Address");
                worked = "0"
            }
        });
        return worked
    }

    function AddPaymentToOrder(billamount, CCNumber, Exp, CVV) {

        var worked = false
        $.ajax({
            url: '/Cart/AddPaymentJSON',
            data: {
                amount: billamount,
                CCNumber: CCNumber,
                Expiry: Exp,
                CVV: CVV
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                if (result[0] == "1") {
                    receipt = result[1]
                    worked = true
                }
                else {
                    worked = false
                    ccerror = result[1]
                }
            },
            error: function (result) {
                alert("Ajax Error: Adding Payment");
                worked = false
            }
        });
        return worked
    }

    function addaddress(typeint, email, name, street, street2, city, state, zip, phone, typestr) {
        var worked = false

        $.ajax({
            url: '/Address/AddAddressJSON',
            data: {
                email: email,
                name: name,
                street: street,
                street2: street2,
                city: city,
                State: state,
                zip: zip,
                phone: phone,
                type: typeint,
                isDefault: true
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
                $('#' + typestr + 'ID').val("0")
                $('#' + typestr + 'alert').removeClass('alert-danger');

                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                // $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = true
                    $('#' + typestr + 'ID').val(result[1])
                    $('#' + typestr + 'alert').removeClass('alert-danger');
                    $('#' + typestr + 'alert').addClass("hidden");
                    $('#' + typestr + 'alertmessage').html('');
                }
                else {
                    worked = false
                    $('#' + typestr + 'alert').addClass('alert-danger');
                    $('#' + typestr + 'alert').removeClass("hidden");
                    $('#' + typestr + 'alertmessage').html(result[1]);
                }
            },
            error: function (result) {
                alert("Ajax Error: Add Address to screen");
                worked = false
            }
        });
        return worked
    }

    function addaddresstoticket(deliveryaddressid, billtoaddressid) {
        var worked = false
        $.ajax({
            url: '/Cart/AddAddressesToTicketJSON',
            data: {
                DeliveryAddressID: deliveryaddressid,
                BilltoAddressID: billtoaddressid
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
                //$('#' + typestr + 'ID').val("0")
                //$('#' + typestr + 'alert').removeClass('alert-danger');

                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                // $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = true

                }
                else {
                    worked = false
                    $('#alert').addClass('alert-danger');
                    $('#alert').removeClass("hidden");
                    $('#alertmessage').html(result[1]);
                }
            },
            error: function (result) {
                alert("Ajax Error: Add Address");
                worked = false
            }
        });
        return worked
    }


</script>

