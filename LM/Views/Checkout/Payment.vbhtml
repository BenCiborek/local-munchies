﻿
@Code
    ViewData("Title") = " | Checkout | Payment"
End Code

@Html.Action("_checkoutheader", "Checkout", New With {.checkoutStep = 4})
<div id="checkout">
    @Html.Action("PaymentView")
</div>


<script>
    $(".btnTip").on('click', function (e) {
        $('#orderTIP').modal('hide');
        var tipamount = $(this).attr("data-amount");
        addTip(tipamount)

    });
</script>
