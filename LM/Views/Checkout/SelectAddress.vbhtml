﻿@Modeltype LM.SelectAddressVM
@Code
    ViewData("Title") = "| Shopping Cart | Address"
End Code

@Html.Action("_checkoutheader","Checkout",New With {.checkoutStep = 3})

<div>
    <div class="alert alert-danger hidden" id="alert"><span id="alertmessage"></span></div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Delivery Address
            </h4>
        </div>
        <div class="panel-body">
            <div>
                <ul class="list-group">
                    <span id="selecteddelivery"></span>
                    <li class="list-group-item">
                        <span id="selectedDeliveryAddress">
                            @If Model.OrderType = 2 Then
                                @<span><a data-toggle="modal" href="#" data-target="#deliveryModal">Add/Change Delivery Address</a></span>
                            Else
                                @<span>No Delivery Address Needed</span>
                            End If
                        </span>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Billing Address
            </h4>
        </div>
        <div class="panel-body">
            <div>
                <ul class="list-group">
                    <span id="selectedBillto"></span>
                    <li class="list-group-item">

                        <span>
                            <a data-toggle="modal" href="#" data-target="#BilltoModal">Add/Change Billing Address</a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div>
        <form>
            <input type="hidden" id="BilltoID" value="0" />
            <input type="hidden" id="deliveryID" value="0" />
            <input type="hidden" id="ordertype" value="@Model.OrderType" />
            <button class="btn btn-success pull-right" id="btnPayment">Payment <span class="glyphicon glyphicon-chevron-right"></span></button>
        </form>
    </div>
</div>

@*Billing POPUP*@

<div class="modal fade" id="BilltoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Billing Address</h4>
            </div>
            <div class="modal-body">
                <ul class="list=group addresslist">
                    @For Each address In Model.BillToAdds
                        @<li class="list-group-item">
                            <a href="#" class="selectAddress" data-Addresstype="Billto" data-AddressID="@address.ID" data-name="@address.Name" data-address1="@address.Address1" data-address2="@address.Address2" data-city="@address.City" data-state="@address.State" data-zip="@address.Zip">
                                @address.Name
                                <br />
                                @address.Address1
                                <br />
                                @IIf(address.Address2 <> "", address.Address2 & "<br />", "")
                                @address.City, @address.State @address.Zip
                            </a>
                        </li>
                    Next
                    <li class="list-group-item">
                        <a data-toggle="collapse" href="#billtonewaddress">Add New Address</a>
                    </li>
                    <div id="billtonewaddress" class="collapse">
                        <li class="list-group-item">

                            <form role="form">
                                <div class="alert alert-danger hidden" id="Billtoalert"><span id="Billtoalertmessage"></span></div>
                                <div class="form-group @IIf(Model.EmailAddress <> "", "hidden", "")">
                                    <label for="BilltoEmail">Email address</label>
                                    <input type="email" class="form-control" id="BilltoEmail" value="@Model.EmailAddress">
                                </div>
                                <div class="form-group">
                                    <label for="BilltoName">Name</label>
                                    <input type="text" class="form-control" id="BilltoName" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="BilltoAddress1">Address 1</label>
                                    <input type="text" class="form-control" id="BilltoAddress1" placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <label for="BilltoAddress2">Address 2</label>
                                    <input type="text" class="form-control" id="BilltoAddress2" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="BilltoCity">City</label>
                                    <input type="text" class="form-control" id="BilltoCity" placeholder="City">
                                </div>
                                <div class="form-group">
                                    <label for="BilltoState">State</label>
                                    <select id="BilltoState" class="form-control">
                                        <option value="">Select State</option>
                                        <option value="OH">OH</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="BilltoZip">Zip</label>
                                    <input type="number" class="form-control" id="BilltoZip" placeholder="Zipcode">
                                </div>
                                <div class="form-group" @IIf(Model.EmailAddress <> "", "hidden", "")">
                                    <label for="BilltoPhone">Phone Number</label>
                                    <input type="email" class="form-control" id="BilltoPhone" placeholder="Phone Number">
                                </div>
                            </form>
                            <button type="submit" class="btn btn-primary btnAddAddress" data-addresstype="Billto" id="btnAddBillto">Add Address to Order</button>
                        </li>
                    </div>
                </ul>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@*Delivery POPUP*@

<div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delivery Address</h4>
            </div>
            <div class="modal-body">
                <ul class="list=group addresslist">
                    @For Each address In Model.ShipToAdds
                        @<li class="list-group-item">
                            <a href="#" class="selectAddress" data-Addresstype="delivery" data-AddressID="@address.ID" data-name="@address.Name" data-address1="@address.Address1" data-address2="@address.Address2" data-city="@address.City" data-state="@address.State" data-zip="@address.Zip">
                                <small>
                                    @address.Name
                                    <br />
                                    @address.Address1
                                    <br />
                                    @IIf(address.Address2 <> "", address.Address2 & "<br />", "")
                                    @address.City, @address.State @address.Zip
                                </small>
                            </a>
                        </li>
                            
                    Next
                <li class="list-group-item">
                    <a data-toggle="collapse" href="#Billtonewaddress">Add New Address</a>
                </li>
                <div id="deliverynewaddress" class="collapse">
                    <li class="list-group-item">

                        <form role="form">
                            <div class="alert alert-danger hidden" id="deliveryalert"><span id="deliveryalertmessage"></span></div>
                            <div class="form-group">
                                <label for="deliveryEmail">Email address</label>
                                <input type="hidden" class="form-control" id="deliveryEmail" value="@Model.EmailAddress">
                            </div>
                            <div class="form-group">
                                <label for="deliveryName">Name</label>
                                <input type="text" class="form-control" id="deliveryName" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="deliveryAddress1">Address 1</label>
                                <input type="text" class="form-control" id="deliveryAddress1" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <label for="deliveryAddress2">Address 2</label>
                                <input type="text" class="form-control" id="deliveryAddress2" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="deliveryCity">City</label>
                                <input type="text" class="form-control" id="deliveryCity" placeholder="City">
                            </div>
                            <div class="form-group">
                                <label for="deliveryState">State</label>
                                <select id="deliveryState" class="form-control">
                                    <option value="">Select State</option>
                                    <option value="OH">OH</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="deliveryZip">Zip</label>
                                <input type="number" class="form-control" id="deliveryZip" placeholder="Zipcode">
                            </div>
                            <div class="form-group">
                                <label for="deliveryPhone">Phone Number</label>
                                <input type="email" class="form-control" id="deliveryPhone" placeholder="Phone Number">
                            </div>
                        </form>
                        <button type="submit" class="btn btn-default btnAddAddress" data-addresstype="delivery" id="btnAdddelivery">Add Address to Order</button>
                    </li>
                </div>
            </ul>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>
    </div>
  </div>
</div>

<script>
    $(function () {
        $('.navbar-search').hide()

        $('.selectAddress').on('click', function (e) {
            var addtype = $(this).attr("data-addresstype")
            var name = $(this).attr("data-name")
            var address1 =$(this).attr("data-address1")
            var address2 = $(this).attr("data-address2")
            var city = $(this).attr("data-city")
            var state = $(this).attr("data-state")
            var zip = $(this).attr("data-zip")
            var addressid = $(this).attr("data-AddressID")
            $("#" + addtype + "ID").val(addressid)

            $('#selected' + addtype).html(
                  '<li class="list-group-item"><small>' +
                  name + '<br />' +
                  address1 + '<br />' +
                  address2 + '<br />' +
                  city + ', ' + state + ' ' + zip +
                  '</small></li>'
                  )


            $('#' + addtype + 'Modal').modal('hide')

        });

        $('#btnPayment').on('click', function (e) {
            e.preventDefault()
            if ($("#BilltoID").val() == "0" || $("#deliveryID").val() == "0" && $("#deliveryID").val() == "1") {
                //Display Error
                //Show Alert
                
                $('#alert').removeClass("hidden");
                $('#alertmessage').html("Please Select An Address");
            }
            else {
                //Add Addresses to ticket
                if (addaddresstoticket($("#deliveryID").val() ,$("#BilltoID").val()))
                {
                    //Move to Payment Page
                    window.location.href = '@Url.Action("Payment", "Checkout")';
                }
                
                
            }

        })

     
      $(".btnAddAddress").on('click', function (e) {
          e.preventDefault()
          var addtype = $(this).attr("data-addresstype")
          var addtypeint = 1
          if (addtype = 'Billto') {
              addtypeint = 2
          }

          var email = $("#" + addtype + "Email").val()
          var name = $("#" + addtype + "Name").val()
          var address1 = $("#" + addtype + "Address1").val()
          var address2 = $("#" + addtype + "Address2").val()
          var city = $("#" + addtype + "City").val()
          var state = $("#" + addtype + "State").val()
          var zip = $("#" + addtype + "Zip").val()
          var phone = $("#" + addtype + "Phone").val()


          if (addaddress(2, email, name, address1, address2, city, state, zip, phone, addtype)) {
              $('#' + addtype + 'Modal').modal('hide')
              $('#selected' + addtype).html(
                  '<li class="list-group-item"><small>' +
                  name + '<br />' +
                  address1  + '<br />' +
                  address2 + '<br />' +
                  city +', ' + state + ' ' + zip +
                  '</small></li>'
                  )
              
            }
        });

    })

    
    function addaddress(typeint,email,name,street,street2,city,state,zip,phone,typestr)
    {
        var worked = false
        $.ajax({
            url: '/Address/AddAddressJSON',
            data: {
                email:email,
                name:name,
                street:street,
                street2:street2,
                city:city,
                State:state,
                zip: zip,
                phone: phone,
                type: typeint,
                isDefault:true
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
                $('#' + typestr + 'ID').val("0")
                $('#' + typestr + 'alert').removeClass('alert-danger');
                
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                // $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = true
                    $('#' + typestr + 'ID').val(result[1])
                }
                else {
                    worked = false
                    $('#' + typestr + 'alert').addClass('alert-danger');
                    $('#' + typestr + 'alert').removeClass("hidden");
                    $('#' + typestr + 'alertmessage').html(result[1]);
                }
            },
            error: function (result) {
                alert("Ajax Error: ");
                worked = false
            }
        });
        return worked
    }

    function addaddresstoticket(deliveryaddressid, billtoaddressid) {
        var worked = false
        $.ajax({
            url: '/Cart/AddAddressesToTicketJSON',
            data: {
                DeliveryAddressID:deliveryaddressid,
                BilltoAddressID: billtoaddressid
            },
            type: 'Post',
            async: false,
            beforeSend: function (xhr) {
                //$('#' + typestr + 'ID').val("0")
                //$('#' + typestr + 'alert').removeClass('alert-danger');

                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                // $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (result) {
                if (result[0] == "1") {
                    worked = true
                    
                }
                else {
                    worked = false
                    $('#alert').addClass('alert-danger');
                    $('#alert').removeClass("hidden");
                    $('#alertmessage').html(result[1]);
                }
            },
            error: function (result) {
                alert("Ajax Error: ");
                worked = false
            }
        });
        return worked
    }


</script>