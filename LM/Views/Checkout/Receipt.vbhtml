﻿@modeltype LM.ReceiptVM
@Code
    ViewData("Title") = " | Checkout | Receipt"
    Dim OldRestID = 0
    Dim ordertime As Date = TimeZoneInfo.ConvertTimeFromUtc(Model.Ticket.ClosingDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
End Code
<div>
    @Html.Action("_checkoutheader", "Checkout", New With {.checkoutStep = 5})
    <h1 style="margin-bottom:0px;">Order Number: @Model.Ticket.ID</h1><br />
    <h1 style="margin-top:0px;">Confirmation Number: @Right(Model.Ticket.ID,2)</h1>
    <h3>You must present this number when picking up your meal.</h3>
    <h2>Order Date: @ordertime</h2>
    @If Model.Ticket.TicketTypeID = 1 Then
        @<h3>You can expect your order to be ready for pick up in 15 - 20 minutes.</h3>
    Else
        @<h3>You can expect your order to be delivered in 45 - 60 minutes.</h3>
    End If
    
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                <h3>Billing Address</h3>
                <address>
                   @Model.Billto.Name<br />
                    @Model.Billto.Address1<br />
                    @Model.Billto.Address2<br />
                    @Model.Billto.City, @Model.Billto.State @Model.Billto.Zip
                </address>
            </div>
        </div>
        @If Model.Ticket.TicketTypeID = 2 Then
            @<div class="col-md-6">
            <div class="well">
                <h3>Deliver to:</h3>
                <address>
                   @Model.Deliverto.Name<br />
                    @Model.Deliverto.Address1<br />
                    @Model.Deliverto.Address2<br />
                    @Model.Deliverto.City, @Model.Deliverto.State @Model.Deliverto.Zip
                </address>
            </div>
        </div>
        End If
    </div>
   @* <div class="row">
            <h2 class="pull-right">Total: @FormatCurrency(Model.Ticket.TotalPrice)</h2>
        </div>*@
    @For Each Restaurant In Model.Rests
        @<h3>@Restaurant.Name</h3>
        @<ul class="list-group">
            @For Each Item In Model.Ticket.ticketitems.Where(Function(y) y.RestaurantID = Restaurant.ID)
                @<li class="list-group-item">
                    <div class="row">
                        <small>
                            <div class="col-xs-8">
                                @Item.Name
                                <br />
                                <small class="text-muted">
                                    @For Each Group In Item.ModifierGroups
                                        @<span>@Group.Name: </span>
                                    For Each modifier In Group.Modifiers
                                        @<span>@modifier.Name </span>
                                    For Each l2group In modifier.TicketItemL2Groups
                                            @<br />
                                            @<span>@l2group.Name: </span>
                                    For Each l2mod In l2group.Modifiers
                                            @<span>@l2mod.Name  @IIf(l2mod.Qty > 1,"x" & l2mod.Qty,"")</span>
                                    Next
                                    Next
                                    Next
                                        @<br />
                                    Next
                                </small>
                            </div>
                            <div class="col-xs-4" style="text-align: right;">
                                @FormatCurrency(Item.TotalItemPrice)
                            </div>
                        </small>
                    </div>
                </li>
            Next
        </ul>
    Next
     <div class="totalbox">
            <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Order Total: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.SubTotal)
                </div>
            </div>
         <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Coupon Code: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.DiscountTotal * -1, , , TriState.False, TriState.True)
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Processing Fee: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.FeePrice)
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Tax: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.TaxTotal)
                </div>
            </div>
         <div class="row">
             <div class="col-xs-10" style="text-align: right;">
                 Tip: 
             </div>
             <div class="col-xs-2" style="text-align: right;">
                 @FormatCurrency(Model.Ticket.DeliveryPrice)
             </div>
         </div>
            <div class="row">
                <h2 class="pull-right">Total: @FormatCurrency(Model.Ticket.TotalPrice)</h2>
            </div>
        </div>
    </div>
    <div>
         <a  href="/" class="btn btn-success pull-right">Return to Home Page <span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
</div>

