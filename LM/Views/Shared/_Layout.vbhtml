﻿<!DOCTYPE html>
<html>
<head>
    <title>Local Munchies @ViewData("Title")</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <meta name="description" content="Kent's Complete restaurant guide for all your needs">
    <meta name="msvalidate.01" content="3A44CC5B8FAC22086E142202AEC73DFF" />


  <link href="/Content/css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="/Content/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="/Content/Site.css" rel="stylesheet" media="screen" />

    <script src="/Content/js/jquery-1.9.1.min.js"></script>
    <script src="/Content/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="/Content/js/bootstrap.min.js"></script>

@*  @System.Web.Optimization.Styles.Render("~/Content/css")
    @System.Web.Optimization.Scripts.Render("~/bundles/jquery")*@

    
    
    <link rel="shortcut icon" href="/favicon.ico" >


    
    @If (Not HttpContext.Current.IsDebuggingEnabled) Then
        @<script>
             <!-- Google Analytics -->
             (function (i, s, o, g, r, a, m) {
                 i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                     (i[r].q = i[r].q || []).push(arguments)
                 }, i[r].l = 1 * new Date(); a = s.createElement(o),
                 m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
             })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

             ga('create', 'UA-37772204-1', 'auto');
             ga('send', 'pageview');
             
             

        </script>
        
        @<script type="text/javascript" id="inspectletjs">
             window.__insp = window.__insp || [];
             __insp.push(['wid', 739775061]);
             (function () {
                 function __ldinsp() { var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
                 if (window.attachEvent) {
                     window.attachEvent('onload', __ldinsp);
                 } else {
                     window.addEventListener('load', __ldinsp, false);
                 }
             })();
        </script>

        
    End If

    
</head>

<body style>
    <div class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header nav navbar-nav">
                <div class="navbar-form">
                <a class="navbar-logo brand"  style="display:inline;"  href="/">
                    <img class="hidden-xs"src="/Content/img/LMLogo.png" />
                    <img class="visible-xs"src="/Content/img/MobileLogo.png" style="padding-left:5px;" />
                </a>
                
                 <div class="btn-group">
    <button type="button" class="btn navbar-btn dropdown-toggle" data-toggle="dropdown">
        <span class="glyphicon glyphicon-map-marker before"></span>
        </span>
    </button>
                        @Html.Action("DropDown","Location")
          </div>
                <a class="btn navbar-btn" href="@Url.Action("Index", "Cart", Nothing)" >
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                </a>

                <div class="btn-group" style="z-index:3000;">
                    <button type="button" class="btn navbar-btn dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user">
                            </span>
                    </button>
                    <ul class="dropdown-menu pull-right" style="z-index:3000;">
                         @Html.Action("LoginHeaders","Account")
                     </ul>
                 </div>

                <div class="btn-group">
                    <button type="button" class="btn navbar-btn dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        </span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="@Url.Action("About", "Home")">About</a></li>
                        <li><a href="@Url.Action("Index", "FAQ")">FAQ</a></li>
                        <li><a href="@Url.Action("Index", "Contactus")">Contact</a></li>
                        <li><a href="https://www.facebook.com/localmunchies" target="_blank">Facebook</a></li>
                        <li><a href="https://twitter.com/LocalMunchies" target="_blank">Twitter</a></li>
                        <li><a href="http://instagram.com/localmunchies" target="_blank">Instagram</a></li>
                    </ul>
                </div>
                    </div>
            </div>
            <!-- /.nav-collapse -->
        </div>
        <!-- /.container -->
    </div>

    <div class="container">
        @RenderBody()
        <footer>
            <hr>
            <div class="row">
                <div class="col-xs-4"><p>&copy; Local Munchies 2014</p></div>
                <div class="col-xs-4">
                    <ul class="list-unstyled">
                        Social
                        <li><a href="https://www.facebook.com/localmunchies" target="_blank">Facebook</a></li>
                        <li><a href="https://twitter.com/LocalMunchies" target="_blank">Twitter</a></li>
                        <li><a href="http://instagram.com/localmunchies" target="_blank">Instagram</a></li>
                    </ul>
                </div>
                <div class="col-xs-4">
                    <ul class="list-unstyled">
                        Site
                        <li><a href="@Url.Action("About", "Home")">About</a></li>
                        <li><a href="@Url.Action("Index", "FAQ")">FAQ</a></li>
                        <li><a href="@Url.Action("Index", "Contactus")">Contact</a></li>
                        <li><a href="@Url.Action("Index", "Location")">Locations</a></li>
                        <li><a href="@Url.Action("Privacy","Home")">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            
        </footer>
    </div>






</body>
</html>

