﻿@ModelType LM.LMMenu

@Code
    ViewData("Title") = "Create"
End Code

<h2>Create</h2>


@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenu</legend>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.RestaurantID, "Restaurant")
        </div>
        <div class="editor-field">
            @Html.DropDownList("RestaurantID", String.Empty)
            @Html.ValidationMessageFor(Function(model) model.RestaurantID)
        </div>
        
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.OnlineOrderingActive)
        </div>
        <div class="editor-field">
         @Html.EditorFor(Function(model) model.OnlineOrderingActive)
            @Html.ValidationMessageFor(Function(model) model.OnlineOrderingActive)
        </div>


        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index")
</div>
