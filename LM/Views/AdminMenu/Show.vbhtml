﻿@modeltype LM.LMMenu
@Code
    ViewData("Title") = "Show"
End Code

<h2>@Model.Restaurant.Name - @Model.Name</h2>

<div class="menushowedit">
    @For Each Cat In Model.MenuCategories.OrderBy(Function(z) z.DisplayOrder)
        @<div class="menushowedit">
            <h2><a href="@Url.Action("Index","AdminMenuCategory",New With {.menuid=Cat.MenuID})">@Cat.Name</a> <small>@Cat.Description</small></h2>
            @For Each Group In Cat.MenuGroups.OrderBy(Function (z) z.DisplayOrder)
                @<div class="menushowedit">
                    <h3><a href="@Url.Action("Index", "AdminMenuGroup", New With {.catid = Group.MenuCategoryID})">@Group.Name</a><small>@Group.Description</small></h3>
                    @For Each Item In Group.MenuItems
                        @<div class="menushowedit">
                            <h4><a href="@Url.Action("Index", "AdminMenuItem", New With {.groupid = Item.MenuGroupID})">@Item.Name</a> <small>@Item.Description</small> @iif(Item.Price<>0,FormatCurrency(Item.Price),"")</h4>
                            @For Each ModifierGroup In Item.Modifiers
                                @<div class="menushowedit">
                                    <h5>
                                        @For Each ModifierItem In ModifierGroup.MenuModifierGroup.Modifiers
                                            @<span><a href="@Url.Action("Index", "AdminMenuItemModifier", New With {.itemid = ModifierGroup.MenuItemID})">@ModifierItem.Name </a><strong>@IIf(ModifierItem.Price <> 0, FormatCurrency(ModifierItem.Price), "")  @iif(ModifierItem.Extra_Price<> 0, "(+ " & FormatCurrency(ModifierItem.Extra_Price) & ")","")</strong> </span>
                                        Next
                                    </h5>
                                </div>
                            Next
                        </div>
                    Next
                </div>
            Next
        </div>
    Next
</div>
