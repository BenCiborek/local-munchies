﻿@ModelType IEnumerable(Of LM.LMMenu)

@Code
    ViewData("Title") = "Index"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th>
            Name
        </th>
<th></th>
        <th>
            Active
        </th>
        <th>
            Restaurant
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
           @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            <a href="@Url.Action("Index", "AdminMenuCategory", New With {.menuid = item.ID})">Menu Categories</a>
            |
            <a href="@Url.Action("Index", "AdminMenuModifierGroup", New With {.menuid = item.ID})">Menu Modifiers</a>
            |
            <a href="@Url.Action("Edit", "AdminRestaurantInfo", New With {.restid = item.RestaurantID})">Order Info</a>
            |
            <a href="@Url.Action("Index", "AdminOrderingOption", New With {.id = item.RestaurantID})">Ordering Options</a>

        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Active)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Restaurant.Name)
        </td>
        <td>
            @Html.ActionLink("Show", "Show", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
