﻿@Code
    ViewData("Title") = " | Error"
End Code

<div class="jumbotron">
  <h1>Ooops there was an error!</h1>
  <p>We are working on fixing this problem. Please click below to start over.</p>
  <p><a class="btn btn-primary btn-lg" href="/" role="button">Try Again</a></p>
</div>
