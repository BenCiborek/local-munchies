﻿@ModelType LM.LMMenuModifierGroup

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuModifierGroup</legend>

           @Html.HiddenFor(Function(model) model.ID)
            @Html.HiddenFor(Function(model) model.ModDate)
            @Html.HiddenFor(Function(model) model.CreateDate)
                    @Html.HiddenFor(Function(model) model.MenuID, "Menu")
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Exclusive)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Exclusive)
            @Html.ValidationMessageFor(Function(model) model.Exclusive)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Required)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Required)
            @Html.ValidationMessageFor(Function(model) model.Required)
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.menuid = Model.MenuID})
</div>
