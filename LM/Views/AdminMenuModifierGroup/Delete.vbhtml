﻿@ModelType LM.LMMenuModifierGroup

@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>LMMenuModifierGroup</legend>

    <div class="display-label">moddate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.moddate)
    </div>

    <div class="display-label">createdate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.createdate)
    </div>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">Exclusive</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Exclusive)
    </div>

    <div class="display-label">Required</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Required)
    </div>

    <div class="display-label">Menu</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Menu.Name)
    </div>
</fieldset>
@Using Html.BeginForm()
    @<p>
        <input type="submit" value="Delete" /> |
        @Html.ActionLink("Back to List", "Index", New With {.menuid = Model.MenuID})
    </p>
End Using
