﻿@ModelType LM.LMMenuModifierGroup

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>LMMenuModifierGroup</legend>

    <div class="display-label">moddate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.moddate)
    </div>

    <div class="display-label">createdate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.createdate)
    </div>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">Exclusive</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Exclusive)
    </div>

    <div class="display-label">Required</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Required)
    </div>

    <div class="display-label">Menu</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Menu.Name)
    </div>
</fieldset>
<p>

    @Html.ActionLink("Edit", "Edit", New With {.id = Model.ID}) |
    @Html.ActionLink("Back to List", "Index")
</p>
