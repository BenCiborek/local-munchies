﻿@ModelType LM.LMMenuModifierGroup

@Code
    ViewData("Title") = "Create"
End Code

<h2>Create</h2>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuModifierGroup</legend>

         <div class="editor-field">
            <input id="ModDate" name="ModDate" type="hidden" value="@Now()" />
            <input id="CreateDate" name="CreateDate" type="hidden" value="@Now()" />
            <input id="MenuID" name="MenuID" type="hidden" value="@ViewBag.menuid" />
        </div>
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Exclusive)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Exclusive)
            @Html.ValidationMessageFor(Function(model) model.Exclusive)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Required)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Required)
            @Html.ValidationMessageFor(Function(model) model.Required)
        </div>
        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.menuid = ViewBag.menuid})
</div>
