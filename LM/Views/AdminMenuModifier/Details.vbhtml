﻿@ModelType LM.LMMenuModifier

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>LMMenuModifier</legend>

    <div class="display-label">ModDate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.ModDate)
    </div>

    <div class="display-label">CreateDate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.CreateDate)
    </div>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Price)
    </div>

    <div class="display-label">Extra_Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Extra_Price)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">MenuModifierGroup</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuModifierGroup.Name)
    </div>
</fieldset>
<p>

    @Html.ActionLink("Edit", "Edit", New With {.id = Model.ID}) |
    @Html.ActionLink("Back to List", "Index", New With {.groupid = Model.MenuModifierGroupID})
</p>
