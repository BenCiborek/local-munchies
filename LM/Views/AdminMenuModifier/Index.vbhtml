﻿@ModelType IEnumerable(Of LM.LMMenuModifier)

@Code
    ViewData("Title") = "Menu Modifier"
    Dim aryurl As Array = Split(ViewBag.BreadCrumbURLS, ",")
    Dim aryName As Array = Split(ViewBag.BreadCrumbNames, ",")
    Dim x = 0
End Code

<h2>Menu Modifier</h2>

<ol class="breadcrumb">
    @While x < aryurl.Length
        @<li><a href="@aryurl(x)">@aryName(x)</a></li>
        x += 1
    End While
</ol>

<p>
    @Html.ActionLink("Create New", "Create", New With {.groupid = ViewBag.MenuModifierGroupID})
</p>
<table class="table">
    <tr>
        <th>
            Name
        </th>
        <th>
            Price
        </th>
        <th>
            Extra_Price
        </th>
        <th>
            Active
        </th>
        <th>Selected</th>
        <th>
            MenuModifierGroup
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
             <a href="@Url.Action("Index", "AdminMenuL2ModifierGroup", New With {.Modid = currentItem.ID})"> @Html.DisplayFor(Function(modelItem) currentItem.Name)</a>
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Price)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Extra_Price)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Active)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Selected)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MenuModifierGroup.Name)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
