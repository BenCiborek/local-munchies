﻿@ModelType LM.LMMenuGroup

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>LMMenuGroup</legend>

    <div class="display-label">ModDate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.ModDate)
    </div>

    <div class="display-label">CreateDate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.CreateDate)
    </div>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

        <div class="display-label">Description</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Description)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">DisplayOrder</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.DisplayOrder)
    </div>

    <div class="display-label">MenuCategory</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuCategory.Name)
    </div>
</fieldset>
<p>

    @Html.ActionLink("Edit", "Edit", New With {.id = Model.ID}) |
    @Html.ActionLink("Back to List", "Index", New With {.catid = Model.MenuCategoryID})
</p>
