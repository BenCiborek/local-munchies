﻿@modeltype LM.ControlTypeVM
@Code
    Layout = Nothing
End Code
<select name="ControlTypeID">
    @For Each item In Model.Controls
        @<option value="@item.ID" @IIf(item.ID = Model.Selected,"selected","")>@item.Name</option>
    Next
</select>
