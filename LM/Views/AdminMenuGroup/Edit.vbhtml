﻿@ModelType LM.LMMenuGroup

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>


@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuGroup</legend>

             <input id="ModDate" name="ModDate" type="hidden" value="@Now()" />
            <input id="MenuCategoryID" name="MenuCategoryID" type="hidden" value="@Model.MenuCategoryID" />
        @Html.HiddenFor(Function(model) model.ID)
            @Html.HiddenFor(Function(model) model.CreateDate)

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

                <div class="editor-label">
            @Html.LabelFor(Function(model) model.Description)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Description)
            @Html.ValidationMessageFor(Function(model) model.Description)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.DisplayOrder)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.DisplayOrder)
            @Html.ValidationMessageFor(Function(model) model.DisplayOrder)
        </div>
        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.catid = Model.MenuCategoryID})
</div>
