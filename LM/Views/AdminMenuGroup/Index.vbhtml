﻿@ModelType IEnumerable(Of LM.LMMenuGroup)

@Code
    ViewData("Title") = "Menu Groups"
    Dim aryurl As Array = Split(ViewBag.BreadCrumbURLS, ",")
    Dim aryName As Array = Split(ViewBag.BreadCrumbNames, ",")
    Dim x = 0
End Code

<h2>Menu Groups</h2>
<ol class="breadcrumb">
    @While x < aryurl.Length
        @<li><a href="@aryurl(x)">@aryName(x)</a></li>
        x += 1
    End While
</ol>
<p>
    @Html.ActionLink("Create New", "Create", New With {.catid = ViewBag.MenuCategoryID})
</p>
<table class="table">
    <tr>
        <th>
            Name
        </th>
        <th>Description</th>
        <th>
            Active
        </th>
        <th>
            Display Order
        </th>
        <th>
            Menu Category
        </th>
        <th></th>
    </tr>

@For Each item In Model.OrderBy(Function(l) l.DisplayOrder)
    Dim currentItem = item
    @<tr>
        <td>
           <a href="@Url.Action("Index", "AdminMenuItem", New With {.groupid = currentItem.ID})">@Html.DisplayFor(Function(modelItem) currentItem.Name)</a>
        </td>
        <td>
             @Html.DisplayFor(Function(modelItem) currentItem.Description)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Active)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.DisplayOrder)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MenuCategory.Name)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
