﻿@Code
    Layout = Nothing
End Code

<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F4F4F4; border-top: 1px solid #FFFFFF; border-collapse: collapse;">
    <tr>
        <td valign="top" class="footerContent" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 150%; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; text-align: left;">
            <a href="http://twitter.com/LocalMunchies" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #606060; font-weight: normal; text-decoration: underline;">Follow on Twitter</a>&nbsp;&nbsp;&nbsp;
                                                <a href="http://www.facebook.com/localmunchies" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #606060; font-weight: normal; text-decoration: underline;">Friend on Facebook</a>&nbsp;&nbsp;&nbsp;
                                                <a href="http://instagram.com/localmunchies" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #606060; font-weight: normal; text-decoration: underline;">Follow On Instagram</a>&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td valign="top" class="footerContent" style="padding-top: 20px; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 150%; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; text-align: left;">
            <em>Copyright &copy; @Now.Year Local Munchies LLC, All rights reserved.</em>
            <br>
        </td>
    </tr>
</table>
