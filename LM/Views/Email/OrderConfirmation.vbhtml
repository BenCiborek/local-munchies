﻿@modeltype LM.ReceiptVM
@Code
    Layout = "~/Views/Shared/_Email.vbhtml"
    Dim ordertime As Date = TimeZoneInfo.ConvertTimeFromUtc(Model.Ticket.ClosingDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))

End Code
<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;background-color: #DEE0E2;border-collapse: collapse;height: 100%;width: 100%;">
            	<tr>
                	<td align="center" valign="top" id="bodyCell" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 20px;border-top: 4px solid #BBBBBB;height: 100%;width: 100%;">
                    	<!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border: 1px solid #BBBBBB;border-collapse: collapse;">
                        	<tr>
                            	<td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                	<!-- BEGIN PREHEADER // -->
                                    @Html.Action("Header", "Email", New With {.message = "Your order has been placed and should be ready in 10-20 minutes."})
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;border-collapse: collapse;">
                                        <tr>
                                            <td valign="top" class="headerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #505050;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: left;vertical-align: middle;">
                                            	<img src="http://localmunchies.com/content/img/LMlogo600.jpg" style="max-width: 600px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" id="headerImage" >
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;border-collapse: collapse;">
                                        <tr>
                                            <td valign="top" class="bodyContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #505050;font-family: Helvetica;font-size: 14px;line-height: 150%;padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;text-align: left;">
                                                <h1 style="display: block;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #202020;">Order Number: @Model.Ticket.id</h1>

                                                <h1 style="display: block;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #202020;">Confirmation Number: @Right(Model.Ticket.ID, 2)</h1>
                                                <h4 style="display: block;font-family: Helvetica;font-size: 14px;font-style: italic;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #808080;">You must present this number when picking up your meal.</h4>
                                                <h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #404040;">Order Time: @ordertime</h2>
                                                <h4 style="display: block;font-family: Helvetica;font-size: 14px;font-style: italic;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #808080;">
                                                    @If Model.Ticket.TicketTypeID = 1 Then
                                                        @<span>You can expect your order to be ready for pick up in 15 - 20 minutes.</span>
                                                    Else
                                                         @<span>You can expect your order to be delivered in 45 - 60 minutes.</span>
                                                    End If
                                                </h4>
                                                <table  align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color: #F4F4F4;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;border-collapse: collapse;height: 100%;width: 100%;">
                                                <tr>
                                                        <td>
                                                            <h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #404040;">Billing Address</h2>
                                                            <address>
                                                               @Model.Billto.Name<br />
                                                                @Model.Billto.Address1<br />
                                                                @Model.Billto.Address2<br />
                                                                @Model.Billto.City, @Model.Billto.State @Model.Billto.Zip
                                                            </address>
                                                        </td>
                                                        <td>
                                                             @If Model.Ticket.TicketTypeID = 2 Then
                                                            @<div>
                                                            <h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #404040;">Delivery Address</h2>
                                                            <address>
                                                               @Model.Deliverto.Name<br />
                                                                @Model.Deliverto.Address1<br />
                                                                @Model.Deliverto.Address2<br />
                                                                @Model.Deliverto.City, @Model.Deliverto.State @Model.Deliverto.Zip
                                                            </address>
                                                             </div>
                                                             End If
                                                        </td>
                                                    </tr>

                                                    </table>
                                                <hr  style="background-color: #F4F4F4;"/>

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color: #F4F4F4;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0; border-collapse: collapse;height: 100%;width: 100%;">
                                                  @For Each Restaurant In Model.Rests
                                                         @<tr><td colspan="100"><h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #404040;">@Restaurant.Name</h2></td></tr>


                                                    
                                                    For Each Item In Model.Ticket.ticketitems.Where(Function(y) y.RestaurantID = Restaurant.ID)
                                                        @<tr style="vertical-align:top;">
                                                            <td>@item.Name</td>
                                                            <td>
                                                               @For Each Group In item.ModifierGroups
                                                                    @<span style="font-weight:bold;">@Group.Name: </span>
                                                               For Each modifier In Group.Modifiers
                                                                    @<span>@modifier.Name </span>
                                                                  For Each l2group In modifier.TicketItemL2Groups
                                                                     @<br />
                                                                    @<span style="font-weight:bold;">@l2group.Name: </span>
                                                                      For Each l2mod In l2group.Modifiers
                                                                    @<span>@l2mod.Name @IIf(l2mod.Qty > 1,"x" & l2mod.Qty,"")</span>
                                                                      Next
                                                                  Next
                                                                  Next
                                                                    @<br />
                                                               Next
                                                            </td>
                                                            <td style="text-align:right;"> @FormatCurrency(item.TotalItemPrice)</td>
                                                         </tr>
                                                                                                            Next
                                                    Next
                                                    <tr>
                                                        <td colspan="100"><hr /></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td><td style="text-align:right;">
                                                             Order Total: 
                                                                 </td>
                                                        <td style="text-align:right;">
                                                             @FormatCurrency(Model.Ticket.SubTotal)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td><td style="text-align:right;">
                                                           Coupon: 
                                                                 </td>
                                                        <td style="text-align:right;">
                                                             @FormatCurrency(Model.Ticket.DiscountTotal * -1, , , TriState.False, TriState.True)
                                                        </td>
                                                    </tr>
                                                                                                      <tr>
                                                        <td></td><td style="text-align:right;">
                                                            Processing Fee: 
                                                                 </td>
                                                        <td style="text-align:right;">
                                                            @FormatCurrency(Model.Ticket.FeePrice)
                                                        </td>
                                                    </tr>
                                                                                                      <tr>
                                                        <td></td><td style="text-align:right;">
                                                            Tax: 
                                                                 </td>
                                                        <td style="text-align:right;">
                                                              @FormatCurrency(Model.Ticket.TaxTotal)
                                                        </td>
                                                    </tr>
                                                                      <tr>
                                                        <td></td><td style="text-align:right;">
                                                            Tip:  
                                                                 </td>
                                                        <td style="text-align:right;">
                                                             @FormatCurrency(Model.Ticket.DeliveryPrice)
                                                        </td>
                                                                                                      <tr>
                                                        <td></td><td style="text-align:right;">
                                                            Order Total:  
                                                                 </td>
                                                        <td style="text-align:right;">
                                                             @FormatCurrency(Model.Ticket.TotalPrice)
                                                        </td>
                                                    </tr>
                                                </table>
                                                 <br>
                                                <br>
                                                <h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #404040;">Follow Us!</h2>
                                                @*<h4 style="display: block;font-family: Helvetica;font-size: 14px;font-style: italic;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #808080;">Make your email easy to read</h4>*@
                                                Be sure and connect with us on <a href="http://www.facebook.com/localmunchies">Facebook</a>, <a href="http://twitter.com/LocalMunchies">Twitter</a> and <a href="http://instagram.com/localmunchies">Instagram</a> for Monthly contest, weekly deals and daily specials.
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                	<!-- BEGIN FOOTER // -->
                                   @Html.Action("Footer","Email")
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>

