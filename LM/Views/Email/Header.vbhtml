﻿@modeltype LM.EmailHeaderVM
@Code
    ViewData("Title") = "Header"
    Layout = Nothing
End Code
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F4F4F4; border-bottom: 1px solid #CCCCCC; border-collapse: collapse;">
    <tr>
        <td valign="top" class="preheaderContent" style="padding-top: 10px; padding-right: 20px; padding-bottom: 10px; padding-left: 20px; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 125%; text-align: left;">@Model.Message
        </td>
        <!-- *|IFNOT:ARCHIVE_PAGE|* -->
      @*  <td valign="top" width="180" class="preheaderContent" style="padding-top: 10px; padding-right: 20px; padding-bottom: 10px; padding-left: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 125%; text-align: left;">Email not displaying correctly?<br>
            <a href="*|ARCHIVE|*" target="_blank" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #606060; font-weight: normal; text-decoration: underline;">View it in your browser</a>.
        </td>*@
        <!-- *|END:IF|* -->
    </tr>
</table>


