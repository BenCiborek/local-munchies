﻿@modeltype LM.AdminOrderOptionsVM
@Code
    ViewData("Title") = "Admin | Ordering Options"
End Code
<a href="@Url.Action("Index", "AdminMenu")"> < Back </a>

<h2>Ordering Options</h2>
@Using Html.BeginForm()
    @<div class="row">
        <div class="col-xs-10">
            <input type="hidden" name="RestaurantID" id="RestaurantID" value ="@Model.Restaurant.ID" />
            <select class="form-control" name="OrderOption" id="OrderOption">
                <option value="">Select A Value</option>
                @For Each item In Model.OrderingOptions.ToList()
                    @<option value="@item.id">@item.name</option>
                Next
            </select>
        </div>
        <div class="col-xs-2">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

        
    </div>
End Using
<br />
<br />
<table class="table">
    <tr>
        <th></th>
        <th>
            Name
        </th>
    </tr>

@For Each item In Model.Restaurant.OrderOptions.ToList()
    @<tr>
        <td>@Html.ActionLink("Remove","delete",New With {.Restaurantid = Model.Restaurant.ID, .OrderOption = item.id})</td>
        <td>
            @item.name
        </td>
    </tr>
Next

</table>
