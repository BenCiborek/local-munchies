﻿
@Code
    ViewData("Title") = " | Shopping Cart"
End Code

@*Partial For header Steps*@
@Html.Action("_checkoutheader","Checkout",New With {.checkoutStep = 1})
<div id="cart">
    @Html.Action("ShowCart", "Cart")
</div>


<script>
    $(function () {
        $('.navbar-search').hide()

    })
    $('#btnDelivery').click(function (e) {
        e.preventDefault()
        $('#orderType').modal('hide')
        try {
            ga('send', 'event', 'cart', 'ordertype', '2', 1);
            //_trackEvent('search', 'filter', $('input[name=ordertype]:checked').val(), 1, true)
        }
        catch (err) {
            //Skip Error
        }
        updateTicketType($("#TicketID").val(), '2')
    })
    $('#btnTakeout').click(function (e) {
        e.preventDefault()
        $('#orderType').modal('hide')
        try {
            ga('send', 'event', 'cart', 'ordertype', '1', 1);
            //_trackEvent('search', 'filter', $('input[name=ordertype]:checked').val(), 1, true)
        }
        catch (err) {
            //Skip Error
        }
        updateTicketType($("#TicketID").val(), '1')
    })
</script>
