﻿@Modeltype LM.ShoppingCartVM
@Code
    Layout = Nothing
End Code

@If Model.ItemCount < 1 Then
    @<div>
        <h2>Your cart is empty</h2>
        <a class="btn btn-default" href="/"><span class="glyphicon glyphicon-chevron-left"></span>Continue Ordering</a>
    </div>
Else
    @<div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                @*<div class="@IIf(Model.DeliveryAvailable, "", "hidden")">
                    <h4>Is this order for take out or delivery? </h4>
                    <div class="btn-group btn-block " data-toggle="buttons" id="OrderType">
                        <label class="ordertype  btn btn-info btn-sm @IIf(Model.Ticket.TicketTypeID = 2, "active", "")"  style="width: 50%">
                            <input type="radio" @IIf(Model.Ticket.TicketTypeID = 2, "checked", "") name="ordertype" id="delivery" value="2">
                            Delivery 
                        </label>
                        <label class="ordertype btn btn-warning btn-sm @IIf(Model.Ticket.TicketTypeID = 1, "active", "")" style="width: 50%">
                            <input type="radio" @IIf(Model.Ticket.TicketTypeID = 1, "checked", "") name="ordertype" id="takeout" value="1">
                            Take Out
                        </label>
                    </div>
                </div>*@
            </div>
            <input type="hidden" id="TicketID" value="@Model.Ticket.ID" />
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h3>Order Type: @IIf(Model.Ticket.TicketTypeID = 1, "Take-Out", "Delivery")<small><a href="@Url.Action("Index","Cart")">(Change)</a></small></h3>
            </div>
        </div>
         @For Each Restaurant In Model.Rests
        @<h3>@Restaurant.Name</h3>
        @<ul class="list-group">
            @For Each Item In Model.Ticket.ticketitems.Where(Function(y) y.RestaurantID = Restaurant.ID)
                @<li class="list-group-item">
                    <div class="row">
                        <small>
                            <div class="col-xs-8">
                               <a href="#" class="text-danger removeitem" data-itemid="@Item.Id"><span class="glyphicon glyphicon-minus-sign"></span> </a>
                                  @Item.Name
                                <br />
                                <small class="text-muted">
                                    @For Each Group In Item.ModifierGroups
                                        @<span>@Group.Name: </span>
                                    For Each modifier In Group.Modifiers
                                        @<span>@modifier.Name </span>
                                        For Each l2group In modifier.TicketItemL2Groups
                                            @<br />
                                            @<span>@l2group.Name: </span>
                                        For Each l2mod In l2group.Modifiers
                                            @<span>@l2mod.Name @IIf(l2mod.Qty > 1,"x" & l2mod.Qty,"")</span>
                                        Next
                                    Next
                                Next
                                        @<br />
                                    Next
                                </small>
                            </div>
                            <div class="col-xs-4" style="text-align: right;">
                                @FormatCurrency(Item.TotalItemPrice)
                            </div>
                        </small>
                    </div>
                </li>
            Next
        </ul>
    Next
        <div class="totalbox">
            <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Order Total: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.SubTotal)
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 form-inline" style="text-align: right;">
                <div class="form-group">
                   Coupon Code: 
    <input type="text" class="form-control" id="txtCouponCode" placeholder="Coupon Code">
                    <button type="button" id="btnCouponCode" class="btn btn-primary btn-sm">Apply</button>
                </div>
                    </div>
                <div class="col-xs-2 @IIf(Model.Ticket.DiscountTotal <> 0, "text-danger", "") " style="text-align: right; vertical-align:bottom; " >
                    @FormatCurrency(Model.Ticket.DiscountTotal * -1, , , TriState.False, TriState.True)
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Processing Fee: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.FeePrice)
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10" style="text-align: right;">
                    Tax: 
                </div>
                <div class="col-xs-2" style="text-align: right;">
                    @FormatCurrency(Model.Ticket.TaxTotal)
                </div>
            </div>
            <div class="row">
                <h2 class="pull-right">Total: @FormatCurrency(Model.Ticket.TotalPrice)</h2>
            </div>
        </div>
    </div>
    @<div>
        <a class="btn btn-default" href="/"><span class="glyphicon glyphicon-chevron-left"></span>Continue Ordering</a>
        <a class="btn btn-success pull-right" href="@Url.Action("Login","Checkout",Nothing)">Place Order<span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    
    @<div class="modal" id="orderType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" style="text-align: center;">
                            <h3>How would you like to get your order?</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#" id="btnTakeout" class="btn btn-warning btn-block btn-large"><br />Pick it up<br /><br /></a>
                        </div>
                        <div class="col-xs-6">
                            @If Model.DeliveryAvailable Then
                                @<a href="#" id="btnDelivery" class="btn btn-info btn-block btn-large"><br />Deliver it to me!<br /><br /></a>
                            Else
                                @<a href="#" class="btn btn-info btn-block btn-large" disabled="disabled"><br />No Delivery for <br /> this Restaurant<br /><br /></a>
                            End If
                        </div>
                    </div>
                    <div style="margin-top:30px"></div>
                </div>
            </div>
        </div>
    </div>
     
    
          End If

<script>
    $(function () {
        $('#orderType').modal('hide')
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        @If Model.showOptions Then
            @:$('#orderType').modal('show')
        End If
        

        $('.removeitem').click(function (e) {
            e.preventDefault()
            $('#optionsModal').modal('show')
            removeitem($(this).attr("data-itemid"))
        })

        //$('#btnDelivery').click(function (e) {
        //    e.preventDefault()
        //    $('#orderType').modal('hide')
        //    try {
        //        ga('send', 'event', 'cart', 'ordertype', '2', 1);
        //        //_trackEvent('search', 'filter', $('input[name=ordertype]:checked').val(), 1, true)
        //    }
        //    catch (err) {
        //        //Skip Error
        //    }
        //    updateTicketType($("#TicketID").val(), '2')
        //})
        //$('#btnTakeout').click(function (e) {
        //    e.preventDefault()
        //    $('#orderType').modal('hide')
        //    try {
        //        ga('send', 'event', 'cart', 'ordertype', '1', 1);
        //        //_trackEvent('search', 'filter', $('input[name=ordertype]:checked').val(), 1, true)
        //    }
        //    catch (err) {
        //        //Skip Error
        //    }
        //    updateTicketType($("#TicketID").val(),'1')
        //})


        $("#OrderType").on('change', function (e) {
            try {
                ga('send', 'event', 'cart', 'ordertype', $('input[name=ordertype]:checked').val(), 1);
                //_trackEvent('search', 'filter', $('input[name=ordertype]:checked').val(), 1, true)
            }
            catch (err) {
                //Skip Error
            }
            updateTicketType($("#TicketID").val(),$('input[name=ordertype]:checked').val())
        });

        $('#btnCouponCode').click(function (e) {
                e.preventDefault()
            //$('#optionsModal').modal('show')
            tryCouponCode($("#TicketID").val(), $("#txtCouponCode").val())
            //removeitem($(this).attr("data-itemid"))
        })

    })


    function updateTicketType(ticketID, ticketTypeID) {
        $('#orderType').modal('hide')
        $.ajax({
            url: '/Cart/ChangeTicketType',
            data: {
                ticketID: ticketID,
                ticketTypeID: ticketTypeID
            },
            dataType: "html",
            type: 'POST',
            beforeSend: function (xhr) {
                $('#orderType').modal('hide')
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                //$('#cart').addClass('ajaxRefreshing');
                ////$('#searchMain').html('')
                ////$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                //$('#cart').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#orderType').modal('hide')
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $('#cart').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#cart').removeClass('ajaxRefreshing');
            },
            complete: function () {
                $('#cart').removeClass('ajaxRefreshing');
            }
        });
    }

    function removeitem(id) {
        $.ajax({
            type: 'POST',
            url: '/Cart/RemoveItemAJAX/' + id,
            //data: {
            //    strstatus: values
            //},
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                $('#cart').addClass('ajaxRefreshing');
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                $('#cart').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#cart').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#cart').removeClass('ajaxRefreshing');
            },
            complete: function () {
                $('#cart').removeClass('ajaxRefreshing');
            }
        });
    }

    function tryCouponCode(ticketID, code) {
        $.ajax({
            type: 'POST',
            url: '/Cart/AddCoupon',
            data: {
                ticketID: ticketID,
                Code: code
            },
            dataType: "html",
            beforeSend: function (xhr) {
                $('#cart').addClass('ajaxRefreshing');
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                $('#cart').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#cart').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#cart').removeClass('ajaxRefreshing');
            },
            complete: function () {
                $('#cart').removeClass('ajaxRefreshing');
            }
        });
    }


</script>
