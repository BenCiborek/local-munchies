﻿@Modeltype IEnumerable(Of LM.LMCategories)
@Code
    Layout = Nothing
    Dim restids As List(Of Integer) = ViewBag.restids
End Code

        <div class="row">
            @For Each item In Model.ToList.OrderByDescending(Function(x) x.Restaurants.Where(Function(y) restids.Contains(y.ID)).Count)
                @<div class="col-md-2 col-sm-4 col-xs-6"  style="margin-bottom:5px;">
                    <a class="btn  btn-default btn-block @IIf(ViewBag.selectedtype = item.id, "active btn-primary", "") catitem" id="@item.id" >
                        @item.name
                        @*<small style="color:@IIf(ViewBag.selectedtype = item.id, "#eeeeee", "#000")">(@item.Restaurants.Where(Function(f) restids.Contains(f.ID)).Count())</small>*@
                    </a>
                 </div>
            Next
        </div>

@*   <ul class="rightnav nav nav-pills nav-stacked">
        @For Each item In Model.ToList.OrderByDescending(Function(x)  x.Restaurants.Where(Function(y) restids.Contains(y.ID)).Count)
            @<li class="@IIf(viewbag.selectedcat=item.id,"active","")"><a><span class="catitem filteritem" id="@item.id">@item.Name <small style="color:@IIf(ViewBag.selectedcat = item.id, "#eeeeee", "#000")">(@item.Restaurants.Where(Function(f) restids.Contains(f.ID)).Count())</small></span><small @IIf(ViewBag.selectedcat = item.id, "", "hidden") class="pull-right remfilter remcat">&times;</small></a></li>
        Next
    </ul>*@

<!--/.well -->
