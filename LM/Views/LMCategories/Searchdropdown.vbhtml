﻿@Modeltype LM.CategorySearchdropdownVM
@Code
    Layout = ""
End Code
<select class="form-control  input-sm" id="category">
    <option value="0">All Restaurants</option>
    @For Each cat In Model.Categories
        @<option @IIf(model.SelectedCategory = cat.id,"selected","") value="@cat.id">@cat.Name</option>
    Next
</select>