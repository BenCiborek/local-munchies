﻿@ModelType IEnumerable(Of LM.LMMenuL3Modifier)

@Code
    ViewData("Title") = "L3 Menu Modifier"
    Dim aryurl As Array = Split(ViewBag.BreadCrumbURLS, ",")
    Dim aryName As Array = Split(ViewBag.BreadCrumbNames, ",")
    Dim x = 0
End Code
<h2>Menu L3 Modifier</h2>

<ol class="breadcrumb">
    @While x < aryurl.Length
        @<li><a href="@aryurl(x)">@aryName(x)</a></li>
        x += 1
    End While
</ol>

<p>
    @Html.ActionLink("Create New", "Create", New With {.Modid = ViewBag.MenuModifierGroupID})
</p>
<table>
    <tr>
        <th>
            Name
        </th>
        <th>
            Extra_Price
        </th>
        <th>
            Active
        </th>
        <th>
            Selected
        </th>
        <th>
            DisplayOrder
        </th>
        <th>
            L2MenuModifier
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Extra_Price)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Active)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Selected)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.DisplayOrder)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.L2MenuModifier.Name)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
