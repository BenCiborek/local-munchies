﻿@ModelType LM.LMMenuL3Modifier

@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>LMMenuL3Modifier</legend>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Extra_Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Extra_Price)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">Selected</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Selected)
    </div>

    <div class="display-label">DisplayOrder</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.DisplayOrder)
    </div>

    <div class="display-label">L2MenuModifier</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.L2MenuModifier.Name)
    </div>
</fieldset>
@Using Html.BeginForm()
    @<p>
        <input type="submit" value="Delete" /> |
        @Html.ActionLink("Back to List", "Index")
    </p>
End Using
