﻿@ModelType LM.LMMenuL3Modifier

@Code
    ViewData("Title") = "Create"
End Code

<h2>Create</h2>

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuL3Modifier</legend>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Extra_Price)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Extra_Price)
            @Html.ValidationMessageFor(Function(model) model.Extra_Price)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Selected)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Selected)
            @Html.ValidationMessageFor(Function(model) model.Selected)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.DisplayOrder)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.DisplayOrder)
            @Html.ValidationMessageFor(Function(model) model.DisplayOrder)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.L2MenuModifierID, "L2MenuModifier")
        </div>
        <div class="editor-field">
            @Html.DropDownList("L2MenuModifierID", String.Empty)
            @Html.ValidationMessageFor(Function(model) model.L2MenuModifierID)
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index")
</div>
