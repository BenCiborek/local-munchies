﻿@ModelType LM.LMMenuL3Modifier

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>LMMenuL3Modifier</legend>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Extra_Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Extra_Price)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">Selected</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Selected)
    </div>

    <div class="display-label">DisplayOrder</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.DisplayOrder)
    </div>

    <div class="display-label">L2MenuModifier</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.L2MenuModifier.Name)
    </div>
</fieldset>
<p>

    @Html.ActionLink("Edit", "Edit", New With {.id = Model.ID}) |
    @Html.ActionLink("Back to List", "Index")
</p>
