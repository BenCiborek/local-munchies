﻿@modeltype LM.FB_QuestionView
@Code
    Layout = ""
    Dim ProgCent As Double = (Model.value / CDbl(Model.Metric + 1.0)) * 100
    Dim testtotal = 17
    Dim testpicked = CInt(Math.Ceiling(Rnd() * testtotal))
    Dim pickpercent = Math.Round((testpicked / CDbl(testtotal)) * 100,2)
End Code

<div class="panel panel-primary">
    <div class="panel-heading">@Model.Question</div>
    <div class="panel-body">
        <div class="progress progress-striped">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="@Model.value" aria-valuemin="0" aria-valuemax="@Model.Metric + 1.0" style="width: @ProgCent%;">
                @*<span class="sr-only">60% Complete</span>*@
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 .col-sm-4 .col-md-4 text-right">
                @Html.Action("ShowVotes", "Football", New With {.UserList = {"MIKE", "JOE", "PAT", "PAUL", "CHRIS"}, .type = 1, .pickedpercent = pickpercent})
            </div>
            <div class="col-xs-4 .col-sm-4 .col-md-4 text-center">
                <p>@Model.value of @CDbl(Model.Metric + 1.0)</p>
            </div>
            <div  class="col-xs-4 .col-sm-4 .col-md-4">
                @Html.Action("ShowVotes", "Football", New With {.UserList = {"MIKE", "JOE", "PAT", "PAUL", "CHRIS"}, .type = 2, .pickedpercent = pickpercent})
            </div>
        </div>
    </div>
</div>
