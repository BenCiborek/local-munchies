﻿@Modeltype List(Of LM.FB_QuestionView)
@Code
    ViewData("Title") = "Scoreboard"
End Code
<div class="jumbotron">
    <h2>SCORE BOARD</h2>
</div>
      <div>
        @For Each item In Model
           @<div class="row">
            @Html.Action("QuestionView", "Football", New With {.Question = item})
           </div>
        Next
    </div>
<script>
    $(function () {
        $(".votepop").popover({ placement: 'top', html: true });
    });
</script>