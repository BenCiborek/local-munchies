﻿@Modeltype LM.ContactUs
@Code
    ViewData("Title") = " | Contact Us"
End Code
<div class="jumbotron">
<h2>Contact Us <small>Let us know whats on your mind.</small></h2>

@Using (Html.BeginForm())
@<div>
  <div class="form-group">
    <label for="Name">Name</label>
    <input type="text" class="form-control" name="Name" id="Name" placeholder="Enter Name">
  </div>
  <div class="form-group">
    <label for="Email">Email Address</label>
    <input type="email" class="form-control" name="Email" id="Email" placeholder="Email Address">
  </div>
    <div class="form-group">
        <label for="Message">Message</label>
        <textarea  rows="5" class="form-control" name="Message" id="Message" placeholder="Message"></textarea>
    </div>
  <button type="submit" class="btn btn-default">Submit</button>
</div>
End Using

</div>
<div class="row">
    <div class="col-md-6">
        <div class="well">
            <h1>Mail
            </h1>
            <address>
                Local Munchies
                <br />
                P.O. Box 781
                <br />
                Hudson, OH 44236
            </address>
        </div>
    </div>
    <div class="col-md-6">
        <div class="well">
            <h1>Phone</h1>
            <span><a href="tel:+18559976808">1-855-997-6808</a></span>
        </div>
        </div>
</div>
<script>
    $(function () {
        $('.navbar-search').hide()
    })
</script>
