﻿@Modeltype list (of LM.Location)
@Code
    ViewData("Title") = "| Locations"
End Code

<h2>Locations</h2>

<ul>
    @For Each item In Model
        @<li>
            <a href="@Url.RouteUrl("Search", New With {.id = item.id})">@item.City, @item.State</a>
         </li>
    Next
</ul>