﻿@modeltype LM.LocationDropDownVM
@Code
    Layout = Nothing
End Code


    <ul class="dropdown-menu">
        @For Each item In Model.Locations
        @<li><a href="@Url.RouteUrl("Search", New With {.id = item.id})">@item.City, @item.State</a></li>
Next
        <li class="divider"></li>
        <li class="disabled"><a href="#">Other Locations Coming Soon</a></li>
    </ul>






