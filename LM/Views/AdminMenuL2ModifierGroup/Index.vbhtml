﻿@ModelType IEnumerable(Of LM.LMMenuL2ModifierGroup)

@Code
    ViewData("Title") = "Menu Modifier Groups"
    Dim aryurl As Array = Split(ViewBag.BreadCrumbURLS, ",")
    Dim aryName As Array = Split(ViewBag.BreadCrumbNames, ",")
    Dim x = 0
End Code

<h2>Menu Modifier Groups</h2>
<ol class="breadcrumb">
    @While x < aryurl.Length
        @<li><a href="@aryurl(x)">@aryName(x)</a></li>
        x += 1
    End While
</ol>
<p>
    @Html.ActionLink("Create New", "Create", New With {.Modid = ViewBag.Modid})
</p>
<table class="table">
    <tr>
       <th>
            Name
        </th>
        <th>
            Active
        </th>
        <th>
            Exclusive
        </th>
        <th>
            Required
        </th>
        <th>
            Modifier
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
           <a href="@Url.Action("Index", "AdminMenuL2Modifier", New With {.groupid = currentItem.ID})"> @Html.DisplayFor(Function(modelItem) currentItem.Name)</a>
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Active)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Exclusive)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Required)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Modifier.Name)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
