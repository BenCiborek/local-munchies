﻿@ModelType LM.LMMenuL2ModifierGroup

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuModifierGroup</legend>

           @Html.HiddenFor(Function(model) model.ID)
            @Html.HiddenFor(Function(model) model.ModDate)
            @Html.HiddenFor(Function(model) model.CreateDate)
            @Html.HiddenFor(Function(model) model.ModifierID, "ModifierID")
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Exclusive)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Exclusive)
            @Html.ValidationMessageFor(Function(model) model.Exclusive)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Required)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Required)
            @Html.ValidationMessageFor(Function(model) model.Required)
        </div>
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MaxQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MaxQty)
            @Html.ValidationMessageFor(Function(model) model.MaxQty)
        </div>
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.MinQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.MinQty)
            @Html.ValidationMessageFor(Function(model) model.MinQty)
        </div>
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.FreeQty)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.FreeQty)
            @Html.ValidationMessageFor(Function(model) model.FreeQty)
        </div>
                <div class="editor-label">
            Control Type
        </div>
        <div class="editor-field">
            @Html.Action("ControlDrop", New With {.selected = Model.ControlTypeID})
        </div>
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.SubQtyMax)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.SubQtyMax)
            @Html.ValidationMessageFor(Function(model) model.SubQtyMax)
        </div>
        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.Modid = Model.ModifierID})
</div>
