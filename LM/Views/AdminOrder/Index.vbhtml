﻿@Code
    ViewData("Title") = "| Admin | Orders"
End Code

<h2>Admin Order</h2>

<div>
    <a href="@Url.Action("CompletedOrderReport", "AdminOrder")">Completed Orders</a><br />
    <a href="@Url.Action("CouponCode")">Coupon Codes</a>
</div>
