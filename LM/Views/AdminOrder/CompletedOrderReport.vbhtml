﻿@modeltype List(Of LM.CompletedOrderResultVM)
@Code
    ViewData("Title") = "| Admin | CompletedOrderReport"
End Code

<h2>Completed Orders</h2>


<table class="table">
     <thead>
         <tr>
             <th>#</th>
             <th>Date</th>
             <th>Restaurant</th>
             <th>Bill To</th>
             <th>Deliver To</th>
             <th>Sub-Total</th>
             <th>Fees</th>
             <th>Total</th>
             <th></th>
         </tr>
     </thead>
    <tbody>
        @For Each orderresult In Model.OrderByDescending(Function(x) x.Ticket.ClosingDate)
            @<tr>
                <td>@orderresult.Ticket.ID</td>
                <td>@orderresult.Ticket.ClosingDate</td>
                <td>
                    @For Each restaurant In orderresult.Restaurants
                    @<span>@restaurant.Name </span>@<br />
                    Next
                </td>
                <td>
                    @orderresult.Billto.Email<br />
                    @orderresult.Billto.Name<br />
                    @orderresult.Billto.Address1<br />
                    @orderresult.Billto.City, @orderresult.Billto.State @orderresult.Billto.Zip<br />
                    @orderresult.Billto.phone
                </td>
                <td>
                    @If Not orderresult.Delivery Is Nothing Then
                        @<span>
                            @orderresult.Delivery.Name<br />
                            @orderresult.Delivery.Address1<br />
                            @orderresult.Delivery.City, @orderresult.Delivery.State @orderresult.Delivery.Zip
                         </span>
                    Else
                        @<span></span>

                    End If

                </td>
                <td>@FormatCurrency(orderresult.Ticket.SubTotal)</td>
                <td>@FormatCurrency((orderresult.Ticket.DeliveryPrice + orderresult.Ticket.FeePrice + orderresult.Ticket.TaxTotal))</td>
                <td>@FormatCurrency(orderresult.Ticket.TotalPrice)</td>
                <td>
                    <a target="_blank" href="@Url.Action("OrderConfirmation", "Email", New With {.id = orderresult.Ticket.ID})">View Email</a><br />
                    @For Each restaurant In orderresult.Restaurants
                    @<a target="_blank" href="@Url.Action("FaxTicket", "PDF", New With {.id = orderresult.Ticket.ID, .Restid = restaurant.ID})">@restaurant.Name - Fax</a>@<br />
                    Next

                </td>
             </tr>
        Next
    </tbody>
</table>
