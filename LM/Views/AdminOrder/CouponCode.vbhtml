﻿@modeltype List(Of LM.CouponCodes)
@Code
    ViewData("Title") = "Admin | Coupon Codes"
End Code

<h2>Coupon Code</h2>

<a href="@Url.Action("Index")"> < Back </a>

<h2>Coupon Codes</h2>
@Using Html.BeginForm()
    @<div class="row">
        <div class="col-sm-4 col-xs-12">
            <input type="hidden" name="Active" id="Active" value="True" />
            <label>Code: </label>
            <input class="form-control" type="text" name="Code" id="Code" />
            <label>Discount: </label>
            <input class="form-control" type="text" name="Discount" id="Discount" /><br />
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </div>
End Using
<br />
<br />
<table class="table">
    <tr>
        <th></th>
        <th>Code</th>
        <th>Active</th>
        <th>Discount</th>
    </tr>

@For Each item In Model
    @<tr>
        <td>@Html.ActionLink("Remove", "delete", New With {.id = item.ID})</td>
        <td>@item.Code</td>
        <td>@item.Active</td>
        <td>@FormatCurrency(item.Discount)</td>
    </tr>
Next

</table>

