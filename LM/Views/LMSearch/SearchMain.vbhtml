﻿@modeltype LM.SearchResultsVM
@Code
    Layout = Nothing
End Code
        <div class="row row-offcanvas row-offcanvas-left">
            <div class="col-xs-12 col-sm-9">
                <div id="searchresults">
                    @*@Html.Action("SearchResults", "LMSearch", New With {.Find = Model.Find, .Categorie = Model.Categorie, .OrderOption = Model.OrderOption, .restids = ViewBag.restids})*@
                    @If Model.Restaurants.Count() > 0 Then
                        @<table class="table table-hover">
                            @For Each item In Model.Restaurants.OrderByDescending(Function(x) x.Open).ThenByDescending(Function(z) z.DeliversToAddress).ThenByDescending(Function(y) y.Restaurant.OnlineOrdering).ThenBy(Function(LR) LR.Restaurant.LocalRank)
                                @<tr>
                                    <td>
                                        @Html.Action("Searchview", "LMRestaurant", New With {.Restaurant = item})
                                    </td>
                                </tr>
                            Next
                            <tr>
                                <td style="text-align:center;">
                                    <a class="clearSearch" style="padding:10px;cursor:pointer;">Show All Restaurants</a>
                                </td>
                            </tr>
                        </table>
                    Else
                        @<div>
                            <h2>No Results. Please search again.
                            </h2>
                        </div>
                    End If

                </div>
            </div>
                        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
               @*Marketing Space*@
            </div>
        </div>

        <!--/row-->

<script>
    $(function () {

      


        $('.clearSearch').on('click', function (e) {
            
            updateSearch(0, 0, '', 'True', '')
        });


//        $("input#txtFind").autocomplete({
//            source: function (request, response) {
//                // define a function to call your Action (assuming UserController)
//                $.ajax({
//url:             '/LMSearch/GetAutocomplete', type: "POST", dataType: "json",

//                    // query will be the param used by your action method
//                    data: { query: request.term },
//                    success: function (data) {
//                        response($.map(data, function (item) {
//                            return { label: item, value: item };
//                        }))
//                    }
//                })
//            }
//        });

    })
    </script>