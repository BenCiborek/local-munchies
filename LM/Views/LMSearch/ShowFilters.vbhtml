﻿@modeltype LM.LMSearchCriteria
@Code
    Layout = Nothing
End Code
<div class="well well-sm sidebar-nav">
    <form>
        @Html.Action("SearchView", "LMCategories", New With {.Find = Model.Find, .Categorie = Model.Categorie, .OrderOption = Model.OrderOption, .restids = ViewBag.restids})      
    </form>
</div>
