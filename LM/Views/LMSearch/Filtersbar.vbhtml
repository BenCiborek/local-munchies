﻿@modeltype lm.LMSearchCriteria
@Code
    ViewData("Title") = ""
    Layout = ""
End Code
<div>
    <nav class="navbar navbar-default filterbar" role="navigation" style="z-index:2;">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="row" style="margin: 0 5px;">
               @* <div class="col-xs-1 visible-xs" style="padding: 4px;">
                    <button type="button" class="btn btn-default btn-block btn-sm">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </div>*@
               
         @*Advanced Filters*@
                <div class="" id="advancedfilter">
                    <div class="col-xs-12 col-sm-12 col-md-6" style="padding: 4px;">
                        @Html.Action("OrderType", "LMSearch", New With {.selected = Model.OrderOption})
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6" style="padding: 4px;">
                        @Html.Action("Searchdropdown", "LMCategories", New With {.categorie = Model.Categorie, .restids = nothing})
                    </div>
                </div>
                @*<div class="col-xs-9 col-sm-4 col-md-2 hidden" style="padding: 4px;">
                    <input type="text" id="txtFind" value="@Model.Find" class="form-control  input-sm" placeholder="Search" style="display: inline;">
                </div>
                <div class="col-xs-3 col-sm-2 col-md-1 hidden" style="padding: 4px;">
                    <button type="submit" id="btnGO" class="btn btn-primary btn-block btn-sm">Search</button>
                </div>*@
            </div>
           @* <div class="row"  style="margin: 0 5px;">
                <div class="collapse" id="showAddress">
                    <div class="col-xs-8 col-sm-6 col-md-5" style="padding: 4px;">
                        <input type="text" id="txtAddress" value="" class ="form-control  input-sm" placeholder="Address: ie. Address, City, State" style="display:inline;" />
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2" style="padding: 4px;">
                        <button type="submit" id="btnAddress" class="btn btn-info btn-block btn-sm">Who Delivers?</button>
                    </div>
                </div>
            </div>*@
        </div>
    </nav>
</div>
<script>
  
    $(function () {

        $("#advancedfilter").on('change', function (e) {
            try {
                ga('send', 'event', 'search', 'filter', $('input[name=ordertype]:checked').val(), 1);
                //_trackEvent('search', 'filter', $('input[name=ordertype]:checked').val(), 1, true)
            }
            catch (err) {
                //Skip Error
            }
            //if ($('input[name=ordertype]:checked').val() == 1) {
            //    $('#showAddress').collapse('show')
            //}
            //else {
            //    $('#showAddress').collapse('hide')
            //}
           
            updateSearch($("#category").val(), $('input[name=ordertype]:checked').val(), escape($("#txtFind").val()),'True','')
        });

        $('#btnAddress').on('click', function (e) {
            try {
                ga('send', 'event', 'search', 'Address', escape($("#txtAddress").val()), 1);
                //_trackEvent('search', 'find',escape($("#txtFind").val()) , 1, true)
                //ga('send', 'event', 'search', 'click', 'nav-buttons', 1);
            }
            catch (err) {
                //Skip Error
            }
            updateSearch($("#category").val(), $('input[name=ordertype]:checked').val(), escape($("#txtFind").val()), 'True', escape($("#txtAddress").val()))
        })

        $('#btnGO').on('click', function (e) {
            //  alert($("#category").val() + $('input[name=ordertype]:checked').val() + escape($("#txtFind").val()))
            
            try {
                ga('send', 'event', 'search', 'find', escape($("#txtFind").val()), 1);
                //_trackEvent('search', 'find',escape($("#txtFind").val()) , 1, true)
                //ga('send', 'event', 'search', 'click', 'nav-buttons', 1);
            }
            catch (err) {
                //Skip Error
            }
            
            updateSearch($("#category").val(), $('input[name=ordertype]:checked').val(), escape($("#txtFind").val()), 'True','')
        });


//        $("input#txtFind").autocomplete({
//            source: function (request, response) {
//                // define a function to call your Action (assuming UserController)
//                $.ajax({
//url:             '/LMSearch/GetAutocomplete', type: "POST", dataType: "json",

//                    // query will be the param used by your action method
//                    data: { query: request.term },
//                    success: function (data) {
//                        response($.map(data, function (item) {
//                            return { label: item, value: item };
//                        }))
//                    }
//                })
//            }
//        });

    })

    //function searchsubmit() {
    //    $("#hdnFind").val($("#txtFind").val())
    //    updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), $("#hdnFind").val())
    //}

</script>
