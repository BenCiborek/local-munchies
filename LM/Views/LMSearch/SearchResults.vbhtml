﻿@modeltype LM.SearchResultsVM
@Code
    Layout = Nothing
End Code
@If Model.Restaurants.Count() > 0 Then
    @<table class="table table-hover">
    @For Each item In Model.Restaurants.OrderByDescending(Function(x) x.Open).ThenByDescending(Function(y) y.Restaurant.OnlineOrdering).ThenByDescending(Function(z) z.DeliversToAddress)
        @<tr>
            <td>   
                 @Html.Action("Searchview", "LMRestaurant", New With {.id = item.Restaurant.ID, .currentDate = Model.now, .open = item.Open, .deliverstome = item.DeliversToAddress})
            </td>
        </tr>
    Next
        <tr>
            <td>
                <a class="clearSearch">Show All Restaurants</a>
            </td>
        </tr>
</table>
Else
    @<div>
        <h2>
             No Results. Please search again.
        </h2>
     </div>
End If
<script>
    $(function () {

      


        $('#clearSearch').on('click', function (e) {
            //  alert($("#category").val() + $('input[name=ordertype]:checked').val() + escape($("#txtFind").val()))
            
            //try {
            //    ga('send', 'event', 'search', 'find', escape($("#txtFind").val()), 1);
            //    //_trackEvent('search', 'find',escape($("#txtFind").val()) , 1, true)
            //    //ga('send', 'event', 'search', 'click', 'nav-buttons', 1);
            //}
            //catch (err) {
            //    //Skip Error
            //}
            
            updateSearch(0, 0, '', 'False', '')
        });


//        $("input#txtFind").autocomplete({
//            source: function (request, response) {
//                // define a function to call your Action (assuming UserController)
//                $.ajax({
//url:             '/LMSearch/GetAutocomplete', type: "POST", dataType: "json",

//                    // query will be the param used by your action method
//                    data: { query: request.term },
//                    success: function (data) {
//                        response($.map(data, function (item) {
//                            return { label: item, value: item };
//                        }))
//                    }
//                })
//            }
//        });

    })
    </script>