﻿@Modeltype LM.LMSearchCriteria
@Code
    Layout = ""
End Code
<div class="row">
    <div class="col-xs-3" style="padding: 4px;">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target=".searchbar-cats">
            Filters
        </button>
    </div>
    <div class="col-xs-6" style="padding: 4px;">
        <input type="text" id="txtFind" value="@Model.Find" class="form-control" placeholder="Search" value="" style="display: inline;">
    </div>
    <div class="col-xs-3" style="padding: 4px;">
        <button type="submit" id="btnGO" class="btn btn-default btn-block">Submit</button>
    </div>
</div>

<div class="collapse pull-left searchbar-cats" style="width: 100%;">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <a class="btn btn-block btn-default" id="clearfilters">Clear Filters</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <span class="white">Ordering Options</span>
            @Html.Action("OrderingOptionsSearchView", "LMRestaurant", New With {.Find = Model.Find, .Categorie = Model.Categorie, .OrderOption = Model.OrderOption, .restids = ""})
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 10px; padding-right: 0px;">
            <span class="white">Categories</span>
            @Html.Action("SearchView", "LMCategories", New With {.Find = Model.Find, .Categorie = Model.Categorie, .OrderOption = Model.OrderOption, .restids = ""})
        </div>
    </div>
    <input type="hidden" value ="@Model.Categorie" id="hdnCat" />
    <input type="hidden" value ="@Model.OrderOption" id="hdnOpt" />
    <input type="hidden" value ="@Model.Find" id="hdnFind" />
</div>
<script>
  
    $(function () {

        $("#clearfilters").on('click', function (e) {
            $(".catitem").removeClass('active')
            $(".catitem").removeClass('btn-primary')
            $(".optitem").removeClass('active')
            $(".optitem").removeClass('btn-primary')
            $("#hdnOpt").val(0)
            $("#hdnCat").val(0)
            updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), escape($("#hdnFind").val()))
        });

        $('.catitem').on('click', function (e) {
            $("#hdnCat").val(e.target.id);
            $(".catitem").removeClass('active')
            $(".catitem").removeClass('btn-primary')
            $(e.target).addClass('active')
            $(e.target).addClass('btn-primary')
            //$(".catitem#" + e.target.id).addClass('active')
            //$(".catitem#" + e.target.id).addClass('btn-primary')
            updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), escape($("#hdnFind").val()))
        });
        $('.remcat').on('click', function (e) {
            $("#hdnCat").val(0)
            updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), escape($("#hdnFind").val()))
        });


        $('#btnGO').on('click', function (e) {
            $("#hdnFind").val($("#txtFind").val())
            updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), escape($("#hdnFind").val()))
        });
        $('.optitem').on('click', function (e) {
            $(".optitem").removeClass('active')
            $(".optitem").removeClass('btn-primary')
            $(e.target).addClass('active')
            $(e.target).addClass('btn-primary')
            //e.target.addClass('');
            $("#hdnOpt").val(e.target.id)
            updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), escape($("#hdnFind").val()))
        });
        $('.remopt').on('click', function (e) {
            $("#hdnOpt").val(0)
            updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), escape($("#hdnFind").val()))
        });

        $("input#txtFind").autocomplete({
            source: function (request, response) {
                // define a function to call your Action (assuming UserController)
                $.ajax({
                    url: '/LMSearch/GetAutocomplete', type: "POST", dataType: "json",

                    // query will be the param used by your action method
                    data: { query: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return { label: item, value: item };
                        }))
                    }
                })
            }
        });

    })

    //function searchsubmit() {
    //    $("#hdnFind").val($("#txtFind").val())
    //    updateSearch($("#hdnCat").val(), $("#hdnOpt").val(), $("#hdnFind").val())
    //}

    function updateSearch(selCat, selOpt, find) {
        $(".searchbar-cats").removeClass("in").addClass("collapse");
        //alert("cat:" + selCat + " Opt:" + selOpt + " Find:" + find)

        $.ajax({
            type: 'POST',
            url: '/LMSearch/SearchMain?find=' + find + "&cat=" + selCat + "&OrderOption=" + selOpt + "&LocationID=Kent-OH",
            //data: {
            //    strstatus: values
            //},
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                $('#searchMain').addClass('ajaxRefreshing');
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                $('#searchresults').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#searchMain').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#searchMain').removeClass('ajaxRefreshing');
            },
            complete: function () {
                $('#searchMain').removeClass('ajaxRefreshing');
            }
        });

    }
</script>
