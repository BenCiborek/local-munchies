﻿@Modeltype LM.OrderTypeSearchVM
@Code
    Layout = ""
End Code

<div class="btn-group btn-block" data-toggle="buttons">
    <label class="ordertype  btn btn-info btn-sm @IIf(Model.selected = 1, "active", "")"  style="width: 33%">
        <input type="radio" @IIf(Model.selected = 1, "checked", "") name="ordertype" id="delivery" value="1">
        Delivery 
    </label>
    <label class="ordertype btn btn-warning btn-sm @IIf(Model.selected = 5, "active", "")" style="width: 33%">
        <input type="radio" @IIf(Model.selected = 5, "checked", "") name="ordertype" id="takeout" value="5">
        Take Out
    </label>
   <label class="ordertype btn btn-primary btn-sm @IIf(Model.selected = 2, "active", "")" style="width: 33%">
        <input type="radio" @IIf(Model.selected = 2, "checked", "") name="ordertype" id="online" value="2">
        Online Ordering
    </label>
</div>
