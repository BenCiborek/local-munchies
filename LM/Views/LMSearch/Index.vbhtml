﻿@Modeltype LM.SearchIndexVM
@Code
End Code
<div class="">
    <div class="">
        <div id="filters">
            @Html.Action("Filtersbar","LMSearch")
        </div>
        <div id="SignUp">
            @Html.action("SignUp", "Account")
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12" style="text-align: center;">
                <h4>@Model.Location.City's Complete restaurant guide for all your needs</h4>
            </div>
        </div>

        <div id="searchMain" class="searchmain">

           @* @Html.Action("SearchMain", "LMSearch", New With {.find = Model.SearchCriteria.Find, .cat = Model.SearchCriteria.Categorie, .OrderOption = Model.SearchCriteria.OrderOption, .LocationID = Model.LocationID})*@
        </div>
        <div id="restaurantmain"></div>
    </div>
</div>
<!--/Container-->

<script>
    $(function () {
        var location = '@Model.Location.id'
        updateSearch(0, 2, '', 'False', '')
    })

    function updateSearch(selCat, selOpt, find, skipsession,address) {
        //$(".searchbar-cats").removeClass("in").addClass("collapse");
        //alert("cat:" + selCat + " Opt:" + selOpt + " Find:" + find)
        if (selCat == undefined) {
            selCat = 0
        }
        if (selOpt == undefined) {
            selOpt = 0
        }
        if (find == 'undefined') {
            find = ''
        }


        $.ajax({
            type: 'POST',
            url: '/LMSearch/SearchMain?find=' + find + "&cat=" + selCat + "&OrderOption=" + selOpt + "&LocationID=" + location + "&skipSession=" + skipsession + "&address=" + address,
            //data: {
            //    strstatus: values
            //},
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                $('#searchMain').addClass('ajaxRefreshing');
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                $('#searchMain').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#searchMain').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#searchMain').removeClass('ajaxRefreshing');
                $('#searchMain').html('<div>Error: ' + thrownError + '</div>')
            },
            complete: function () {
                $('#searchMain').removeClass('ajaxRefreshing');
            }
        });

    }

</script>

