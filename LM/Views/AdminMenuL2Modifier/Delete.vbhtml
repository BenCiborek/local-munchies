﻿@ModelType LM.LMMenuL2Modifier

@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>LMMenuModifier</legend>

    <div class="display-label">ModDate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.ModDate)
    </div>

    <div class="display-label">CreateDate</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.CreateDate)
    </div>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Price)
    </div>

    <div class="display-label">Extra_Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Extra_Price)
    </div>

    <div class="display-label">Active</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Active)
    </div>

    <div class="display-label">MenuModifierGroup</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.L2MenuModifierGroup.Name)
    </div>
</fieldset>
@Using Html.BeginForm()
    @<p>
        <input type="submit" value="Delete" /> |
        @Html.ActionLink("Back to List", "Index", New With {.groupid = Model.L2MenuModifierGroupID})
    </p>
End Using
