﻿@ModelType LM.LMMenuL2Modifier

@Code
    ViewData("Title") = "Create"
End Code

<h2>Create</h2>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuModifier</legend>

            <input id="ModDate" name="ModDate" type="hidden" value="@Now()" />
            <input id="CreateDate" name="CreateDate" type="hidden" value="@Now()" />
            <input id="L2MenuModifierGroupID" name="L2MenuModifierGroupID" type="hidden" value="@ViewBag.MenuModifierGroupID" />


       <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Price)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Price)
            @Html.ValidationMessageFor(Function(model) model.Price)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Extra_Price)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Extra_Price)
            @Html.ValidationMessageFor(Function(model) model.Extra_Price)
        </div>
                <div class="editor-label">
            @Html.LabelFor(Function(model) model.Selected)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Selected)
            @Html.ValidationMessageFor(Function(model) model.Selected)
        </div>
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Active)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Active)
            @Html.ValidationMessageFor(Function(model) model.Active)
        </div>
        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.Groupid = ViewBag.MenuModifierGroupID})
</div>
