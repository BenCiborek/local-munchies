﻿@Code
    ViewData("Title") = " | About Us"
End Code

<h3>
Local Munchies is the #1 local restaurant guide in your community, delivering tools and complete details on every restaurant at your campus.
    </h3>
<div class="list-group">
    <div class="list-group-item">
        <h2>Explore</h2>
        <p>
            Local Munchies is the best way to explore the restaurants around your campus, through our complete list of restaurants and menus. 
        </p>
    </div>
    <div class="list-group-item">
        <h2>Experience</h2>
        <p>
            Ready to experience your campus restaurants in a new way? Local Munchies not only delivers the necessary information you need, but in the near future you will also be able to place online orders and find specials and deals.
        </p>
    </div>
    <div class="list-group-item">
        <h2>Know</h2>
        <p>
            Local Munchies is committed to providing you with a listing of all the restaurants around your campus, along with all their important information. No need to jump site to site looking for information about a single restaurant. We've done it all for you.
        </p>
    </div>
    <div class="list-group-item">
        <h2>Future</h2>
        <p>
            Local Munchies is continuously growing and adding new features with a goal of delivering all the information you need about your local restaurant scene. Leave us some feedback about what you think. You’re our #1 priority!!!
        </p>
    </div>
</div>