﻿@Code
    ViewData("Title") = ""
End Code


<div class="row">
    <div class="col-sm-12" style="text-align: center;">
        <img src="http://localmunchies.com/content/img/LMlogo600.jpg" style="max-width: 600px; width: 100%; -ms-interpolation-mode: bicubic; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;" id="headerImage">
    </div>
</div>
<div class="row" style="margin-top: 50px;">
    <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4">
        <div class="btn-group btn-block">
            <button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown">
                Select A City  <span class="caret"></span>
            </button>
            @Html.Action("DropDown", "Location")
        </div>
    </div>
</div>
<div class="row" style="margin-top: 15px;">
    <div class="col-sm-3 col-sm-offset-3 col-lg-2 col-lg-offset-4">
        <div class="pull-left">

            <a href="https://itunes.apple.com/us/app/local-munchies/id899175394?mt=8&uo=4" target="itunes_store" style="display: inline-block; overflow: hidden; background: url(https://linkmaker.itunes.apple.com/htmlResources/assets/en_us//images/web/linkmaker/badge_appstore-lrg.png) no-repeat; width: 135px; height: 40px; {background-image: url(https://linkmaker.itunes.apple.com/htmlResources/assets/en_us//images/web/linkmaker/badge_appstore-lrg.svg); }"></a>
        </div>
    </div>
    <div class="col-sm-3 col-lg-2">
        <div class="pull-right">
            <a href="https://play.google.com/store/apps/details?id=com.localmunchies">
                <img alt="Get it on Google Play"
                    src="/content/img/Android.png" />
            </a>
        </div>
    </div>

</div>
<div style="height:100px;"></div>
<div style="width:100%;text-align:center;padding-bottom:25px;" >
    <h2>How It Works</h2>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4" style="height:100%;">
        <div class="panel panel-default sameheight">
            <div class="panel-heading ">
                <h4 class="text-primary">1. Select Your City</h4>
            </div>
            <div class="panel-body">
                Local Munchies is an online ordering restaurant guide that contains every restaurant in your city, complete with their menus. Click the dropdown to find your local city. 
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-default sameheight">
            <div class="panel-heading ">
                <h4 class="text-primary">2. Select a Restaurant</h4>
            </div>
            <div class="panel-body">
                Browse through all the local restaurants to find what you are looking for. You can filter the restaurants by food category or sort them by delivery method (Take-out or delivery.) All restaurants offering online ordering are highlighted in Blue. Click any restaurant to see their full menu.
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-default sameheight">
            <div class="panel-heading ">
                <h4 class="text-primary">3. Place Online Order</h4>
            </div>
            <div class="panel-body">
                Take your time looking over the restaurants menu, Once you’ve found the delicious food you want to order, simply add it to the cart by clicking on the item. Easy as that. Either head on over to the restaurant and pick up your food or sit back and wait for the doorbell to ring.
            </div>
        </div>
    </div>
</div>
<div class="jumbotron">
<div class="row">
    <div class="col-xs-12" >
        <h2 style="width:100%;text-align:center;padding-bottom:25px;">Online Ordering For Local Restaurants Made Easy</h2>
        <p style="font-size:14px;">Local Munchies is here to cover all your hunger needs. Feel free to look over our online ordering restaurant guide containing every restaurant in your city, complete with their menus. Local Munchies allows you to quickly order takeout or delivery from local restaurants through our website or mobile apps. Find your favorite restaurant and order away! Enjoy your online ordering experience with Local Munchies!</p>
    </div>
</div>
    </div>
<div class="row">
    <div class="col-xs-12" >
        <h2 style="width:100%;text-align:center;padding-bottom:25px;">We Need Your Help!</h2>
        <p>Looking for an opportunity to join a brand new start-up? You’re at the right place, Local Munchies is looking for young creative minds to join our team and create the next great tech company. <a href="http://goo.gl/forms/uzn70gDbRD">Click here</a> to apply today!</p>
    </div>
</div>

<script>
    boxes = $('.sameheight');
    maxHeight = Math.max.apply(
    Math, boxes.map(function () {
        return $(this).height();
    }).get());
    boxes.height(maxHeight);
</script>