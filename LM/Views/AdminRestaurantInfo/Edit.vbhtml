﻿@Modeltype LM.RestaurantInfo
@Code
    ViewData("Title") = "Admin Restaurant Info | Edit"
End Code

<h2>Edit</h2>

<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        @Using Html.BeginForm()
            @Html.EditorForModel(Model)
            @<button class="btn btn-primary">
                Submit
            </button>
        End Using
    </div>
</div>

<script src="/Content/FixForm.js"></script>

<script>
    $(function () {
        $("#RestaurantID").addClass("hidden");
        $("label[for = 'RestaurantID']").addClass("hidden");
    })
</script>