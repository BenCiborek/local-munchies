﻿@Modeltype LM.FaxTicketVM
@Code
    Layout = Nothing
    Dim EasternNow As DateTime = DateAdd(DateInterval.Minute, 20, System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")))

End Code
<itext creationdate="@DateTime.Now.ToString()" producer="RazorPDF">
<image url="@Context.Server.MapPath("~/Content/img/LMLogo.png")"/>
 
    <newline />
    <newline />
    <paragraph style="font-family:Helvetica;font-size:18;font-weight:bold;">
	<chunk red="0" green="0" blue="0">@Model.RestaurantName</chunk>
</paragraph>
    <newline />
    <paragraph style="font-family:Helvetica;font-size:18;font-weight:bold;">
	<chunk red="0" green="0" blue="0">@Model.OrderType</chunk>
</paragraph>
    <newline />
    @If Model.IntOrderType = 2 Then
     @<paragraph style="font-family:Helvetica;font-size:14;font-weight:bold;">
	<chunk red="0" green="0" blue="0">@Model.DeliveryAddress.Name</chunk><newline />
	<chunk red="0" green="0" blue="0">@Model.DeliveryAddress.Address1</chunk><newline />
	<chunk red="0" green="0" blue="0">@Model.DeliveryAddress.Address2</chunk><newline />
	<chunk red="0" green="0" blue="0">@Model.DeliveryAddress.City, @Model.DeliveryAddress.State @Model.DeliveryAddress.Zip</chunk><newline />
    <chunk red="0" green="0" blue="0">Phone: @Model.BilltoAddress.phone</chunk><newline />
    </paragraph>
    @<newline />
    End If


    <paragraph style="font-family:Helvetica;font-size:18;font-weight:bold;">
	<chunk red="0" green="0" blue="0">Order Number: @Model.OrderNumber - @EasternNow</chunk>
</paragraph>
    <newline />    
    <paragraph style="font-family:Helvetica;font-size:18;font-weight:bold;">
	<chunk red="0" green="0" blue="0">Confirmation: @Model.Confirmation</chunk>
</paragraph>
    <newline />
<table width="100%" cellpadding="1.0" cellspacing="1.0" widths="5;5">
    @For Each item In Model.Items
        @<row>
            <cell borderwidth="0.5" left="false" right="false" top="false" bottom="true">@item.Name</cell>

	        <cell borderwidth="0.5" left="false" right="false" top="false" bottom="true">
                                 @For Each Group In Item.ModifierGroups
                     @<chunk style="font-family:Helvetica;font-weight:bold;">@Group.Name :</chunk>
                    For Each modifier In Group.Modifiers
                        @modifier.Name @<newline />
                                 For Each l2group In modifier.TicketItemL2Groups
                                             @<newline />
                                            @<chunk style="font-family:Helvetica;font-weight:bold;">@l2group.Name: </chunk>
                                 For Each l2mod In l2group.Modifiers
                                            @l2mod.Name @IIf(l2mod.Qty > 1,"x" & l2mod.Qty,"")
                                            @<newline />
                                 Next
                                 Next
                 Next
                Next
                @item.SpecialInstructions
	        </cell>
            <cell borderwidth="0.5" left="false" right="false" top="false" bottom="true" horizontalalign="right">@FormatCurrency(item.TotalItemPrice)</cell>
        </row>
            Next
    <row>
        <cell></cell>
        <cell></cell>
        <cell horizontalalign="right">Total: @FormatCurrency(Model.TotalAmount)</cell>
    </row>
    <row>
        <cell></cell>
        <cell></cell>
        <cell horizontalalign="right">Tip Total: @FormatCurrency(Model.DeliveryPrice)</cell>
    </row>
    <row>
    </row>
</table>
    <paragraph style="font-family:Helvetica;font-size:14;font-weight:bold;">
        <chunk red="0" green="0" blue="0">@Model.Notes</chunk>
    </paragraph>
    </itext>