﻿@ModelType LM.LMMenuItem

@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>LMMenuItem</legend>

    <div class="display-label">Name</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Name)
    </div>

    <div class="display-label">Description</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Description)
    </div>

    <div class="display-label">Price</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Price)
    </div>

    <div class="display-label">MenuGroup</div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MenuGroup.Name)
    </div>
</fieldset>
@Using Html.BeginForm()
    @<p>
        <input type="submit" value="Delete" /> |
        @Html.ActionLink("Back to List", "Index", New With {.groupid = Model.MenuGroupID})
    </p>
End Using
