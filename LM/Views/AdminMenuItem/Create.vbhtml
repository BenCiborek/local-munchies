﻿@ModelType LM.LMMenuItem

@Code
    ViewData("Title") = "Create"
End Code

<h2>Create</h2>

@Using Html.BeginForm()
    @Html.ValidationSummary(True)
    @<fieldset>
        <legend>LMMenuItem</legend>
        <input id="MenuGroupID" name="MenuGroupID" type="hidden" value="@ViewBag.MenuGroupID" />
        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Description)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Description)
            @Html.ValidationMessageFor(Function(model) model.Description)
        </div>
          <div class="editor-label">
            @Html.LabelFor(Function(model) model.Taxable)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Taxable)
            @Html.ValidationMessageFor(Function(model) model.Taxable)
        </div>
         <div class="editor-label">
             @Html.LabelFor(Function(model) model.isUpsell)
         </div>
         <div class="editor-field">
             @Html.EditorFor(Function(model) model.isUpsell)
             @Html.ValidationMessageFor(Function(model) model.isUpsell)
         </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Price)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Price)
            @Html.ValidationMessageFor(Function(model) model.Price)
        </div>
        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index", New With {.groupid = ViewBag.MenuGroupid})
</div>
