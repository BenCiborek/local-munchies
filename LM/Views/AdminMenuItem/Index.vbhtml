﻿@ModelType IEnumerable(Of LM.LMMenuItem)

@Code
    ViewData("Title") = "Menu Items"
    Dim aryurl As Array = Split(ViewBag.BreadCrumbURLS, ",")
    Dim aryName As Array = Split(ViewBag.BreadCrumbNames, ",")
    Dim x = 0
End Code

<h2>Menu Items</h2>

<ol class="breadcrumb">
    @While x < aryurl.Length
        @<li><a href="@aryurl(x)">@aryName(x)</a></li>
        x += 1
    End While
</ol>

<p>
    @Html.ActionLink("Create New", "Create", New With {.groupid = ViewBag.MenuGroupID})
</p>
<table class="table">
    <tr>
        <th>
            Name
        </th>
        <th>
            Modifiers
        </th>
        <th>
            Description
        </th>
        <th>
            Price
        </th>
        <th>
            MenuGroup
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            <a href="@Url.Action("Index", "AdminMenuItemModifier", New With {.itemid = item.ID})">@currentItem.Modifiers.Count()</a>
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Description)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Price)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MenuGroup.Name)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
