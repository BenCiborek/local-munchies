﻿@Code
    Layout = Nothing
End Code

<div class="alert alert-warning">
    <div class="row">
        <div class="col-xs-12 col-sm-6" style="vertical-align:middle">
            Sign up today to get Updates and Deals from Local Munchies!
        </div>
        <div class="col-xs-8 col-sm-4">
            <input class="form-control" placeholder="Email" type="email" id="txtSignUpEmail" name="txtSignUpEmail" />
        </div>
        <div class="col-xs-4 col-sm-2">
            <button class="btn btn-block btn-sm btn-danger" id="btnSignUp">Sign Up!</button>
        </div>
    </div>
</div>