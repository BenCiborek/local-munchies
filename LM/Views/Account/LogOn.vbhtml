﻿@Modeltype Lm.LogonVM
@Code
    ViewData("Title") = " | Log On"
    
End Code
<div class="row">
    
    <form class="form-signin center-block" role="form" method="post"  style="max-width: 400px;">
       @If Not Model.Message Is Nothing Then
           @<div class="alert alert-danger" id="signinalert"><span id="signinalertmessage">@Model.Message</span></div>
       End If
        <h2 class="form-signin-heading">Sign In <small>Don't have an Account? <a href="@Url.Action("Register")">Click Here to Register</a></small></h2>
        <div class="form-group">
            <label for="SIEmail">Email address</label>
            <input type="email" class="form-control" id="Email" name="Email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="SIPassword">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <input type="hidden" value="@Model.ReturnURL" id="returnurl" name="returnurl" />
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
    
</div>

