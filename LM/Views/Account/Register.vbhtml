﻿@ModelType LM.RegisterVM

@Code
    ViewData("Title") = " | Register"
 
End Code


<div class="row">
    <form class="form-signin center-block" role="form" method="post" style="max-width: 400px;">
        @If Not Model Is Nothing Then
            If Not Model.Message Is Nothing Then
            @<div class="alert alert-danger" id="signinalert"><span id="signinalertmessage">@Html.Raw(Model.Message)</span></div>
            End If
        End If
        <div class="alert alert-danger hidden" id="registeralert"><span id="registeralertmessage"></span></div>
        <h2>Register today. <small>Already have an Account? <a href="@Url.Action("Logon")">Click Here to Login.</a></small></h2>
        <div class="form-group">
            <label for="Email">Email address</label>
            <input type="email" class="form-control" id="Email" name="Email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="Password">Password</label>
            <input type="password" class="form-control" id="Password" name="Password" placeholder="Password">
        </div>
        <div class="form-group">
            <label for="ConfirmPassword">Confirm Password</label>
            <input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block" id="btnRegister">Register</button>
    </form>
</div>
