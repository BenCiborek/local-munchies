﻿@modeltype LM.LoginHeadersVM
@Code
    Layout = Nothing
End Code

@If Model.Loggedin Then
    @<li class=""><a href="@Url.Action("Index", "Account")">My Account</a></li>
    @<li class=""><a href="@Url.Action("Index", "Account")">My Orders</a></li>
    @<li class=""><a href="@Url.Action("Logoff", "Account")">Log Out</a></li>
Else
    @<li class=""><a href="@Url.Action("Logon", "Account")">Login</a></li>
    @<li class=""><a href="@Url.Action("Register", "Account")">Register</a></li>
End If