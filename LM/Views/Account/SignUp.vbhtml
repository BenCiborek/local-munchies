﻿@Code
    Layout = Nothing
End Code

<div id="signupDetail">
    @Html.action("SignUpDetail")
</div>


<script>
    $('#btnSignUp').on('click', function (e) {
        submitEmail($("#txtSignUpEmail").val())
    })


    function submitEmail(emailaddress) {
        $.ajax({
            type: 'POST',
            url: '/Account/SignUp/',
            data: {
                Email: emailaddress
            },
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {

            },
            success: function (evt) {
                $('#signupDetail').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {

            },
            complete: function () {

            }
        });
    }
</script>