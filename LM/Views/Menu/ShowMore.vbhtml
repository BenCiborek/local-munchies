﻿@ModelType LM.LMMenu

@Code
    ViewData("Title") = ""
    Layout = ""
End Code
@If Model Is Nothing Then
    @<h2>Menu Coming Soon</h2>
Else


@<div class="menuaccordian">
    @For Each Cat In Model.MenuCategories.OrderBy(Function(z) z.DisplayOrder)
        For Each Group In Cat.MenuGroups.OrderBy(Function(z) z.DisplayOrder)
        @<div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#@Group.ID" class=".accordion-toggle">
                    <i class="glyphicon glyphicon-plus"></i>   @IIf(Cat.Name = "-", "", Cat.Name)  @IIf(Group.Name = "-", "", "- " & Group.Name)
                </a>
            </h4>
        </div>
        <div id="@Group.ID" class="panel-collapse collapse in">
            <div class="panel-body">
                @For Each Item In Group.MenuItems
                    @<div class="menushowitembox">
                        <h4 class="pull-right" style="margin-top:0px;"> @IIf(Item.Price <> 0, FormatCurrency(Item.Price), "")</h4>
                        <h4>@Item.Name<br /><small class="menushowitemdescription"> @Item.Description</small></h4>
                        @*@For Each ModifierGroup In Item.Modifiers
                            @<div class="menushow">
                                <h5>
                                    @For Each ModifierItem In ModifierGroup.MenuModifierGroup.Modifiers
                                        @<span>@ModifierItem.Name <strong>@IIf(ModifierItem.Price <> 0, FormatCurrency(ModifierItem.Price), "")  @IIf(ModifierItem.Extra_Price <> 0, "(+ " & FormatCurrency(ModifierItem.Extra_Price) & ")", "")</strong> |</span>
                                    Next
                                </h5>
                            </div>
                        Next*@
                    </div>

                Next
                </div>
            </div>
            </div>
            Next

        Next
        </div>
        
End If
<script>
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
</script>



