﻿@ModelType lm.getQtyDropVM
@Code
    Layout = Nothing
    
End Code
<select class="form-control">
    @For index As Integer = Model.Start To Model.Finish
        @<option>@index</option>
    Next
</select>

