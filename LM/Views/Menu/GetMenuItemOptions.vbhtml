﻿@Modeltype LM.GetMenuItemOptionsVM
@Code
    Layout = ""
    Dim Datatype = ""
    Dim L2Datatype = ""
End Code

<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">@Model.Item.Name <small><br /><i>@Model.Item.Description</i></small></h4>
      </div>
    <div class="modal-body">
        <input type="hidden" id="hdnItemID" value="@Model.Item.ID" />
        
        
        @For Each ModifierGroup In Model.Item.Modifiers
            @<div class="itemoptionserror" id="div-@ModifierGroup.ID">
              <span class="text-black">  @ModifierGroup.MenuModifierGroup.Name: </span>
                <span id="ct-@ModifierGroup.ID" class="choosetext text-warning">
                    @If ModifierGroup.MaxQty = 1 And ModifierGroup.MinQty = 1 Then
                        @<span>Choose 1</span>     
                    Datatype = "Radio"
                    ElseIf ModifierGroup.MaxQty = 0 Then
                    @<span>Choose As Many As You Want! @IIf(ModifierGroup.FreeQty > 0, ModifierGroup.FreeQty & " Free!" ,"") </span>
                    Datatype = "Checkbox"
                    Else
                        @<span>Choose Up to @ModifierGroup.MaxQty  @IIf(ModifierGroup.FreeQty > 0, ModifierGroup.FreeQty & " Free!" ,"")</span>
                    Datatype = "Checkbox"
                    End If
                </span>
                <ul class="list-group modgroup l1" id="ul-@ModifierGroup.ID" data-groupid="@ModifierGroup.ID" data-type="@Datatype" data-max="@ModifierGroup.MaxQty" data-min="@ModifierGroup.MinQty" data-itemmodid="@ModifierGroup.ID">
                   
                    @For Each ModifierItem In ModifierGroup.MenuModifierGroup.Modifiers
                            @<li class="list-group-item" data-modid="@ModifierItem.ID">
                            <a href="#@ModifierItem.ID" class="modifieritem l1 @IIf(ModifierGroup.MenuModifierGroup.Exclusive Or ModifierItem.Selected, "selected", "")" data-modid="@ModifierItem.ID">
                                <div class="row">
                                    <div class="col-xs-8">
                                        @ModifierItem.Name
                            <span id="mod-@ModifierItem.ID" class="glyphicon glyphicon-ok @IIf(ModifierGroup.MenuModifierGroup.Exclusive Or ModifierItem.Selected,"","hidden") "></span>
                                    </div>
                                    <div class="col-xs-4" style="text-align: right;">
                                        @IIf(ModifierItem.Price <> 0, FormatCurrency(ModifierItem.Price), "")  @IIf(ModifierItem.Extra_Price <> 0, "(+ " & FormatCurrency(ModifierItem.Extra_Price) & ")", "")
                            </div>
                                </div>
                            </a>
                            <div class="l2 hidden">

                            @For Each L2Group In ModifierItem.L2ModifierGroups
                                @<div  class="itemoptionserror" id="divl2-@L2Group.ID">
                                  <span class="text-black">  @L2Group.Name: </span>
                <span id="ctl2-@L2Group.ID" class="choosetext text-warning">
                    @If L2Group.MaxQty = 1 And L2Group.MinQty = 1 Then
                        @<span>Choose 1</span>     
                    L2Datatype = "Radio"
                    ElseIf L2Group.MaxQty = 0 Then
                        @<span>Choose As Many As You Want!  @IIf(ModifierGroup.FreeQty > 0, ModifierGroup.FreeQty & " Free!" ,"")</span>
                    L2Datatype = "Checkbox"
                    Else
                        @<span>Choose Up to @L2Group.MaxQty  @IIf(ModifierGroup.FreeQty > 0, ModifierGroup.FreeQty & " Free!" ,"")</span>
                    L2Datatype = "Checkbox"
                    End If
                                </span>
                                    <ul class="list-group modgroup l2" id="ull2-@L2Group.ID" data-controltype="@L2Group.ControlTypeID" data-groupid="@L2Group.ID" data-type="@L2Datatype" data-max="@L2Group.MaxQty" data-min="@L2Group.MinQty" data-itemmodid="@L2Group.ID">
                                         @Select Case L2Group.ControlTypeID
                                         Case 2
                                             @Html.Action("getMixedQtyModifiers", "Menu", New With {.ModGroup = L2Group})
                                         Case 3
                                             @Html.Action("getHalfWholeModifiers", "Menu", New With {.ModGroup = L2Group})
                                         Case Else
                                         For Each L2Modifier In L2Group.L2Modifiers
                                             @<li class="list-group-item" data-modid="@L2Modifier.ID">
                                                 <a href="#@L2Modifier.ID" class="l2modifieritem l2 @IIf(L2Group.Exclusive Or L2Modifier.Selected, "selected", "")" data-modid="@L2Modifier.ID">
                                                     <div class="row">
                                                         <div class="col-xs-8">
                                                             @L2Modifier.Name
                                                             <span id="modl2-@L2Modifier.ID" class="glyphicon glyphicon-ok @IIf(L2Group.Exclusive Or L2Modifier.Selected, "", "hidden") "></span>
                                                         </div>
                                                         <div class="col-xs-4" style="text-align: right;">
                                                             @IIf(L2Modifier.Price <> 0, FormatCurrency(L2Modifier.Price), "")  @IIf(L2Modifier.Extra_Price <> 0, "(+ " & FormatCurrency(L2Modifier.Extra_Price) & ")", "")
                                                         </div>
                                                     </div>
                                                 </a>
                                             </li>
                                        Next
                                         End Select


                                    </ul>
                                </div>
                            Next
                        </div>

                        </li>
                  
                                Next

                </ul>
            </div>
                    Next
        <div>
            Special Instructions: 
            <textarea class="form-control" rows="3" id="txtSpecialInstructions"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        <h3 class="pull-right" style="padding-right:25px;"><span id="totalloading"></span> <span id="itemprice">@FormatCurrency(Model.Item.Price)</span></h3>
            </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          @If Model.Open Then
              @<button type="button" class="btn btn-primary"  id="btnAddItem">Add to Order</button>
          Else
              @<button type="button" class="btn btn-primary" disabled="disabled">Restaurant Closed. Sorry.</button>
          End If
        
      </div>
    </div><!-- /.modal-content -->

<script>
    $(function () {
        getRunningPrice()
        $('.modifieritem').click(function (ev) {
            ev.preventDefault();
            var modifier = $(this).attr("data-modid");
            //alert($(this).closest("ul").attr("data-groupid"));
            var groupid = $(this).closest("ul").attr("data-groupid");
            var min = $(this).closest("ul").attr("data-min");
            var max = $(this).closest("ul").attr("data-max");
            var type = $(this).closest("ul").attr("data-type");

            //alert(type)

            if (type == "Radio") {
                //Hide All
                $("#ul-" + groupid + " > li > a").find('span').addClass("hidden");
                $("#ul-" + groupid + " > li > a").removeClass("selected");
                $("#ul-" + groupid + " > li > div.l2").addClass("hidden");


            }

            $(this).toggleClass("selected");
            $("#mod-" + modifier).toggleClass("hidden");
            $(this).closest('li').find('div.l2').toggleClass("hidden");
            
          
            getRunningPrice()
        });

        $('.l2modifieritem').click(function (ev) {
            ev.preventDefault();
            var modifier = $(this).attr("data-modid");
            //alert($(this).closest("ul").attr("data-groupid"));
            var groupid = $(this).closest("ul").attr("data-groupid");
            var min = $(this).closest("ul").attr("data-min");
            var max = $(this).closest("ul").attr("data-max");
            var type = $(this).closest("ul").attr("data-type");

            //alert(type)

            if (type == "Radio") {
                //Hide All
                $("#ull2-" + groupid + " > li > a").find('span').addClass("hidden");
                $("#ull2-" + groupid + " > li > a").removeClass("selected");

            }

            $(this).toggleClass("selected");
            $("#modl2-" + modifier).toggleClass("hidden");

            
            getRunningPrice()
        });


        $('#btnAddItem').click(function (ev) {
                  ev.preventDefault()
            //check Page
                  var mods = []
            var l2mods = []
            var modgroups = $(".modgroup.l1");
            var valid = true
            var itemid = $("#hdnItemID").val();
            var specialinstructions = $("#txtSpecialInstructions").val();
            modgroups.each(function (idx, ul) {
                var groupid = $(ul).attr("data-groupid");
                var min = $(ul).attr("data-min");
                var max = $(ul).attr("data-max");
                var menumod = $(ul).attr("data-itemmodid");
                //alert(min)
                  var selectedcount = 0
                  $(ul).find("li .selected.l1").each(function (liidx, li) {
                    //alert("Selected: " + $(li).attr("data-modid"))
                    //alert(menumod)
                    mods.push(menumod + '-' + $(li).attr("data-modid"))

                    //Level2
                    $(li).closest("li").find(".modgroup.l2").each(function (l2idx, ull2) {
                        // alert('t2')
                        var l2groupid = $(ull2).attr("data-groupid");
                        var l2min = $(ull2).attr("data-min");
                        var l2max = $(ull2).attr("data-max");
                        var l2menumod = $(ull2).attr("data-itemmodid");
                        var l2selectedcount = 0
                        var l2controltype = $(ull2).attr("data-controltype");

                    

                        if (l2controltype == "2") {
                            $(ull2).find("li .l2").each(function (l2liidx, l2li) {
                                //alert("Selected L2: " + $(l2li).attr("data-modid"))
                                
                                var qty = $(l2li).find("select").val()
                                if (qty > 0) {
                                   // alert($(l2li).attr("data-modid") + '-' + qty)
                                    l2mods.push($(l2li).attr("data-modid") + '-' + qty)
                                    l2selectedcount += parseInt(qty)
                                    //alert(l2selectedcount)
                                }
                            })
                        }
                        else {
                        $(ull2).find("li .selected.l2").each(function (l2liidx, l2li) {
                            //alert("Selected L2: " + $(l2li).attr("data-modid"))
                            l2mods.push($(l2li).attr("data-modid"))
                            l2selectedcount++
                        })
                        }
                        if ((l2selectedcount < l2min || (l2selectedcount > l2max && l2max != 0))) {
                            //change text to red
                            $("#ctl2-" + l2groupid).addClass("text-danger")
                            $("#ctl2-" + l2groupid).removeClass("text-warning")
                            $("#ctl2-" + l2groupid).removeClass("text-success")
                            $("#divl2-" + l2groupid).addClass("alert alert-danger")
                            valid = false
                        }
                        else {
                            $("#ctl2-" + l2groupid).removeClass("text-danger")
                            $("#ctl2-" + l2groupid).addClass("text-success")
                            $("#divl2-" + l2groupid).removeClass("alert alert-danger")
                        }
                    })



                    selectedcount++
                })
                //alert(selectedcount + ":" + min + ":" + max)
                  if (selectedcount < min || selectedcount > max && max != 0) {
                      //change text to red
                      $("#ct-" + groupid).addClass("text-danger")
                      $("#ct-" + groupid).removeClass("text-warning")
                      $("#ct-" + groupid).removeClass("text-success")
                      $("#div-" + groupid).addClass("alert alert-danger")
                      valid = false
                  }
                  else {
                      //Good now remove Errors
                      $("#ct-" + groupid).removeClass("text-danger")
                      $("#ct-" + groupid).addClass("text-success")
                      $("#div-" + groupid).removeClass("alert alert-danger")
                  }
                    
                // and the rest of your code
            })
            if (valid) {
                          addItemToCart(itemid, 1, mods, specialinstructions, l2mods)
            }
           
        });

    });

    function getRunningPrice() {
        var price = 0
        var mods = []
        var l2mods = []
        var modgroups = $(".modgroup.l1");
        var itemid = $("#hdnItemID").val();
      
        modgroups.each(function (idx, ul) {
            var groupid = $(ul).attr("data-groupid");
            var min = $(ul).attr("data-min");
            var max = $(ul).attr("data-max");
            var menumod = $(ul).attr("data-itemmodid");
            //alert(min)
            var selectedcount = 0
            //alert('t1')
            $(ul).find("li .selected.l1").each(function (liidx, li) {
               // alert("Selected L1: " + $(li).attr("data-modid"))
                //alert(menumod)
                mods.push(menumod + '-' + $(li).attr("data-modid"))

                //Level2
                $(li).closest("li").find(".modgroup.l2").each(function (l2idx, ull2) {
                   // alert('t2')
                    var l2groupid = $(ull2).attr("data-groupid");
                    var l2min = $(ull2).attr("data-min");
                    var l2max = $(ull2).attr("data-max");
                    var l2menumod = $(ull2).attr("data-itemmodid");
                    var l2selectedcount = 0
                    $(ull2).find("li .selected.l2").each(function (l2liidx, l2li) {
                        //alert("Selected L2: " + $(l2li).attr("data-modid"))
                        l2mods.push($(l2li).attr("data-modid"))
                    })
                })


                selectedcount++
            })
            //alert(selectedcount + ":" + min + ":" + max)

            // and the rest of your code
        })
        findupdatedprice(itemid, 1, mods, l2mods)

    }

    function findupdatedprice(item, qty, modifiers, l2modifiers) {
        $.ajax({
            type: 'POST',
            url: '/Menu/FindItemPrice/',
            data: {
                itemid: item,
                qty: qty,
                modifiers: modifiers.toString(),
                specialinstructions: '',
                l2modifiers: l2modifiers.toString()
            },
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                //Show Loading gif
                //$('#itemprice').addClass('itemPriceLoading')

            },
            success: function (evt) {
                $('#itemprice').html(evt)
            },
            error: function (xhr, ajaxOptions, thrownError) {
               // $('#totalloading').html('')
            },
            complete: function () {
              //  $('#totalloading').html('')
            }
        });
    }

    function addItemToCart(item, qty, modifiers, specialinstructions, l2modifiers) {
        //alert(item)
        $.ajax({
            type: 'POST',
            url: '/Menu/AddItemToCart/',
            data: {
                itemid: item,
                qty: qty,
                modifiers: modifiers.toString(),
                specialinstructions: specialinstructions,
                l2modifiers: l2modifiers.toString()
            },
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                //Show button clicked
                $('#btnAddItem').addClass('active')

                //Show Loading gif
                $('#btnAddItem').html('<img src="/Content/img/pacman-loader.gif"/> Adding Item To Order')

                //$('#itemoptionsmenu').addClass('ajaxRefreshing');
                //$('#itemoptionsmenu').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                //xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#optionsModal').modal('hide');
                //$("#addtocartmessage").show().animate({ opacity: 1.0 }, 3000).fadeOut(1000);
                $('#itemAddedModal').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //$('#itemoptionsmenu').removeClass('ajaxRefreshing');
                //alert(xhr.error)
            },
            complete: function () {
                //Show button clicked
                $('#btnAddItem').removeClass('active')

                //Show Loading gif
                $('#btnAddItem').html('Add to Order')
            }
        });
    }
</script>
