﻿@Modeltype List(Of LM.UpsellItemVM)
@Code
    Layout = Nothing
End Code
@If Model.Count > 0 Then
    @<div style="margin-top:10px;">
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-group">

                    @For Each item In Model
                        @<li class="list-group-item">
                             <a href="#@Item.ID" data-menuitemid="@Item.ID" class="usmenuitem" style="display:inline-block;width:100%;height:100%;">
                                 <div class="row">
                                     <div class="col-xs-8">
                                         @item.Name <small class="text-muted"> @item.Description</small>
                                     </div>
                                     <div class="col-xs-4" style="text-align:right;">
                                         @IIf(item.Price <> 0, FormatCurrency(item.Price), "")<br />
                                         <p class="btn btn-primary btn-xs">Add ></p>
                                     </div>
                                 </div>
                             </a>
                        </li>
                    Next
                </ul>
            </div>
        </div>
    </div>
End If

