﻿@ModelType LM.LMMenu

@Code
    ViewData("Title") = ""
    Layout = ""
End Code
<div class="menushow">
    @For Each Cat In Model.MenuCategories.OrderBy(Function(z) z.DisplayOrder)
        @<div class="menushow">
            <h2>@IIf(Cat.Name = "-","",Cat.Name) <small> @Cat.Description</small></h2>
            @For Each Group In Cat.MenuGroups.OrderBy(Function (z) z.DisplayOrder)
                @<div class="menushow">
                    <h3>@iif(Group.Name="-","",Group.Name)<small> @Group.Description</small></h3>
                    @For Each Item In Group.MenuItems
                        @<div class="menushow">
                            <h4>@Item.Name<small> @Item.Description</small> @iif(Item.Price<>0,FormatCurrency(Item.Price),"")</h4>
                            @For Each ModifierGroup In Item.Modifiers
                                @<div class="menushow">
                                    <h5>
                                        @For Each ModifierItem In ModifierGroup.MenuModifierGroup.Modifiers
                                            @<span>@ModifierItem.Name <strong>@IIf(ModifierItem.Price <> 0, FormatCurrency(ModifierItem.Price), "")  @iif(ModifierItem.Extra_Price<> 0, "(+ " & FormatCurrency(ModifierItem.Extra_Price) & ")","")</strong> |</span>
                                        Next
                                    </h5>
                                </div>
                            Next
                        </div>
                    Next
                </div>
            Next
        </div>
    Next
</div>

