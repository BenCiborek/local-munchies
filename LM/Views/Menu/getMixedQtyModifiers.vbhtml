﻿@ModelType LM.LMMenuL2ModifierGroup
@Code
    Layout = Nothing
End Code

@For Each L2Modifier In Model.L2Modifiers
    @<li class="list-group-item" data-modid="@L2Modifier.ID">
        <a href="#@L2Modifier.ID" class="l2modifieritem l2" data-modid="@L2Modifier.ID">
            <div class="row">
                <div class="col-xs-6 col-sm-8">
                    @L2Modifier.Name
                </div>
                <div class="col-xs-6 col-sm-4" style="text-align: right;">
                    @Html.Action("getQtyDrop", "Menu", New With {.Start = 0, .Finish = Model.SubQtyMax})
                </div>
            </div>
        </a>
    </li>
           
Next