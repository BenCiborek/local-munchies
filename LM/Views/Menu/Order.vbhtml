﻿@ModelType LM.LMMenu
@Code
    Layout = ""
    Dim firstItem = ""
End Code

<div class="">
    <ul class="list-group">
        @For Each Cat In Model.MenuCategories.OrderBy(Function(z) z.DisplayOrder)
            For Each Group In Cat.MenuGroups.OrderBy(Function(z) z.DisplayOrder)
                If firstItem = "" Then
                    firstItem = Group.ID
                End If
            @<li class="list-group-item oomenugroup">
                <a data-toggle="collapse" href="#@Group.ID" class=".accordion-toggle" style="display:inline-block;width:100%;height:100%;">
                    <i class="glyphicon glyphicon-plus"></i> @IIf(Cat.Name = "-", "", Cat.Name)  @IIf(Group.Name = "-", "", "- " & Group.Name)
                </a>
            </li>
            @<div id="@Group.ID" class="collapse @IIf(firstItem = Group.ID,"firstitem","")" >
               
                @For Each Item In Group.MenuItems
                    @<li class="list-group-item">
                        <a href="#@Item.ID" data-menuitemid="@Item.ID" class="oomenuitem" style="display:inline-block;width:100%;height:100%;">
                            <div class="row">
                            <div class="col-xs-8">
                                @Item.Name <small class="text-muted"> @Item.Description</small>
                                    </div>
                                    <div class="col-xs-4" style="text-align:right;">
                                        @IIf(Item.Price <> 0, FormatCurrency(Item.Price), "")<br />
                                        <p class="btn btn-primary btn-xs">Add ></p>
                                    </div>
                                </div>
                            </a>
                             </li>
                        Next
                    
                </div>
            
            Next
        Next
    </ul>
</div>
<!-- Modal -->
<div class="modal fade" id="optionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div id="itemoptionsmenu">

        </div>
    </div>
</div>

@*Item Added to cart Message*@
@*<div id="addtocartmessage" class="addtocartmessage alert-warning alert">
    <span>Item Added to Order</span>
</div>*@

@*Item Added to Cart Popup*@
<div class="modal fade" id="itemAddedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Item Added to your cart.</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Continue Order</button>
                </div>
                <div class="col-xs-6">
                    <a type="button" class="btn btn-primary btn-block" href="@Url.Action("Index", "Cart")">View Cart</a>
                </div>
            </div>
            <div class="row">
                @Html.Action("getUpsellMenu", New With {.RestaurantId = Model.RestaurantID})
            </div>
        </div>
    </div>
  </div>
</div>

<script>
    $(function () {
        setTimeout(
        function () {
            $(".firstitem").collapse('toggle')
        }, 1000);
        $('.collapse').on('shown.bs.collapse', function () {
            $(this).prev().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function () {
            $(this).prev().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
        $('.usmenuitem').click(function (e) {
           //UpSell Menu
            e.preventDefault()
            try {
                ga('send', 'event', 'upsellitem', 'show', $(this).attr("data-menuitemid"), 1);
            }
            catch (err) {
                //Skip Error
            }
            $('#itemAddedModal').modal('hide');
            $('#optionsModal').modal('show')
            showOptions($(this).attr("data-menuitemid"))
        });
        $('.oomenuitem').click(function (e) {
           //Ordering Option
            e.preventDefault()
            try {
                //_trackEvent('orderingoption', 'show', $(this).attr("data-menuitemid"), 1, true)
                ga('send', 'event', 'orderingoption', 'show', $(this).attr("data-menuitemid"), 1);
                //ga('send', 'event', 'search', 'click', 'nav-buttons', 1);
            }
            catch (err) {
                //Skip Error
            }
            $('#itemAddedModal').modal('hide');
            $('#optionsModal').modal('show')
            showOptions($(this).attr("data-menuitemid"))
        })
    })
    function showOptions(menuitemid) {
        $.ajax({
            type: 'POST',
            url: '/Menu/GetMenuItemOptions/' + menuitemid,
            //data: {
            //    strstatus: values
            //},
            // data: ,
            dataType: "html",
            beforeSend: function (xhr) {
                $('#itemoptionsmenu').addClass('ajaxRefreshing');
                //$('#searchMain').html('')
                //$('#searchresults').html('<div class="progress-indicator"><h2>WORKING...</h2><img src="/Content/img/spinner.gif" alt="Loading" /></div>')
                $('#itemoptionsmenu').append('<div class="loading"><img src="/Content/img/ajax-loader.gif" class="loadingspinner" /></div>')
                xhr.setRequestHeader('X-Client', 'jQuery');
            },
            success: function (evt) {
                $('#itemoptionsmenu').html(evt);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#itemoptionsmenu').removeClass('ajaxRefreshing');
            },
            complete: function () {
                $('#itemoptionsmenu').removeClass('ajaxRefreshing');
            }
        });
    }

</script>