﻿@Modeltype LM.EmailSendVM
@Code
    Layout = Nothing
End Code

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Send</title>
</head>
<body>
    <div>
        @Model.Message
    </div>
</body>
</html>
