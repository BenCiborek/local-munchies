﻿@modeltype LM.LM_LMD_CustomersServed
@Code
    Layout = "~/Views/Shared/_Email.vbhtml"
    
    

End Code
@ViewData("Message")


@*<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;background-color: #DEE0E2;border-collapse: collapse;height: 100%;width: 100%;">
        <tr>
            <td align="center" valign="top" id="bodyCell" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 20px;border-top: 4px solid #BBBBBB;height: 100%;width: 100%;">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border: 1px solid #BBBBBB;border-collapse: collapse;">
                    <tr>
                        <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <!-- BEGIN PREHEADER // -->
                            @Html.Action("Header", "Email", New With {.message = "Customers Served"})
                            <!-- // END PREHEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <!-- BEGIN HEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;border-collapse: collapse;">
                                <tr>
                                    <td valign="top" class="headerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #505050;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: left;vertical-align: middle;">
                                        <img src="http://localmunchies.com/content/img/LMlogo600.jpg" style="max-width: 600px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" id="headerImage">
                                    </td>
                                </tr>
                            </table>
                            <!-- // END HEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <!-- BEGIN BODY // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #F4F4F4;border-top: 1px solid #FFFFFF;border-bottom: 1px solid #CCCCCC;border-collapse: collapse;">
                                <tr>
                                    <td valign="top" class="bodyContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #505050;font-family: Helvetica;font-size: 14px;line-height: 150%;padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;text-align: left;">
                                        <h1 style="display: block;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: Center;color: #202020;">Customers Served</h1>

                                        <h1 style="display: block;font-family: Helvetica;font-size: 68px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: Center;color: #202020;">@(Model.LM_CustomersServed.TotalCustomers + Model.LMD_CustomersServed.TotalCustomers)</h1> 

                                        <hr style="background-color: #F4F4F4;" />

                                        <table align="center" border="0" cellpadding="5" cellspacing="0" height="100%" width="100%" style="background-color: #F4F4F4;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0; border-collapse: collapse;height: 100%;width: 100%;">
                                            @*Header*@ @*
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <h2 style="display: block; font-family: Helvetica; font-size: 20px; font-style: normal; font-weight: bold; line-height: 100%; letter-spacing: normal; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: center; color: #404040;">LM</h2>
                                                </td>
                                                <td>
                                                    <h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: center;color: #404040;">LM + LMD</h2>
                                                </td>
                                                <td>
                                                    <h2 style="display: block; font-family: Helvetica; font-size: 20px; font-style: normal; font-weight: bold; line-height: 100%; letter-spacing: normal; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: center; color: #404040;">LMD</h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; font-size: 20px;">Yesterday</td>
                                                <td style="text-align: center; font-size: 20px;">@Model.LM_CustomersServed.TotalCustomersDay</td>
                                                <td style="text-align: center; font-size: 20px; ">@(Model.LM_CustomersServed.TotalCustomersDay + Model.LMD_CustomersServed.TotalCustomersDay)</td>
                                                <td style="text-align: center; font-size: 20px; ">@Model.LMD_CustomersServed.TotalCustomersDay</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; font-size: 20px;">Last Week</td>
                                                <td style="text-align: center; font-size: 20px;">@Model.LM_CustomersServed.TotalCustomersWeek</td>
                                                <td style="text-align: center; font-size: 20px; ">@(Model.LM_CustomersServed.TotalCustomersWeek + Model.LMD_CustomersServed.TotalCustomersWeek)</td>
                                                <td style="text-align: center; font-size: 20px; ">@Model.LMD_CustomersServed.TotalCustomersWeek</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; font-size: 20px;">Last Month</td>
                                                <td style="text-align: center; font-size: 20px;">@Model.LM_CustomersServed.TotalCustomersMonth</td>
                                                <td style="text-align: center; font-size: 20px; ">@(Model.LM_CustomersServed.TotalCustomersMonth + Model.LMD_CustomersServed.TotalCustomersMonth)</td>
                                                <td style="text-align: center; font-size: 20px; ">@Model.LMD_CustomersServed.TotalCustomersMonth</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; font-size: 20px;">Last Year</td>
                                                <td style="text-align: center; font-size: 20px;">@Model.LM_CustomersServed.TotalCustomersYear</td>
                                                <td style="text-align: center; font-size: 20px; ">@(Model.LM_CustomersServed.TotalCustomersYear + Model.LMD_CustomersServed.TotalCustomersYear)</td>
                                                <td style="text-align: center; font-size: 20px; ">@Model.LMD_CustomersServed.TotalCustomersYear</td>
                                            </tr>

                                        </table>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END BODY -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <!-- BEGIN FOOTER // -->
                            @Html.Action("Footer", "Email")
                            <!-- // END FOOTER -->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
                                            *@