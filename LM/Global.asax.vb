﻿' Note: For instructions on enabling IIS6 or IIS7 classic mode, 
' visit http://go.microsoft.com/?LinkId=9394802
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Web.Optimization
Imports LM.LM

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Shared Sub RegisterGlobalFilters(ByVal filters As GlobalFilterCollection)
        filters.Add(New HandleErrorAttribute())
    End Sub

    Shared Sub RegisterRoutes(ByVal routes As RouteCollection)
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")

        routes.MapRoute( _
            "Restaurant", _
            "r/{id}/{name}", _
            New With {.controller = "LMRestaurant", .action = "Zoom", .id = UrlParameter.Optional, .name = UrlParameter.Optional} _
        )

        routes.MapRoute( _
    "Search", _
    "s/{id}", _
    New With {.controller = "LMSearch", .action = "Index", .id = UrlParameter.Optional} _
)
        routes.MapRoute( _
    "Calls", _
    "calls/ordercall/{id}/{restid}/{itemnumber}", _
    New With {.controller = "calls", .action = "ordercall", .id = UrlParameter.Optional, .restid = UrlParameter.Optional, .itemnumber = UrlParameter.Optional} _
    )


        ' MapRoute takes the following parameters, in order:
        ' (1) Route name
        ' (2) URL with parameters
        ' (3) Parameter defaults
        routes.MapRoute( _
            "Default", _
            "{controller}/{action}/{id}", _
            New With {.controller = "Home", .action = "Index", .id = UrlParameter.Optional} _
        )

    End Sub

    Sub Application_Start()
        AreaRegistration.RegisterAllAreas()

        ' Use LocalDB for Entity Framework by default
        'Database.DefaultConnectionFactory = New SqlConnectionFactory("Data Source=(localdb)\v11.0; Integrated Security=True; MultipleActiveResultSets=True")

        RegisterGlobalFilters(GlobalFilters.Filters)
        RegisterRoutes(RouteTable.Routes)

        RegisterBundles(BundleTable.Bundles)
        BundleTable.EnableOptimizations = True

    End Sub
    Protected Sub FormsAuthentication_OnAuthenticate(sender As [Object], e As FormsAuthenticationEventArgs)
        If FormsAuthentication.CookiesSupported = True Then
            If Request.Cookies(FormsAuthentication.FormsCookieName) IsNot Nothing Then
                Try
                    'let us take out the username now                
                    Dim username As String = FormsAuthentication.Decrypt(Request.Cookies(FormsAuthentication.FormsCookieName).Value).Name
                    Dim roles As String = String.Empty

                    Using entities As New FBEntities
                        Dim user As User = entities.Users.SingleOrDefault(Function(u) u.UserName = username)

                        roles = user.Roles
                    End Using
                    'let us extract the roles from our own custom cookie


                    'Let us set the Pricipal with our user specific details
                    e.User = New System.Security.Principal.GenericPrincipal(New System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(";"c))
                    'somehting went wrong
                Catch generatedExceptionName As Exception
                End Try
            End If
        End If
    End Sub
    Public Sub RegisterBundles(bundles As BundleCollection)
        bundles.Add(New StyleBundle("~/Content/css").Include(
                        "~/Content/css/smoothness/jquery-ui-1.10.3.custom.css",
                         "~/Content/css/bootstrap.css",
                         "~/Content/Site.css"))
        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
            "~/Content/js/jquery-1.9.1.min.js",
            "~/Content/js/jquery-ui-1.10.3.custom.min.js",
            "~/Content/js/bootstrap.min.js"))
    End Sub
#If Not Debug Then
    Protected Sub Application_Error(sender As Object, e As EventArgs)

        Dim ex = Server.GetLastError().GetBaseException()

        Server.ClearError()
        Dim routeData = New RouteData()
        routeData.Values.Add("controller", "Error")
        routeData.Values.Add("action", "Index")

        If ex.[GetType]() = GetType(HttpException) Then
            Dim httpException = DirectCast(ex, HttpException)
            Dim code = httpException.GetHttpCode()
            routeData.Values.Add("status", code)
        Else
            routeData.Values.Add("status", 500)
        End If

        routeData.Values.Add("error", ex)

        Dim errorController As IController = New ErrorController()
        errorController.Execute(New RequestContext(New HttpContextWrapper(Context), routeData))
    End Sub

    Protected Sub Application_EndRequest(sender As Object, e As EventArgs)
        If Context.Response.StatusCode = 401 Then
            ' this is important, because the 401 is not an error by default!!!
            Throw New HttpException(401, "You are not authorised")
        End If
    End Sub

#End If
    


End Class
