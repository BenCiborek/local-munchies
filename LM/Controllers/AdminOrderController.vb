﻿Imports System.Data.Entity
Namespace LM

    <Authorize(Roles:="Admin")> _
    Public Class AdminOrderController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /AdminOrder
        Private db As FBEntities = New FBEntities

        Function Index() As ActionResult
            Return View()
        End Function
        Function CompletedOrderReport() As ActionResult
            Dim OrderResults As New List(Of CompletedOrderResultVM)

            Dim tickets = db.Tickets.Where(Function(x) x.TicketStatus > 1)

            For Each Ticket In tickets
                Dim OrderResult As New CompletedOrderResultVM
                OrderResult.Ticket = Ticket
                OrderResult.Billto = db.Addresses.Where(Function(x) x.ID = Ticket.BilltoAddress).FirstOrDefault
                OrderResult.Delivery = db.Addresses.Where(Function(x) x.ID = Ticket.DeliveryAddress).FirstOrDefault
                Dim rests = Ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList
                OrderResult.Restaurants = db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList

                OrderResults.Add(OrderResult)
            Next
            Return View(OrderResults)
        End Function

        Function CouponCode() As ActionResult
            Dim couponCodes = db.CouponCodes.OrderByDescending(Function(x) x.Active).ToList()
            Return View(couponCodes)
        End Function

        <HttpPost>
        Function CouponCode(ByVal cc As CouponCodes)
            If ModelState.IsValid Then
                db.CouponCodes.Add(cc)
                db.SaveChanges()
            End If
            Return RedirectToAction("CouponCode")
        End Function

        Function Delete(ByVal id As Integer)
            Dim cc = db.CouponCodes.Find(id)
            cc.Active = False
            db.Entry(cc).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("CouponCode")
        End Function



    End Class
End Namespace
