﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuItemModifierController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuItemModifier/

        Function Index(ByVal itemid As Integer) As ViewResult
            Dim menuitemmodifiers = db.MenuItemModifiers.Where(Function(x) x.MenuItemID = itemid).Include(Function(l) l.MenuModifierGroup).Include(Function(l) l.MenuItem)

            Dim item = db.MenuItems.Find(itemid)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.MenuGroup.MenuCategory.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuCategory", New With {.Menuid = item.MenuGroup.MenuCategory.MenuID})
            Names &= "," & item.MenuGroup.MenuCategory.Menu.Name

            URLs &= "," & Url.Action("Index", "AdminMenuGroup", New With {.catid = item.MenuGroup.MenuCategoryID})
            Names &= "," & item.MenuGroup.MenuCategory.Name

            URLs &= "," & Url.Action("Index", "AdminMenuItem", New With {.groupid = item.MenuGroupID})
            Names &= "," & item.MenuGroup.Name

            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names

            ViewBag.MenuItemID = itemid

            Return View(menuitemmodifiers.ToList())
        End Function

        '
        ' GET: /AdminMenuItemModifier/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenuitemmodifier As LMMenuItemModifier = db.MenuItemModifiers.Find(id)
            Return View(lmmenuitemmodifier)
        End Function

        '
        ' GET: /AdminMenuItemModifier/Create

        Function Create(ByVal itemid As Integer) As ViewResult
            Dim menuid = db.MenuItems.Find(itemid).MenuGroup.MenuCategory.Menu.ID
            ViewBag.MenuModifierGroupID = New SelectList(db.MenuModifierGroups.Where(Function(x) x.MenuID = menuid), "ID", "Name")
            ViewBag.MenuItemID = itemid
            Return View()
        End Function

        '
        ' POST: /AdminMenuItemModifier/Create

        <HttpPost()>
        Function Create(lmmenuitemmodifier As LMMenuItemModifier) As ActionResult
            If ModelState.IsValid Then
                db.MenuItemModifiers.Add(lmmenuitemmodifier)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.itemid = lmmenuitemmodifier.MenuItemID})
            End If

            ViewBag.MenuModifierGroupID = New SelectList(db.MenuModifierGroups.Where(Function(x) x.MenuID = lmmenuitemmodifier.MenuItem.MenuGroup.MenuCategory.MenuID), "ID", "Name")
            ViewBag.MenuItemID = lmmenuitemmodifier.MenuItemID
            Return View(lmmenuitemmodifier)
        End Function

        '
        ' GET: /AdminMenuItemModifier/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenuitemmodifier As LMMenuItemModifier = db.MenuItemModifiers.Find(id)
            ViewBag.MenuModifierGroupID = New SelectList(db.MenuModifierGroups.Where(Function(x) x.MenuID = lmmenuitemmodifier.MenuItem.MenuGroup.MenuCategory.MenuID), "ID", "Name")
            ViewBag.MenuItemID = lmmenuitemmodifier.MenuItemID
            Return View(lmmenuitemmodifier)
        End Function

        '
        ' POST: /AdminMenuItemModifier/Edit/5

        <HttpPost()>
        Function Edit(lmmenuitemmodifier As LMMenuItemModifier) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenuitemmodifier).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.itemid = lmmenuitemmodifier.MenuItemID})
            End If

            ViewBag.MenuModifierGroupID = New SelectList(db.MenuModifierGroups.Where(Function(x) x.MenuID = lmmenuitemmodifier.MenuItem.MenuGroup.MenuCategory.MenuID), "ID", "Name")
            ViewBag.MenuItemID = lmmenuitemmodifier.MenuItemID
            Return View(lmmenuitemmodifier)
        End Function

        '
        ' GET: /AdminMenuItemModifier/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenuitemmodifier As LMMenuItemModifier = db.MenuItemModifiers.Find(id)
            Return View(lmmenuitemmodifier)
        End Function

        '
        ' POST: /AdminMenuItemModifier/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenuitemmodifier As LMMenuItemModifier = db.MenuItemModifiers.Find(id)
            db.MenuItemModifiers.Remove(lmmenuitemmodifier)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.itemid = lmmenuitemmodifier.MenuItemID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace