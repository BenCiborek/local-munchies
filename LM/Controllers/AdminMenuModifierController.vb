﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuModifierController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuModifier/

        Function Index(ByVal GroupID As Integer) As ViewResult
            Dim menumodifiers = db.MenuModifiers.Where(Function(z) z.MenuModifierGroupID = GroupID).Include(Function(l) l.MenuModifierGroup)

            Dim item = db.MenuModifierGroups.Find(GroupID)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuModifierGroup", New With {.Menuid = item.MenuID})
            Names &= "," & item.Menu.Name

            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names


            ViewBag.MenuModifierGroupID = GroupID
            Return View(menumodifiers.ToList())
        End Function

        '
        ' GET: /AdminMenuModifier/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenumodifier As LMMenuModifier = db.MenuModifiers.Find(id)
            Return View(lmmenumodifier)
        End Function

        '
        ' GET: /AdminMenuModifier/Create

        Function Create(ByVal GroupID As Integer) As ViewResult
            ViewBag.MenuModifierGroupID = GroupID
            Return View()
        End Function

        '
        ' POST: /AdminMenuModifier/Create

        <HttpPost()>
        Function Create(lmmenumodifier As LMMenuModifier) As ActionResult
            If ModelState.IsValid Then
                db.MenuModifiers.Add(lmmenumodifier)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.groupID = lmmenumodifier.MenuModifierGroupID})
            End If

            ViewBag.MenuModifierGroupID = lmmenumodifier.MenuModifierGroupID
            Return View(lmmenumodifier)
        End Function

        '
        ' GET: /AdminMenuModifier/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenumodifier As LMMenuModifier = db.MenuModifiers.Find(id)
            ViewBag.MenuModifierGroupID = lmmenumodifier.MenuModifierGroupID
            Return View(lmmenumodifier)
        End Function

        '
        ' POST: /AdminMenuModifier/Edit/5

        <HttpPost()>
        Function Edit(lmmenumodifier As LMMenuModifier) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenumodifier).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.groupID = lmmenumodifier.MenuModifierGroupID})
            End If

            ViewBag.MenuModifierGroupID = lmmenumodifier.MenuModifierGroupID
            Return View(lmmenumodifier)
        End Function

        '
        ' GET: /AdminMenuModifier/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenumodifier As LMMenuModifier = db.MenuModifiers.Find(id)
            Return View(lmmenumodifier)
        End Function

        '
        ' POST: /AdminMenuModifier/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenumodifier As LMMenuModifier = db.MenuModifiers.Find(id)
            db.MenuModifiers.Remove(lmmenumodifier)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.groupID = lmmenumodifier.MenuModifierGroupID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace