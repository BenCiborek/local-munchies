﻿Imports System.Web.Mvc
Imports System.Data.Entity
Namespace Controllers
    <Authorize(Roles:="Admin")> _
        Public Class AdminRestaurantInfoController
        Inherits Controller
        Private db As FBEntities = New FBEntities
        ' GET: AdminRestaurantInfo/Edit/5
        Function Edit(ByVal restid As Integer) As ActionResult
            Dim Model = db.RestaurantInfo.Where(Function(x) x.RestaurantID = restid).FirstOrDefault()
            If Model Is Nothing Then
                Model = New RestaurantInfo
                Model.RestaurantID = restid
            End If
            Return View(Model)
        End Function

        ' POST: AdminRestaurantInfo/Edit/5
        <HttpPost()>
        Function Edit(ByVal Model As RestaurantInfo) As ActionResult

            If Model.id <> 0 Then
                'Update
                If ModelState.IsValid Then
                    db.Entry(Model).State = EntityState.Modified
                    db.SaveChanges()
                    Return RedirectToAction("Index", "AdminMenu")
                End If
            Else
                'Add
                Try
                    Dim insertModel As New RestaurantInfo
                    insertModel.DeliveryRadius = Model.DeliveryRadius
                    insertModel.EmailTo = Model.EmailTo
                    insertModel.hasLMDAccount = Model.hasLMDAccount
                    insertModel.Notes = Model.Notes
                    insertModel.OrderFaxNumber = Model.OrderFaxNumber
                    insertModel.OrderPhoneNumber = Model.OrderPhoneNumber
                    insertModel.RestaurantID = Model.RestaurantID
                    insertModel.SubmissionType = Model.SubmissionType

                    db.Entry(Model).State = EntityState.Added
                    'db.RestaurantInfo.Add(insertModel)
                    db.SaveChanges()
                    Return RedirectToAction("Index", "AdminMenu")
                Catch ex As Exception
                    ModelState.AddModelError("Error", ex.Message)
                End Try
            End If




            Return View(Model)
        End Function

        ' GET: AdminRestaurantInfo/Delete/5
        Function Delete(ByVal id As Integer) As ActionResult
            Return View()
        End Function

        ' POST: AdminRestaurantInfo/Delete/5
        <HttpPost()>
        Function Delete(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add delete logic here

                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function
    End Class
End Namespace