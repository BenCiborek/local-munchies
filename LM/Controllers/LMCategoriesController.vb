﻿Namespace LM
    Public Class LMCategoriesController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /LMCategories
        Dim db As New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function
        Function Searchview(ByVal find As String, ByVal categorie As Integer, ByVal OrderOption As Integer, ByVal restids As List(Of Integer)) As ActionResult
            Dim cats = db.Categories
            'If cat <> 0 Then
            '    cats = cats.Where(Function(z) z.id = cat)
            'End If
            ViewBag.selectedcat = categorie
            ViewBag.restids = restids
            Return View(cats)
        End Function
        Function ShowCats(ByVal Rest As LMRestaurant)
            Dim rests = Rest.Categories()
            Return View(rests)
        End Function
        Function Searchdropdown(ByVal categorie As Integer, ByVal restids As List(Of Integer))
            Dim model As New CategorySearchdropdownVM
            model.Categories = db.Categories.ToList
            model.SelectedCategory = categorie
            model.Restids = restids
            Return View(model)
        End Function

    End Class
End Namespace
