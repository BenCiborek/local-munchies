﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminRestaurantController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminRestaurant/

        Function Index() As ViewResult
            Return View(db.Restaurants.ToList())
        End Function

        '
        ' GET: /AdminRestaurant/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmrestaurant As LMRestaurant = db.Restaurants.Find(id)
            Return View(lmrestaurant)
        End Function

        '
        ' GET: /AdminRestaurant/Create

        Function Create() As ViewResult
            Return View()
        End Function

        '
        ' POST: /AdminRestaurant/Create

        <HttpPost()>
        Function Create(lmrestaurant As LMRestaurant) As ActionResult
            If ModelState.IsValid Then
                db.Restaurants.Add(lmrestaurant)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            Return View(lmrestaurant)
        End Function

        '
        ' GET: /AdminRestaurant/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmrestaurant As LMRestaurant = db.Restaurants.Find(id)
            Return View(lmrestaurant)
        End Function

        '
        ' POST: /AdminRestaurant/Edit/5

        <HttpPost()>
        Function Edit(lmrestaurant As LMRestaurant) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmrestaurant).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            Return View(lmrestaurant)
        End Function

        '
        ' GET: /AdminRestaurant/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmrestaurant As LMRestaurant = db.Restaurants.Find(id)
            Return View(lmrestaurant)
        End Function

        '
        ' POST: /AdminRestaurant/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmrestaurant As LMRestaurant = db.Restaurants.Find(id)
            db.Restaurants.Remove(lmrestaurant)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace