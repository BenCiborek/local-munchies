﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenu/

        Function Index() As ViewResult
            Dim menus = db.Menus.Include(Function(l) l.Restaurant)
            Return View(menus.ToList())
        End Function

        '
        ' GET: /AdminMenu/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenu As LMMenu = db.Menus.Find(id)
            Return View(lmmenu)
        End Function

        '
        ' GET: /AdminMenu/Create

        Function Create() As ViewResult
            ViewBag.RestaurantID = New SelectList(db.Restaurants, "ID", "Name")
            Return View()
        End Function

        '
        ' POST: /AdminMenu/Create

        <HttpPost()>
        Function Create(lmmenu As LMMenu) As ActionResult
            If ModelState.IsValid Then
                db.Menus.Add(lmmenu)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            ViewBag.RestaurantID = New SelectList(db.Restaurants, "ID", "Name", lmmenu.RestaurantID)
            Return View(lmmenu)
        End Function

        '
        ' GET: /AdminMenu/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenu As LMMenu = db.Menus.Find(id)
            ViewBag.RestaurantID = New SelectList(db.Restaurants, "ID", "Name", lmmenu.RestaurantID)
            Return View(lmmenu)
        End Function

        '
        ' POST: /AdminMenu/Edit/5

        <HttpPost()>
        Function Edit(lmmenu As LMMenu) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenu).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            ViewBag.RestaurantID = New SelectList(db.Restaurants, "ID", "Name", lmmenu.RestaurantID)
            Return View(lmmenu)
        End Function

        '
        ' GET: /AdminMenu/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenu As LMMenu = db.Menus.Find(id)
            Return View(lmmenu)
        End Function

        '
        ' POST: /AdminMenu/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenu As LMMenu = db.Menus.Find(id)
            db.Menus.Remove(lmmenu)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

        Function Show(ByVal id As Integer) As ActionResult
            Dim menu = db.Menus.Find(id)
            Return View(menu)
        End Function

    End Class
End Namespace