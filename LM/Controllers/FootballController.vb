﻿Imports System.IO
Imports System.Web.Script.Serialization


Namespace LM
    Public Class FootballController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Football

        Dim db As New FBEntities
        Function Index() As ActionResult


            Dim Questions = db.Questions.Where(Function(f) f.Active = True)
            Dim itemlist As New List(Of FB_QuestionView)

            For Each Question In Questions
                Dim gameid As String = Question.gameid
                'Dim jobject As New 
                'Dim j As String = 
                'Dim jo = JsonResult.Linq.JObject.Parse(j)
                'Dim playerid = jo("params")("data")("player")("playerid")
                Dim j As Object = New JavaScriptSerializer().Deserialize(Of Object)(GetJsonGame(gameid))

                'ViewBag.Test = Getresult({"2013092901", "home", "stats", "passing", "00-0026625", "yds"}, 0, j)

                Dim item As New FB_QuestionView
                item.Question = Question.text
                item.value = Getresult(Split(gameid & "|" & Question.field, "|"), 0, j)
                item.Metric = Question.metric

                itemlist.Add(item)
            Next




            Return View(itemlist)
        End Function
        Function Getresult(ByVal locary As Array, ByVal Metric As Double, ByVal j As Object)
            Dim i = 0
            Dim retval = 0

            Try
                While i < locary.Length
                    j = j(locary(i))
                    i += 1

                End While
                retval = j
            Catch
            End Try
            Return retval
        End Function
        Function GetJsonGame(id As String)
            Dim req As System.Net.WebRequest = System.Net.WebRequest.Create("http://www.nfl.com/liveupdate/game-center/" & id & "/" & id & "_gtd.json")
            Dim resp As System.Net.WebResponse = req.GetResponse
            Dim reader As New StreamReader(resp.GetResponseStream)
            Dim str = reader.ReadToEnd()
            Return str
        End Function
        Function QuestionView(ByVal Question As FB_QuestionView) As ActionResult
            Return View(Question)
        End Function
        Function ShowVotes(ByVal UserList As List(Of String), ByVal type As Integer, ByVal pickedpercent As Double)
            ViewBag.votetype = type
            ViewBag.PickedPercent = pickedpercent
            Return View(UserList)
        End Function
    End Class
End Namespace
