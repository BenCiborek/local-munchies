﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuModifierGroupController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuModifierGroup/

        Function Index(ByVal Menuid As Integer) As ViewResult
            Dim menumodifiergroups = db.MenuModifierGroups.Where(Function(x) x.MenuID = Menuid).Include(Function(l) l.Menu)

            Dim item = db.Menus.Find(Menuid)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.Restaurant.Name

            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names

            ViewBag.menuid = Menuid
            Return View(menumodifiergroups.ToList())
        End Function

        '
        ' GET: /AdminMenuModifierGroup/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenumodifiergroup As LMMenuModifierGroup = db.MenuModifierGroups.Find(id)
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' GET: /AdminMenuModifierGroup/Create

        Function Create(ByVal Menuid As Integer) As ViewResult
            ViewBag.MenuID = Menuid
            Return View()
        End Function

        '
        ' POST: /AdminMenuModifierGroup/Create

        <HttpPost()>
        Function Create(lmmenumodifiergroup As LMMenuModifierGroup) As ActionResult
            If ModelState.IsValid Then
                db.MenuModifierGroups.Add(lmmenumodifiergroup)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.menuid = lmmenumodifiergroup.MenuID})
            End If

            ViewBag.MenuID = lmmenumodifiergroup.MenuID
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' GET: /AdminMenuModifierGroup/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenumodifiergroup As LMMenuModifierGroup = db.MenuModifierGroups.Find(id)
            ViewBag.MenuID = lmmenumodifiergroup.MenuID
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' POST: /AdminMenuModifierGroup/Edit/5

        <HttpPost()>
        Function Edit(lmmenumodifiergroup As LMMenuModifierGroup) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenumodifiergroup).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.menuid = lmmenumodifiergroup.MenuID})
            End If

            ViewBag.MenuID = lmmenumodifiergroup.MenuID
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' GET: /AdminMenuModifierGroup/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenumodifiergroup As LMMenuModifierGroup = db.MenuModifierGroups.Find(id)
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' POST: /AdminMenuModifierGroup/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenumodifiergroup As LMMenuModifierGroup = db.MenuModifierGroups.Find(id)
            db.MenuModifierGroups.Remove(lmmenumodifiergroup)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.menuid = lmmenumodifiergroup.MenuID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace