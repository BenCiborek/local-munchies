﻿Namespace LM
    Public Class LocationController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Location
        Dim db As New FBEntities
        Function Index() As ActionResult
            Dim Locations = db.Locations.ToList
            Return View(Locations)
        End Function
        Function Dropdown()
            Dim location As New LocationDropDownVM
            location.SelectedLocation = Session("Location")
            location.Locations = db.Locations.ToList
            Return View(location)
        End Function
    End Class
End Namespace
