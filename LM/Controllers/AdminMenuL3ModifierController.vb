﻿Imports System.Data.Entity
Imports LM

Namespace LM
    Public Class AdminMenuL3ModifierController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuL3Modifier/

        Function Index(ByVal ModID As Integer) As ViewResult
            Dim menumodifiers = db.MenuL3Modifiers.Where(Function(z) z.L2MenuModifierID = ModID).Include(Function(l) l.L2MenuModifier)

            Dim item = db.MenuL2Modifiers.Find(ModID)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.L2MenuModifierGroup.Modifier.MenuModifierGroup.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuModifierGroup", New With {.Menuid = item.L2MenuModifierGroup.Modifier.MenuModifierGroup.MenuID})
            Names &= "," & item.L2MenuModifierGroup.Modifier.MenuModifierGroup.Name

            URLs &= "," & Url.Action("Index", "AdminMenuModifier", New With {.GroupId = item.L2MenuModifierGroup.Modifier.MenuModifierGroupID})
            Names &= "," & item.L2MenuModifierGroup.Modifier.Name

            URLs &= "," & Url.Action("Index", "AdminMenuL2Modifier", New With {.GroupId = item.L2MenuModifierGroupID})
            Names &= "," & item.L2MenuModifierGroup.Name


            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names


            ViewBag.MenuModifierGroupID = ModID
            Return View(menumodifiers.ToList())
        End Function

        '
        ' GET: /AdminMenuL3Modifier/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenul3modifier As LMMenuL3Modifier = db.MenuL3Modifiers.Find(id)
            Return View(lmmenul3modifier)
        End Function

        '
        ' GET: /AdminMenuL3Modifier/Create

        Function Create As ViewResult
            ViewBag.L2MenuModifierID = New SelectList(db.MenuL2Modifiers, "ID", "Name")
            return View()
        End Function

        '
        ' POST: /AdminMenuL3Modifier/Create

        <HttpPost()>
        Function Create(lmmenul3modifier As LMMenuL3Modifier) As ActionResult
            If ModelState.IsValid Then
                db.MenuL3Modifiers.Add(lmmenul3modifier)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            ViewBag.L2MenuModifierID = New SelectList(db.MenuL2Modifiers, "ID", "Name", lmmenul3modifier.L2MenuModifierID)
            Return View(lmmenul3modifier)
        End Function
        
        '
        ' GET: /AdminMenuL3Modifier/Edit/5
 
        Function Edit(id As Integer) As ViewResult
            Dim lmmenul3modifier As LMMenuL3Modifier = db.MenuL3Modifiers.Find(id)
            ViewBag.L2MenuModifierID = New SelectList(db.MenuL2Modifiers, "ID", "Name", lmmenul3modifier.L2MenuModifierID)
            Return View(lmmenul3modifier)
        End Function

        '
        ' POST: /AdminMenuL3Modifier/Edit/5

        <HttpPost()>
        Function Edit(lmmenul3modifier As LMMenuL3Modifier) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenul3modifier).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            ViewBag.L2MenuModifierID = New SelectList(db.MenuL2Modifiers, "ID", "Name", lmmenul3modifier.L2MenuModifierID)
            Return View(lmmenul3modifier)
        End Function

        '
        ' GET: /AdminMenuL3Modifier/Delete/5
 
        Function Delete(id As Integer) As ViewResult
            Dim lmmenul3modifier As LMMenuL3Modifier = db.MenuL3Modifiers.Find(id)
            Return View(lmmenul3modifier)
        End Function

        '
        ' POST: /AdminMenuL3Modifier/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenul3modifier As LMMenuL3Modifier = db.MenuL3Modifiers.Find(id)
            db.MenuL3Modifiers.Remove(lmmenul3modifier)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace