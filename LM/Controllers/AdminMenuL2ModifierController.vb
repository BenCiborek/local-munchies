﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuL2ModifierController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuL2Modifier/

        Function Index(ByVal GroupID As Integer) As ViewResult
            Dim menumodifiers = db.MenuL2Modifiers.Where(Function(z) z.L2MenuModifierGroupID = GroupID).Include(Function(l) l.L2MenuModifierGroup)

            Dim item = db.MenuL2ModifierGroups.Find(GroupID)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.Modifier.MenuModifierGroup.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuModifierGroup", New With {.Menuid = item.Modifier.MenuModifierGroup.MenuID})
            Names &= "," & item.Modifier.MenuModifierGroup.Name

            URLs &= "," & Url.Action("Index", "AdminMenuModifier", New With {.GroupId = item.Modifier.MenuModifierGroupID})
            Names &= "," & item.Modifier.Name


            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names


            ViewBag.MenuModifierGroupID = GroupID
            Return View(menumodifiers.ToList())
        End Function

        '
        ' GET: /AdminMenuL2Modifier/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenumodifier As LMMenuL2Modifier = db.MenuL2Modifiers.Find(id)
            Return View(lmmenumodifier)
        End Function

        '
        ' GET: /AdminMenuL2Modifier/Create

        Function Create(ByVal GroupID As Integer) As ViewResult
            ViewBag.MenuModifierGroupID = GroupID
            Return View()
        End Function

        '
        ' POST: /AdminMenuL2Modifier/Create

        <HttpPost()>
        Function Create(lmmenumodifier As LMMenuL2Modifier) As ActionResult
            If ModelState.IsValid Then
                db.MenuL2Modifiers.Add(lmmenumodifier)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.groupID = lmmenumodifier.L2MenuModifierGroupID})
            End If

            ViewBag.MenuModifierGroupID = lmmenumodifier.L2MenuModifierGroupID
            Return View(lmmenumodifier)
        End Function

        '
        ' GET: /AdminMenuL2Modifier/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenumodifier As LMMenuL2Modifier = db.MenuL2Modifiers.Find(id)
            ViewBag.MenuModifierGroupID = lmmenumodifier.L2MenuModifierGroupID
            Return View(lmmenumodifier)
        End Function

        '
        ' POST: /AdminMenuL2Modifier/Edit/5

        <HttpPost()>
        Function Edit(lmmenumodifier As LMMenuL2Modifier) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenumodifier).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.groupID = lmmenumodifier.L2MenuModifierGroupID})
            End If

            ViewBag.MenuModifierGroupID = lmmenumodifier.L2MenuModifierGroupID
            Return View(lmmenumodifier)
        End Function

        '
        ' GET: /AdminMenuL2Modifier/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenumodifier As LMMenuL2Modifier = db.MenuL2Modifiers.Find(id)
            Return View(lmmenumodifier)
        End Function

        '
        ' POST: /AdminMenuModifier/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenumodifier As LMMenuL2Modifier = db.MenuL2Modifiers.Find(id)
            db.MenuL2Modifiers.Remove(lmmenumodifier)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.groupID = lmmenumodifier.L2MenuModifierGroupID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace
