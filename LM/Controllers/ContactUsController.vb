﻿Namespace LM
    Public Class ContactUsController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /ContactUs
        Dim db As FBEntities = New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function

        <HttpPost> _
        Function Index(ByVal cus As ContactUs)
            If ModelState.IsValid Then
                db.ContacUs.Add(cus)
                db.SaveChanges()

                Dim email As New EmailModel
                Dim body = "<table><tr><td>From: " & cus.Name & " - " & cus.Email & " </td><td> " & cus.Message & "</td></tr></table>"
                email.AddEmail("CU", 0, "Support@Localmunchies.com", "NoReply@Localmunchies.com", "Contact Local Munchies", body, False, "", 1)


                Return RedirectToAction("Index", "LMSearch")
            End If

            Return View(cus)
        End Function

    End Class
End Namespace
