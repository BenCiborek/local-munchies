﻿Namespace LM
    Public Class StatsController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Stats

        Function Index() As ActionResult
            Return View()
        End Function

        Function CustomersServed() As ActionResult
            Dim SQL As String = ""
            Dim CS As New LM_LMD_CustomersServed
            ViewData("Message") = GetWebResponse("http://lmd.azurewebsites.net")
            Return View(CS)
        End Function
        Function GetCustomersServed(ByVal Connstr As String, ByVal Sql As String) As CustomersServedVM
            Dim CS As New CustomersServedVM
            Dim ObjConn As System.Data.SqlClient.SqlConnection
            Dim ObjCmd As System.Data.SqlClient.SqlCommand
            Dim ObjDR As System.Data.SqlClient.SqlDataReader

            'Create DB connection object
            ObjConn = New System.Data.SqlClient.SqlConnection(Connstr)

            'Open DB connection
            ObjConn.Open()

            'Create command Object
            ObjCmd = New System.Data.SqlClient.SqlCommand(Sql, ObjConn)
            ObjCmd.CommandTimeout = 900000

            'Create data reader object
            ObjDR = ObjCmd.ExecuteReader()

            'Get product prices
            If ObjDR.HasRows Then
                While ObjDR.Read()
                    ' AddtoEmails(ObjDR.Item("emailconnstr").ToString, ObjDR.Item("emailquery").ToString, ObjDR.Item("emailtext").ToString, ObjDR.Item("emailsubject").ToString, ObjDR.Item("emailfrom").ToString, ObjDR.Item("id").ToString)
                    CS.TotalCustomers = ObjDR.Item("TotalCustomers").ToString
                    CS.TotalCustomersDay = ObjDR.Item("TotalCustomersDay").ToString
                    CS.TotalCustomersWeek = ObjDR.Item("TotalCustomersWeek").ToString
                    CS.AvgCustomersPerDayLastWeek = ObjDR.Item("AvgCustomersPerDayLastWeek").ToString
                    CS.TotalCustomersMonth = ObjDR.Item("TotalCustomersMonth").ToString
                    CS.AvgCustomersPerDayLastMonth = ObjDR.Item("AvgCustomersPerDayLastMonth").ToString
                    CS.TotalCustomersYear = ObjDR.Item("TotalCustomersYear").ToString
                    CS.AvgCustomersPerDayLastYear = ObjDR.Item("AvgCustomersPerDayLastYear").ToString


                End While
            End If
            ObjDR.Close()
            ObjCmd.Dispose()
            ObjConn.Dispose()
            Return CS
        End Function
        Function AddCustomersServedEmail()
            Dim EmailSendVM As New EmailSendVM
            Try


                Dim Email As New EmailModel

                Dim SQL As String = ""
                Dim CS As New LM_LMD_CustomersServed
                CS.LM_CustomersServed = GetCustomersServed(System.Configuration.ConfigurationManager.ConnectionStrings("FBEntities").ConnectionString, "select * from [LM_Customer_Report]")
                CS.LMD_CustomersServed = GetCustomersServedXML("http://LocalMunchiesDelivery.com/emails/CustomersServedXML")


                Dim emailto = "Bennyc66@gmail.com,sjones@localmunchies.com,nj5121@gmail.com"
                Dim emailfrom = "NoReply@LocalMunchiesDelivery.com"
                Dim emailsubject = "Customers Served"
                Dim Email_Body = Email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/Stats/CustomersServed.vbhtml", CS, "_Email.vbhtml")

                Email.AddEmail("A", 1, emailto, emailfrom, emailsubject, Email_Body, False, "", 1)

            Catch ex As Exception
                EmailSendVM.Message = ex.ToString
            End Try

            Return View(EmailSendVM)
        End Function
        Function GetCustomersServedXML(ByVal WebAddress As String)
            Dim CS As New CustomersServedVM

            Dim contents = GetWebResponse(WebAddress)

            ' Create a new, empty XML document
            Dim document As New System.Xml.XmlDocument()

            ' Load the contents into the XML document
            document.LoadXml(contents)


            Dim xmlelement As System.Xml.XmlElement = document.FirstChild()

            CS.TotalCustomers = xmlelement.SelectSingleNode("TotalCustomers").InnerText
            CS.TotalCustomersDay = xmlelement.SelectSingleNode("TotalCustomersDay").InnerText
            CS.TotalCustomersWeek = xmlelement.SelectSingleNode("TotalCustomersWeek").InnerText
            CS.AvgCustomersPerDayLastWeek = xmlelement.SelectSingleNode("AvgCustomersPerDayLastWeek").InnerText
            CS.TotalCustomersMonth = xmlelement.SelectSingleNode("TotalCustomersMonth").InnerText
            CS.AvgCustomersPerDayLastMonth = xmlelement.SelectSingleNode("AvgCustomersPerDayLastMonth").InnerText
            CS.TotalCustomersYear = xmlelement.SelectSingleNode("TotalCustomersYear").InnerText
            CS.AvgCustomersPerDayLastYear = xmlelement.SelectSingleNode("AvgCustomersPerDayLastYear").InnerText

            Return CS
        End Function
        Function GetWebResponse(ByVal url As String) As String
            Dim Val As String = ""
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(url)


            ' Call the remote site, and parse the data in a response object
            Dim response As System.Net.HttpWebResponse = request.GetResponse()

            ' Check if the response is OK (status code 200)
            If response.StatusCode = System.Net.HttpStatusCode.OK Then

                ' Parse the contents from the response to a stream object
                Dim stream As System.IO.Stream = response.GetResponseStream()
                ' Create a reader for the stream object
                Dim reader As New System.IO.StreamReader(stream)
                ' Read from the stream object using the reader, put the contents in a string
                Val = reader.ReadToEnd()
            Else
                Throw New Exception("Could not retrieve document from the URL, response code: " & response.StatusCode)
            End If

            Return Val
        End Function


    End Class
End Namespace
