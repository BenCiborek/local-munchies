﻿Imports System.ComponentModel.DataAnnotations

Namespace LM
    Public Class AddressController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Address
        Dim db As FBEntities = New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function


        <HttpPost()> _
        Function AddAddressJSON(ByVal email As String, ByVal name As String, ByVal street As String, ByVal street2 As String, ByVal city As String, ByVal State As String, ByVal zip As String, ByVal phone As String, ByVal type As Integer, ByVal isDefault As Boolean)
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim errormessage As String = ""

            Dim account = db.Users.Where(Function(x) x.UserName = User.Identity.Name).FirstOrDefault

            If account Is Nothing Then
                account = New User
            End If

            Dim arr = New List(Of String)
            Dim address As New Address
            address.Email = email
            address.UserID = account.UserID
            address.SessionID = cartid
            address.Name = name
            address.Address1 = street
            address.Address2 = street2
            address.City = city
            address.State = State
            address.Zip = zip
            address.phone = phone
            address.Type = type
            address.AddressDefault = True


            Dim validationContext As ValidationContext = New ValidationContext(address)
            Dim validationResults As List(Of ValidationResult) = New List(Of ValidationResult)

            Validator.TryValidateObject(address, validationContext, validationResults, True)

            For Each validationresult In validationResults
                errormessage &= validationresult.ErrorMessage & "<br />"
            Next


            If errormessage <> "" Then
                arr.Add("0")
                arr.Add(errormessage)
            Else
                Try
                    db.Addresses.Add(address)
                    db.SaveChanges()
                    arr.Add("1")
                    arr.Add(address.ID)
                Catch ex As Exception
                    arr.Add("0")
                    arr.Add(ex.Message)
                End Try

            End If
            Return Json(arr)
        End Function

    End Class
End Namespace
