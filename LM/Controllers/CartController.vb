﻿Imports System.Data.Entity

Namespace LM
    Public Class CartController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Cart
        Dim Db As FBEntities = New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function

        Function ShowCart(Optional ByVal showOptions As Boolean = True) As ActionResult
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)

            Dim ShoppingCartVM As New ShoppingCartVM
            ShoppingCartVM.showOptions = showOptions
            ShoppingCartVM.Ticket = ticket
            ShoppingCartVM.ItemCount = 0
            If Not ticket Is Nothing Then
                Dim rests = ShoppingCartVM.Ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList

                ShoppingCartVM.DeliveryAvailable = True
                ShoppingCartVM.Rests = Db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList
                ShoppingCartVM.ItemCount = ticket.ticketitems.Count()
                For Each rest In ShoppingCartVM.Rests
                    Dim RestaurantInfo = Db.RestaurantInfo.Where(Function(x) x.RestaurantID = rest.ID).FirstOrDefault

                    If RestaurantInfo Is Nothing Then
                        ShoppingCartVM.DeliveryAvailable = False
                    Else
                        If RestaurantInfo.DeliveryRadius = 0 Then
                            ShoppingCartVM.DeliveryAvailable = False
                        End If
                    End If
                Next
            End If


            Return View(ShoppingCartVM)
        End Function

        <HttpPost()> _
        Function AddAddressesToTicketJSON(ByVal DeliveryAddressID As Integer, BilltoAddressID As Integer)
            Dim arr = New List(Of String)
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)
            ticket.DeliveryAddress = DeliveryAddressID
            ticket.BilltoAddress = BilltoAddressID
            Try
                Db.Entry(ticket).State = EntityState.Modified
                Db.SaveChanges()
                arr.Add("1")
            Catch ex As Exception
                arr.Add("0")
                arr.Add(ex.Message)
            End Try



            Return (Json(arr))
        End Function
        <HttpPost()> _
        Function AddPaymentJSON(ByVal amount As Double, ByVal CCNumber As String, ByVal Expiry As String, ByVal CVV As String)
            Dim arr = New List(Of String)
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)

            Dim CreditCard As New CreditCard
            Dim ResponseCode As String = ""
            Dim ResponseMessage As String = ""
            Dim Authcode As String = ""
            Dim billtoaddress = Db.Addresses.Find(ticket.BilltoAddress)

            Try
                If CreditCard.ChargeCard(amount, billtoaddress.Name, CCNumber, billtoaddress.Zip, Expiry, CVV, ticket.ID, ResponseCode, ResponseMessage, Authcode) Then

                    Dim Payment As New Payment
                    Payment.Authcode = Authcode
                    Payment.Authorized = True
                    Payment.Name = ""
                    Payment.Type = 1

                    Db.Payments.Add(Payment)
                    Db.SaveChanges()

                    ticket.Paid = True
                    ticket.PaidAmount = ticket.TotalPrice
                    ticket.TicketStatus = 2
                    ticket.ClosingDate = DateTime.UtcNow()
                    ticket.ReceiptID = Guid.NewGuid().ToString
                    ticket.PaymentID = Payment.ID

                    Db.Entry(ticket).State = EntityState.Modified
                    Db.SaveChanges()

                    Dim user = Db.Users.Where(Function(x) x.UserName = ticket.Username).FirstOrDefault
                    Dim userid = 0
                    If user Is Nothing Then
                        userid = 0
                    Else
                        userid = user.UserID
                    End If

                    Dim ReceiptVM As New ReceiptVM
                    ReceiptVM.Ticket = ticket

                    Dim rests = ReceiptVM.Ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList

                    ReceiptVM.Rests = Db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList

                    ReceiptVM.Billto = Db.Addresses.Find(ReceiptVM.Ticket.BilltoAddress)
                    ReceiptVM.Deliverto = Db.Addresses.Find(ReceiptVM.Ticket.DeliveryAddress)

                    Dim email As New EmailModel
                    Dim emailbody = email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/Email/OrderConfirmation.vbhtml", ReceiptVM, "_Email.vbhtml")
                    email.AddEmail("C", userid, billtoaddress.Email, "NoReply@Localmunchies.com", "Your Local Munchies order has been placed and will be ready soon", emailbody, False, "", 1)

                    For Each rest In ReceiptVM.Rests
                        Dim Restid = rest.ID
                        Dim Restaurantinfo = Db.RestaurantInfo.Where(Function(x) x.RestaurantID = Restid).FirstOrDefault

                        Dim submitReceiptVM = ReceiptVM
                        submitReceiptVM.Rests = Db.Restaurants.Where(Function(x) x.ID = Restid).ToList
                        submitReceiptVM.Notes = Restaurantinfo.Notes


                        'If Customer Gets Email submission then 
                        Select Case Restaurantinfo.SubmissionType
                            Case 1

                            Case 2
                                'Create Email
                                Dim emailsubmission As New EmailModel
                                Dim emailsubmissionbody = emailsubmission.RenderViewToString(Me.ControllerContext.Controller, "~/Views/Email/OrderSubmission.vbhtml", submitReceiptVM, "_Email.vbhtml")
                                email.AddEmail("R", userid, Restaurantinfo.EmailTo, "NoReply@Localmunchies.com", "New Local Munchies Order: " & ticket.ID, emailsubmissionbody, False, "", 1)

                                email.AddEmail("R", userid, "OrderFax@localmunchies.com", "NoReply@Localmunchies.com", "New Local Munchies Order: " & ticket.ID, emailsubmissionbody, False, "", 1)


                                'Update Ticket Status
                                ticket.TicketStatus = 3
                                Db.Entry(ticket).State = EntityState.Modified
                                Db.SaveChanges()

                                'Creat Fax Doc
                                Dim FaxSent As New FaxSent
                                FaxSent.DateSent = DateTime.UtcNow()
                                FaxSent.FaxPage = "Email"
                                FaxSent.RestID = Restid
                                FaxSent.Ticketid = ticket.ID
                                FaxSent.ToNumber = Restaurantinfo.EmailTo
                                FaxSent.Status = 1
                                FaxSent.DateConfirmed = #1/1/1900#

                                Db.Faxsent.Add(FaxSent)
                                Db.SaveChanges()

                            Case 3
                                'Create Fax sent to make order phone call
                                'Creat Fax Doc
                                Dim FaxSent As New FaxSent
                                FaxSent.DateSent = DateTime.UtcNow()
                                FaxSent.FaxPage = "Order Call"
                                FaxSent.RestID = Restid
                                FaxSent.Ticketid = ticket.ID
                                FaxSent.ToNumber = Restaurantinfo.OrderPhoneNumber
                                FaxSent.Status = 3
                                FaxSent.DateConfirmed = #1/1/1900#

                                Db.Faxsent.Add(FaxSent)
                                Db.SaveChanges()

                                'Update Ticket Status
                                ticket.TicketStatus = 3
                                Db.Entry(ticket).State = EntityState.Modified
                                Db.SaveChanges()

                        End Select

                        If Restaurantinfo.SubmissionType = 2 Then

                        End If

                        'If we need to send to LMD
                        If Restaurantinfo.hasLMDAccount And ticket.TicketTypeID = 2 Then


                            'Add LMD status to let everyone know it needs to be sent
                            ticket.LMDStatus = 2
                            'Update Ticket 
                            Db.Entry(ticket).State = EntityState.Modified
                            Db.SaveChanges()

                        End If
                    Next

                    arr.Add("1")
                    arr.Add(ticket.ReceiptID)
                    cart.CloseCart(Me.HttpContext)
                Else
                    arr.Add("0")
                    arr.Add(ResponseMessage)
                End If
            Catch ex As Exception
                arr.Add("0")
                arr.Add(ex.Message)
            End Try
            Return Json(arr)
        End Function
        <HttpPost()> _
        Function RemoveItemAJAX(ByVal id As Integer)
            Dim cart = ShoppingCart.GetCart(Me.HttpContext)
            cart.RemoveFromCart(id)
            Return RedirectToAction("ShowCart", "Cart", New With {.showOptions = False})

        End Function

        <HttpPost()> _
        Function ChangeTicketType(ByVal ticketID As Integer, ByVal ticketTypeID As Integer)
            Dim cart = New ShoppingCart
            cart.UpdateTicketType(ticketID, ticketTypeID)
            Return RedirectToAction("ShowCart", "Cart", New With {.showOptions = False})
        End Function
        <HttpPost()> _
        Function isDeliverable(ByVal Address As String, ByVal city As String, ByVal State As String, ByVal zip As String) As JsonResult
            Dim arr = New List(Of String)
            Dim restaurant As New Restaurant
            Dim adddressgeocode As GeoCode = restaurant.AddressToGeocode(Address & " , " & city & " , " & State & " , " & zip)

            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)

            Dim rests = ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList

            Dim TicketRestaurants = Db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList
            Dim deliverable As Boolean = True
            For Each ticketrestaurant In TicketRestaurants
                If restaurant.isAddressinDeliveryZone(adddressgeocode, ticketrestaurant) <> 2 Then
                    deliverable = False
                End If
            Next
            If deliverable Then
                arr.Add("1")
            Else
                arr.Add("0")
            End If

            Return (Json(arr))
        End Function

        <HttpPost()> _
        Function AddCoupon(ByVal ticketID As Integer, ByVal Code As String)
            Dim cart = New ShoppingCart
            cart.AddCoupon(ticketID, Code)
            Return RedirectToAction("ShowCart", "Cart", New With {.showOptions = False})
        End Function

        Function AddTip(ByVal tip As Double)
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)

            cart.AddTip(tip, cartid)

            Return RedirectToAction("PaymentView", "Checkout", New With {.showTip = False})
        End Function

    End Class
End Namespace
