﻿Namespace LM
    Public Class EmailController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Email
        Dim Db As FBEntities = New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function

        Function Header(Optional ByVal Message As String = "")
            Dim EmailHeaderVM As New EmailHeaderVM
            EmailHeaderVM.Message = Message
            Return View(EmailHeaderVM)
        End Function

        Function SignUp(user As User) As ActionResult
            Return View(user)
        End Function
        Function SendEmail(ByVal View As String, ByVal Model As Object)
            Dim email As New EmailModel
            Dim testemail = email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/Email/SignUp.vbhtml", Nothing, "_Email.vbhtml")
            Return testemail
        End Function
        Function Footer() As ActionResult
            Return View()
        End Function
        Function OrderConfirmation(ByVal id As Integer)
            Dim ReceiptVM As New ReceiptVM
            ReceiptVM.Ticket = Db.Tickets.Find(id)

            Dim rests = ReceiptVM.Ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList

            ReceiptVM.Rests = Db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList

            ReceiptVM.Billto = Db.Addresses.Find(ReceiptVM.Ticket.BilltoAddress)
            ReceiptVM.Deliverto = Db.Addresses.Find(ReceiptVM.Ticket.DeliveryAddress)
            Return View(ReceiptVM)
        End Function
        Function OrderSubmission(ByVal id As Integer)
            Dim ReceiptVM As New ReceiptVM
            ReceiptVM.Ticket = Db.Tickets.Find(id)

            Dim rests = ReceiptVM.Ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList

            ReceiptVM.Rests = Db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList

            ReceiptVM.Billto = Db.Addresses.Find(ReceiptVM.Ticket.BilltoAddress)
            ReceiptVM.Deliverto = Db.Addresses.Find(ReceiptVM.Ticket.DeliveryAddress)
            Return View(ReceiptVM)
        End Function
        Function SignUpThankYou()
            Return View()
        End Function
    End Class
End Namespace
