﻿Imports RazorPDF
Namespace LM
    Public Class PDFController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /PDF
        Private db As FBEntities = New FBEntities

        Function Index() As ActionResult
            Return View()
        End Function

        Function Sample()
            Dim pdfview As New PdfResult(Nothing, "sample")

            Return pdfview
        End Function

        Function FaxTicket(ByVal id As Integer, ByVal Restid As Integer)
            Dim FaxTicketVM As New FaxTicketVM
            Dim Restinfo = db.RestaurantInfo.Where(Function(x) x.RestaurantID = Restid).FirstOrDefault
            FaxTicketVM.Items = db.TicketItems.Where(Function(x) x.TicketID = id And x.RestaurantID = Restid).ToList
            FaxTicketVM.RestaurantName = FaxTicketVM.Items(0).Restaurant.Name

            FaxTicketVM.TotalAmount = 0
            For Each item In FaxTicketVM.Items
                FaxTicketVM.TotalAmount += item.TotalItemPrice
            Next

            FaxTicketVM.Notes = Restinfo.Notes

            FaxTicketVM.OrderNumber = id
            FaxTicketVM.Confirmation = Right(id, 2)

            Dim ticket = db.Tickets.Find(id)
            FaxTicketVM.IntOrderType = ticket.TicketTypeID
            FaxTicketVM.DeliveryPrice = ticket.DeliveryPrice
            FaxTicketVM.BilltoAddress = db.Addresses.Find(ticket.BilltoAddress)
            If ticket.TicketTypeID = 1 Then
                'Pickup 
                FaxTicketVM.OrderType = "Take-Out"
            Else
                'Delivery
                FaxTicketVM.OrderType = "Delivery"

                FaxTicketVM.DeliveryAddress = db.Addresses.Find(ticket.DeliveryAddress)
            End If

            Dim pdfview As New PdfResult(FaxTicketVM, "FaxTicket")

            Return pdfview
        End Function

    End Class
End Namespace
