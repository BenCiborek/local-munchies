﻿Imports System.Diagnostics.CodeAnalysis
Imports System.Security.Principal
Imports System.Web.Routing
Imports System.ComponentModel.DataAnnotations
Imports System.Security.Cryptography
Imports System.Text

Public Class AccountController
    Inherits System.Web.Mvc.Controller



    Dim db As FBEntities = New FBEntities
    <Authorize()> _
    Function Index(Optional ByVal Message As String = "") As ActionResult
        Return View(Message)
    End Function

    '
    ' GET: /Account/LogOn
    <ProdNeedHTTPS> _
    Public Function LogOn(Optional ByVal returnurl As String = "") As ActionResult
        Dim logonVM As New LogonVM
        logonVM.ReturnURL = returnurl
        Return View(logonVM)
    End Function

    '
    ' POST: /Account/LogOn

    <HttpPost()> _
    <ProdNeedHTTPS> _
    Public Function LogOn(ByVal model As LogonVM) As ActionResult
        If ModelState.IsValid Then


            Dim logonresults As JsonResult = JsonLogin(model.Email, model.Password)

            If logonresults.Data(0) = "1" Then
                If Url.IsLocalUrl(model.ReturnURL) AndAlso model.ReturnURL.Length > 1 AndAlso model.ReturnURL.StartsWith("/") _
                  AndAlso Not model.ReturnURL.StartsWith("//") AndAlso Not model.ReturnURL.StartsWith("/\\") Then
                    Return Redirect(model.ReturnURL)
                Else
                    Return RedirectToAction("Index", "LMSearch")
                End If
            Else
                model.Message = "The user name or password provided is incorrect."
            End If
        End If

        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    'POST:/Account/JsonLogin
    <HttpPost()> _
    <ProdNeedHTTPS> _
    Function JsonLogin(ByVal user As String, ByVal pass As String)
        Dim arr = New List(Of String)
        Dim passhash = CreateMD5Hash(pass)
        Dim userisvalid As Boolean = db.Users.Any(Function(f) f.Email = user And f.password = passhash)
        If userisvalid Then
            Dim account = db.Users.Where(Function(f) f.Email = user And f.password = passhash).FirstOrDefault
            FormsAuthentication.SetAuthCookie(account.UserName, False)
            arr.Add("1")
            arr.Add("User is logged in")

            Dim cart = ShoppingCart.GetCart(Me.HttpContext)
            cart.AddCarttoUser(account.UserName)

        Else
            arr.Add("0")
            arr.Add("The user name or password provided is incorrect.")
        End If
        Return Json(arr)
    End Function

    '
    ' GET: /Account/LogOff

    Public Function LogOff() As ActionResult
        FormsAuthentication.SignOut()

        Return RedirectToAction("Index", "LMSearch")
    End Function

    '
    ' GET: /Account/Register
    <ProdNeedHTTPS> _
    Public Function Register() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Account/Register

    <HttpPost()> _
    <ProdNeedHTTPS> _
    Public Function Register(ByVal model As RegisterVM) As ActionResult


        Dim Registerresults As JsonResult = JSONRegister(model)

        If Registerresults.Data(0) = "1" Then
            Return RedirectToAction("Index", "Account")
        Else
            model.Message = Registerresults.Data(1)
        End If


        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    ' POST: /Account/JSONRegister
    <HttpPost()> _
    <ProdNeedHTTPS> _
    Public Function JSONRegister(ByVal model As RegisterVM) As ActionResult
        Dim arr = New List(Of String)
        Dim errormessage As String = ""


        If ModelState.IsValid() Then
            Try

                If db.Users.SingleOrDefault(Function(x) x.Email = model.Email) Is Nothing Then
                    'No User already

                    Dim newuser As New User
                    newuser.UserName = Guid.NewGuid.ToString
                    newuser.Email = model.Email
                    newuser.Roles = "User"
                    newuser.password = CreateMD5Hash(model.Password)

                    db.Users.Add(newuser)
                    db.SaveChanges()
                    FormsAuthentication.SetAuthCookie(newuser.UserName, False)

                    'Add Email to list

                    Dim email As New EmailModel
                    Dim emailbody = email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/Email/SignUp.vbhtml", newuser, "_Email.vbhtml")
                    email.AddEmail("C", newuser.UserID, newuser.Email, "NoReply@Localmunchies.com", "Welcome to LocalMunchies.com", emailbody, False, "", 1)


                    arr.Add("1")
                    arr.Add("User is Registered in")
                Else
                    'User Already Registered 
                    arr.Add("0")
                    arr.Add("You are already registerd. Please Login.")
                End If
            Catch ex As Exception
                arr.Add("0")
                arr.Add(ex.Message)
            End Try
        Else
            For Each ModelError In ModelState.Values.SelectMany(Function(v) v.Errors)
                errormessage &= ModelError.ErrorMessage & "<br />"
            Next
            arr.Add("0")
            arr.Add(errormessage)
        End If

        Return Json(arr)
    End Function

    '
    ' GET: /Account/ChangePassword

    <Authorize()> _
    <ProdNeedHTTPS> _
    Public Function ChangePassword() As ActionResult

        Return View()
    End Function

    '
    ' POST: /Account/ChangePassword

    <Authorize()> _
    <HttpPost()> _
    <ProdNeedHTTPS> _
    Public Function ChangePassword(ByVal model As ChangePasswordModel) As ActionResult
        If ModelState.IsValid Then
            ' ChangePassword will throw an exception rather
            ' than return false in certain failure scenarios.
            Dim changePasswordSucceeded As Boolean

            Try
                Dim currentUser As MembershipUser = Membership.GetUser(User.Identity.Name, True)
                changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword)
            Catch ex As Exception
                changePasswordSucceeded = False
            End Try

            If changePasswordSucceeded Then
                Return RedirectToAction("ChangePasswordSuccess")
            Else
                ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.")
            End If
        End If

        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    ' GET: /Account/ChangePasswordSuccess

    Public Function ChangePasswordSuccess() As ActionResult
        Return View()
    End Function


    Public Function CreateMD5Hash(ByVal input As String) As String
        Dim md5 As MD5 = md5.Create()
        Dim sb As New StringBuilder
        Dim inBytes() As Byte = System.Text.Encoding.ASCII.GetBytes(input)
        Dim hashbytes() As Byte = md5.ComputeHash(inBytes)

        For i = 0 To hashbytes.Length - 1
            sb.Append(hashbytes(i).ToString(ConfigurationManager.AppSettings("PasswordHash")))
        Next
        Return sb.ToString()
    End Function

    Function LoginHeaders()
        Dim LoginHeadersVM As New LoginHeadersVM
        LoginHeadersVM.Loggedin = User.Identity.IsAuthenticated
        Return View(LoginHeadersVM)
    End Function

    Function SignUpDetail() As ActionResult
        Return View()
    End Function

    Function SignUp() As ActionResult
        Return View()
    End Function
    <HttpPost()>
    Function SignUp(ByVal Model As SignUp) As ActionResult
        Model.SignUpDate = Now.ToUniversalTime
        If ModelState.IsValid Then
            'Add to table
            Try
                db.Signups.Add(Model)
                db.SaveChanges()
                Dim email As New EmailModel
                Dim emailbody = email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/Email/SignUp.vbhtml", Nothing, "_Email.vbhtml")
                email.AddEmail("C", 0, Model.Email, "NoReply@Localmunchies.com", "Welcome to LocalMunchies.com", emailbody, False, "", 1)

            Catch ex As Exception

            End Try
        End If
        Return RedirectToAction("ThankYou")
    End Function

    Function ThankYou() As ActionResult
        Return View()
    End Function




#Region "Status Code"
    Public Function ErrorCodeToString(ByVal createStatus As MembershipCreateStatus) As String
        ' See http://go.microsoft.com/fwlink/?LinkID=177550 for
        ' a full list of status codes.
        Select Case createStatus
            Case MembershipCreateStatus.DuplicateUserName
                Return "User name already exists. Please enter a different user name."

            Case MembershipCreateStatus.DuplicateEmail
                Return "A user name for that e-mail address already exists. Please enter a different e-mail address."

            Case MembershipCreateStatus.InvalidPassword
                Return "The password provided is invalid. Please enter a valid password value."

            Case MembershipCreateStatus.InvalidEmail
                Return "The e-mail address provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidAnswer
                Return "The password retrieval answer provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidQuestion
                Return "The password retrieval question provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidUserName
                Return "The user name provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.ProviderError
                Return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator."

            Case MembershipCreateStatus.UserRejected
                Return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator."

            Case Else
                Return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator."
        End Select
    End Function
#End Region

End Class
