﻿Namespace LM
    Public Class MenuController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Menu


        Private db As FBEntities = New FBEntities

        Function Index() As ActionResult
            Return View()
        End Function
        Function Show(ByVal id As Integer)
            Dim Menu = db.Menus.Find(id)
            Return View(Menu)
        End Function
        Function ShowMore(ByVal id As Integer)
            Dim Menu = db.Menus.Find(id)
            Return View(Menu)
        End Function
        Function Order(ByVal id As Integer)
            Dim Menu = db.Menus.Find(id)
            Return View(Menu)
        End Function

        Function GetMenuItemOptions(ByVal id As Integer)
            Dim Item = db.MenuItems.Find(id)
            Dim GetMenuItemOptionsVM As New GetMenuItemOptionsVM
            Dim Rest As New Restaurant
            GetMenuItemOptionsVM.Item = Item
            GetMenuItemOptionsVM.Open = Rest.isOpen(Item.MenuGroup.MenuCategory.Menu.Restaurant)
            Return View(GetMenuItemOptionsVM)
        End Function
        Function AddItemToCart(ByVal Itemid As Integer, ByVal Qty As Integer, ByVal modifiers As String, ByVal specialinstructions As String, ByVal l2modifiers As String)

            Dim cart = ShoppingCart.GetCart(Me.HttpContext)
            Dim l2array = Split(l2modifiers, ",")

            Dim ModArray = Split(modifiers, ",")
            Dim l2selectedmodifiers As New List(Of SelectedL2Modifier)
            Dim selectedmodifiers As New List(Of SelectedModifier)
            Dim menuitem = db.MenuItems.Find(Itemid)
            For Each Item In ModArray
                If Item <> "" Then
                    Dim ItemAry = Split(Item, "-")
                    Dim selecteditem As New SelectedModifier With
                        {.MenuModifier = db.MenuItemModifiers.Find(CInt(ItemAry(0))),
                         .SelectedModifier = db.MenuModifiers.Find(CInt(ItemAry(1)))}
                    selectedmodifiers.Add(selecteditem)
                End If
            Next
            For Each item In l2array
                If item <> "" Then
                    Dim itemary = Split(item, "-")
                    Dim ModifierQty = 1
                    If itemary.Count > 1 Then
                        ModifierQty = itemary(1)
                    End If
                    Dim l2selecteditem As New SelectedL2Modifier
                    l2selecteditem.SelectedModifier = db.MenuL2Modifiers.Find(CInt(itemary(0)))
                    l2selecteditem.Qty = ModifierQty
                    l2selectedmodifiers.Add(l2selecteditem)
                End If
            Next
            cart.AddItemToTicket(menuitem, Qty, selectedmodifiers, specialinstructions, User.Identity.Name, l2selectedmodifiers)

            Return True
        End Function
        Function FindItemPrice(ByVal Itemid As Integer, ByVal Qty As Integer, ByVal modifiers As String, ByVal specialinstructions As String, ByVal l2modifiers As String)
            Dim cart As New ShoppingCart
            Dim ModArray = Split(modifiers, ",")
            Dim l2array = Split(l2modifiers, ",")
            Dim selectedmodifiers As New List(Of SelectedModifier)

            Dim l2selectedmodifiers As New List(Of LMMenuL2Modifier)
            Dim menuitem = db.MenuItems.Find(Itemid)
            For Each Item In ModArray
                If Item <> "" Then
                    Dim ItemAry = Split(Item, "-")
                    Dim selecteditem As New SelectedModifier With
                        {.MenuModifier = db.MenuItemModifiers.Find(CInt(ItemAry(0))),
                         .SelectedModifier = db.MenuModifiers.Find(CInt(ItemAry(1)))}
                    selectedmodifiers.Add(selecteditem)
                End If
            Next
            For Each item In l2array
                If item <> "" Then
                    Dim l2selecteditem = db.MenuL2Modifiers.Find(CInt(item))
                    l2selectedmodifiers.Add(l2selecteditem)
                End If
            Next
            Return FormatCurrency(cart.GetItemPrice(menuitem, Qty, selectedmodifiers, l2selectedmodifiers))
        End Function


        Function getMixedQtyModifiers(ModGroup As LMMenuL2ModifierGroup)
            Return View(ModGroup)
        End Function
        Function getHalfWholeModifiers(ModGroup As LMMenuL2ModifierGroup)
            Return View(ModGroup)
        End Function

        Function getQtyDrop(ByVal Model As getQtyDropVM)
            Return View(Model)
        End Function
        Function getWholeHalfDrop()
            Return View()
        End Function

        Function getUpsellMenu(ByVal RestaurantId As Integer)
            Dim upsellItems As New List(Of UpsellItemVM)

            Dim Items = db.MenuItems.Where(Function(x) x.MenuGroup.MenuCategory.Menu.RestaurantID = RestaurantId And
                                               x.isUpsell = True).Take(4).ToList

            For Each item In Items
                Dim UpsellItem As New UpsellItemVM
                UpsellItem.ID = item.ID
                UpsellItem.Name = item.Name
                UpsellItem.Description = item.Description
                UpsellItem.Price = item.Price

                upsellItems.Add(UpsellItem)
            Next

            Return View(upsellItems)
        End Function
    End Class
End Namespace
