﻿Namespace LM
    Public Class CheckoutController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Checkout
        Dim Db As FBEntities = New FBEntities
        Function Index() As ActionResult
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)
            Return View(ticket)
        End Function

        Function SelectAddress()
            Return RedirectToAction("Payment", "Checkout")
            Dim account = Db.Users.Where(Function(x) x.UserName = User.Identity.Name).FirstOrDefault

            If account Is Nothing Then
                account = New User
            End If

            Dim model As New SelectAddressVM
            model.ShipToAdds = Db.Addresses.Where(Function(y) y.UserID = account.UserID And y.UserID <> 0 And y.Type = 1).ToList
            model.BillToAdds = Db.Addresses.Where(Function(y) y.UserID = account.UserID And y.UserID <> 0 And y.Type = 2).ToList
            model.EmailAddress = account.Email
            Return View(model)
        End Function

        Function _checkoutheader(Optional ByVal checkoutstep As Integer = 1)
            Dim model As New CheckoutHeaderVM
            model.checkoutStep = checkoutstep
            Return View(model)
        End Function
        <ProdNeedHTTPS> _
        Function Login()
            If User.Identity.Name <> Nothing Then
                Return RedirectToAction("SelectAddress", "Checkout")
            End If
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)
            Return View(ticket)
        End Function
        <ProdNeedHTTPS> _
        Function Payment()
            Return View()
        End Function
        Function PaymentView(Optional ByVal showTip As Boolean = True)
            Dim cart = New ShoppingCart
            Dim cartid As String = cart.GetCartId(Me.HttpContext)
            Dim ticket = Db.Tickets.SingleOrDefault(Function(x) x.ShoppingCartID = cartid)

            Dim account = Db.Users.Where(Function(x) x.UserName = User.Identity.Name).FirstOrDefault

            If account Is Nothing Then
                account = New User
            End If



            Dim paymentvm As New PaymentVM
            paymentvm.BilltoAddress = Db.Addresses.Find(ticket.BilltoAddress)
            paymentvm.ExpMonths = getExpMonths()
            paymentvm.ExpYears = getExpYears()
            paymentvm.BillableAmount = ticket.TotalPrice
            paymentvm.SubTotal = ticket.SubTotal
            paymentvm.Fee = ticket.FeePrice
            paymentvm.Tip = ticket.DeliveryPrice
            paymentvm.Promo = ticket.DiscountTotal

            paymentvm.OrderTotal = ticket.SubTotal + ticket.FeePrice

            paymentvm.ShipToAdds = Db.Addresses.Where(Function(y) y.UserID = account.UserID And y.UserID <> 0 And y.Type = 1).ToList
            paymentvm.BillToAdds = Db.Addresses.Where(Function(y) y.UserID = account.UserID And y.UserID <> 0 And y.Type = 2).ToList
            paymentvm.EmailAddress = account.Email
            paymentvm.OrderType = ticket.TicketTypeID

            paymentvm.showTip = showtip


            Return View(paymentvm)
        End Function
        <HttpGet()> _
        Function Receipt(ByVal id As String)
            Dim ReceiptVM As New ReceiptVM
            ReceiptVM.Ticket = Db.Tickets.Where(Function(x) x.ReceiptID = id).FirstOrDefault

            Dim rests = ReceiptVM.Ticket.ticketitems.Select(Function(y) y.RestaurantID).ToList

            ReceiptVM.Rests = Db.Restaurants.Where(Function(x) rests.Contains(x.ID)).ToList

            ReceiptVM.Billto = Db.Addresses.Find(ReceiptVM.Ticket.BilltoAddress)
            ReceiptVM.Deliverto = Db.Addresses.Find(ReceiptVM.Ticket.DeliveryAddress)
            Return View(ReceiptVM)
        End Function

        Function getExpMonths()
            Dim month = New List(Of SelectListItem)
            month.Add(New SelectListItem With {.Value = "01", .Text = "1 - Jan"})
            month.Add(New SelectListItem With {.Value = "02", .Text = "2 - Feb"})
            month.Add(New SelectListItem With {.Value = "03", .Text = "3 - Mar"})
            month.Add(New SelectListItem With {.Value = "04", .Text = "4 - Apr"})
            month.Add(New SelectListItem With {.Value = "05", .Text = "5 - May"})
            month.Add(New SelectListItem With {.Value = "06", .Text = "6 - Jun"})
            month.Add(New SelectListItem With {.Value = "07", .Text = "7 - Jul"})
            month.Add(New SelectListItem With {.Value = "08", .Text = "8 - Aug"})
            month.Add(New SelectListItem With {.Value = "09", .Text = "9 - Sep"})
            month.Add(New SelectListItem With {.Value = "10", .Text = "10 - Oct"})
            month.Add(New SelectListItem With {.Value = "11", .Text = "11 - Nov"})
            month.Add(New SelectListItem With {.Value = "12", .Text = "12 - Dec"})
            Return month
        End Function
        Function getExpYears()
            Dim years = New List(Of SelectListItem)
            For x = 0 To 20
                Dim year = Now.Year + x
                years.Add(New SelectListItem With {.Value = Right(year, 2), .Text = year})
            Next

            Return years
        End Function

    End Class
End Namespace
