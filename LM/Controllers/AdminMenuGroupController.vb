﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuGroupController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuGroup/

        Function Index(ByVal catid As Integer) As ViewResult
            Dim menugroups = db.MenuGroups.Where(Function(x) x.MenuCategoryID = catid).Include(Function(l) l.MenuCategory)
            ViewBag.MenuCategoryID = catid

            Dim item = db.MenuCategories.Find(catid)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuCategory", New With {.Menuid = item.MenuID})
            Names &= "," & item.Menu.Name

            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names

            Return View(menugroups.ToList())
        End Function

        '
        ' GET: /AdminMenuGroup/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenugroup As LMMenuGroup = db.MenuGroups.Find(id)
            Return View(lmmenugroup)
        End Function

        '
        ' GET: /AdminMenuGroup/Create

        Function Create(ByVal catid As Integer) As ViewResult
            ViewBag.MenuCategoryID = catid
            Return View()
        End Function

        '
        ' POST: /AdminMenuGroup/Create

        <HttpPost()>
        Function Create(lmmenugroup As LMMenuGroup) As ActionResult
            If ModelState.IsValid Then
                db.MenuGroups.Add(lmmenugroup)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.catid = lmmenugroup.MenuCategoryID})
            End If

            ViewBag.MenuCategoryID = lmmenugroup.MenuCategoryID
            Return View(lmmenugroup)
        End Function

        '
        ' GET: /AdminMenuGroup/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenugroup As LMMenuGroup = db.MenuGroups.Find(id)
            ViewBag.MenuCategoryID = lmmenugroup.MenuCategoryID
            Return View(lmmenugroup)
        End Function

        '
        ' POST: /AdminMenuGroup/Edit/5

        <HttpPost()>
        Function Edit(lmmenugroup As LMMenuGroup) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenugroup).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.catid = lmmenugroup.MenuCategoryID})
            End If

            ViewBag.MenuCategoryID = lmmenugroup.MenuCategoryID
            Return View(lmmenugroup)
        End Function

        '
        ' GET: /AdminMenuGroup/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenugroup As LMMenuGroup = db.MenuGroups.Find(id)
            Return View(lmmenugroup)
        End Function

        '
        ' POST: /AdminMenuGroup/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenugroup As LMMenuGroup = db.MenuGroups.Find(id)
            db.MenuGroups.Remove(lmmenugroup)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.catid = lmmenugroup.MenuCategoryID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub



    End Class
End Namespace