﻿Imports System.Data.Entity
Imports LM

Namespace LM
    Public Class AdminMenuL2ModifierGroupController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuL2ModifierGroup/

        Function Index(ByVal Modid As Integer) As ViewResult
            Dim menumodifiergroups = db.MenuL2ModifierGroups.Where(Function(x) x.ModifierID = Modid).Include(Function(l) l.Modifier)

            Dim item = db.MenuModifiers.Find(Modid)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.MenuModifierGroup.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuModifierGroup", New With {.Menuid = item.MenuModifierGroup.MenuID})
            Names &= "," & item.MenuModifierGroup.Name

            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names

            ViewBag.Modid = Modid
            Return View(menumodifiergroups.ToList())
        End Function

        '
        ' GET: /AdminMenuL2ModifierGroup/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenumodifiergroup As LMMenuL2ModifierGroup = db.MenuL2ModifierGroups.Find(id)
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' GET: /AdminMenuL2ModifierGroup/Create

        Function Create(ByVal Modid As Integer) As ViewResult
            ViewBag.Modid = Modid
            Return View()
        End Function

        '
        ' POST: /AdminMenuL2ModifierGroup/Create

        <HttpPost()>
        Function Create(lmmenumodifiergroup As LMMenuL2ModifierGroup) As ActionResult
            If ModelState.IsValid Then
                db.MenuL2ModifierGroups.Add(lmmenumodifiergroup)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.Modid = lmmenumodifiergroup.ModifierID})
            End If

            ViewBag.Modid = lmmenumodifiergroup.ModifierID
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' GET: /AdminMenuL2ModifierGroup/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenumodifiergroup As LMMenuL2ModifierGroup = db.MenuL2ModifierGroups.Find(id)
            ViewBag.Modid = lmmenumodifiergroup.ModifierID
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' POST: /AdminMenuL2ModifierGroup/Edit/5

        <HttpPost()>
        Function Edit(lmmenumodifiergroup As LMMenuL2ModifierGroup) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenumodifiergroup).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.Modid = lmmenumodifiergroup.ModifierID})
            End If

            ViewBag.Modid = lmmenumodifiergroup.ModifierID
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' GET: /AdminMenuL2ModifierGroup/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenumodifiergroup As LMMenuL2ModifierGroup = db.MenuL2ModifierGroups.Find(id)
            Return View(lmmenumodifiergroup)
        End Function

        '
        ' POST: /AdminMenuL2ModifierGroup/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenumodifiergroup As LMMenuL2ModifierGroup = db.MenuL2ModifierGroups.Find(id)
            db.MenuL2ModifierGroups.Remove(lmmenumodifiergroup)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.menuid = lmmenumodifiergroup.ModifierID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

        Function ControlDrop(Optional ByVal selected As String = "")
            Dim ControlType As New ControlTypeVM
            ControlType.Controls = db.ControlTypes.ToList
            ControlType.Selected = selected
            Return View(ControlType)
        End Function



    End Class
End Namespace


