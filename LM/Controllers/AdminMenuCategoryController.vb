﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuCategoryController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuCategory/

        Function Index(ByVal Menuid As Integer) As ViewResult
            Dim menucategories = db.MenuCategories.Where(Function(x) x.MenuID = Menuid).Include(Function(l) l.Menu)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= db.Menus.Find(Menuid).Restaurant.Name


            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names

            ViewBag.menuid = Menuid
            Return View(menucategories.ToList())
        End Function

        '
        ' GET: /AdminMenuCategory/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenucategory As LMMenuCategory = db.MenuCategories.Find(id)
            Return View(lmmenucategory)
        End Function

        '
        ' GET: /AdminMenuCategory/Create

        Function Create(ByVal Menuid As Integer) As ViewResult
            ViewBag.MenuID = Menuid
            Return View()
        End Function

        '
        ' POST: /AdminMenuCategory/Create

        <HttpPost()>
        Function Create(lmmenucategory As LMMenuCategory) As ActionResult
            If ModelState.IsValid Then
                db.MenuCategories.Add(lmmenucategory)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.menuid = lmmenucategory.MenuID})
            End If

            ViewBag.MenuID = lmmenucategory.MenuID
            Return View(lmmenucategory)
        End Function

        '
        ' GET: /AdminMenuCategory/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenucategory As LMMenuCategory = db.MenuCategories.Find(id)
            ViewBag.MenuID = lmmenucategory.MenuID
            Return View(lmmenucategory)
        End Function

        '
        ' POST: /AdminMenuCategory/Edit/5

        <HttpPost()>
        Function Edit(lmmenucategory As LMMenuCategory) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenucategory).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.menuid = lmmenucategory.MenuID})
            End If

            ViewBag.MenuID = lmmenucategory.MenuID
            Return View(lmmenucategory)
        End Function

        '
        ' GET: /AdminMenuCategory/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenucategory As LMMenuCategory = db.MenuCategories.Find(id)
            Return View(lmmenucategory)
        End Function

        '
        ' POST: /AdminMenuCategory/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenucategory As LMMenuCategory = db.MenuCategories.Find(id)
            db.MenuCategories.Remove(lmmenucategory)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.menuid = lmmenucategory.MenuID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace