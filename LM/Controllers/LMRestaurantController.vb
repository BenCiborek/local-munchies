﻿Namespace LM
    Public Class LMRestaurantController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /LMRestaurant
        Dim db As New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function
        Function Searchview(ByVal Restaurant As DetailSearchResultVM)

            Return View(Restaurant)
        End Function
        Function Zoom(ByVal id As Integer)
            Dim rest = db.Restaurants.Find(id)
            If rest Is Nothing Then
                Return RedirectToAction("Missing")
            Else
                Return View(rest)
            End If

        End Function
        Function ShowHours1(ByVal rest As LMRestaurant, Optional ByVal type As Integer = 0)
            'Get local time
            Dim timeutc = Now.ToUniversalTime
            Dim cstZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
            Dim cstTime As Date = TimeZoneInfo.ConvertTimeFromUtc(timeutc, cstZone)
            Select Case type
                Case 0
                    ViewBag.now = cstTime
                    Return View(rest)
                Case 1
                    Return RedirectToAction("ShowHours2", "LMRestaurant", New With {.currenttime = cstTime, .rest = rest})
                Case Else
                    ViewBag.now = cstTime
                    Return View(rest)
            End Select
        End Function
        Function ShowHours2(ByVal rest As LMRestaurant)
            Dim timeutc = Now.ToUniversalTime
            Dim cstZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
            Dim currenttime As Date = TimeZoneInfo.ConvertTimeFromUtc(timeutc, cstZone)
            Dim dt As DateTime = currenttime
            Dim alertclass = ""
            Dim alerttext = ""

            Dim restaurant As New Restaurant
            If restaurant.isOpen(rest) Then
                alertclass = "success"
            Else
                alertclass = "danger"
            End If


            Select Case dt.DayOfWeek
                Case DayOfWeek.Sunday
                    alerttext = "S: " & IIf(rest.SunOpen > CDate("1/1/1900") And rest.SunClose > CDate("1/1/1900"), rest.SunOpen.ToString("h:mm tt") & " - " & rest.SunClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.SunOpen.TimeOfDay And dt.TimeOfDay < rest.SunClose.TimeOfDay Or (rest.SunClose.TimeOfDay < rest.SunOpen.TimeOfDay And (dt.TimeOfDay < rest.SunClose.TimeOfDay Or dt.TimeOfDay > rest.SunOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
                Case DayOfWeek.Monday
                    alerttext = "M: " & IIf(rest.MonOpen > CDate("1/1/1900") And rest.MonClose > CDate("1/1/1900"), rest.MonOpen.ToString("h:mm tt") & " - " & rest.MonClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.MonOpen.TimeOfDay And dt.TimeOfDay < rest.MonClose.TimeOfDay Or (rest.MonClose.TimeOfDay < rest.MonOpen.TimeOfDay And (dt.TimeOfDay < rest.MonClose.TimeOfDay Or dt.TimeOfDay > rest.MonOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
                Case DayOfWeek.Tuesday
                    alerttext = "T: " & IIf(rest.TueOpen > CDate("1/1/1900") And rest.TueClose > CDate("1/1/1900"), rest.TueOpen.ToString("h:mm tt") & " - " & rest.TueClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.TueOpen.TimeOfDay And dt.TimeOfDay < rest.TueClose.TimeOfDay Or (rest.TueClose.TimeOfDay < rest.TueOpen.TimeOfDay And (dt.TimeOfDay < rest.TueClose.TimeOfDay Or dt.TimeOfDay > rest.TueOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
                Case DayOfWeek.Wednesday
                    alerttext = "W: " & IIf(rest.WedOpen > CDate("1/1/1900") And rest.WedClose > CDate("1/1/1900"), rest.WedOpen.ToString("h:mm tt") & " - " & rest.WedClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.WedOpen.TimeOfDay And dt.TimeOfDay < rest.WedClose.TimeOfDay Or (rest.WedClose.TimeOfDay < rest.WedOpen.TimeOfDay And (dt.TimeOfDay < rest.WedClose.TimeOfDay Or dt.TimeOfDay > rest.WedOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
                Case DayOfWeek.Thursday
                    alerttext = "T: " & IIf(rest.ThuOpen > CDate("1/1/1900") And rest.ThuClose > CDate("1/1/1900"), rest.ThuOpen.ToString("h:mm tt") & " - " & rest.ThuClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.ThuOpen.TimeOfDay And dt.TimeOfDay < rest.ThuClose.TimeOfDay Or (rest.ThuClose.TimeOfDay < rest.ThuOpen.TimeOfDay And (dt.TimeOfDay < rest.ThuClose.TimeOfDay Or dt.TimeOfDay > rest.ThuOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
                Case DayOfWeek.Friday
                    alerttext = "F: " & IIf(rest.FriOpen > CDate("1/1/1900") And rest.FriClose > CDate("1/1/1900"), rest.FriOpen.ToString("h:mm tt") & " - " & rest.FriClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.FriOpen.TimeOfDay And dt.TimeOfDay < rest.FriClose.TimeOfDay Or (rest.FriClose.TimeOfDay < rest.FriOpen.TimeOfDay And (dt.TimeOfDay < rest.FriClose.TimeOfDay Or dt.TimeOfDay > rest.FriOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
                Case DayOfWeek.Saturday
                    alerttext = "S: " & IIf(rest.SatOpen > CDate("1/1/1900") And rest.SatClose > CDate("1/1/1900"), rest.SatOpen.ToString("h:mm tt") & " - " & rest.SatClose.ToString("h:mm tt"), "Closed")
                    'If dt.TimeOfDay > rest.SatOpen.TimeOfDay And dt.TimeOfDay < rest.SatClose.TimeOfDay Or (rest.SatClose.TimeOfDay < rest.SatOpen.TimeOfDay And (dt.TimeOfDay < rest.SatClose.TimeOfDay Or dt.TimeOfDay > rest.SatOpen.TimeOfDay)) Then
                    '    alertclass = "success"
                    'Else
                    '    alertclass = "danger"
                    'End If
            End Select
            ViewBag.class = alertclass
            ViewBag.text = alerttext
            Return View()
        End Function
        Function FindLikeBusinesses(ByVal restid As Integer, Optional ByVal cnt As Integer = 5)
            Dim p = db.Restaurants.Where(Function(f) f.Active = True)
            Dim catlist = db.Restaurants.Find(restid).Categories.Select(Function(y) y.id).ToList
            p = p.Where(Function(z) z.Categories.Any(Function(y) catlist.Contains(y.id)) And z.ID <> restid).Take(cnt)
            Return View(p)
        End Function
        Function ShowMap(ByVal Lat As String, ByVal Lon As String)
            ViewBag.Lat = Lat
            ViewBag.Lon = Lon
            Return View()
        End Function
        Function ShowOrderingOptions(ByVal Rest As LMRestaurant)
            Return View(Rest)
        End Function
        Function ShowSpecial(ByVal Rest As LMRestaurant)
            Return View(Rest)
        End Function
        Function OrderingOptionsSearchView(ByVal find As String, ByVal categorie As Integer, ByVal OrderOption As Integer, ByVal restids As List(Of Integer)) As ActionResult
            Dim opts = db.OrderOptions.Where(Function(m) m.active = True)
            'If OrderOption <> 0 Then
            '    opts = opts.Where(Function(z) z.id = OrderOption)
            'End If
            ViewBag.selectedtype = OrderOption
            ViewBag.restids = restids
            Return View(opts)
        End Function
        Function OOVote(ByVal restid As Integer) As JsonResult
            Dim vote As New OOVote
            vote.VDate = Now()
            vote.VRestID = restid
            db.OOVotes.Add(vote)
            db.SaveChanges()
            Return Json("1")
        End Function
        Function LocationDrop(Optional ByVal selected As String = "")
            Dim LocationVM As New LocationsVM
            LocationVM.Locations = db.Locations.ToList
            LocationVM.Selected = selected
            Return View(LocationVM)
        End Function
        Function Missing() As ActionResult
            Return View()
        End Function
    End Class
End Namespace
