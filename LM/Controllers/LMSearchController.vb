﻿Namespace LM
    Public Class LMSearchController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /LM

        Dim db As New FBEntities
        Function Index(Optional ByVal searchcriteria As LMSearchCriteria = Nothing, Optional ByVal id As String = "Kent-OH") As ActionResult
            If searchcriteria Is Nothing Then
                searchcriteria = New LMSearchCriteria With {.Find = "", .Categorie = 0, .OrderOption = 0}
            End If
            'Get local time
            Dim SearchIndexVM As New SearchIndexVM
            SearchIndexVM.SearchCriteria = searchcriteria
            SearchIndexVM.Location = db.Locations.Find(id)
            Session("Location") = id
            Return View(SearchIndexVM)
        End Function
        'Function SearchResults(ByVal find As String, ByVal cat As Integer, ByVal OrderOption As Integer, ByVal restids As List(Of Integer)) As ActionResult
        '    Dim sc As New LMSearchCriteria
        '    sc.Find = find
        '    sc.Categorie = cat
        '    sc.OrderOption = OrderOption
        '    Dim p As List(Of LMRestaurant) = db.Restaurants.Where(Function(f) restids.Contains(f.ID)).ToList()

        '    Dim timeutc = Now.ToUniversalTime
        '    Dim cstZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
        '    Dim cstTime As Date = TimeZoneInfo.ConvertTimeFromUtc(timeutc, cstZone)

        '    Dim Rest As New Restaurant
        '    Dim SRs As New SearchResultsVM
        '    SRs.now = cstTime
        '    SRs.SearchCriteria = sc
        '    For Each Restaurant In p
        '        Dim SR As New DetailSearchResultVM

        '        SR.Restaurant = Restaurant
        '        SR.Open = Rest.isOpen(Restaurant)
        '        SRs.Restaurants.Add(SR)
        '    Next



        '    ' ViewBag.now = cstTime
        '    'ViewBag.searchcriteria = sc

        '    Return View(SRs)
        'End Function

        Function ShowFilters(ByVal find As String, ByVal categorie As Integer, ByVal OrderOption As Integer, ByVal restids As List(Of Integer)) As ActionResult
            Dim sc As New LMSearchCriteria
            sc.Find = find
            sc.Categorie = categorie
            sc.OrderOption = OrderOption
            ViewBag.restids = restids
            Return View(sc)
        End Function

        Function SearchMain(ByVal find As String, ByVal cat As Integer, ByVal OrderOption As Integer, ByVal LocationID As String, Optional ByVal skipSession As Boolean = False, Optional ByVal Address As String = "") As ActionResult
            If find = "" And cat = 0 And OrderOption = 0 And skipSession = False Then
                find = Session("searchfind")
                cat = Session("searchcat")
                OrderOption = Session("searchOrderOption")
                Address = Session("Address")
                LocationID = Session("Location")
            End If
            Session("searchfind") = find
            Session("searchcat") = cat
            Session("searchOrderOption") = OrderOption
            Session("Address") = Address
            LocationID = Session("Location")



            Dim sc As New LMSearchCriteria
            sc.Find = find
            sc.Categorie = cat
            sc.OrderOption = OrderOption

            Dim p = db.Restaurants.Where(Function(f) f.Active = True And f.LocationID = LocationID)
            If Not sc Is Nothing Then
                If sc.Find <> "" Then
                    p = p.Where(Function(f) f.Name.Contains(sc.Find))
                End If
                If sc.OrderOption <> 0 Then
                    p = p.Where(Function(f) f.OrderOptions.Any(Function(y) y.id = sc.OrderOption))
                End If
                If sc.Categorie <> 0 Then
                    p = p.Where(Function(f) f.Categories.Any(Function(y) y.id = sc.Categorie))
                End If
            End If
            ViewBag.restids = p.Select(Function(f) f.ID).ToList


            Dim timeutc = Now.ToUniversalTime
            Dim cstZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
            Dim cstTime As Date = TimeZoneInfo.ConvertTimeFromUtc(timeutc, cstZone)

            Dim Rest As New Restaurant
            Dim AddressGeocode As New GeoCode
            If Address <> "" Then
                AddressGeocode = Rest.AddressToGeocode(Address)
            End If
            Dim SRs As New SearchResultsVM
            SRs.now = cstTime
            SRs.SearchCriteria = sc
            For Each Restaurant In p
                Dim SR As New DetailSearchResultVM

                SR.Restaurant = Restaurant
                SR.Open = Rest.isOpen(Restaurant)
                If AddressGeocode Is Nothing Then
                    SR.DeliversToAddress = 0
                Else
                    SR.DeliversToAddress = Rest.isAddressinDeliveryZone(AddressGeocode, Restaurant)
                End If
                SRs.Restaurants.Add(SR)
            Next

            Return View(SRs)
        End Function
        Function SearchBarHeader() As ActionResult
            Dim find = Session("searchfind")
            Dim cat = Session("searchcat")
            Dim OrderOption = Session("searchOrderOption")
            If cat Is Nothing Then
                cat = 0
            End If
            If OrderOption Is Nothing Then
                OrderOption = 0
            End If
            Dim searchcriteria = New LMSearchCriteria With {.Find = find, .Categorie = cat, .OrderOption = OrderOption}

            Return View(searchcriteria)
        End Function
        Function GetAutocomplete(query As String) As ActionResult
            Dim users = db.Restaurants.Where(Function(y) y.Name.Contains(query)).Select(Function(c) c.Name).Distinct().ToArray()

            Return Json(users)
        End Function

        Function Filtersbar() As ActionResult
            Dim find = Session("searchfind")
            Dim cat = Session("searchcat")
            Dim OrderOption = Session("searchOrderOption")
            If cat Is Nothing Then
                cat = 0
            End If
            If OrderOption Is Nothing Then
                OrderOption = 0
            End If
            Dim searchcriteria = New LMSearchCriteria With {.Find = find, .Categorie = cat, .OrderOption = OrderOption}

            Return View(searchcriteria)
        End Function
        Function OrderType(Optional ByVal Selected As Integer = 0) As ActionResult
            Dim model As New OrderTypeSearchVM
            model.selected = Selected
            Return View(model)
        End Function




    End Class
End Namespace
