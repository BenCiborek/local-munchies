﻿Imports System.Data.Entity

Namespace LM
    Public Class CallsController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Calls
        Private db As FBEntities = New FBEntities
        Function Index() As ActionResult
            Return View()
        End Function
        Function faxconfirmation(ByVal id As Integer)

            Dim faxsent = db.Faxsent.Find(id)


            Dim voice = "alice"
            Dim lang = "en"
            Dim timestosay = 3
            Dim say1 = "You have a new local munchies order, please enter the confirmation code followed by the pound sign. For order: "
            Dim say2 = "followed by the pound sign."
            Dim sayorder = faxsent.Ticketid

            Dim xmlstring As String = "<?xml version='1.0' encoding='utf-8' ?>"
            xmlstring &= "<Response>"
            xmlstring &= " <Gather action='/calls/faxconfirmationStep2/" & id & "' method='GET'>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & say1 & " " & sayorder & ".</Say>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='" & timestosay & "'>" & sayorder & ".</Say>"
            xmlstring &= "</Gather>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>We didn't receive any confirmation code. We will call back soon!</Say>"

            xmlstring &= "</Response>"
            Return Me.Content(xmlstring, "text/xml")
        End Function
        Function faxconfirmationStep2(ByVal id As Integer, ByVal digits As String)
            Dim faxsent = db.Faxsent.Find(id)

            Dim voice = "alice"
            Dim lang = "en"
            Dim timestosay = 3

            Dim say1 = "Confirmation code incorrect. Please enter the confirmation code followed by the pound sign. For order: "
            Dim say2 = "followed by the pound sign."
            Dim sayorder = faxsent.Ticketid

            Dim xmlstring As String = "<?xml version='1.0' encoding='utf-8' ?>"
            If Right(digits, 2) = Right(faxsent.Ticketid, 2) Then
                'Correct

                faxsent.Status = 2
                faxsent.DateConfirmed = Now.ToUniversalTime()
                db.Entry(faxsent).State = EntityState.Modified
                db.SaveChanges()

                xmlstring &= "<Response>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>Order Confirmed. Thank You. Have a great day.</Say>"
                xmlstring &= "</Response>"
            Else
                xmlstring &= "<Response>"
                xmlstring &= " <Gather action='/calls/faxconfirmationStep2/" & id & "' method='GET'>"
                'Testing
                'xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & digits & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & say1 & " " & sayorder & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='" & timestosay & "'>" & sayorder & ".</Say>"
                xmlstring &= "</Gather>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>We didn't receive any input. We will call back soon!</Say>"

                xmlstring &= "</Response>"
            End If


            Return Me.Content(xmlstring, "text/xml")
        End Function

        Function ordercall(ByVal id As Integer, ByVal Restid As Integer, ByVal itemnumber As Integer, Optional ByVal digits As String = "")
            'Get Ticket info
            Dim FaxTicketVM As FaxTicketVM = getFaxticketVM(id, Restid)
            Dim xmlstring As String = ""
            If digits = "9" Then
                'Update/Close Fax Sent
                Dim faxsent = db.Faxsent.Where(Function(x) x.Ticketid = id And x.RestID = Restid).First

                faxsent.Status = 2
                faxsent.DateConfirmed = Now.ToUniversalTime()
                db.Entry(faxsent).State = EntityState.Modified
                db.SaveChanges()




                xmlstring = closing()
            Else

                If digits = "1" Then
                    itemnumber = itemnumber + 1
                End If

                Select Case itemnumber
                    Case 0
                        xmlstring = OrderCallHeader(id, Restid, FaxTicketVM)
                    Case Else
                        If itemnumber > FaxTicketVM.Items.Count Then
                            xmlstring = OrdercallFooter(id, Restid, FaxTicketVM, itemnumber)
                        Else
                            xmlstring = OrderCallItem(id, Restid, FaxTicketVM, itemnumber)
                        End If
                End Select

            End If
            xmlstring = Replace(xmlstring, "&", "and")
            Return Me.Content(xmlstring, "text/xml")
        End Function

        Function OrdercallFooter(ByVal id As Integer, ByVal Restid As Integer, ByVal FaxTicketVM As FaxTicketVM, ByVal itemnumber As Integer)
            Dim voice = "alice"
            Dim lang = "en"

            Dim say1 = "You have received a " & FaxTicketVM.OrderType & " order from Local Munchies. "
            Dim say2 = "Order Number " & FaxTicketVM.OrderNumber & ". "
            Dim say3 = "There are a total of " & FaxTicketVM.Items.Count & " items on this order. "

            'Header
            Dim xmlstring As String = "<?xml version='1.0' encoding='utf-8' ?>"
            xmlstring &= "<Response>"
            xmlstring &= " <Gather action='/calls/ordercall/" & id & "/" & Restid & "/" & itemnumber & "' method='GET'>"


            If FaxTicketVM.OrderType = "Delivery" Then
                'Add if delivery
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>Deliver to.</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FaxTicketVM.DeliveryAddress.Name & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FaxTicketVM.DeliveryAddress.Address1 & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FaxTicketVM.DeliveryAddress.Address2 & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FaxTicketVM.DeliveryAddress.City & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FaxTicketVM.DeliveryAddress.State & ".</Say>"
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FaxTicketVM.DeliveryAddress.Zip & ".</Say>"
            End If




            'Footer
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>That completes Order Number " & FaxTicketVM.OrderNumber & " for " & FaxTicketVM.OrderType & ".</Say>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & FinishStatement() & "</Say>"
            xmlstring &= "</Gather>"


            xmlstring &= "</Response>"
            Return xmlstring
        End Function

        Function OrderCallItem(ByVal id As Integer, ByVal Restid As Integer, ByVal FaxTicketVM As FaxTicketVM, ByVal itemnumber As Integer)

            Dim item As TicketItem = FaxTicketVM.Items(itemnumber - 1)

            Dim voice = "alice"
            Dim lang = "en"


            Dim xmlstring As String = "<?xml version='1.0' encoding='utf-8' ?>"
            'Header
            xmlstring &= "<Response>"
            xmlstring &= " <Gather action='/calls/ordercall/" & id & "/" & Restid & "/" & itemnumber & "' method='GET'>"
            'Item Info
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>Item " & itemnumber & " is " & item.Name & ".</Say>"





            For Each Group In item.ModifierGroups
                'Group Name Info
                xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & Group.Name & " Choices Are .</Say>"
                For Each modifier In Group.Modifiers
                    'Modifier Info
                    xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & modifier.Name & ".</Say>"
                    For Each l2group In modifier.TicketItemL2Groups
                        'L2 Group Name Info
                        xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & l2group.Name & " Choices Are.</Say>"
                        For Each l2mod In l2group.Modifiers
                            'L2 Modifier info
                            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & l2mod.Name & ".</Say>"
                        Next
                    Next
                Next
            Next

            'Special Instructions
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & item.SpecialInstructions & "</Say>"

            'Footer
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & continuestatement() & "</Say>"
            xmlstring &= "</Gather>"



            xmlstring &= "</Response>"
            Return xmlstring
        End Function

        Function OrderCallHeader(ByVal id As Integer, ByVal Restid As Integer, ByVal FaxTicketVM As FaxTicketVM)

            Dim voice = "alice"
            Dim lang = "en"

            Dim say1 = "You have received a " & FaxTicketVM.OrderType & " order from Local Munchies. "
            Dim say2 = "Order Number " & FaxTicketVM.OrderNumber & ". "
            Dim say3 = "There are a total of " & FaxTicketVM.Items.Count & " items on this order. "

            Dim xmlstring As String = "<?xml version='1.0' encoding='utf-8' ?>"
            xmlstring &= "<Response>"
            xmlstring &= " <Gather action='/calls/ordercall/" & id & "/" & Restid & "/0' method='GET'>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & say1 & "</Say>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & say2 & "</Say>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & say3 & "</Say>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>" & continuestatement() & "</Say>"
            xmlstring &= "</Gather>"


            xmlstring &= "</Response>"
            Return xmlstring
        End Function
        Function continuestatement() As String
            Return "Press 1 to continue. To hear again press 2. Again to continue press 1. to hear again press 2."
        End Function
        Function FinishStatement()
            Return "Press 9 to Complete this order. To hear again press 2. Again to Finish Order press 9. to hear again press 2."
        End Function
        Function closing()
            Dim xmlstring As String = ""
            Dim voice = "alice"
            Dim lang = "en"

            xmlstring &= "<Response>"
            xmlstring &= "  <Say voice='" & voice & "' language='" & lang & "' loop='1'>Order Confirmed. Thank You. Have a great day.</Say>"
            xmlstring &= " <Hangup/>"
            xmlstring &= "</Response>"
            Return xmlstring
        End Function

        Function getFaxticketVM(ByVal id As Integer, ByVal Restid As Integer) As FaxTicketVM
            Dim FaxTicketVM As New FaxTicketVM
            FaxTicketVM.Items = db.TicketItems.Where(Function(x) x.TicketID = id And x.RestaurantID = Restid).ToList
            FaxTicketVM.RestaurantName = FaxTicketVM.Items(0).Restaurant.Name

            FaxTicketVM.TotalAmount = 0
            For Each item In FaxTicketVM.Items
                FaxTicketVM.TotalAmount += item.TotalItemPrice
            Next

            FaxTicketVM.OrderNumber = id
            FaxTicketVM.Confirmation = Right(id, 2)

            Dim ticket = db.Tickets.Find(id)
            FaxTicketVM.IntOrderType = ticket.TicketTypeID
            FaxTicketVM.BilltoAddress = db.Addresses.Find(ticket.BilltoAddress)
            If ticket.TicketTypeID = 1 Then
                'Pickup 
                FaxTicketVM.OrderType = "Take-Out"
            Else
                'Delivery
                FaxTicketVM.OrderType = "Delivery"

                FaxTicketVM.DeliveryAddress = db.Addresses.Find(ticket.DeliveryAddress)
            End If
            Return FaxTicketVM
        End Function

    End Class
End Namespace
