﻿Namespace LM
    Public Class ErrorController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Error

        Function Index(ByVal status As Integer, ByVal err As Exception) As ActionResult
            'Response.Status = status
            Return View(status)
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Me.Dispose(disposing)
        End Sub
    End Class
End Namespace
