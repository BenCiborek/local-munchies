﻿Imports System.Web.Mvc
Imports System.Data.Entity
Public Class AdminOrderingOptionController
    Inherits Controller
    Dim db As New FBEntities
    ' GET: /AdminOrderingOption
    Function Index(ByVal id As Integer) As ActionResult
        Dim AdminOrderOptionsVM As New AdminOrderOptionsVM
        AdminOrderOptionsVM.OrderingOptions = db.OrderOptions.ToList()
        AdminOrderOptionsVM.Restaurant = db.Restaurants.Find(id)
        Return View(AdminOrderOptionsVM)
    End Function
    <HttpPost()>
    Function Index(ByVal id As Integer, ByVal RestaurantID As Integer, ByVal OrderOption As Integer)

        Dim restaurant = db.Restaurants.Find(RestaurantID)
        Dim newOrderOption = db.OrderOptions.Find(OrderOption)

        restaurant.OrderOptions.Add(newOrderOption)
        db.Entry(restaurant).State = EntityState.Modified
        db.SaveChanges()


        Return RedirectToAction("Index", New With {.id = RestaurantID})
    End Function
    Function Delete(ByVal RestaurantID As Integer, ByVal OrderOption As Integer)
        Dim restaurant = db.Restaurants.Find(RestaurantID)
        Dim newOrderOption = db.OrderOptions.Find(OrderOption)

        restaurant.OrderOptions.Remove(newOrderOption)
        db.Entry(restaurant).State = EntityState.Modified
        db.SaveChanges()

        Return RedirectToAction("Index", New With {.id = RestaurantID})
    End Function
End Class