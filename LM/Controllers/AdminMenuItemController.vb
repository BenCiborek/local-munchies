﻿Imports System.Data.Entity
Imports LM

Namespace LM
    <Authorize(Roles:="Admin")> _
    Public Class AdminMenuItemController
        Inherits System.Web.Mvc.Controller

        Private db As FBEntities = New FBEntities

        '
        ' GET: /AdminMenuItem/

        Function Index(ByVal Groupid As Integer) As ViewResult
            Dim menuitems = db.MenuItems.Where(Function(x) x.MenuGroupID = Groupid).Include(Function(l) l.MenuGroup)
            ViewBag.MenuGroupID = Groupid


            Dim item = db.MenuGroups.Find(Groupid)

            Dim URLs As String = ""
            Dim Names As String = ""

            URLs &= Url.Action("Index", "AdminMenu")
            Names &= item.MenuCategory.Menu.Restaurant.Name

            URLs &= "," & Url.Action("Index", "AdminMenuCategory", New With {.Menuid = item.MenuCategory.MenuID})
            Names &= "," & item.MenuCategory.Menu.Name

            URLs &= "," & Url.Action("Index", "AdminMenuGroup", New With {.catid = item.MenuCategoryID})
            Names &= "," & item.MenuCategory.Name

            ViewBag.BreadCrumbURLS = URLs
            ViewBag.BreadCrumbNames = Names

            Return View(menuitems.ToList())
        End Function

        '
        ' GET: /AdminMenuItem/Details/5

        Function Details(id As Integer) As ViewResult
            Dim lmmenuitem As LMMenuItem = db.MenuItems.Find(id)
            Return View(lmmenuitem)
        End Function

        '
        ' GET: /AdminMenuItem/Create

        Function Create(ByVal GroupID As Integer) As ViewResult
            ViewBag.MenuGroupID = GroupID
            Return View()
        End Function

        '
        ' POST: /AdminMenuItem/Create

        <HttpPost()>
        Function Create(lmmenuitem As LMMenuItem) As ActionResult
            If ModelState.IsValid Then
                db.MenuItems.Add(lmmenuitem)
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.groupID = lmmenuitem.MenuGroupID})
            End If

            ViewBag.MenuGroupID = lmmenuitem.MenuGroupID
            Return View(lmmenuitem)
        End Function

        '
        ' GET: /AdminMenuItem/Edit/5

        Function Edit(id As Integer) As ViewResult
            Dim lmmenuitem As LMMenuItem = db.MenuItems.Find(id)
            ViewBag.MenuGroupID = lmmenuitem.MenuGroupID
            Return View(lmmenuitem)
        End Function

        '
        ' POST: /AdminMenuItem/Edit/5

        <HttpPost()>
        Function Edit(lmmenuitem As LMMenuItem) As ActionResult
            If ModelState.IsValid Then
                db.Entry(lmmenuitem).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index", New With {.groupID = lmmenuitem.MenuGroupID})
            End If

            ViewBag.MenuGroupID = lmmenuitem.MenuGroupID
            Return View(lmmenuitem)
        End Function

        '
        ' GET: /AdminMenuItem/Delete/5

        Function Delete(id As Integer) As ViewResult
            Dim lmmenuitem As LMMenuItem = db.MenuItems.Find(id)
            Return View(lmmenuitem)
        End Function

        '
        ' POST: /AdminMenuItem/Delete/5

        <HttpPost()>
        <ActionName("Delete")>
        Function DeleteConfirmed(id As Integer) As RedirectToRouteResult
            Dim lmmenuitem As LMMenuItem = db.MenuItems.Find(id)
            db.MenuItems.Remove(lmmenuitem)
            db.SaveChanges()
            Return RedirectToAction("Index", New With {.groupID = lmmenuitem.MenuGroupID})
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            db.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace