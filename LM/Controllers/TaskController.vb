﻿Imports Twilio
Imports Intuit
Imports System.Data.Entity

Namespace LM
    Public Class TaskController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Task
        Dim Db As FBEntities = New FBEntities
        Dim ManagerEmail As String = "sjones@localmunchies.com,njones@localmunchies.com,bennyc66@gmail.com"
        Dim ITEmail As String = "Bennyc66@gmail.com"

        Function Index() As ActionResult
            Return View()
        End Function

        Function SendFax()
            Dim tickets = Db.Tickets.Where(Function(x) x.TicketStatus = 2).ToList

            For Each Ticket In tickets
                For Each RestID In Ticket.ticketitems.GroupBy(Function(x) x.RestaurantID).Select(Function(y) y.First()).ToList()
                    'For Each Restaurant in the tickets
                    Dim querystring = Ticket.ID & "?Restid=" & RestID.RestaurantID
                    Dim RI = Db.RestaurantInfo.Where(Function(x) x.RestaurantID = RestID.RestaurantID).FirstOrDefault

                    'Get page from web.config
                    Dim Page As String = ConfigurationManager.AppSettings("FAXURL") & querystring
#If Not Debug Then
                    SendFaxEmail(RI.OrderFaxNumber.ToString & "@fax.tc", "njones@localmunchies.com", "", "", True, Page)
                    SendFaxEmail("orderfax@localmunchies.com", "njones@localmunchies.com",  "Order: " & Ticket.ID,"To: " & RI.OrderFaxNumber.ToString & "@fax.tc", True, Page)


#End If
#If DEBUG Then
                    SendFaxEmail("bennyc66@gmail.com", "njones@localmunchies.com", "Order: " & Ticket.ID, "To: " & RI.OrderFaxNumber.ToString & "@fax.tc", True, Page)
#End If


                    'Add Fax Sent 
                    Dim fax As New FaxSent
                    fax.ToNumber = RI.OrderFaxNumber
                    fax.FaxPage = Page
                    fax.DateSent = Now.ToUniversalTime
                    fax.Status = "1"
                    fax.Ticketid = Ticket.ID
                    fax.RestID = RestID.RestaurantID
                    fax.DateConfirmed = #1/1/1900#

                    Db.Faxsent.Add(fax)
                    Db.SaveChanges()


                Next
                'Update Status to sent
                Ticket.TicketStatus = 3
                Db.Entry(Ticket).State = EntityState.Modified
                Db.SaveChanges()
            Next
            Return View()
        End Function

        Function SendtoLMD()
            Dim tickets = Db.Tickets.Where(Function(x) x.LMDStatus = 2).ToList



            'Get 20 min from now for driver pickup time
            Dim EasternNow As DateTime = DateAdd(DateInterval.Minute, 20, System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")))

            For Each Ticket In tickets
                Dim tip = Ticket.DeliveryPrice

                Dim DeliveryAdd = Db.Addresses.Find(Ticket.DeliveryAddress)
                Dim billtoaddress = Db.Addresses.Find(Ticket.BilltoAddress)

                'Submit to each restaurant
                For Each RestID In Ticket.ticketitems.GroupBy(Function(x) x.RestaurantID).Select(Function(y) y.First()).ToList()

                    Dim subtotal = Ticket.ticketitems.Where(Function(x) x.RestaurantID = RestID.RestaurantID).Select(Function(y) y.TotalItemPrice).Sum()

                    Dim LMDInfo As New LMDVer1
                    LMDInfo.TicketID = Ticket.ID
                    LMDInfo.LMID = RestID.RestaurantID
                    LMDInfo.ScheduledPickupTime = EasternNow
                    LMDInfo.OrderNumber = Ticket.ID
                    LMDInfo.OrderTotal = subtotal
                    LMDInfo.Tip = tip
                    LMDInfo.Notes = ""
                    LMDInfo.DeliveryName = DeliveryAdd.Name
                    LMDInfo.DeliveryAddress1 = DeliveryAdd.Address1
                    LMDInfo.DeliveryAddress2 = DeliveryAdd.Address2
                    LMDInfo.DeliveryCity = DeliveryAdd.City
                    LMDInfo.DeliveryState = DeliveryAdd.State
                    LMDInfo.DeliveryZip = DeliveryAdd.Zip
                    LMDInfo.DeliveryPhone = billtoaddress.phone
                    SendOrderToLMD(LMDInfo)
                    tip = 0
                Next


            Next

            Return View()
        End Function

        Function SendCalls()
            Dim fiveMinutesAgo As DateTime = DateAdd(DateInterval.Minute, -5, Now.ToUniversalTime())
            Dim faxes = Db.Faxsent.Where(Function(x) x.Status = 1 And x.DateSent < fiveMinutesAgo And x.DateConfirmed < fiveMinutesAgo).ToList

            For Each fax In faxes
                Try
                    Dim RestInfo = Db.RestaurantInfo.Where(Function(x) x.RestaurantID = fax.RestID).FirstOrDefault()
                    Dim CallURL As String = ConfigurationManager.AppSettings("CallURL") & fax.ID
#If DEBUG Then
                    RestInfo.OrderPhoneNumber = "3307143167"
#End If
                    PlaceCall(RestInfo.OrderPhoneNumber, ConfigurationManager.AppSettings("CallNumber"), CallURL)

                    fax.DateConfirmed = Now.ToUniversalTime
                    Db.Entry(fax).State = EntityState.Modified
                    Db.SaveChanges()
                Catch ex As Exception
                    SendEmail("bennyc66@gmail.com", "noreply@localmunchies.com", "Error Placing Confirm Call: Order: " & fax.Ticketid & ":" & fax.ID, ex.ToString, False, "")
                End Try

            Next
            Return View()
        End Function

        Function SendEmails()

            Dim emails = Db.Emails.Where(Function(x) x.Email_Status = 1).ToList

            For Each Email In emails
                Try
#If DEBUG Then
                    Email.Email_To = "bennyc66@gmail.com"
#End If

                    SendHTMLEmail(Email.Email_To, "NoReply@localmunchies.com", Email.Email_Subject, Email.Email_Body, Email.Email_Attachment_bln, Email.Email_Attachment_Path)
                Catch ex As Exception
                    ' MsgBox(ex.Message)
                    SendEmail(ITEmail, "NoReply@localmunchies.com", "Error Sending Emails", ex.ToString, False, "")
                End Try
                Email.Email_Status = 2
                Email.Email_Sent_Date = Now.ToUniversalTime()
                Db.Entry(Email).State = EntityState.Modified
                Db.SaveChanges()
            Next
            Return View()
        End Function

        Function SendtoQuickBooks(Optional ByVal id As Integer = 0)

            'Clean QB Classes
            QBReadClassesToTable()


            Dim TokenInfo = Db.QBTokenInfos.Find(1)

            Dim tickets = Db.Tickets.Where(Function(x) x.TicketStatus = 3).OrderBy(Function(x) x.ClosingDate).ToList

            If id <> 0 Then
                tickets = Db.Tickets.Where(Function(x) x.ID = id).ToList
            End If

            For Each ticket In tickets
                Dim tip = ticket.DeliveryPrice
                Dim Discount = ticket.DiscountTotal
                Dim Tax = ticket.TaxTotal
                Dim fee = ticket.FeePrice

                Dim billtoaddress = Db.Addresses.Find(ticket.BilltoAddress)

                Dim Restcount = 1


                Try
                    'Submit to each restaurant
                    For Each RestID In ticket.ticketitems.GroupBy(Function(x) x.RestaurantID).Select(Function(y) y.First()).ToList()

                        Dim subtotal = ticket.ticketitems.Where(Function(x) x.RestaurantID = RestID.RestaurantID).Select(Function(y) y.TotalItemPrice).Sum()

                        'Get Class Info
                        Dim QBClass = Db.QBClasses.Where(Function(x) x.LMRestaurantID = RestID.RestaurantID).FirstOrDefault()

                        If QBClass Is Nothing Then
                            QBClass = New QBClass With {.QBClassID = 0}
                        End If

                        Dim ticketnumber As String = "LM" & ticket.ID

                        'TicketNumber
                        If Restcount > 1 Then
                            ticketnumber &= "-" & Restcount
                        End If

                        Dim QBSalesReciept As New QBSalesReciept
                        QBSalesReciept.Tip = tip
                        QBSalesReciept.Discount = Discount
                        QBSalesReciept.Subtotal = subtotal
                        QBSalesReciept.Fee = fee
                        QBSalesReciept.BilltoAdd = billtoaddress
                        QBSalesReciept.ClassID = QBClass.QBClassID
                        QBSalesReciept.Tax = Tax
                        QBSalesReciept.TicketID = ticketnumber
                        QBSalesReciept.TicketDate = System.TimeZoneInfo.ConvertTimeFromUtc(ticket.ClosingDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))

                        buildSalesReceipt(QBSalesReciept, TokenInfo)

                        tip = 0
                        Tax = 0
                        Discount = 0
                        fee = 0
                        Restcount += 1
                    Next
                    ticket.TicketStatus = 4
                    Db.Entry(ticket).State = EntityState.Modified
                    Db.SaveChanges()
                Catch ex As Exception
                    Dim email As New EmailModel
                    email.AddEmail("E", "1", ManagerEmail, "NoReply@Localmunchies.com", "Error Adding Ticket to QuickBooks: " & ticket.ID, ReadException(ex), False, "", 1)
                End Try

            Next


            Return View()
        End Function
        Function buildSalesReceipt(ByVal ticket As QBSalesReciept, ByVal TokenInfo As QBTokenInfo) As Intuit.Ipp.Data.SalesReceipt
            Dim SalesReciept As New Intuit.Ipp.Data.SalesReceipt

            'Dim billtoAddress As Address = Db.Addresses.Find(ticket.BilltoAddress)

            'Static
            SalesReciept.DocNumber = ticket.TicketID
            SalesReciept.TxnDateSpecified = True
            SalesReciept.TxnDate = ticket.TicketDate
            SalesReciept.domain = "QBO"
            SalesReciept.SyncToken = "0"


            'Address Info
            Dim billaddr As New Intuit.Ipp.Data.PhysicalAddress

            billaddr.Line1 = ticket.BilltoAdd.Address1
            billaddr.City = ticket.BilltoAdd.City
            billaddr.CountrySubDivisionCode = ticket.BilltoAdd.State
            billaddr.PostalCode = ticket.BilltoAdd.Zip
            SalesReciept.BillAddr = billaddr

            'Email Info
            Dim email As New Intuit.Ipp.Data.EmailAddress
            email.Address = ticket.BilltoAdd.Email
            SalesReciept.BillEmail = email


            'Tax info - Leave at 0 for now
            'Dim taxdetail As Intuit.Ipp.Data.TaxLineDetail
            'SalesReciept.TxnTaxDetail.TotalTax = 0



            SalesReciept.Balance = 0
            SalesReciept.ApplyTaxAfterDiscount = False

            '
            ''Class info
            '
            If ticket.ClassID <> 0 Then
                Dim classRef As New Intuit.Ipp.Data.ReferenceType
                classRef.Value = ticket.ClassID
                SalesReciept.ClassRef = classRef
            End If

            '
            ''Discount
            '
            Dim discountLine As New Intuit.Ipp.Data.Line
            Dim discountlineDetail As New Intuit.Ipp.Data.DiscountLineDetail
            Dim discountitemRef As New Intuit.Ipp.Data.ReferenceType

            discountLine.DetailTypeSpecified = True
            discountLine.DetailType = Ipp.Data.LineDetailTypeEnum.DiscountLineDetail
            discountLine.AmountSpecified = True
            discountLine.Amount = ticket.Discount

            discountitemRef.Value = 61
            discountlineDetail.DiscountAccountRef = discountitemRef
            discountlineDetail.PercentBasedSpecified = True
            discountlineDetail.PercentBased = False

            discountLine.AnyIntuitObject = discountlineDetail

            '
            ''Tax
            '
            Dim TaxLine As New Intuit.Ipp.Data.Line
            Dim TaxlineDetail As New Intuit.Ipp.Data.SalesItemLineDetail
            Dim TaxitemRef As New Intuit.Ipp.Data.ReferenceType

            TaxLine.AmountSpecified = True
            TaxLine.Amount = ticket.Tax
            TaxLine.DetailTypeSpecified = True
            TaxLine.DetailType = Ipp.Data.LineDetailTypeEnum.SalesItemLineDetail

            TaxitemRef.Value = 8
            TaxlineDetail.ItemRef = TaxitemRef
            TaxlineDetail.Qty = 1
            TaxlineDetail.QtySpecified = True

            'Assign Sales Item Line Detail to Line Item
            TaxLine.AnyIntuitObject = TaxlineDetail

            '
            ''Tip
            '
            Dim TipLine As New Intuit.Ipp.Data.Line
            Dim TiplineDetail As New Intuit.Ipp.Data.SalesItemLineDetail
            Dim TipitemRef As New Intuit.Ipp.Data.ReferenceType

            TipLine.AmountSpecified = True
            TipLine.Amount = ticket.Tip
            TipLine.DetailTypeSpecified = True
            TipLine.DetailType = Ipp.Data.LineDetailTypeEnum.SalesItemLineDetail



            TipitemRef.Value = 3
            TiplineDetail.ItemRef = TipitemRef
            TiplineDetail.Qty = 1
            TiplineDetail.QtySpecified = True

            'Assign Sales Item Line Detail to Line Item
            TipLine.AnyIntuitObject = TiplineDetail

            '
            ''Fee
            '

            'Fee
            Dim FeeLine As New Intuit.Ipp.Data.Line
            Dim FeelineDetail As New Intuit.Ipp.Data.SalesItemLineDetail
            Dim FeeitemRef As New Intuit.Ipp.Data.ReferenceType

            FeeLine.AmountSpecified = True
            FeeLine.Amount = ticket.Fee
            FeeLine.DetailTypeSpecified = True
            FeeLine.DetailType = Ipp.Data.LineDetailTypeEnum.SalesItemLineDetail



            FeeitemRef.Value = 9
            'itemRef.name = "Food Sales"
            FeelineDetail.ItemRef = FeeitemRef
            FeelineDetail.Qty = 1
            FeelineDetail.QtySpecified = True

            'Assign Sales Item Line Detail to Line Item
            FeeLine.AnyIntuitObject = FeelineDetail



            ' ''Add Food Line
            Dim FoodLine As New Intuit.Ipp.Data.Line
            Dim FoodlineDetail As New Intuit.Ipp.Data.SalesItemLineDetail
            Dim FooditemRef As New Intuit.Ipp.Data.ReferenceType

            FoodLine.AmountSpecified = True
            FoodLine.Amount = ticket.Subtotal
            FoodLine.DetailTypeSpecified = True
            FoodLine.DetailType = Ipp.Data.LineDetailTypeEnum.SalesItemLineDetail



            FooditemRef.Value = 2
            'itemRef.name = "Food Sales"
            FoodlineDetail.ItemRef = FooditemRef
            FoodlineDetail.Qty = 1
            FoodlineDetail.QtySpecified = True

            'Assign Sales Item Line Detail to Line Item
            FoodLine.AnyIntuitObject = FoodlineDetail



            'Assign Line Item to Invoice
            SalesReciept.Line = {FoodLine, FeeLine, TipLine, TaxLine, discountLine}
            'invoice.Line = new Line[] { invoiceLine };



            Try
#If Not Debug Then

                Dim accessToken = TokenInfo.AccessToken 
                Dim accessTokenSecret = TokenInfo.accessTokenSecret 
                Dim consumerKey = TokenInfo.consumerKey 
                Dim consumerSecret = TokenInfo.consumerSecret 


                Dim consumerContext = New DevDefined.OAuth.Consumer.OAuthConsumerContext With {
                    .ConsumerKey = consumerKey,
                    .ConsumerSecret = consumerSecret,
                    .SignatureMethod = DevDefined.OAuth.Framework.SignatureMethod.HmacSha1
                  }



                Dim oauthValidator As Intuit.Ipp.Security.OAuthRequestValidator = New Intuit.Ipp.Security.OAuthRequestValidator(accessToken, accessTokenSecret, consumerKey, consumerSecret)

                Dim AppToken = TokenInfo.AppToken 
                Dim CompanyID = TokenInfo.CompanyID 

                Dim Context As Intuit.Ipp.Core.ServiceContext = New Intuit.Ipp.Core.ServiceContext(AppToken, CompanyID, Intuit.Ipp.Core.IntuitServicesType.QBO, oauthValidator)


                ' Dim added = Intuit.Ipp.Data
                Dim Service As New Ipp.DataService.DataService(Context)
                Dim x = Service.Add(SalesReciept)
#End If
            Catch ex As Intuit.Ipp.Exception.IdsException
                Dim sendemail As New EmailModel
                sendemail.AddEmail("E", "1", ManagerEmail, "NoReply@Localmunchies.com", "Error Adding Ticket to QucikBooks: " & ticket.TicketID, ex.ToString, False, "", 1)
                ' Next
            End Try

            Return SalesReciept
        End Function


        Function QBReadClassesToTable()
            Try


                Dim TokenInfo = Db.QBTokenInfos.Find(1)

                Dim accessToken = TokenInfo.AccessToken
                Dim accessTokenSecret = TokenInfo.accessTokenSecret
                Dim consumerKey = TokenInfo.consumerKey
                Dim consumerSecret = TokenInfo.consumerSecret


                Dim consumerContext = New DevDefined.OAuth.Consumer.OAuthConsumerContext With {
                    .ConsumerKey = consumerKey,
                    .ConsumerSecret = consumerSecret,
                    .SignatureMethod = DevDefined.OAuth.Framework.SignatureMethod.HmacSha1
                  }



                Dim oauthValidator As Intuit.Ipp.Security.OAuthRequestValidator = New Intuit.Ipp.Security.OAuthRequestValidator(accessToken, accessTokenSecret, consumerKey, consumerSecret)

                Dim AppToken = TokenInfo.AppToken
                Dim CompanyID = TokenInfo.CompanyID

                Dim Context As Intuit.Ipp.Core.ServiceContext = New Intuit.Ipp.Core.ServiceContext(AppToken, CompanyID, Intuit.Ipp.Core.IntuitServicesType.QBO, oauthValidator)

                Dim Service As New Ipp.DataService.DataService(Context)

                Dim Classes = Service.FindAll(New Intuit.Ipp.Data.Class())

                Dim connstr = System.Configuration.ConfigurationManager.ConnectionStrings("FBEntities").ConnectionString
                'Delete from the table
                Db_Execute("Delete From QBClasses", connstr)
                'Reset ID
                Db_Execute("DBCC CHECKIDENT ('QBClasses', RESEED, 0)", connstr)

                For Each Cls In Classes

                    Dim QBclass As New QBClass With {
                        .QBClassID = Cls.Id,
                        .LMRestaurantID = Trim(Left(Cls.Name, InStr(Cls.Name, "-") - 1))}

                    Db.QBClasses.Add(QBclass)
                    Db.SaveChanges()
                Next
            Catch ex As Exception
                Dim email As New EmailModel
                email.AddEmail("E", "1", ManagerEmail, "NoReply@Localmunchies.com", "Error Adding ID's From QuickBooks", ex.ToString, False, "", 1)

            End Try
            Return True
        End Function

        Shared Function SendFaxEmail(ByVal StrTo As String, ByVal StrFrom As String, ByVal StrSubject As String, ByVal StrBody As String, ByVal BlnAttachment As Boolean, ByVal StrAttachment As String) As Boolean
            'Send email notifictaion

            'Create the email objects
            Dim ObjEmail As New System.Net.Mail.SmtpClient
            Dim ObjEmailMsg As New System.Net.Mail.MailMessage(StrFrom, StrTo)
            Dim ObjAttachment As System.Net.Mail.Attachment
            'Dim htmlview As AlternateView = AlternateView.CreateAlternateViewFromString(Replace(StrBody, vbCr, "<br />"), Nothing, "text/html")

            'Initialize the email message ohject variables
            With ObjEmailMsg
                .Subject = StrSubject
                .Body = StrBody
            End With

            If BlnAttachment Then
                Dim WC As New System.Net.WebClient
                Dim x As New System.IO.MemoryStream(WC.DownloadData(StrAttachment))
                'Add File Attachment
                ObjAttachment = New System.Net.Mail.Attachment(x, "Fax.PDF", "")
                ObjEmailMsg.Attachments.Add(ObjAttachment)


            End If
            ' ObjEmailMsg.AlternateViews.Add(htmlview)

            'Initialize the SMTP server
            ObjEmail.Host = "mail.localmunchies.com"
            ObjEmail.Credentials = New Net.NetworkCredential("njones@localmunchies.com", ConfigurationManager.AppSettings("MailPassword"))
            ObjEmail.Port = 25

            'Send email
            ObjEmail.Send(ObjEmailMsg)

            'Release the email object
            ObjEmail = Nothing
        End Function

        Function SendOrderToLMD(ByVal LMDVer1 As LMDVer1)
            Dim worked As Boolean = False
            Dim EmailMessage As String = ""


            Dim querystr As String = "?"
            querystr += "LMID" & "=" & fixStr(LMDVer1.LMID)
            querystr += "&ScheduledPickupTime" & "=" & fixStr(LMDVer1.ScheduledPickupTime.ToString("MM-dd-yyyy H:mm"))
            querystr += "&OrderNumber" & "=" & fixStr(LMDVer1.OrderNumber)
            querystr += "&OrderTotal" & "=" & fixStr(LMDVer1.OrderTotal)
            querystr += "&Tip" & "=" & fixStr(LMDVer1.Tip)
            querystr += "&Notes" & "=" & fixStr(LMDVer1.Notes)
            querystr += "&DeliveryName" & "=" & fixStr(LMDVer1.DeliveryName)
            querystr += "&DeliveryAddress1" & "=" & fixStr(LMDVer1.DeliveryAddress1)
            querystr += "&DeliveryAddress2" & "=" & fixStr(LMDVer1.DeliveryAddress2)
            querystr += "&DeliveryCity" & "=" & fixStr(LMDVer1.DeliveryCity)
            querystr += "&DeliveryState" & "=" & fixStr(LMDVer1.DeliveryState)
            querystr += "&DeliveryZip" & "=" & fixStr(LMDVer1.DeliveryZip)
            querystr += "&DeliveryPhone" & "=" & fixStr(LMDVer1.DeliveryPhone)

            Dim URL = ConfigurationManager.AppSettings("LMDURL")

            Dim Location = "/LM/AddJobVer1"

            '
            '' Get XML Response
            '

            Try


                Dim xmlResponse = GetWebResponse(URL & Location & querystr)

                ' Create a new, empty XML document
                Dim document As New System.Xml.XmlDocument()

                ' Load the contents into the XML document
                document.LoadXml(xmlResponse)


                Dim xmlelement As System.Xml.XmlElement = document.FirstChild()

                If xmlelement.SelectSingleNode("status").InnerText = "1" Then
                    Dim LMDID = xmlelement.SelectSingleNode("ID").InnerText

                    'Add this ID to the table
                    'Update Status Sent to LMD
                    Dim ticket = Db.Tickets.Find(LMDVer1.TicketID)
                    ticket.LMDID = LMDID
                    ticket.LMDStatus = 3

                    'Update Ticket
                    Db.Entry(ticket).State = EntityState.Modified
                    Db.SaveChanges()




                    'Update worked
                    worked = True
                Else
                    'Error add email with message
                    EmailMessage = xmlelement.SelectSingleNode("Message").InnerText
                    worked = False
                End If
            Catch ex As Exception
                EmailMessage = ex.ToString
                worked = False
            End Try

            If Not worked Then
                Dim email As New EmailModel
                email.AddEmail("E", "1", ManagerEmail, "NoReply@Localmunchies.com", "Error Adding Ticket to LMD: " & LMDVer1.TicketID, querystr & " - " & EmailMessage, False, "", 1)
            End If

        End Function

        Function GetWebResponse(ByVal url As String) As String
            Dim Val As String = ""
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(url)


            ' Call the remote site, and parse the data in a response object
            Dim response As System.Net.HttpWebResponse = request.GetResponse()

            ' Check if the response is OK (status code 200)
            If response.StatusCode = System.Net.HttpStatusCode.OK Then

                ' Parse the contents from the response to a stream object
                Dim stream As System.IO.Stream = response.GetResponseStream()
                ' Create a reader for the stream object
                Dim reader As New System.IO.StreamReader(stream)
                ' Read from the stream object using the reader, put the contents in a string
                Val = reader.ReadToEnd()
            Else
                Throw New Exception("Could not retrieve document from the URL, response code: " & response.StatusCode)
            End If

            Return Val
        End Function

        Function fixStr(ByVal str As String) As String
            If str Is Nothing Then
                str = ""
            End If
            Return System.Text.RegularExpressions.Regex.Replace(str, "[^a-zA-Z0-9_ +-:.]", "")
        End Function

        Function PlaceCall(ByVal To_Number As String, ByVal From_Number As String, ByVal URL As String)
            Dim AccountSid As String = ConfigurationManager.AppSettings("TwilioSID")
            Dim AuthToken As String = ConfigurationManager.AppSettings("TwilioToken")
            Dim twilio = New TwilioRestClient(AccountSid, AuthToken)

            Dim options As New CallOptions()
            options.From = From_Number
            options.To = To_Number
            options.Url = URL
            options.Method = "GET"
            options.FallbackMethod = "GET"
            options.StatusCallbackMethod = "GET"
            options.Record = "false"

            Dim makecall = twilio.InitiateOutboundCall(options)
            If Not makecall.RestException Is Nothing Then
                'Log Error
                ' MsgBox(makecall.RestException.Code)
            End If

            Return True
        End Function


        Shared Function SendEmail(ByVal StrTo As String, ByVal StrFrom As String, ByVal StrSubject As String, ByVal StrBody As String, ByVal BlnAttachment As Boolean, ByVal StrAttachment As String) As Boolean
            'Send email notifictaion

            'Create the email objects
            Dim ObjEmail As New System.Net.Mail.SmtpClient
            Dim ObjEmailMsg As New System.Net.Mail.MailMessage(StrFrom, StrTo)
            Dim ObjAttachment As System.Net.Mail.Attachment
            'Dim htmlview As AlternateView = AlternateView.CreateAlternateViewFromString(Replace(StrBody, vbCr, "<br />"), Nothing, "text/html")

            'Initialize the email message ohject variables
            With ObjEmailMsg
                .Subject = StrSubject
                .Body = StrBody
            End With

            If BlnAttachment Then
                'Add File Attachment
                ObjAttachment = New System.Net.Mail.Attachment(StrAttachment)
                ObjEmailMsg.Attachments.Add(ObjAttachment)
            End If
            ' ObjEmailMsg.AlternateViews.Add(htmlview)

            'Initialize the SMTP server
            ObjEmail.Host = "mail.localmunchies.com"
            ObjEmail.Credentials = New Net.NetworkCredential(StrFrom, ConfigurationManager.AppSettings("MailPassword"))
            ObjEmail.Port = 25

            'Send email
            ObjEmail.Send(ObjEmailMsg)

            'Release the email object
            ObjEmail = Nothing
        End Function
        Shared Function SendHTMLEmail(ByVal StrTo As String, ByVal StrFrom As String, ByVal StrSubject As String, ByVal StrBody As String, ByVal BlnAttachment As Boolean, ByVal StrAttachment As String) As Boolean
            'Send email notifictaion


            'Create the email objects
            Dim ObjEmail As New System.Net.Mail.SmtpClient
            Dim ObjEmailMsg As New System.Net.Mail.MailMessage(StrFrom, StrTo)
            Dim ObjAttachment As System.Net.Mail.Attachment
            'Dim htmlview As AlternateView = AlternateView.CreateAlternateViewFromString(Replace(StrBody, vbCr, "<br />"), Nothing, "text/html")

            'Initialize the email message ohject variables
            With ObjEmailMsg
                .Subject = StrSubject
                .Body = StrBody
            End With

            ObjEmailMsg.IsBodyHtml = True


            If BlnAttachment Then
                'Add File Attachment
                ObjAttachment = New System.Net.Mail.Attachment(StrAttachment)
                ObjEmailMsg.Attachments.Add(ObjAttachment)
            End If
            ' ObjEmailMsg.AlternateViews.Add(htmlview)

            'Initialize the SMTP server
            ObjEmail.Host = "mail.localmunchies.com"
            ObjEmail.Credentials = New Net.NetworkCredential(StrFrom, ConfigurationManager.AppSettings("MailPassword"))
            ObjEmail.Port = 25

            'Send email
            ObjEmail.Send(ObjEmailMsg)

            'Release the email object
            ObjEmail = Nothing
        End Function
        Public Function Db_Execute(ByVal Sql, ByVal connstr)
            'Execute the query in DB
            Dim ObjConn As System.Data.SqlClient.SqlConnection
            Dim ObjCmd As System.Data.SqlClient.SqlCommand
            Dim StrConnectionString As String
            Dim worked As Boolean = False

            StrConnectionString = connstr

            'Create DB connection object
            ObjConn = New System.Data.SqlClient.SqlConnection(StrConnectionString)

            'Create command Object
            ObjCmd = New System.Data.SqlClient.SqlCommand(Sql, ObjConn)

            'Open connection to DB
            ObjConn.Open()
            While worked <> True
                ObjCmd.ExecuteNonQuery()
                worked = True

                While ObjConn.State = ConnectionState.Executing
                    worked = True
                End While
            End While

            ObjConn.Close()

            Return worked


        End Function

        Public Function ReadException(ByVal ex As Exception) As String
            Dim msg As String = ex.Message
            If ex.InnerException IsNot Nothing Then
                msg = msg & vbCrLf & "---------" & vbCrLf & ReadException(ex.InnerException)
            End If
            Return msg
        End Function

    End Class
End Namespace
